/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.logic;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import se.esss.ics.rbac.logic.exception.IllegalRBACArgumentException;
import se.esss.ics.rbac.logic.exception.RBACException;
import se.esss.ics.rbac.ConfigurationParameter;

/**
 * <code>SignatureUtilities</code> is a singleton bean containing utility methods for dealing with encryption
 * signatures. Signatures are generated or verified using RSA 512 bit public and private key pair, which is read from
 * disk, or generated, if it is not available. Public key is also exposed through {@link #getPublicKey()} method.
 * 
 * @author <a href="mailto:jakob.battelino@cosylab.com">Jakob Battelino Prelog</a>
 * 
 */
@Singleton(description = "The bean provides facilities to generate and verify the RBAC signature.")
public class SignatureEJB implements Serializable {

    private static final long serialVersionUID = -4237608689083411153L;

    /** Algorithm name for key pair generation. */
    public static final String KEY_PAIR_ALGORITHM_NAME = "RSA";
    /** The key size. */
    public static final int KEY_SIZE = 512;
    /** Algorithm name for signature generation. */
    public static final String SIGNATURE_ALGORITHM_NAME = "SHA1withRSA";
    /** The name of parameter that describes the public key location. */
    public static final String PARAM_PUBLIC_KEY_PATH = "RSA_PUB_PATH";
    /** The name of parameter that describes the private key location. */
    public static final String PARAM_PRIVATE_KEY_PATH = "RSA_PVT_PATH";
    /** Default location of the public key. */
    public static final String DEFAULT_LOCATION_PUBLIC = "public.key";
    /** Default location of the private key. */
    public static final String DEFAULT_LOCATION_PRIVATE = "private.key";

    @PersistenceContext(unitName = "RBAC")
    private transient EntityManager entityManager;

    private KeyPair keyPair;

    /**
     * Generates signature data by updating the RSA signature with provided data and signing it with a private key.
     * 
     * @param data for which the signature will be made.
     * @return signature data, as a byte array, for the provided data.
     * @throws IllegalRBACArgumentException if the data is null
     * @throws RBACException if the key is invalid, signing algorithm is invalid or if there is an error while signing
     *             the signature.
     */
    public byte[] generateSignature(byte[] data) throws IllegalRBACArgumentException, RBACException {
        if (data == null) {
            throw new IllegalRBACArgumentException(Messages.getString(Messages.PARAMETER_NOT_PROVIDED,
                    "Data byte array"));
        }
        try {
            PrivateKey privateKey = getKeyPair().getPrivate();
            Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM_NAME);
            signature.initSign(privateKey);
            signature.update(data);
            return signature.sign();
        } catch (InvalidKeyException | NoSuchAlgorithmException | SignatureException e) {
            throw new RBACException(null, e);
        }
    }

    /**
     * Checks if the signature data corresponds to the other provided data. Signature is verified using the public RSA
     * and SHA1 with RSA algorithm.
     * 
     * @param data to be used for verification of the signature.
     * @param signatureData signature data to be verified.
     * @return <code>true</code> if the signature data has been verified.
     * @throws RBACException if the key is invalid, signing algorithm is invalid or if there is an error while verifying
     *             the signature.
     */
    public boolean verifySignature(byte[] data, byte[] signatureData) throws RBACException {
        if (data == null) {
            throw new IllegalRBACArgumentException(Messages.getString(Messages.PARAMETER_NOT_PROVIDED,
                    "Data byte array"));
        } else if (signatureData == null) {
            throw new IllegalRBACArgumentException(Messages.getString(Messages.PARAMETER_NOT_PROVIDED,
                    "Signature byte array"));
        }
        try {
            PublicKey publicKey = getKeyPair().getPublic();
            Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM_NAME);
            signature.initVerify(publicKey);
            signature.update(data);
            return signature.verify(signatureData);
        } catch (InvalidKeyException | NoSuchAlgorithmException | SignatureException e) {
            throw new RBACException(null, e);
        }
    }

    /**
     * Returns the public part of the RSA key pair used for signing data.
     * 
     * @return the public RSA key.
     * @throws RBACException if there was an error loading the public key
     */
    public PublicKey getPublicKey() throws RBACException {
        return getKeyPair().getPublic();
    }

    /**
     * @return the public/private key pair
     * 
     * @throws RBACException if the key could not be generated
     */
    private KeyPair getKeyPair() throws RBACException {
        // if the key has been deleted, regenerate it
        ConfigurationParameter pubParameter = entityManager.find(ConfigurationParameter.class, PARAM_PUBLIC_KEY_PATH);
        ConfigurationParameter pvtParameter = entityManager.find(ConfigurationParameter.class, PARAM_PRIVATE_KEY_PATH);
        if (pubParameter == null || pvtParameter == null) {
            keyPair = null;
        }
        if (keyPair == null) {
            try {
                keyPair = loadKeyPair();
            } catch (IOException | GeneralSecurityException e) {
                // ignore could not be loaded, because it doesn't exist
            }

            if (keyPair == null) {
                try {
                    KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance(KEY_PAIR_ALGORITHM_NAME);
                    keyGenerator.initialize(KEY_SIZE, new SecureRandom());
                    keyPair = keyGenerator.genKeyPair();
                    saveKeyPair(keyPair);
                } catch (IOException | NoSuchAlgorithmException | RBACException e) {
                    throw new RBACException(null, e);
                }
            }
        }
        return keyPair;
    }

    /**
     * Load the key pair from a file. The method loads the paths to the public and private keys from the configuration
     * table, reads the keys from the paths and returns the key pair.
     * 
     * @return the key pair with valid public and private keys
     * @throws IOException if reading from the file failed
     * @throws GeneralSecurityException if the keys could not be decoded
     */
    private KeyPair loadKeyPair() throws IOException, GeneralSecurityException {
        // Load public key from a file.
        ConfigurationParameter pubParameter = entityManager.find(ConfigurationParameter.class, PARAM_PUBLIC_KEY_PATH);
        if (pubParameter == null) {
            return null;
        }
        String pubPath = pubParameter.getValue();
        File pubFile = new File(pubPath); 
        byte[] pubEncodedKey = new byte[(int) pubFile.length()];
        X509EncodedKeySpec pubKeySpec = null;
        try (FileInputStream pubFIS = new FileInputStream(pubFile)) {
            pubFIS.read(pubEncodedKey);
            pubKeySpec = new X509EncodedKeySpec(pubEncodedKey);
        }

        // Load private key from a file.
        ConfigurationParameter pvtParameter = entityManager.find(ConfigurationParameter.class, PARAM_PRIVATE_KEY_PATH);
        if (pvtParameter == null) {
            return null;
        }

        String pvtPath = pvtParameter.getValue();
        File pvtFile = new File(pvtPath);
        byte[] pvtEncodedKey = new byte[(int) pvtFile.length()];
        PKCS8EncodedKeySpec pvtKeySpec = null;
        try (FileInputStream pvtFIS = new FileInputStream(pvtFile)) {
            pvtFIS.read(pvtEncodedKey);
            pvtKeySpec = new PKCS8EncodedKeySpec(pvtEncodedKey);
        }

        // Decode and create a key pair.
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_PAIR_ALGORITHM_NAME);
        PublicKey pubKey = keyFactory.generatePublic(pubKeySpec);
        PrivateKey pvtKey = keyFactory.generatePrivate(pvtKeySpec);
        return new KeyPair(pubKey, pvtKey);
    }

    /**
     * Saves the key pair into a file. The path for the file is loaded from the configuration table.
     * 
     * @param keyPair the key pair to store
     * @throws IOException if there was an error when writing to file
     * @throws RBACException if the path to save the file could not be found
     */
    private void saveKeyPair(KeyPair keyPair) throws IOException, RBACException {
        // Save public key to a file.
        ConfigurationParameter pubParameter = entityManager.find(ConfigurationParameter.class, PARAM_PUBLIC_KEY_PATH);
        if (pubParameter == null) {
            pubParameter = new ConfigurationParameter();
            pubParameter.setName(PARAM_PUBLIC_KEY_PATH);
            pubParameter.setValue(new File(DEFAULT_LOCATION_PUBLIC).getAbsolutePath());
            entityManager.persist(pubParameter);
        }
        String pubPath = pubParameter.getValue();
        PublicKey pubKey = keyPair.getPublic();
        X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(pubKey.getEncoded());
        try (FileOutputStream pubFOS = new FileOutputStream(pubPath)) {
            pubFOS.write(pubKeySpec.getEncoded());
        }

        // Save private key to a file.
        ConfigurationParameter pvtParameter = entityManager.find(ConfigurationParameter.class, PARAM_PRIVATE_KEY_PATH);
        if (pvtParameter == null) {
            pvtParameter = new ConfigurationParameter();
            pvtParameter.setName(PARAM_PRIVATE_KEY_PATH);
            pvtParameter.setValue(new File(DEFAULT_LOCATION_PRIVATE).getAbsolutePath());
            entityManager.persist(pvtParameter);
        }
        String pvtPath = pvtParameter.getValue();
        PrivateKey pvtKey = keyPair.getPrivate();
        PKCS8EncodedKeySpec pvtKeySpec = new PKCS8EncodedKeySpec(pvtKey.getEncoded());
        try (FileOutputStream pvtFOS = new FileOutputStream(pvtPath)) {
            pvtFOS.write(pvtKeySpec.getEncoded());
        }
    }
}
