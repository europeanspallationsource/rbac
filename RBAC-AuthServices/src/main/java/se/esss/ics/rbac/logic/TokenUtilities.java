/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import se.esss.ics.rbac.jaxb.TokenInfo;
import se.esss.ics.rbac.jaxb.util.TokenUtil;
import se.esss.ics.rbac.datamodel.Token;
import se.esss.ics.rbac.datamodel.TokenRole;

/**
 * 
 * <code>TokenUtilities</code> is a utility class for transforming the TokenInfo to bytes, generating the token id or
 * transforming the persisted object {@link Token} to transfer object {@link TokenInfo}.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public final class TokenUtilities {

    /**
     * Constructor.
     */
    private TokenUtilities() {
    }

    /**
     * Generates the token's unique ID. Generated ID is unique for each token and is UUID composed of the token
     * signature.
     * 
     * @param tokenInfo the info to be used for generating the ID.
     * @return the token's unique ID
     */
    public static String generateTokenID(TokenInfo tokenInfo) {
        return UUID.nameUUIDFromBytes(tokenInfo.getSignature()).toString();
    }

    /**
     * Generates a byte array containing all token info's data. Provided TokenInfo's field values are transformed into
     * byte arrays and copied into a single array which is then returned back. Token info field values appear in the
     * returned array in the following order:
     * <ol>
     * <li>userID</li>
     * <li>first name</li>
     * <li>last name</li>
     * <li>roles</li>
     * <li>IP</li>
     * <li>creation time</li>
     * <li>tokenID</li>
     * </ol>
     * 
     * @see TokenUtil#generateTokenData(TokenInfo)
     * @param tokenInfo to be transformed into a byte array.
     * @return byte array containing all of the provided token info's data.
     */
    public static byte[] generateTokenData(TokenInfo tokenInfo) {
        return TokenUtil.generateTokenData(tokenInfo);
    }

    /**
     * Constructs a TokenInfo containing data from the parameter Token. Returned TokenInfo does not have the signature
     * field set.
     * 
     * @param token to be wrapped into a TokenInfo.
     * @return TokenInfo containing data from the parameter Token.
     */
    public static TokenInfo fromToken(Token token) {
        TokenInfo tokenInfo = new TokenInfo();
        tokenInfo.setTokenID(token.getTokenId());
        tokenInfo.setUserID(token.getUserId());
        tokenInfo.setCreationTime(token.getCreationDate().getTime());
        tokenInfo.setExpirationTime(token.getExpirationDate().getTime());
        tokenInfo.setIp(token.getIp());
        List<String> roles = new ArrayList<>();
        Set<TokenRole> tokenRoles = token.getRole();
        if (tokenRoles != null) {
            String r;
            for (TokenRole role : tokenRoles) {
                r = role.getRole().getRole().getName();
                if (!roles.contains(r)) {
                    roles.add(r);
                }
            }
            Collections.sort(roles);
        }
        tokenInfo.setRoleNames(roles);
        return tokenInfo;
    }
}
