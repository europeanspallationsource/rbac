/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.logic;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import se.esss.ics.rbac.RBACLog;

/**
 * Logging utility singleton bean, that provides means to log specific RBAC services' actions. The logs are store into
 * the RBAC database.
 * 
 * Convenience methods are provided for creating log entries of different severities.
 * 
 * @author <a href="mailto:jakob.battelino@cosylab.com">Jakob Battelino Prelog</a>
 */
@Singleton(description = "The bean provides the facilities to log statements.")
public class RBACLogger implements Serializable {

    private static final long serialVersionUID = 1301853827458990470L;

    private static final String UNKNOWN = "unknown";

    @PersistenceContext(unitName = "RBAC")
    private transient EntityManager entityManager;

    /**
     * Constructs an {@link RBACLog} entity from the parameters. The time stamp of the log entry is set to the current
     * time in milliseconds. If <code>userID</code> parameter is <code>null</code>, unknown is used instead. Once the
     * log entry fields are set, the entry is persisted by the entity manager.
     * 
     * @param userID of the user that initiated this logged action.
     * @param ip address of the user that initiated this logged action.
     * @param severity of the log entry.
     * @param action taken, that trigger logging.
     * @param content message containing additional information about the action.
     */
    public void queueLogEntry(String userID, String ip, RBACLog.Severity severity, 
            RBACLog.Action action, String content) {
        RBACLog entry = new RBACLog();
        entry.setTimestamp(new Timestamp(System.currentTimeMillis()));
        entry.setUserID(userID == null ? UNKNOWN : userID);
        entry.setIp(ip == null ? UNKNOWN : ip);
        entry.setSeverity(severity);
        entry.setAction(action);
        entry.setContent(content);

        entityManager.persist(entry);
    }

    /**
     * Queues a log entry with severity level {@link se.esss.ics.rbac.RBACLog.Severity#INFO}. This is equivalent to
     * calling <code>queueLogEntry(String, String, Severity.INFO, LogEntry.Action, String)</code>
     * 
     * @param userID of the user that initiated this logged action.
     * @param ip address of the user that initiated this logged action.
     * @param action taken, that trigger logging.
     * @param content message containing additional information about the action.
     */
    public void info(String userID, String ip, RBACLog.Action action, String content) {
        queueLogEntry(userID, ip, RBACLog.Severity.INFO, action, content);
    }

    /**
     * Queues a log entry with severity level {@link se.esss.ics.rbac.RBACLog.Severity#WARNING}. This is equivalent
     * to calling <code>queueLogEntry(String, String, Severity.WARNING, LogEntry.Action, String)</code>
     * 
     * @param userID of the user that initiated this logged action.
     * @param ip address of the user that initiated this logged action.
     * @param action taken, that trigger logging.
     * @param content message containing additional information about the action.
     */
    public void warn(String userID, String ip, RBACLog.Action action, String content) {
        queueLogEntry(userID, ip, RBACLog.Severity.WARNING, action, content);
    }

    /**
     * Queues a log entry with severity level {@link se.esss.ics.rbac.RBACLog.Severity#ERROR}. This is equivalent to
     * calling <code>queueLogEntry(String, String, Severity.ERROR, LogEntry.Action, String)</code>
     * 
     * @param userID of the user that initiated this logged action.
     * @param ip address of the user that initiated this logged action.
     * @param action taken, that trigger logging.
     * @param content message containing additional information about the action.
     */
    public void error(String userID, String ip, RBACLog.Action action, String content) {
        queueLogEntry(userID, ip, RBACLog.Severity.ERROR, action, content);
    }
}
