/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.logic.exception;

/**
 * 
 * <code>DataConsistencyException</code> is thrown when data in the persistence module in inconsistent (e.g. duplicated
 * entries, where there should not be etc.).
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class DataConsistencyException extends RBACException {

    private static final long serialVersionUID = 60882884727908846L;

    /**
     * Constructs a new exception.
     * 
     * @param username the name of the user who triggered the exception
     * @param message the message of the exception
     */
    public DataConsistencyException(String username, String message) {
        super(username, message);
    }
    
    /**
     * Constructs a new exception.
     * 
     * @param username the name of the user who triggered the exception
     * @param message the message of the exception
     * @param cause the cause of this exception
     */
    public DataConsistencyException(String username, String message, Throwable cause) {
        super(username, message, cause);
    }
}
