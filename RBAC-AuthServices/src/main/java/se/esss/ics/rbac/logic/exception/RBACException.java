/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.logic.exception;

/**
 * 
 * <code>RBACException</code> is thrown by different methods when an invalid request is made to one of the services.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class RBACException extends Exception {

    private static final long serialVersionUID = 60882884727908846L;

    private final String username;

    /**
     * Constructs a new exception.
     * 
     * @param username the name of the user who triggered the exception
     * @param message the message of the exception
     */
    public RBACException(String username, String message) {
        super(message);
        this.username = username;
    }

    /**
     * Constructs a new exception.
     * 
     * @param username the name of the user who triggered the exception
     * @param cause the cause of this exception
     */
    public RBACException(String username, Throwable cause) {
        super(cause);
        this.username = username;
    }

    /**
     * Constructs a new exception.
     * 
     * @param username the name of the user who triggered the exception
     * @param message the message explaining the exception
     * @param cause the cause of this exception
     */
    public RBACException(String username, String message, Throwable cause) {
        super(message, cause);
        this.username = username;
    }

    /**
     * @return the username who triggered the exception
     */
    public final String getUsername() {
        return username;
    }
}
