/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.logic;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;
import java.security.ProtectionDomain;
import java.util.jar.Manifest;

import javax.ejb.Singleton;

/**
 * <code>ManifestEJB</code> is a singleton bean containing utility methods for dealing with application's manifest file.
 * Application's {@link Manifest} can be obtained using {@link #getManifest()}. There is also a convenience method
 * {@link #getVersion()} for reading the build version from the manifest file.
 * 
 * @author <a href="mailto:jakob.battelino@cosylab.com">Jakob Battelino Prelog</a>
 */
@Singleton(description = "The bean provides means to extract the version number from the manifest.")
public class ManifestEJB implements Serializable {

    private static final long serialVersionUID = -1853253675012355015L;

    private static final String MANIFEST_RELATIVE_PATH = "../META-INF/MANIFEST.MF";
    private static final String ATTRIBUTE_BUILD_VERSION = "Build-Version";

    private transient Manifest manifest;

    /**
     * Returns the application's {@link Manifest}. Manifest file resides at <code>META-INF/MANIFEST.MF</code> inside the
     * web application's deployment directory.
     * 
     * @return the application's manifest file.
     * 
     * @throws IOException if there is an error while reading the manifest file.
     */
    public Manifest getManifest() throws IOException {
        if (manifest == null) {
            ProtectionDomain domain = ManifestEJB.class.getProtectionDomain();
            if (domain != null && domain.getCodeSource() != null) {
                try (InputStream stream = new URL(domain.getCodeSource().getLocation(), 
                        MANIFEST_RELATIVE_PATH).openStream()) {
                    manifest = new Manifest(stream);
                }
            }
            if (manifest == null) {
                throw new FileNotFoundException("Manifest file could not be found or accessed.");
            }
        }
        return manifest;
    }

    /**
     * Returns the application's build version. Build version is read from the manifest file as a value of the main
     * attribute named <code>Build-Version</code>.
     * 
     * @return the application's build version or <code>null</code> if manifest could not be found.
     * 
     * @throws IOException if there is an error while reading the manifest file.
     */
    public String getVersion() throws IOException {
        return getManifest().getMainAttributes().getValue(ATTRIBUTE_BUILD_VERSION);
    }
}
