/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.logic.timer;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import se.esss.ics.rbac.logic.Messages;
import se.esss.ics.rbac.logic.RBACLogger;
import se.esss.ics.rbac.RBACLog.Action;
import se.esss.ics.rbac.datamodel.Token;

/**
 * 
 * <code>TimerBean</code> is an implementation of the remote interface that cleans the database of all expired entities.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
@Stateless(description = "The bean provides a timer that purges expired entities from the persistence context.")
public class TimerBean implements TimerBeanRemote, Serializable {

    private static final long serialVersionUID = -6976548743132025215L;

    @Resource
    private transient SessionContext context;
    @PersistenceContext
    private transient EntityManager entityManager;
    @EJB
    private RBACLogger logger;

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.logic.TimerBeanRemote#startTimer()
     */
    @Override
    public void startTimer() {
        TimerConfig config = new TimerConfig();
        config.setInfo("DB Cleanup Scheduler");
        config.setPersistent(false);
        // check every 10 minutes, so if there is something to delete, there won't be that much
        context.getTimerService().createIntervalTimer(new Date(), 10 * 60 * 1000, config);
    }

    /**
     * Remove expired tokens, expired exclusive access and expired user roles assignments.
     * 
     * @param timer the timer
     */
    @Timeout
    public void timeout(Timer timer) {
        // remove tokens one by one, because there is no cascade option available on the TokenRole
        // if ON DELETE CASCADE is defined on the entities, you may safely use Token.deleteExpired
        // int to = entityManager.createNamedQuery("Token.deleteExpired").executeUpdate();
        List<Token> tokens = entityManager.createNamedQuery("Token.findExpired", Token.class).getResultList();
        for (Token t : tokens) {
            entityManager.remove(t);
        }
        int to = tokens.size();
        int ex = entityManager.createNamedQuery("ExclusiveAccess.deleteExpired").executeUpdate();
        int ur = entityManager.createNamedQuery("UserRole.deleteExpired").executeUpdate();

        if (to + ex + ur > 0) {
            String address = "localhost";
            try {
                address = InetAddress.getLocalHost().getHostAddress();
            } catch (UnknownHostException e) {
                // ignore, cannot happen
            }
            logger.info("RBAC", address, Action.CLEANUP, Messages.getString(Messages.CLEANUP, to, ex, ur));
        }
    }
}
