pipeline {
    agent {
        label 'docker-ce'
    }
    stages {
        stage('Build') {
            agent {
                docker {
                    image 'europeanspallationsource/oracle-jdk-maven-jenkins:8u161-b12-1'
                    reuseNode true
                }
            }
            steps {
                script {
                    env.POM_VERSION = readMavenPom().version
                    currentBuild.displayName = env.POM_VERSION
                }
                sh 'mvn --batch-mode -Dmaven.test.failure.ignore clean install'
            }
        }
        stage('SonarQube analysis') {
            agent {
                docker {
                    image 'europeanspallationsource/oracle-jdk-maven-jenkins:8u161-b12-1'
                    reuseNode true
                }
            }
            steps {
                withCredentials([string(credentialsId: 'sonarqube', variable: 'TOKEN')]) {
                    sh 'mvn --batch-mode -Dsonar.login=$TOKEN -Dsonar.branch=${BRANCH_NAME} sonar:sonar'
                }
            }
        }
        stage('Publish') {
            when {
                branch 'master'
            }
            agent {
                docker {
                    image 'europeanspallationsource/oracle-jdk-maven-jenkins:8u161-b12-1'
                    reuseNode true
                }
            }
            steps {
                withCredentials([usernamePassword(credentialsId: 'artifactory', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                    sh 'mvn --batch-mode -Dartifactory.username=$USERNAME -Dartifactory.password=$PASSWORD deploy'
                }
            }
        }
        stage('Build Docker image') {
            when {
                branch 'master'
            }
            steps {
                sh 'docker build -t registry.esss.lu.se/ics-software/rbac:latest -t registry.esss.lu.se/ics-software/rbac:${POM_VERSION} .'
            }
        }
        stage('Push Docker image') {
            when {
                branch 'master'
            }
            steps {
                withCredentials([usernamePassword(credentialsId: 'gitlab', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                    sh 'docker login registry.esss.lu.se --username ${USERNAME} --password ${PASSWORD}'
                }
                sh 'docker push registry.esss.lu.se/ics-software/rbac:latest'
                sh 'docker push registry.esss.lu.se/ics-software/rbac:${POM_VERSION}'
                sh 'docker logout registry.esss.lu.se'
            }
        }
    }
    post {
        failure {
            slackSend (color: 'danger', message: "FAILED: <${env.BUILD_URL}|${env.JOB_NAME} [${env.BUILD_NUMBER}]>")
            emailext (
                subject: '${DEFAULT_SUBJECT}',
                body: '${DEFAULT_CONTENT}',
                recipientProviders: [[$class: 'CulpritsRecipientProvider']]
            )
        }
        success {
            slackSend (color: 'good', message: "SUCCESSFUL: <${env.BUILD_URL}|${env.JOB_NAME} [${env.BUILD_NUMBER}]>")
        }
    }
}
