/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jaxb;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * <code>UsersInfo</code> represents a list of users, which all have a specific role.
 *
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
@XmlRootElement(name = "userInfo")
@XmlType(propOrder = { "role", "resource", "permission", "users" })
public class UsersInfo implements Serializable {

    private static final long serialVersionUID = 278344906252019336L;
    
    private String role;
    private String permission;
    private String resource;
    private List<String> users;
    
    /**
     * Constructs a new UsersInfo.
     */
    public UsersInfo() {
        
    }
    
    /**
     * Constructs a new UsersInfo.
     * 
     * @param resource the resource name
     * @param permission the permission name
     * @param users the list of users that have the role
     */
    public UsersInfo(String resource, String permission, List<String> users) {
        this.users = users;
        this.resource = resource;
        this.permission = permission;
    }
    
    /**
     * Constructs a new UsersInfo.
     * 
     * @param role the role name
     * @param users the list of users that have the role
     */
    public UsersInfo(String role, List<String> users) {
        this.users = users;
        this.role = role;
    }

    /**
     * @return the name of the role, which all users have
     */
    @XmlElement(name = "role", required = false)
    public String getRole() {
        return role;
    }

    /**
     * Set the name of the role, which all users have.
     * 
     * @param role the name of the role
     */
    public void setRole(String role) {
        this.role = role;
    }
    
    /**
     * @return the name of the resource that owns the permission
     */
    @XmlElement(name = "resource", required = false)
    public String getResource() {
        return resource;
    }

    /**
     * Set the name of the resources that owns the permission.
     * 
     * @param resource the name of the resource
     */
    public void setResource(String resource) {
        this.resource = resource;
    }
    
    /**
     * @return the name of the permission, which all users have
     */
    @XmlElement(name = "permission", required = false)
    public String getPermission() {
        return permission;
    }

    /**
     * Set the name of the permission, which all users have.
     * 
     * @param permission the name of the permission
     */
    public void setPermission(String permission) {
        this.permission = permission;
    }

    /**
     * @return the lists of all users (usernames) that have the role
     */
    @XmlElement(name = "user")
    @XmlElementWrapper(name = "users")
    public List<String> getUsers() {
        return users;
    }

    /**
     * Set the list of all users that have the role.
     * 
     * @param users the list of all users with the role
     */
    public void setUsers(List<String> users) {
        this.users = users;
    }
}
