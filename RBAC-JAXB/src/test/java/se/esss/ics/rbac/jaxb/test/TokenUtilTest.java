package se.esss.ics.rbac.jaxb.test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import org.junit.Test;

import se.esss.ics.rbac.jaxb.TokenInfo;
import se.esss.ics.rbac.jaxb.util.TokenUtil;

/**
 * 
 * <code>TokenUtilTest</code> tests methods from the {@link TokenUtil} class.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class TokenUtilTest {

    /**
     * Verifies that roles are properly transformed into strings.
     */
    @Test
    public void testRolesToString() {
        String[] roles = new String[] { "Role 1", "Role 2", "Role 3" };
        String s = TokenUtil.rolesToString(roles);
        assertEquals("Roles to string should concatenate roles", "Role 1,Role 2,Role 3", s);

        s = TokenUtil.rolesToString(Arrays.asList(roles));
        assertEquals("Roles to string should concatenate roles", "Role 1,Role 2,Role 3", s);

        s = TokenUtil.rolesToString((String[]) null);
        assertTrue("Empty string should be returned", s.isEmpty());

        s = TokenUtil.rolesToString((Collection<String>) null);
        assertTrue("Empty string should be returned", s.isEmpty());
    }

    /**
     * Tests that {@link TokenUtil#generateTokenData(TokenInfo)} and similar methods generate the byte array correctly.
     */
    @Test
    public void testGenerateTokenData() {
        String username = "johndoe";
        String[] roles = new String[] { "Role 1", "Role 2", "Role 3" };
        String ip = "192.168.1.1";
        String firstName = "John";
        String lastName = "Doe";
        String tokenID = "blabla";
        Date creationDate = new Date(1401440089670L);

        byte[] expectedResult = new byte[] { 68, 111, 101, 110, 100, 111, 101, 0, 0, 0, 0, 0, 0, 0, 82, 111, 108, 101,
                32, 49, 44, 82, 111, 108, 101, 32, 50, 44, 82, 111, 108, 101, 32, 51, 49, 57, 50, 46, 49, 54, 56, 46,
                49, 46, 49, 0, 0, 1, 70, 76, 86, -74, 70 };

        byte[] data = TokenUtil.generateTokenData(username, firstName, lastName, roles, ip, creationDate);
        assertArrayEquals("Arrays should match", expectedResult, data);

        data = TokenUtil.generateTokenData(username, firstName, lastName, Arrays.asList(roles), ip,
                creationDate.getTime());
        assertArrayEquals("Arrays should match", expectedResult, data);

        TokenInfo info = new TokenInfo();
        info.setUserID(username);
        info.setRoleNames(Arrays.asList(roles));
        info.setCreationTime(creationDate.getTime());
        info.setIp(ip);
        info.setFirstName(firstName);
        info.setLastName(lastName);
        info.setTokenID(tokenID);

        data = TokenUtil.generateTokenData(info);
        assertArrayEquals("Arrays should match", expectedResult, data);

        info.setExpirationTime(321321321321L);
        data = TokenUtil.generateTokenData(info);
        assertArrayEquals("Expiration time should not have effect on the generated byte array.", expectedResult, data);

        info.setSignature(new byte[] { 1, 2, 3, 4 });
        data = TokenUtil.generateTokenData(info);
        assertArrayEquals("Signature should not have effect on the generated byte array.", expectedResult, data);

        info.setCreationTime(32321L);
        data = TokenUtil.generateTokenData(info);
        assertFalse("Generated array should be different", Arrays.equals(expectedResult, data));
        info.setCreationTime(creationDate.getTime());

        info.setFirstName("dsdsa");
        data = TokenUtil.generateTokenData(info);
        assertFalse("Generated array should be different", Arrays.equals(expectedResult, data));
        info.setFirstName(firstName);

        info.setLastName("rfdsfdsf");
        data = TokenUtil.generateTokenData(info);
        assertFalse("Generated array should be different", Arrays.equals(expectedResult, data));
        info.setLastName(lastName);
        
        info.setIp("rfdsfdsf");
        data = TokenUtil.generateTokenData(info);
        assertFalse("Generated array should be different", Arrays.equals(expectedResult, data));
        info.setIp(ip);

        info.setTokenID("fdfdsfdsfdsfds");
        data = TokenUtil.generateTokenData(info);
        assertTrue("Generated array should be equal", Arrays.equals(expectedResult, data));
        info.setTokenID(tokenID);
    }
}
