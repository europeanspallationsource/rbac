/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.sso;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.container.AsyncResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.esss.ics.rbac.jaxb.TokenInfo;

/**
 * <code>TokenMap</code> holds informations about stored tokens.
 *
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
public class TokenMap implements StreamGobbler.StreamListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(TokenMap.class);

    private static final String WIN_SCRIPT = "retrieveUsername.bat";
    private static final String LINUX_SCRIPT = "retrieveUsername.sh";
    private static final String WIN_SCRIPT_PATH = "scripts/" + WIN_SCRIPT;
    private static final String LINUX_SCRIPT_PATH = "scripts/" + LINUX_SCRIPT;

    private static final TokenMap INSTANCE = new TokenMap();
    private Map<String, TokenInfo> usernameTokenMap;
    private Map<String, String> tokenIdUsernameMap;
    private Map<String, List<AsyncResponse>> listeners;

    private File scriptFile;

    /**
     * @return class instance.
     */
    public static TokenMap getInstance() {
        return INSTANCE;
    }

    /**
     * Constructor in which all maps are constructed.
     */
    private TokenMap() {
        usernameTokenMap = new HashMap<String, TokenInfo>();
        tokenIdUsernameMap = new HashMap<String, String>();
        listeners = new HashMap<String, List<AsyncResponse>>();
    }

    /**
     * Method stores token into map and returns stored token informations.
     *
     * @param tokenInfo token informations
     * @param ip IP number
     * @param port port number
     *
     * @return stored token informations.
     */
    public synchronized TokenInfo setToken(TokenInfo tokenInfo, String ip, int port) {
        String username = getSystemUsername(ip, port);
        if (username != null && !username.isEmpty() && tokenInfo != null) {
            usernameTokenMap.put(username, tokenInfo);
            tokenIdUsernameMap.put(tokenInfo.getTokenID(), username);
            return tokenInfo;
        }
        return null;
    }

    /**
     * Method deletes and returns deleted token informations. If token is not found returns <code>null</code>
     *
     * @param tokenId token ID
     * @param ip IP number
     * @param port port number
     *
     * @return token informations if token was successfully deleted, otherwise null.
     */
    public synchronized TokenInfo deleteToken(String tokenId, String ip, int port) {
        String username = getSystemUsername(ip, port);
        if (username != null && !username.isEmpty() && tokenIdUsernameMap.containsKey(tokenId)) {
            TokenInfo token = null;
            if (username.equals(tokenIdUsernameMap.get(tokenId))) {
                token = usernameTokenMap.get(username);
                usernameTokenMap.remove(username);
                tokenIdUsernameMap.remove(tokenId);
            }
            return token;
        }
        return null;
    }

    /**
     * Method retrieves and returns token for map if exists. If token is not found returns <code>null</code>
     *
     * @param ip IP number
     * @param port port number
     *
     * @return token if exists, otherwise false.
     */
    public synchronized TokenInfo getToken(String ip, int port) {
        String username = getSystemUsername(ip, port);
        if (username != null && !username.isEmpty() && usernameTokenMap.containsKey(username)) {
            return usernameTokenMap.get(username);
        }
        return null;
    }

    /**
     * Creates and adds new listener to the map.
     *
     * @param ip IP number
     * @param port port number
     * @param response asynchronous response
     */
    public void addListener(String ip, int port, AsyncResponse response) {
        String username = getSystemUsername(ip, port);
        if (username != null) {
            List<AsyncResponse> responseList = new ArrayList<AsyncResponse>();
            if (listeners.containsKey(username)) {
                responseList.addAll(listeners.get(username));
            }
            responseList.add(response);
            listeners.put(username, responseList);
        }
    }

    /**
     * Removes action listener from list.
     *
     * @param username listeners key
     */
    public void removeActionListener(String username) {
        listeners.remove(username);
    }

    /**
     * @return map of listeners. Each user (map key) have a list of asynchronous responses waiting for data.
     */
    public Map<String, List<AsyncResponse>> getListeners() {
        return listeners;
    }

    /**
     * Retrieves system user username. Method executes batch script which takes IP number and port number as parameter.
     *
     * @param ip IP number
     * @param port port number
     *
     * @return system user username.
     */
    public String getSystemUsername(String ip, int port) {
        try {
            copyScriptFile();
            String[] cmd = {scriptFile.getAbsolutePath(), String.valueOf(port), ip};
            Process process = Runtime.getRuntime().exec(cmd);
            StreamGobbler gobbler = new StreamGobbler(process.getErrorStream(), "TokenRetrieval", this);
            gobbler.start();
            BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String username = br.readLine();
            br.close();
            process.waitFor();
            return username;
        } catch (IOException | InterruptedException e) {
            LOGGER.error("Error while trying to retrieve username using the port '" + port + "'.", e);
            return null;
        }
    }

    /**
     * Copy the script file from within the jar to the temporary folder so that it
     * can be executed with {@link Runtime}.
     *
     * @throws IOException if there was an error in copying the script
     * @throws InterruptedException
     */
    private void copyScriptFile() throws IOException, InterruptedException {
        if (scriptFile != null && scriptFile.exists()) {
            return;
        }
        InputStream stream = TokenMap.class.getClassLoader().getResourceAsStream(getOSRelatedScriptPath());
        if (stream == null) {
            throw new IOException("Script is missing.");
        }

        File file = new File(System.getProperty("java.io.tmpdir"), "rbac");
        if (!file.exists()) {
        	file.mkdirs();
            LOGGER.info("Created folder " + file + " to store the script files to.");
        }
        String osRelatedScript = getOSRelatedScript();
        file = new File(file,osRelatedScript);
        if (file.exists() && !file.delete()) {
            throw new IOException("Cannot delete file " + file + ".");
        }
        try (FileOutputStream os = new FileOutputStream(file)) {
            byte[] b = new byte[1024];
            int len = 0;
            do {
                len = stream.read(b);
                if (len < 1) {
                    break;
                }
                os.write(b, 0, len);
            } while(len > 0);
        }

        if (LINUX_SCRIPT.equals(osRelatedScript)) {
        	Set<PosixFilePermission> permissions = new HashSet<PosixFilePermission>();
            //add owners permission
        	permissions.add(PosixFilePermission.OWNER_READ);
        	permissions.add(PosixFilePermission.OWNER_EXECUTE);
            //add group permissions
        	permissions.add(PosixFilePermission.GROUP_READ);
        	permissions.add(PosixFilePermission.GROUP_EXECUTE);
            //add others permissions
        	permissions.add(PosixFilePermission.OTHERS_READ);
        	permissions.add(PosixFilePermission.OTHERS_EXECUTE);
            Files.setPosixFilePermissions(Paths.get(file.getAbsolutePath()), permissions);
        	LOGGER.info("Changed script file permissions.");
        }

        scriptFile = file;
    }

    private static String getOSRelatedScript() {
        String os = System.getProperty("os.name").toLowerCase();
        if (os.indexOf("win") >= 0) {
            return WIN_SCRIPT;
        } else if (os.indexOf("mac") >= 0 || os.indexOf("linux") >= 0 || os.indexOf("nix") >= 0) {
            return LINUX_SCRIPT;
        }
        return null;
    }

    private static String getOSRelatedScriptPath() {
        String os = System.getProperty("os.name").toLowerCase();
        if (os.indexOf("win") >= 0) {
            return WIN_SCRIPT_PATH;
        } else if (os.indexOf("mac") >= 0 || os.indexOf("linux") >= 0 || os.indexOf("nix") >= 0) {
            return LINUX_SCRIPT_PATH;
        }
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.sso.StreamGobbler.StreamListener#error(java.lang.String)
     */
    @Override
    public void error(String message) {
        LOGGER.error(message);
    }

    /**
     * @return the map of all tokens, where the key is the system username of the session that
     *          requested the token and the value if the token that belongs to that session
     */
    public Map<String,TokenInfo> getTokens() {
        return new HashMap<String, TokenInfo>(usernameTokenMap);
    }
}
