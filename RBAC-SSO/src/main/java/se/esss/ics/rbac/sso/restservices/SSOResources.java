/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.sso.restservices;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.glassfish.grizzly.http.server.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.esss.ics.rbac.sso.TokenMap;
import se.esss.ics.rbac.jaxb.TokenInfo;

/**
 * <code>SSOResources</code> exposes ReST service methods for local storing, retrieving and deleting tokens.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@Path("/sso")
public class SSOResources {

    private static final Logger LOGGER = LoggerFactory.getLogger(SSOResources.class);

    private TokenMap tokenMap;

    /**
     * Constructor in which <code>TokenMap</code> instance is retrieved.
     */
    public SSOResources() {
        tokenMap = TokenMap.getInstance();
    }

    /**
     * <p>
     * Method looks for the token of the connected user. User is identified by the IP number and port number which are
     * provided using custom HTTP headers. Port number is stored in the <code>RBACAddressPort</code> header, IP number
     * is stored in the <code>RBACAddress</code> header. If port or IP is not provided within the header it is extracted
     * from HTTP context.
     * </p>
     * <p>
     * If the token exists, it is returned as XML in a response with status 302 (FOUND) to the caller. If the token does
     * not exist 404 (NOT FOUND) is returned, if information about port number is missing 400 (BAD REQUEST) is returned.
     * </p>
     * 
     * @param request GET request
     * 
     * @return token information with FOUND status if retrieval was successful or, BAD REQUEST, NOT FOUND explaining the
     *         reason for failure if retrieval was unsuccessful or if exception message occurs in case of unexpected
     *         behaviour.
     */
    @GET
    @Path("/token")
    @Produces(MediaType.APPLICATION_XML)
    public Response getLocalToken(@Context Request request) {
        String ip = null;
        int port = 0;
        try {
            ip = SSORequestUtilites.getIP(request);
            port = SSORequestUtilites.getPort(request);
            TokenInfo tokenInfo = tokenMap.getToken(ip, port);
            return tokenInfo != null ? Response.status(Status.FOUND).entity(tokenInfo).build() : Response.status(
                    Status.NOT_FOUND).build();
        } catch (Exception e) {
            LOGGER.error("Error retrieving token for '" + ip + ":" + port + "'.", e);
            return Response.status(Status.BAD_REQUEST).build();
        }
    }

    /**
     * <p>
     * Method stores token for the authenticated user. Token is stored with the key which is equals to the system
     * username. System user is identified by the IP number and port number which are provided using custom HTTP
     * headers. Port number is stored in the <code>RBACAddressPort</code> header, IP number is stored in the
     * <code>RBACAddress</code> header. If IP number or port is not provided within the header it is extracted from HTTP
     * context.
     * </p>
     * <p>
     * If token is successfully stored response with status 201 (CREATED) is returned to the caller, otherwise status
     * 409 (CONFLICT) is returned. If information about port number or IP number is missing 400 (BAD REQUEST) is
     * returned.
     * </p>
     * 
     * @param request POST request
     * @param tokenInfo token informations
     * 
     * @return CREATED status if the token was successfully stored or, CONFLICT, BAD REQUEST explaining the reason for
     *         failure if storing was unsuccessful or if exception message occurs in case of unexpected behaviour.
     */
    @POST
    @Path("/token")
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
    public Response setLocalToken(@Context Request request, TokenInfo tokenInfo) {
        String ip = null;
        int port = 0;
        try {
            ip = SSORequestUtilites.getIP(request);
            port = SSORequestUtilites.getPort(request);
            TokenInfo savedToken = tokenMap.setToken(tokenInfo, ip, port);
            if (savedToken != null) {
                String username = tokenMap.getSystemUsername(ip, port);
                notifyClients(username, savedToken, true);
                return Response.status(Status.CREATED).build();
            }
            return Response.status(Status.CONFLICT).build();
        } catch (Exception e) {
            LOGGER.error("Error setting token for '" + ip + ":" + port + "'.", e);
            return Response.status(Status.BAD_REQUEST).build();
        }
    }

    /**
     * <p>
     * Method deletes token from local authentication service. Token ID is given as path parameter. System user is
     * identified by the IP number and port number which are provided using custom HTTP headers. Port number is stored
     * in the <code>RBACAddressPort</code> header, IP number is stored in the <code>RBACAddress</code> header. If port
     * or IP number is not provided within the header it is extracted from HTTP context.
     * </p>
     * <p>
     * If token is successfully deleted response with status 200 (OK) is returned to the caller. If the token does not
     * exist 404 (NOT FOUND) is returned, if information about IP address or port number is missing 400 (BAD REQUEST) is
     * returned.
     * </p>
     * 
     * @param tokenId token ID
     * @param request DELETE request
     * 
     * @return OK status if the token was successfully deleted or, NOT FOUND, BAD REQUEST explaining the reason for
     *         failure if deletion was unsuccessful or if exception message occurs in case of unexpected behaviour.
     */
    @DELETE
    @Path("/token/{tokenId}")
    @Produces(MediaType.APPLICATION_XML)
    public Response deleteLocalToken(@PathParam(value = "tokenId") String tokenId, @Context Request request) {
        String ip = null;
        int port = 0;
        try {
            ip = SSORequestUtilites.getIP(request);
            port = SSORequestUtilites.getPort(request);
            TokenInfo deletedToken = tokenMap.deleteToken(tokenId, ip, port);
            if (deletedToken != null) {
                String username = tokenMap.getSystemUsername(ip, port);
                notifyClients(username, deletedToken, false);
                return Response.status(Status.OK).build();
            }
            return Response.status(Status.NOT_FOUND).build();
        } catch (Exception e) {
            LOGGER.error("Error deleting token for '" + ip + ":" + port + "'.", e);
            return Response.status(Status.BAD_REQUEST).build();
        }
    }

    /**
     * <p>
     * Method suspends request processing and stores asynchronous response as a listener. When response is available
     * request processing is resumed.
     * </p>
     * <p>
     * If asynchronous response is successfully saved, 200 (OK) status is returned.
     * 
     * @param request GET request
     * @param response suspended response
     * 
     * @return OK status if the response was successfully saved as a listener.
     */
    @GET
    @Path("/notify")
    @Produces(MediaType.APPLICATION_XML)
    public Response registerListener(@Context Request request, @Suspended AsyncResponse response) {
        String ip = SSORequestUtilites.getIP(request);
        int port = SSORequestUtilites.getPort(request);
        String requestHeader = SSORequestUtilites.getCurrentTokenPart(request);
        // try to retrieve token
        TokenInfo tokenInfo = tokenMap.getToken(ip, port);
        boolean resumed = false;
        if (tokenInfo != null) {
            // check request header
            if (requestHeader == null || requestHeader.isEmpty()) {
                response.resume(Response.status(Status.CREATED).entity(tokenInfo).build());
                resumed = true;
            } else {
                String tokenId = tokenInfo.getTokenID();
                String tokenPart = tokenId.substring(0, tokenId.indexOf('-') - 1);
                if (!tokenPart.equals(requestHeader)) {
                    response.resume(Response.status(Status.CREATED).entity(tokenInfo).build());
                    resumed = true;
                }
            }
        } else {
            if (requestHeader != null && !requestHeader.isEmpty()) {
                response.resume(Response.status(Status.FORBIDDEN).build());
                resumed = true;
            }
        }
        if (!resumed) {
            tokenMap.addListener(ip, port, response);
        }
        return Response.ok().build();
    }

    /**
     * Notifies all client with open connection if the token was saved or deleted from local authentication service. If
     * token was saved response with status CREATED and token is returned, otherwise response with status GONE and token
     * is returned.
     * 
     * @param tokenInfo token informations
     * @param tokenCreated true if token is created, otherwise false
     */
    private void notifyClients(String username, TokenInfo tokenInfo, boolean tokenCreated) {
        List<AsyncResponse> responseList = tokenMap.getListeners().get(username);
        if (responseList != null) {
            for (AsyncResponse response : responseList) {
                if (tokenCreated) {
                    response.resume(Response.status(Status.CREATED).entity(tokenInfo).build());
                } else {
                    response.resume(Response.status(Status.OK).entity(tokenInfo).build());
                }
            }
            tokenMap.getListeners().remove(username);
        }
    }
}
