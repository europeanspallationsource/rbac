FROM jboss/wildfly:8.2.1.Final
LABEL maintainer="anders.harrisson@esss.se"

# PostgreSQL jdbc driver module
RUN mkdir -p /opt/jboss/wildfly/modules/org/postgresql/main
ADD --chown=jboss:jboss https://artifactory.esss.lu.se/artifactory/repo1/org/postgresql/postgresql/42.2.0/postgresql-42.2.0.jar /opt/jboss/wildfly/modules/org/postgresql/main/
ADD --chown=jboss:jboss https://artifactory.esss.lu.se/artifactory/wildfly-modules/org.postgresql/postgresql/postgresql-42.2.0.xml /opt/jboss/wildfly/modules/org/postgresql/main/module.xml
COPY --chown=jboss:jboss standalone.xml /opt/jboss/wildfly/standalone/configuration/

# GELF logging module
ADD --chown=jboss:jboss https://artifactory.esss.lu.se/artifactory/repo1/biz/paluch/logging/logstash-gelf/1.12.0/logstash-gelf-1.12.0-logging-module.zip /tmp/gelf-logging-module.zip
RUN unzip -q /tmp/gelf-logging-module.zip -d /tmp/ && mv /tmp/logstash-gelf-*/* /opt/jboss/wildfly/modules/ && rmdir /tmp/logstash-gelf-* && rm /tmp/gelf-logging-module.zip

COPY RBAC-Enterprise/target/rbac-ear-*.ear /opt/jboss/wildfly/standalone/deployments/
