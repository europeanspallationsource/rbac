/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.dsaccess.ldap.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import se.esss.ics.rbac.dsaccess.UserInfo;
import se.esss.ics.rbac.dsaccess.ldap.LDAPUserInfo;

/**
 * 
 * <code>LDAPUserInfoTest</code> tests methods in the {@link LDAPUserInfo}.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class LDAPUserInfoTest {

    /**
     * Test that user info can be created with all parameters.
     * 
     * @throws Exception on error
     */
    @Test
    public void testCreation() throws Exception {
        Constructor<LDAPUserInfo> c = LDAPUserInfo.class.getDeclaredConstructor(String.class, String.class,
                String.class, String.class, String.class, String.class, String.class, String.class, String.class);
        c.setAccessible(true);

        LDAPUserInfo info2 = c.newInstance("johndoe", "Doe", "John", "F", "JDG1", "john@doe.com", "123456789", null,
                "JDB1");
        assertEquals("Username should match", "johndoe", info2.getUsername());
        assertEquals("Last name should match", "Doe", info2.getLastName());
        assertEquals("First name should match", "John", info2.getFirstName());
        assertEquals("Middle name should match", "F", info2.getMiddleName());
        assertEquals("Group should match", "JDG1", info2.getGroup());
        assertEquals("Email should match", "john@doe.com", info2.getEMail());
        assertEquals("Phone number should match", "123456789", info2.getPhoneNumber());
        assertEquals("Location should match", "JDB1", info2.getLocation());

        LDAPUserInfo info3 = c.newInstance("johndoe", "Doe", "John", "F", "JDG1", "john@doe.com", null, "123456789",
                "JDB1");
        assertEquals("Users should be equals, the phone and mobile phone number are interchangeable", info2, info3);

        LDAPUserInfo info4 = c.newInstance("johndoe", "Doe", "John", "F", "JDG1", "john@doe.com", "123456789", "6789",
                "JDB1");
        assertEquals("Users should be equals, the phone number has priority over mobile", info2, info4);

    }

    /**
     * Test that certain null parameters are allowed, other not.
     * 
     * @throws Exception on error
     */
    @Test
    public void testNullValues() throws Exception {
        Constructor<LDAPUserInfo> c = LDAPUserInfo.class.getDeclaredConstructor(String.class, String.class,
                String.class, String.class, String.class, String.class, String.class, String.class, String.class);
        c.setAccessible(true);

        try {
            c.newInstance("User", "lastname", null, null, null, null, null, null, null);
            fail("Exception should ocurr, first name cannot be null");
        } catch (InvocationTargetException e) {
            assertEquals("Illegal argument exceptio should be thrown by constructor", IllegalArgumentException.class, e
                    .getCause().getClass());
            assertEquals("Exception expected", "Username, last name, and first name must not be null.", e.getCause()
                    .getMessage());
        }

        try {
            c.newInstance("User", null, "firstname", null, null, null, null, null, null);
            fail("Exception should ocurr, last name cannot be null");
        } catch (InvocationTargetException e) {
            assertEquals("Illegal argument exceptio should be thrown by constructor", IllegalArgumentException.class, e
                    .getCause().getClass());
            assertEquals("Exception expected", "Username, last name, and first name must not be null.", e.getCause()
                    .getMessage());
        }
        try {
            c.newInstance(null, "lastname", "firstname", null, null, null, null, null, null);
            fail("Exception should ocurr, username cannot be null");
        } catch (InvocationTargetException e) {
            assertEquals("Illegal argument exceptio should be thrown by constructor", IllegalArgumentException.class, e
                    .getCause().getClass());
            assertEquals("Exception expected", "Username, last name, and first name must not be null.", e.getCause()
                    .getMessage());
        }

        LDAPUserInfo info = c.newInstance("user", "lastname", "firstname", null, null, null, null, null, null);

        assertEquals("Username should be null", "user", info.getUsername());
        assertEquals("Last name should be null", "lastname", info.getLastName());
        assertEquals("First name should be null", "firstname", info.getFirstName());
        assertNull("Middle name should be null", info.getMiddleName());
        assertNull("Group should be null", info.getGroup());
        assertNull("Email should be null", info.getEMail());
        assertNull("Phone number should be null", info.getPhoneNumber());
        assertNull("Location should be null", info.getLocation());
    }

    /**
     * Test if comparison of different user info objects is done according to the API documentation: last name are
     * compared first, after that first names are compared and then usernames are compared. The rest is optional.
     * 
     * @throws Exception on error
     */
    @Test
    public void testComparison() throws Exception {
        Constructor<LDAPUserInfo> c = LDAPUserInfo.class.getDeclaredConstructor(String.class, String.class,
                String.class, String.class, String.class, String.class, String.class, String.class, String.class);
        c.setAccessible(true);

        LDAPUserInfo ldapInfo1 = c.newInstance("johndoe", "Doe", "John", "F", "JDG1", "john@doe.com", "123456789",
                null, "JDB1");

        assertNotEquals("UserInfo should be different than null", ldapInfo1.equals(null));

        LDAPUserInfo ldapInfo2 = c.newInstance("johndoe", "Doe", "John", "F", "JDG1", "john@doe.com", "123456789",
                null, "JDB1");
        LDAPUserInfo ldapInfo3 = c.newInstance("janedoe", "Doe", "Jane", "F", "JDG1", "jane@doe.com", "123456789",
                null, "JDB1");
        TestUserInfo testInfo = new TestUserInfo("johndoe", "Doe", "John", "F", "JDG1", "john@doe.com", "123456789",
                "JDB1");

        assertEquals("If fields match, comparison should be 0", 0, ldapInfo1.compareTo(ldapInfo2));
        assertEquals("If fields match, comparison should be 0", 0, ldapInfo1.compareTo(testInfo));

        assertEquals("User infos should be equal", ldapInfo1, ldapInfo1);
        assertEquals("User infos should be equal", ldapInfo1, ldapInfo2);
        assertNotEquals("User infos should not be equal", ldapInfo1, testInfo);
        assertNotEquals("User infos should not be equal", ldapInfo1, ldapInfo3);

        LDAPUserInfo ldapInfo4 = c.newInstance("johndoe", "Axe", "John", "F", "JDG1", "john@doe.com", "123456789",
                null, "JDB1");
        LDAPUserInfo ldapInfo5 = c.newInstance("johndoe2", "Doe", "John", "F", "JDG1", "john@doe.com", "123456789",
                null, "JDB1");

        assertTrue("John Doe is after Jane Doe (name precedence)", ldapInfo1.compareTo(ldapInfo3) > 0);
        assertTrue("John Doe is after John Axe (lastname precedence)", ldapInfo1.compareTo(ldapInfo4) > 0);
        assertTrue("John Doe is before John Doe 2 (username precedence)", ldapInfo1.compareTo(ldapInfo5) < 0);

        LDAPUserInfo ldapInfo6 = c.newInstance("johndoe", "Doe", "John", "F", "JDG1", "john@doe.com", "123456789",
                null, "JDB1");
        LDAPUserInfo ldapInfo7 = c.newInstance("johndoe", "Doe", "John", "F", "JDG1", "john@doe.com", "123456789",
                null, "JDB2");

        assertTrue("LDAPInfo6 is before LDAPInfo7", ldapInfo6.compareTo(ldapInfo7) < 0);

        LDAPUserInfo ldapInfo8 = c.newInstance("johndoe", "Doe", "John", "F", "JDG1", "john@doe.com", "123456789",
                null, "JDB1");
        LDAPUserInfo ldapInfo9 = c.newInstance("johndoe", "Doe", "John", "F", "JDG1", "john@doe.com", "123456799",
                null, "JDB1");

        assertTrue("LDAPInfo8 is before LDAPInfo9", ldapInfo8.compareTo(ldapInfo9) < 0);

        LDAPUserInfo ldapInfo10 = c.newInstance("johndoe", "Doe", "John", "F", "JDG1", "john@doe.com", "123456789",
                null, "JDB1");
        LDAPUserInfo ldapInfo11 = c.newInstance("johndoe", "Doe", "John", "F", "JDG2", "john@doe.com", "123456789",
                null, "JDB1");

        assertTrue("LDAPInfo10 is before LDAPInfo11", ldapInfo10.compareTo(ldapInfo11) < 0);

        LDAPUserInfo ldapInfo12 = c.newInstance("johndoe", "Doe", "John", "F", "JDG1", "john@doe.com", "123456789",
                null, "JDB1");
        LDAPUserInfo ldapInfo13 = c.newInstance("johndoe", "Doe", "John", "F", "JDG1", "john@doe1.com", "123456789",
                null, "JDB1");

        assertTrue("LDAPInfo12 is before LDAPInfo13", ldapInfo12.compareTo(ldapInfo13) < 0);
    }

    private static class TestUserInfo implements UserInfo {
        private static final long serialVersionUID = -3826109296767680943L;

        private String username;
        private String lastName;
        private String firstName;
        private String middleName;
        private String group;
        private String eMail;
        private String phoneNumber;
        private String location;

        public TestUserInfo(String username, String lastName, String firstName, String middleName, String group,
                String eMail, String phoneNumber, String location) {
            super();
            this.username = username;
            this.lastName = lastName;
            this.firstName = firstName;
            this.middleName = middleName;
            this.group = group;
            this.eMail = eMail;
            this.phoneNumber = phoneNumber;
            this.location = location;
        }

        @Override
        public int compareTo(UserInfo o) {
            return 0;
        }

        @Override
        public String getUsername() {
            return username;
        }

        @Override
        public String getLastName() {
            return lastName;
        }

        @Override
        public String getFirstName() {
            return firstName;
        }

        @Override
        public String getMiddleName() {
            return middleName;
        }

        @Override
        public String getGroup() {
            return group;
        }

        @Override
        public String getEMail() {
            return eMail;
        }

        @Override
        public String getPhoneNumber() {
            return phoneNumber;
        }

        @Override
        public String getLocation() {
            return location;
        }
    }
}
