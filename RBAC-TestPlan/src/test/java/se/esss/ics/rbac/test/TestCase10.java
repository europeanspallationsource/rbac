/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Map;

import org.csstudio.utility.pv.PV;
import org.csstudio.utility.pv.PVException;
import org.csstudio.utility.pv.PVFactoryProvider;
import org.junit.Before;
import org.junit.Test;

import se.esss.ics.rbac.access.Credentials;
import se.esss.ics.rbac.access.ISecurityFacade;
import se.esss.ics.rbac.access.SecurityFacade;
import se.esss.ics.rbac.access.SecurityFacadeException;
import se.esss.ics.rbac.access.Token;
import se.esss.ics.rbac.access.swing.SwingSecurityCallback;
import se.esss.ics.rbac.test.internal.Base;

/**
 * 
 * <code>TestCase10: Authorization</code> verifies that authorisation for permissions is based on the valid token, IP,
 * machine state and roles.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class TestCase10 extends Base {

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        SecurityFacade.getDefaultInstance().setDefaultSecurityCallback(new SwingSecurityCallback() {
            @Override
            public Credentials getCredentials() {
                return new Credentials(username, password);
            }
        });
    }

    /**
     * Test that permission is granted if proper role is there and denied if missing.
     */
    @Test
    public void rbac180Role() throws SecurityFacadeException {
        ISecurityFacade facade = SecurityFacade.getDefaultInstance();
        facade.authenticate();
        boolean permission = facade.hasPermission("RBACDemo", "Add");
        assertTrue("Permission Add should be granted", permission);

        // check if permission is denied if the role is not available
        facade.logout();
        facade.setDefaultSecurityCallback(new SwingSecurityCallback() {
            @Override
            public Credentials getCredentials() {
                return new Credentials(username, password, "RBACAdministrator");
            }
        });
        facade.authenticate();
        permission = facade.hasPermission("RBACDemo", "Add");
        assertFalse("Permission Add should be denied", permission);
    }

    /**
     * Test that permission is granted with a valid token and denied if token is invalid.
     */
    @Test
    public void rbac180Token() throws SecurityFacadeException {
        ISecurityFacade facade = SecurityFacade.getDefaultInstance();
        Token token = facade.authenticate();
        boolean permission = facade.hasPermission("RBACDemo", "Add");
        assertTrue("Permission Add is granted", permission);
        // invalidate token
        facade.logout();

        facade.setToken(token.getTokenID());
        try {
            permission = facade.hasPermission("RBACDemo", "Add");
            fail("Exception should occur, because token is invalid");
        } catch (SecurityFacadeException e) {
            assertEquals("Exception expected, token is invalid", "Service responded unexpectedly: Token with ID '"
                    + new String(token.getTokenID()) + "' could not be found.", e.getMessage().trim());
        }
    }

    /**
     * Test if authorization can be based on the IP of the caller.
     */
    @Test
    public void rbac180IP() throws SecurityFacadeException {
        ISecurityFacade facade = SecurityFacade.getDefaultInstance();
        //first login with IP that is allowed        
        facade.setDefaultSecurityCallback(new SwingSecurityCallback() {
            @Override
            public Credentials getCredentials() {
                return new Credentials(username, password, null, "10.5.2.224");
            }
        });
        facade.authenticate();
        boolean ans = facade.hasPermission("RBACDemo", "AddIP");
        assertTrue("Permission should be granted, because request came from the correct IP", ans);
        facade.logout();
        
        facade.setDefaultSecurityCallback(new SwingSecurityCallback() {
            @Override
            public Credentials getCredentials() {
                return new Credentials(username, password, null, "192.168.1.123");
            }
        });
        facade.authenticate();
        ans = facade.hasPermission("RBACDemo", "AddIP");
        assertFalse("Permission is not granted, because request came from a wrong IP", ans);
        facade.logout();
    }

    /**
     * Test if authorisation can be based on the machine state. This test requires an IOC with 
     * a PV called MachineState.
     * @throws InterruptedException 
     */
    @Test
    public void rbac490and180MachineState() throws SecurityFacadeException, PVException, InterruptedException {
        PV pv = PVFactoryProvider.getInstance().getDefaultFactory().createPV("MachineState");
        pv.start();
        pv.blockUntilFirstUpdate(3000);
        pv.setValue(500);
        //wait a while that the new value is registered by the service
        Thread.sleep(1000);
        
        ISecurityFacade facade = SecurityFacade.getDefaultInstance();
        //first login with IP that is not allowed
        facade.setDefaultSecurityCallback(new SwingSecurityCallback() {
            @Override
            public Credentials getCredentials() {
                return new Credentials(username, password);
            }
        });
        facade.authenticate();
        boolean ans = facade.hasPermission("RBACDemo", "Remove");
        assertFalse("Permission should not be granted, because the value of MachineState is not 1", ans);
        facade.logout();
        
        pv.setValue(1);
        //wait a while that the new value is registered by the service
        Thread.sleep(1000);
        
        facade.setDefaultSecurityCallback(new SwingSecurityCallback() {
            @Override
            public Credentials getCredentials() {
                return new Credentials(username, password);
            }
        });
        facade.authenticate();
        ans = facade.hasPermission("RBACDemo", "Remove");
        assertTrue("Permission should be granted, because the value of MachineState is 1", ans);
        facade.logout();
        
        pv.stop();
    }

    /**
     * Test if multiple permissions can be retrieved at once for a user.
     */
    @Test
    public void rbac249() throws Exception {
        ISecurityFacade facade = SecurityFacade.getDefaultInstance();
        facade.authenticate();
        Map<String, Boolean> permissions = facade.hasPermissions("RBACManagementStudio", "ManageRole",
                "ManageResource", "ManageRule", "ReleaseExclusiveAccess", "InvalidateTokens");

        assertEquals("Permissions map should contain all 5 permissions", 5, permissions.size());
        assertTrue("ManageRole permission should be granted", permissions.get("ManageRole").booleanValue());
        assertTrue("ManageResource permission should be granted", permissions.get("ManageResource").booleanValue());
        assertTrue("ManageRule permission should be granted", permissions.get("ManageRule").booleanValue());
        assertTrue("ReleaseExclusiveAccess permission should be granted", permissions.get("ReleaseExclusiveAccess")
                .booleanValue());
        assertTrue("InvalidateTokens permission should be granted", permissions.get("InvalidateTokens").booleanValue());

    }
}
