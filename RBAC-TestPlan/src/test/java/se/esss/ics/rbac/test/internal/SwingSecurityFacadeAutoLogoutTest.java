/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.test.internal;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.awt.GraphicsEnvironment;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.lang.reflect.Field;
import java.security.cert.X509Certificate;

import javax.swing.JFrame;

import org.junit.Ignore;
import org.junit.Test;

import se.esss.ics.rbac.test.TestCase8;
import se.esss.ics.rbac.access.AutoLogout;
import se.esss.ics.rbac.access.ISecurityFacade;
import se.esss.ics.rbac.access.swing.SecurityFacade;
import se.esss.ics.rbac.access.swing.SwingSecurityCallback;
import se.esss.ics.rbac.access.test.SecurityFacadeAutoLogoutTest;

/**
 * 
 * <code>SwingSecurityFacadeAutoLogoutTest</code> test the auto logout functionality using the swing security facade.
 * This class uses a Robot to manipulate input events. This class is used within the {@link TestCase8} and should not be
 * executed on its own.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
@Ignore
public class SwingSecurityFacadeAutoLogoutTest extends SecurityFacadeAutoLogoutTest {

    private JFrame frame;

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.access.test.AbstractSecurityFacadeTest#createFacade()
     */
    @Override
    protected ISecurityFacade createFacade() {
        ISecurityFacade facade = new SecurityFacade(super.createFacade());
        facade.setDefaultSecurityCallback(new SwingSecurityCallback() {
            @Override
            public boolean acceptCertificate(String host, X509Certificate certificate) {
                return true;
            }
        });
        return facade;
    }

    /**
     * Forcefully set the auto logout features, which cannot be set in a standard way. This is necessary to make the
     * test shorter, so one doesn't need to wait for long timeouts.
     * 
     * @throws Exception
     */
    @Override
    protected void setUpAutoLogout() throws Exception {
        // force autoLogout of 1000 milliseconds to avoid excessive waiting during this test
        Field field = se.esss.ics.rbac.access.SecurityFacade.class.getDeclaredField("autoLogout");
        field.setAccessible(true);
        Field delegate = SecurityFacade.class.getDeclaredField("delegate");
        delegate.setAccessible(true);
        AutoLogout logout = (AutoLogout) field.get(delegate.get(facade));
        Field f = AutoLogout.class.getDeclaredField("logoutTimeout");
        f.setAccessible(true);
        f.set(logout, Long.valueOf(1000));
    }

    private void setUpFrame() {
        if (GraphicsEnvironment.isHeadless()) {
            fail("Cannot test swing auto logout in a headless environment");
        }
        frame = new JFrame();
        frame.setSize(300, 300);
        frame.setLocation(0, 0);
        frame.setVisible(true);
    }

    @Override
    public void tearDown() throws Exception {
        if (frame != null) {
            frame.setVisible(false);
            frame.dispose();
        }
        super.tearDown();

    }

    /**
     * Test if auto logout is cancelled when mouse is moving.
     * 
     * @throws Exception
     */
    @Test
    public void testWithMouseMovement() throws Exception {
        setUpFrame();
        Robot robot = new Robot();

        Callback cb = new Callback(true, true);
        facade.setDefaultSecurityCallback(cb);

        synchronized (cb) {
            robot.mouseMove(100, 100);
            cb.wait(250);
            robot.mousePress(InputEvent.BUTTON1_MASK);
            // add time between press and release, so the system recognizes that as a click
            cb.wait(250);
            robot.mouseRelease(InputEvent.BUTTON1_MASK);
            cb.wait(250);
            for (int i = 0; i < 15; i++) {
                robot.mouseMove(100 + i * 5, 100 + i * 5);
                cb.wait(500);
            }
        }

        assertNull("Auto logout should not be called", cb.token);

        synchronized (cb) {
            cb.wait(4000);
        }

        assertNotNull("Auto logout should be called", cb.token);
    }

    /**
     * Test if auto logout is cancelled when mouse buttons are pressed.
     * 
     * @throws Exception
     */
    @Test
    public void testWithMouseClick() throws Exception {
        setUpFrame();
        Robot robot = new Robot();

        Callback cb = new Callback(true, true);
        facade.setDefaultSecurityCallback(cb);

        synchronized (cb) {
            robot.mouseMove(100, 100);
            cb.wait(250);
            for (int i = 0; i < 15; i++) {
                robot.mousePress(InputEvent.BUTTON1_MASK);
                // add time between press and release, so the system recognizes that as a click
                cb.wait(250);
                robot.mouseRelease(InputEvent.BUTTON1_MASK);
                cb.wait(500);
            }
        }

        assertNull("Auto logout should not be called", cb.token);

        synchronized (cb) {
            cb.wait(4000);
        }

        assertNotNull("Auto logout should be called", cb.token);
    }

    /**
     * Test if auto logout is cancelled when mouse wheel is moving.
     * 
     * @throws Exception
     */
    @Test
    public void testWithMouseWheelMove() throws Exception {
        setUpFrame();
        Robot robot = new Robot();

        Callback cb = new Callback(true, true);
        facade.setDefaultSecurityCallback(cb);

        synchronized (cb) {
            robot.mouseMove(100, 100);
            cb.wait(250);
            robot.mousePress(InputEvent.BUTTON1_MASK);
            // add time between press and release, so the system recognizes that as a click
            cb.wait(250);
            robot.mouseRelease(InputEvent.BUTTON1_MASK);
            cb.wait(250);
            for (int i = 0; i < 15; i++) {
                robot.mouseWheel(20);
                cb.wait(500);
            }
        }

        assertNull("Auto logout should not be called", cb.token);

        synchronized (cb) {
            cb.wait(4000);
        }

        assertNotNull("Auto logout should be called", cb.token);
    }

    /**
     * Test if auto logout is cancelled when keys are typed.
     * 
     * @throws Exception
     */
    @Test
    public void testWitKeyTyped() throws Exception {
        setUpFrame();
        Robot robot = new Robot();

        Callback cb = new Callback(true, true);
        facade.setDefaultSecurityCallback(cb);

        synchronized (cb) {
            robot.mouseMove(100, 100);
            cb.wait(250);
            robot.mousePress(InputEvent.BUTTON1_MASK);
            // add time between press and release, so the system recognizes that as a click
            cb.wait(250);
            robot.mouseRelease(InputEvent.BUTTON1_MASK);
            cb.wait(250);
            for (int i = 0; i < 15; i++) {
                robot.keyPress(KeyEvent.VK_F);
                // add time between press and release, so the system recognizes that as a click
                cb.wait(250);
                robot.keyRelease(KeyEvent.VK_F);
                cb.wait(500);
            }
        }

        assertNull("Auto logout should not be called", cb.token);

        synchronized (cb) {
            cb.wait(4000);
        }

        assertNotNull("Auto logout should be called", cb.token);
    }
}
