/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import se.esss.ics.rbac.access.Credentials;
import se.esss.ics.rbac.access.ISecurityFacade;
import se.esss.ics.rbac.access.SecurityCallbackAdapter;
import se.esss.ics.rbac.access.SecurityFacade;
import se.esss.ics.rbac.access.SecurityFacadeException;
import se.esss.ics.rbac.access.Token;
import se.esss.ics.rbac.test.internal.Base;

/**
 * 
 * <code>TestCase2: Token Verification</code> verifies the correct content of the token received after authentication.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class TestCase2 extends Base {

    private String preferredRole = "RBACAdministrator";
    private String expectedIP = "10.5.1.189";

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        SecurityFacade.getDefaultInstance().setDefaultSecurityCallback(new SecurityCallbackAdapter() {
            @Override
            public Credentials getCredentials() {
                return new Credentials(username, password);
            }
        });
    }

    /**
     * Test that the token is created by the services and that it can always be obtained if requested.
     */
    @Test
    public void rbac130() throws SecurityFacadeException {
        ISecurityFacade facade = SecurityFacade.getDefaultInstance();
        Token token = facade.authenticate();
        assertNotNull("Received token should not be null", token);

        // create new facade and force it to retrieve a token from the
        // service to verify that the token is known by the service
        SecurityFacade facade2 = new SecurityFacade();
        assertNull("No token should be available initially in the secondary facade", facade2.getToken());
        facade2.setToken(token.getTokenID());
        Token newToken = facade2.getToken();
        assertEquals("Original and new token should be equal", token, newToken);
        assertEquals("Original and new token should have the same hash code", token.hashCode(), newToken.hashCode());
    }

    /**
     * Test the the token contains all the user's roles or his preferred role if requested so.
     */
    @Test
    public void rbac133() throws SecurityFacadeException {
        ISecurityFacade facade = SecurityFacade.getDefaultInstance();
        Token token = facade.authenticate();
        String[] roles = token.getRoles();
        assertTrue("There are at least two roles defined for the user", roles.length > 1);
        for (String s : roles) {
            assertNotNull("None of the roles is null", s);
            assertTrue("None of the roles is empty", !s.isEmpty());
        }

        // request a new token with specified role
        facade.logout();
        facade.setDefaultSecurityCallback(new SecurityCallbackAdapter() {
            @Override
            public Credentials getCredentials() {
                return new Credentials(username, password, preferredRole);
            }
        });
        token = facade.authenticate();
        roles = token.getRoles();
        assertEquals("There is exactly one role associated with the token", 1, roles.length);
        assertEquals("The role should match the requested role", preferredRole, roles[0]);
    }

    /**
     * Test that the token carries information about the IP of the machine, which was used for authentication.
     */
    @Test
    public void rbac134() throws SecurityFacadeException, UnknownHostException {
        String hostIP = InetAddress.getLocalHost().getHostAddress();
        ISecurityFacade facade = SecurityFacade.getDefaultInstance();
        Token token = facade.authenticate();

        assertEquals("Token should be bound to the localhost IP", hostIP, token.getIP());
        assertEquals("The IP should be the same as known to the user", expectedIP, token.getIP());
    }

    /**
     * Test that the token contains the digital signature.
     */
    @Test
    public void rbac135() throws SecurityFacadeException {
        ISecurityFacade facade = SecurityFacade.getDefaultInstance();
        Token token = facade.authenticate();
        assertNotNull("RBAC signature should exist", token.getRBACSignature());
        assertEquals("Signature should be 512-bit", 512 / 8, token.getRBACSignature().length);
        // the content of the signature cannot be verified without public key,
        // however we can at least verify that the content of the signature was received,
        // because it is highly unlikely that the signature would be all zeroes
        byte[] signature = token.getRBACSignature();
        boolean foundNotZero = false;
        for (byte b : signature) {
            if (b != 0) {
                foundNotZero = true;
                break;
            }
        }
        assertTrue("At least one non-zero byte was found in the signature", foundNotZero);
    }

    /**
     * Test that the token has an expiration time.
     */
    @Test
    public void rbac140() throws SecurityFacadeException {
        ISecurityFacade facade = SecurityFacade.getDefaultInstance();
        Token token = facade.authenticate();
        assertNotNull("Expiration time should exist", token.getExpirationDate());
        assertTrue("Expiration time should be in the future", new Date().before(token.getExpirationDate()));
        // verification that authorisation with expired token is not possible would require
        // to wait at least an hour for the token to expire
    }

    /**
     * Test that logout invalidates the token.
     */
    @Test
    public void rbac150() throws SecurityFacadeException {
        ISecurityFacade facade = SecurityFacade.getDefaultInstance();
        Token token = facade.authenticate();
        // logout and set the token, which is supposed to be invalidated (because the user logged out)
        facade.logout();
        facade.setToken(token.getTokenID());
        try {
            facade.getToken();
            fail("Exception should occur, because token does not exist anymore");
        } catch (SecurityFacadeException e) {
            assertTrue(
                    "Exception expected",
                    e.getMessage().contains(
                            "Token with ID '" + new String(token.getTokenID()) + "' could not be found."));
        }
    }
}
