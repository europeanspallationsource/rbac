This project implements the semi automated integration tests for RBAC as defined in the RBAC Test Plan document (see ../RBAC-Documentation/RBAC_TestPlan.docx).

This project is intended to ease the testing of RBAC and is not intended to be compiled by maven.

