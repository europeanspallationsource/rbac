/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.bind.JAXB;

import org.junit.Test;

import se.esss.ics.rbac.access.SecurityCallback;
import se.esss.ics.rbac.access.SecurityFacade;
import se.esss.ics.rbac.access.SecurityFacadeException;
import se.esss.ics.rbac.jaxb.PermissionsInfo;

/**
 * 
 * <code>SecurityFacadeAuthorizationTest</code> tests the authorisation related methods from the {@link SecurityFacade}.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class SecurityFacadeAuthorizationTest extends AbstractSecurityFacadeBase {

    @Override
    public void setUp() throws Exception {
        super.setUp();
        setUpForAuthentication();
        facade.authenticate();
        setUpForPermissionCheck();
    }

    private void setUpForPermissionCheck() throws Exception {
        HashMap<String, Boolean> p1 = new HashMap<>();
        p1.put("p1", Boolean.TRUE);
        setUpPermissions(p1);

        HashMap<String, Boolean> p2 = new LinkedHashMap<>();
        p2.put("p1", Boolean.TRUE);
        p2.put("p2", Boolean.FALSE);
        setUpPermissions(p2);

        HashMap<String, Boolean> p3 = new HashMap<>();
        p3.put("p2", Boolean.FALSE);
        setUpPermissions(p3);

    }

    private void setUpPermissions(Map<String, Boolean> permissions) throws Exception {
        StringBuilder sb = new StringBuilder();
        for (String s : permissions.keySet().toArray(new String[permissions.size()])) {
            sb.append(s).append(',');
        }
        HttpURLConnection connection = mock(HttpURLConnection.class);
        when(
                factory.getConnection(any(SecurityCallback.class), eq("/auth/token/tokenID/resource/" 
                        + URLEncoder.encode(sb.substring(0, sb.length() - 1), "UTF-8")), eq("GET"))).thenReturn(
                                connection);
        when(connection.getResponseCode()).thenReturn(200);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        PermissionsInfo pInfo = new PermissionsInfo("resource", permissions);
        JAXB.marshal(pInfo, out);
        InputStream stream = new ByteArrayInputStream(out.toByteArray());
        when(connection.getInputStream()).thenReturn(stream);

        InputStream stream2 = new ByteArrayInputStream("Error".getBytes(CHARSET));
        when(connection.getErrorStream()).thenReturn(stream2);

    }

    /**
     * Test the permission checking in the security facade.
     * 
     * @throws Exception on error
     */
    @Test
    public void testHasPermission() throws Exception {
        boolean p1 = facade.hasPermission("resource", "p1");
        assertTrue("Permission for p1 granted", p1);

        // HttpURLConnection connection =
        // factory.getConnection(facade.getDefaultSecurityCallback(),"primary/auth/token/tokenID/resource/p1","GET");
        // String ip = InetAddress.getLocalHost().getHostAddress();
        // verify(connection,times(1)).setRequestProperty(SecurityFacade.HEADER_IP, ip);

        boolean p2 = facade.hasPermission("resource", "p2");
        assertFalse("Permission for p2 denied", p2);
    }

    /**
     * Tests checking of multiple permissions at once.
     * 
     * @throws Exception on error
     */
    @Test
    public void testHasPermissions() throws Exception {
        Map<String, Boolean> p1 = facade.hasPermissions("resource", "p1");
        assertTrue("Permission for p1 granted", p1.get("p1"));
        assertEquals("Size should be 1", 1, p1.size());

        // HttpURLConnection connection =
        // factory.getConnection(facade.getDefaultSecurityCallback(),"primary/auth/token/tokenID/resource/p1","GET");
        // String ip = InetAddress.getLocalHost().getHostAddress();
        // verify(connection,times(1)).setRequestProperty(SecurityFacade.HEADER_IP, ip);

        p1 = facade.hasPermissions("resource", "p2");
        assertFalse("Permission for p2 denied", p1.get("p2"));
        assertEquals("Size should be 1", 1, p1.size());

        p1 = facade.hasPermissions("resource", "p1", "p2");
        assertTrue("Permission for p1 granted", p1.get("p1"));
        assertFalse("Permission for p2 denied", p1.get("p2"));
        assertEquals("Size should be 2", 2, p1.size());
    }

    /**
     * Test response when providing invalid parameters.
     * 
     * @throws Exception on error
     */
    @Test
    public void testInvalid() throws Exception {
        try {
            facade.hasPermission("", "p1");
            fail("Exception should occur");
        } catch (IllegalArgumentException e) {
            assertEquals("Exception expected", "Parameter 'resource' must not be null or empty.", e.getMessage());
        }

        try {
            facade.hasPermission(null, "p1");
            fail("Exception should occur");
        } catch (IllegalArgumentException e) {
            assertEquals("Exception expected", "Parameter 'resource' must not be null or empty.", e.getMessage());
        }

        try {
            facade.hasPermission("resource", "");
            fail("Exception should occur");
        } catch (IllegalArgumentException e) {
            assertEquals("Exception expected", "Parameter 'permission' must not be null or empty.", e.getMessage());
        }

        facade.logout();

        try {
            facade.hasPermissions("resource", "p1");
            fail("Exception should occur");
        } catch (SecurityFacadeException e) {
            assertEquals("Exception expected", "RBAC authentication token is missing.", e.getMessage());
        }
    }

    /**
     * Tests behaviour when the service returns wrong answer or is not available.
     * 
     * @throws Exception on error
     */
    @Test
    public void testReponseFailure() throws Exception {
        HttpURLConnection connection = factory.getConnection(facade.getDefaultSecurityCallback(),
                "/auth/token/tokenID/resource/p1", "GET");
        when(connection.getResponseCode()).thenReturn(400);
        try {
            facade.hasPermission("resource", "p1");
            fail("Exception should occur");
        } catch (SecurityFacadeException e) {
            assertEquals("Message should match", "Service responded unexpectedly: Error", e.getMessage());
        }

        connection.getErrorStream().reset();
        when(connection.getInputStream()).thenReturn(null);
        try {
            facade.hasPermission("resource", "p1");
            fail("Exception should occur");
        } catch (SecurityFacadeException e) {
            assertEquals("Message should match", "Service responded unexpectedly: Error", e.getMessage());
        }
    }
}
