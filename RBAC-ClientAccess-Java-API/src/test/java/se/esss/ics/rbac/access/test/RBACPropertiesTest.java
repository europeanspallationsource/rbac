/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.Field;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import se.esss.ics.rbac.access.RBACProperties;

/**
 * 
 * <code>RBACPropertiesTest</code> test the methods of the {@link RBACProperties} class.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class RBACPropertiesTest {

    @Before
    public void setUp() throws Exception {
        destroy();
    }

    @After
    public void tearDown() throws Exception {
        destroy();
    }

    private static void destroy() throws Exception {
        // destroy the singleton instance
        Field f = RBACProperties.class.getDeclaredField("instance");
        f.setAccessible(true);
        f.set(null, null);

        System.clearProperty(RBACProperties.KEY_CERTIFICATE_STORE);
        System.clearProperty(RBACProperties.KEY_INACTIVITY_RESPONSE_GRACE_PERIOD);
        System.clearProperty(RBACProperties.KEY_INACTIVITY_TIMEOUT_DEFAULT);
        System.clearProperty(RBACProperties.KEY_PRIMARY_SERVICES_URL);
        System.clearProperty(RBACProperties.KEY_PRIMARY_SSL_HOST);
        System.clearProperty(RBACProperties.KEY_PRIMARY_SSL_PORT);
        System.clearProperty(RBACProperties.KEY_SECONDARY_SERVICES_URL);
        System.clearProperty(RBACProperties.KEY_SECONDARY_SSL_HOST);
        System.clearProperty(RBACProperties.KEY_SECONDARY_SSL_PORT);
        System.clearProperty(RBACProperties.KEY_VERIFY_SIGNATURE);
        System.clearProperty(RBACProperties.KEY_PUBLICK_KEY_LOCATION);
        System.clearProperty(RBACProperties.KEY_SHOW_ROLE_SELECTOR);
    }

    /**
     * Test construction of the singleton instance.
     */
    @Test
    public void testConstruction() {
        RBACProperties properties = RBACProperties.getInstance();
        assertNotNull(properties);
        RBACProperties properties2 = RBACProperties.getInstance();
        assertSame("getInstance should return the same object every time", properties, properties2);
    }

    /**
     * Test default parameters loaded from the file.
     */
    @Test
    public void testLoadingFromFile() {
        RBACProperties props = RBACProperties.getInstance();
        assertNull("No default certificate store defined", props.getCertificateStore());
        assertEquals("Default inactivity timeout is 900 seconds", 900, props.getInactivityDefaultTimeout());
        assertEquals("Inactivity response period is 30 seconds", 30, props.getInactivityResponseGracePeriod());
        assertNotNull("Primary service should be defined", props.getPrimaryServicesBaseURL());
        assertNotNull("Primary host should be defined", props.getPrimarySSLHostname());
        try {
            props.getPrimarySSLPort();
        } catch (NumberFormatException e) {
            fail("Primary port should be defined");
        }
        assertNotNull("Secondary service should be defined", props.getSecondaryServicesBaseURL());
        assertNotNull("Secondary host should be defined", props.getSecondarySSLHostname());
        try {
            props.getSecondarySSLPort();
        } catch (NumberFormatException e) {
            fail("Secondary port should be defined");
        }

        assertNotNull("Public key path should be defined", props.getPublicKeyLocation());
    }

    /**
     * Test overridden parameters through system properties.
     * 
     * @throws Exception on error
     */
    @Test
    public void testOverride() throws Exception {
        System.setProperty(RBACProperties.KEY_CERTIFICATE_STORE, "mystore");
        System.setProperty(RBACProperties.KEY_INACTIVITY_RESPONSE_GRACE_PERIOD, "100");
        System.setProperty(RBACProperties.KEY_INACTIVITY_TIMEOUT_DEFAULT, "1500");
        System.setProperty(RBACProperties.KEY_PRIMARY_SERVICES_URL, "primaryService");
        System.setProperty(RBACProperties.KEY_PRIMARY_SSL_HOST, "primaryHost");
        System.setProperty(RBACProperties.KEY_PRIMARY_SSL_PORT, "8888");
        System.setProperty(RBACProperties.KEY_SECONDARY_SERVICES_URL, "secondaryService");
        System.setProperty(RBACProperties.KEY_SECONDARY_SSL_HOST, "secondaryHost");
        System.setProperty(RBACProperties.KEY_SECONDARY_SSL_PORT, "4444");
        System.setProperty(RBACProperties.KEY_PUBLICK_KEY_LOCATION, "myLoc");
        System.setProperty(RBACProperties.KEY_VERIFY_SIGNATURE, "true");

        RBACProperties props = RBACProperties.getInstance();
        assertEquals("Default certificate store should be mystore", "mystore", props.getCertificateStore());
        assertEquals("Default inactivity timeout is 1500 seconds", 1500, props.getInactivityDefaultTimeout());
        assertEquals("Inactivity response period is 100 seconds", 100, props.getInactivityResponseGracePeriod());
        assertEquals("Primary service should be defined", "primaryService", props.getPrimaryServicesBaseURL());
        assertEquals("Primary host should be defined", "primaryHost", props.getPrimarySSLHostname());
        assertEquals("Primary port should be defined", 8888, props.getPrimarySSLPort());
        assertEquals("Secondary service should be defined", "secondaryService", props.getSecondaryServicesBaseURL());
        assertEquals("Secondary host should be defined", "secondaryHost", props.getSecondarySSLHostname());
        assertEquals("Secondary port should be defined", 4444, props.getSecondarySSLPort());
        assertEquals("Public key location should be defined", "myLoc", props.getPublicKeyLocation());
        assertTrue("Signature validation should be on", props.isVerifySignature());
    }

    /**
     * Test wrong port parameters.
     * 
     * @throws Exception on error
     */
    @Test
    public void testWrongPort() throws Exception {
        System.setProperty(RBACProperties.KEY_PRIMARY_SSL_HOST, "host");
        System.setProperty(RBACProperties.KEY_PRIMARY_SSL_PORT, "foo");
        System.setProperty(RBACProperties.KEY_SECONDARY_SSL_HOST, "host");
        System.setProperty(RBACProperties.KEY_SECONDARY_SSL_PORT, "bar");
        RBACProperties props = RBACProperties.getInstance();
        try {
            props.getPrimarySSLPort();
            fail("Exception should occur, primary port number is not valid");
        } catch (NumberFormatException e) {
            assertEquals("For input string: \"foo\"", e.getMessage());
        }

        try {
            props.getSecondarySSLPort();
            fail("Exception should occur, secondary port number is not valid");
        } catch (NumberFormatException e) {
            assertEquals("For input string: \"bar\"", e.getMessage());
        }
    }
}
