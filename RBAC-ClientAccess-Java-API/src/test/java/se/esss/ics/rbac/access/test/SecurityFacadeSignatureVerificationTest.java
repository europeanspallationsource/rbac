/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.security.KeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;

import org.junit.Test;

import se.esss.ics.rbac.access.FacadeUtilities;
import se.esss.ics.rbac.access.RBACProperties;
import se.esss.ics.rbac.access.SecurityFacadeException;
import se.esss.ics.rbac.access.Token;
import se.esss.ics.rbac.jaxb.TokenInfo;
import se.esss.ics.rbac.jaxb.util.TokenUtil;

/**
 * 
 * <code>SecurityFacadeSignatureVerificationTest</code> verifies that the signature is properly verified for the tokens.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class SecurityFacadeSignatureVerificationTest extends AbstractSecurityFacadeBase {

    private PublicKey publicKey;
    private File noFile;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        init();
        KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance("RSA");
        keyGenerator.initialize(512, new SecureRandom());
        KeyPair keyPair = keyGenerator.genKeyPair();
        byte[] data = TokenUtil.generateTokenData(info);
        PrivateKey privateKey = keyPair.getPrivate();
        Signature signature = Signature.getInstance("SHA1withRSA");
        signature.initSign(privateKey);
        signature.update(data);
        info.setSignature(signature.sign());
        publicKey = keyPair.getPublic();

        Field f = FacadeUtilities.class.getDeclaredField("publicKey");
        f.setAccessible(true);
        f.set(FacadeUtilities.getInstance(), null);

        System.setProperty(RBACProperties.KEY_VERIFY_SIGNATURE, "true");
        noFile = new File("rbac.key");
        noFile.delete();
        System.setProperty(RBACProperties.KEY_PUBLICK_KEY_LOCATION, noFile.getAbsolutePath());
        setUpForAuthentication();
    }

    @Override
    public void tearDown() throws Exception {
        noFile.delete();
    }

    /**
     * Test if verification is successful if proper key and token is used.
     * 
     * @throws Exception on error
     */
    @Test
    public void testSignatureVerification() throws Exception {
        Constructor<Token> c = Token.class.getDeclaredConstructor(TokenInfo.class);
        c.setAccessible(true);
        Token token = c.newInstance(info);
        boolean verified = FacadeUtilities.verifySignature(publicKey, token);
        assertTrue("Verification should be successful", verified);

        long time = info.getExpirationTime();
        info.setExpirationTime(2132132321L);
        token = c.newInstance(info);
        verified = FacadeUtilities.verifySignature(publicKey, token);
        assertTrue("Verification should be successful", verified);
        info.setExpirationTime(time);
    }

    /**
     * Test if verification fails if the token data is changed.
     * 
     * @throws Exception on error
     */
    @Test
    public void testSignatureVerificationChangedToken() throws Exception {
        Constructor<Token> c = Token.class.getDeclaredConstructor(TokenInfo.class);
        c.setAccessible(true);
        Token token = c.newInstance(info);
        long time = info.getCreationTime();
        info.setCreationTime(2132132321L);
        token = c.newInstance(info);
        boolean verified = FacadeUtilities.verifySignature(publicKey, token);
        assertFalse("Verification should fail", verified);
        info.setCreationTime(time);

        String username = info.getUserID();
        info.setUserID("someone");
        token = c.newInstance(info);
        verified = FacadeUtilities.verifySignature(publicKey, token);
        assertFalse("Verification should fail", verified);
        info.setUserID(username);

        String ip = info.getIp();
        info.setIp("someone");
        token = c.newInstance(info);
        verified = FacadeUtilities.verifySignature(publicKey, token);
        assertFalse("Verification should fail", verified);
        info.setIp(ip);

        Collection<String> roles = info.getRoleNames();
        info.setRoleNames(new ArrayList<String>());
        token = c.newInstance(info);
        verified = FacadeUtilities.verifySignature(publicKey, token);
        assertFalse("Verification should fail", verified);
        info.setRoleNames(roles);

        info.setSignature(new byte[64]);
        token = c.newInstance(info);
        verified = FacadeUtilities.verifySignature(publicKey, token);
        assertFalse("Verification should fail", verified);
    }

    /**
     * Tests if authentication fails if the verification of the token was not possible.
     * 
     * @throws Exception on error
     */
    @Test
    public void testAuthenticationWithVerificationFailed() throws Exception {
        try {
            facade.authenticate();
            fail("Exception should occur.");
        } catch (SecurityFacadeException e) {
            assertEquals("Exception expected", "Token signature verification failed.", e.getMessage());
        }

        try (FileOutputStream pw = new FileOutputStream(noFile)) {
            pw.write("dhdsfdsiufdgyufguia".getBytes());
        }
        setUpForAuthentication();
        try {
            facade.authenticate();
            fail("Exception should occur.");
        } catch (SecurityFacadeException e) {
            assertEquals("Exception expected", "Token signature verification failed.", e.getMessage());
        }

        KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance("RSA");
        keyGenerator.initialize(512, new SecureRandom());
        KeyPair keyPair = keyGenerator.genKeyPair();
        try (FileOutputStream pw = new FileOutputStream(noFile)) {
            X509EncodedKeySpec spec = new X509EncodedKeySpec(keyPair.getPublic().getEncoded());
            pw.write(spec.getEncoded());
        }
        setUpForAuthentication();
        try {
            facade.authenticate();
            fail("Exception should occur.");
        } catch (SecurityFacadeException e) {
            assertEquals("Exception expected", "Token signature verification failed.", e.getMessage());
        }
    }

    /**
     * Test if authentication fails if the key location is unknown.
     * 
     * @throws Exception on error
     */
    @Test
    public void testAuthenticationWithoutKey() throws Exception {
        System.clearProperty(RBACProperties.KEY_PUBLICK_KEY_LOCATION);
        RBACProperties prop = RBACProperties.getInstance();
        Field f = RBACProperties.class.getDeclaredField("properties");
        f.setAccessible(true);
        ((Properties) f.get(prop)).remove(RBACProperties.KEY_PUBLICK_KEY_LOCATION);
        setUpForAuthentication();
        try {
            facade.authenticate();
        } catch (SecurityFacadeException e) {
            assertEquals("Exception expected", "Token signature verification failed.", e.getMessage());
            assertEquals("Cause of exception is key", KeyException.class, e.getCause().getClass());
            assertEquals("Cause message should match", "Public Key location is not available.", e.getCause()
                    .getMessage());
        }
    }

    /**
     * Test if authentication is successful, if verification succeeds.
     * 
     * @throws Exception on error
     */
    @Test
    public void testAuthenticationWithVerificationSuccessful() throws Exception {
        try (FileOutputStream pw = new FileOutputStream(noFile)) {
            X509EncodedKeySpec spec = new X509EncodedKeySpec(publicKey.getEncoded());
            pw.write(spec.getEncoded());
        }
        Token token = facade.authenticate();
        assertNotNull("Authentication successful", token);
    }
}
