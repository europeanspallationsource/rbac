/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.security.KeyException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.X509EncodedKeySpec;

import se.esss.ics.rbac.jaxb.util.TokenUtil;

/**
 * 
 * <code>FacadeUtilities</code> provides a set of static methods that can be used to extract error messages from HTTP
 * streams or to check data received from the service.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public final class FacadeUtilities {

    /** Return code for OK */
    public static final int OK = 200;
    /** Return code for resource created */
    public static final int CREATED = 201;
    /** Return code for resource deleted */
    public static final int DELETED = 204;
    /** Return code for resource found */
    public static final int FOUND = 302;
    /** Return code for bad request (e.g. invalid parameters) */
    public static final int BAD_REQUEST = 400;
    /** Return code for failed authentication */
    public static final int UNAUTHORIZED = 401;
    /** Return code for forbidden access (not authenticated or not authorised) */
    public static final int FORBIDDEN = 403;
    /** Return code for resource not found */
    public static final int NOT_FOUND = 404;
    /** Return code for resource that is gone - token has been removed */
    public static final int GONE = 410;
    /** Server error code */
    public static final int ERROR = 500;
    /** Irrelevant response code */
    public static final int IRRELEVANT = 0;

    // it has the be spelled with z, to follow the standards, which are American
    /** The name of the authorisation header */
    public static final String HEADER_AUTHORISATION = "Authorization"; 
    /** The name of the duration header */
    public static final String HEADER_EXCLUSIVE_DURATION = "RBACDuration";
    /** The name of the IP address header */
    public static final String HEADER_IP = "RBACAddress";
    /** The name of the port header */
    public static final String HEADER_PORT = "RBACAddressPort";
    /** The name of the roles header */
    public static final String HEADER_ROLES = "RBACRoles";
    /** The name of the current token part header */
    public static final String HEADER_TOKEN_PART = "RBACTokenPart";

    /** The identifier for the HTTP GET method */
    public static final String GET = "GET";
    /** The identifier for the HTTP POST method */
    public static final String POST = "POST";
    /** The identifier for the HTTP DELETE method */
    public static final String DELETE = "DELETE";

    /**
     * The message prefix for all exceptions where the service responded, but the response was not OK or not as
     * expected.
     */
    public static final String INCORRECT_RESPONSE_MESSAGE = "Service responded unexpectedly: ";
    /** The message for all exceptions related to signature verification */
    public static final String VERIFICATION_FAILED_MESSAGE = "Token signature verification failed.";
    /**
     * The message prefix for all exceptions where the service responded with an error or did not respond at all.
     */
    public static final String ERROR_RESPONSE_MESSAGE = "Service response error: ";
    /** Message used when the public key location is not available */
    public static final String PUBLIC_KEY_LOCATION_UNAVAILABLE = "Public Key location is not available.";
    /** Message used when the public key could not be created */
    public static final String KEY_UNAVAILABLE = "Key could not be loaded.";
    /** Message when the response message could not be read due to an IO error */
    public static final String ERROR_READING_RESPONSE = "Error while reading error response.";
    /** Message when the connection to the service failed */
    public static final String ERROR_CONNECTING_TO_SERVICE = "Error while creating a connection to service. ";

    /** The character encoding type */
    public static final String CHAR_ENCODING = "UTF-8";
    /** The character set used for encoding all characters into bytes */
    public static final Charset CHARSET = Charset.forName(CHAR_ENCODING);
    /** The signature algorithm name used for verifying the token signature */
    public static final String SIGNATURE_ALGORITHM_NAME = "SHA1withRSA";
    /** The algorithm for the key factory */
    public static final String KEY_FACTORY_ALGORITHM = "RSA";

    private static final String HTTP_STATUS = "HTTP Status";
    private static final String DESCRIPTION = "description";
    private static final String HTML_SPLIT_REGEX = "<[/!]?[a-zA-Z0-9 \"=]+>";
    private static final String HTML_TAG = "<html>";
    private static final String HTML_404_NOT_FOUND_RESPONSE = "<html><head><title>Error</title></head>"
            + "<body>404 - Not Found</body></html>";

    private PublicKey publicKey;

    private static final FacadeUtilities INSTANCE = new FacadeUtilities();

    /**
     * Returns the singleton instance of this facilities.
     * 
     * @return the singleton instance
     */
    public static FacadeUtilities getInstance() {
        return INSTANCE;
    }

    private FacadeUtilities() {
    }

    /**
     * Verifies the signature in the token. If the verification is turned off, the method will return true. If the
     * verification is on the public key will be loaded and used to verify the signature of the token. If the
     * verification is on and key is not available, method throws an exception.
     * 
     * @param token the token to verify
     * @return true if the token signature is OK or false if the content and signature do not match
     * @throws GeneralSecurityException if the public key could not be loaded or if there was an error during signature
     *             verification
     */
    public boolean verifySignature(Token token) throws GeneralSecurityException {
        if (RBACProperties.getInstance().isVerifySignature()) {
            PublicKey key = getPublicKey();
            if (key == null) {
                throw new KeyException(PUBLIC_KEY_LOCATION_UNAVAILABLE);
            } else {
                return verifySignature(key, token);
            }
        }
        return true;
    }

    /**
     * Reads the public key from the location on the hard drive.
     * 
     * @return the public key
     * @throws KeyException if the file from which to load the key does not exist. if the file could not be read, if key
     *             factory for {@value #KEY_FACTORY_ALGORITHM} could not be created, or if the key data was corrupted
     *             and key could not be created
     */
    public PublicKey getPublicKey() throws KeyException {
        if (publicKey == null) {
            String loc = RBACProperties.getInstance().getPublicKeyLocation();
            if (loc == null) {
                return null;
            }
            File pubFile = new File(loc);
            try (FileInputStream pubFIS = new FileInputStream(pubFile)) {
                byte[] pubEncodedKey = new byte[(int) pubFile.length()];
                pubFIS.read(pubEncodedKey);
                KeyFactory keyFactory = KeyFactory.getInstance(FacadeUtilities.KEY_FACTORY_ALGORITHM);
                publicKey = keyFactory.generatePublic(new X509EncodedKeySpec(pubEncodedKey));
            } catch (GeneralSecurityException | IOException e) {
                throw new KeyException(KEY_UNAVAILABLE, e);
            }
        }
        return publicKey;
    }

    /**
     * Verify that the signature in the token and the provided public key match.
     * 
     * @param key the public key used to verify the signature
     * @param token the token to verify
     * @return true if the signature is OK or false if there was a mismatch
     * @throws GeneralSecurityException if there was an error during verification
     */
    public static boolean verifySignature(PublicKey key, Token token) throws GeneralSecurityException {
        byte[] data = TokenUtil.generateTokenData(token.getUsername(), token.getFirstName(), token.getLastName(),
                token.getRoles(), token.getIP(), token.getCreationDate());
        Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM_NAME);
        signature.initVerify(key);
        signature.update(data);
        return signature.verify(token.getRBACSignature());
    }

    /**
     * Checks if the array is null or if any of the elements in the array are null or empty. If yes, the method returns
     * false, otherwise it returns true.
     * 
     * @param array the array to check
     * @return true if the array is OK or false otherwise
     */
    static boolean isArrayValid(String[] array) {
        if (array == null || array.length == 0) {
            return false;
        }
        for (int i = 0; i < array.length; i++) {
            if (array[i] == null || array[i].isEmpty()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Verifies the signature in the token. If the signature and the data do not match, an exception is thrown.
     * 
     * @param token the token to verify
     * @throws SecurityFacadeException if the signature and data do not match
     */
    static void verifySignatureInternal(Token token) throws SecurityFacadeException {
        try {
            if (!FacadeUtilities.getInstance().verifySignature(token)) {
                throw new SecurityFacadeException(VERIFICATION_FAILED_MESSAGE, IRRELEVANT);
            }
        } catch (GeneralSecurityException e) {
            throw new SecurityFacadeException(VERIFICATION_FAILED_MESSAGE, e, IRRELEVANT);
        }
    }

    /**
     * Constructs a {@link SecurityFacadeException} that contains a more precise cause for why the received HTTP
     * response from the services was not as expected.
     * If primary exception is relevant to the cause it will be included as the cause.
     * 
     * @param connection used for connecting to the RBAC services.
     * @param primaryException exception caught beforehand, that may be relevant to the cause.
     * @param <T> the type of return value 
     * 
     * @return nothing, method always throws an exception
     * 
     * @throws SecurityFacadeException containing a more precise cause for why the received HTTP response from
     *            the services was not as expected.
     */
    public static <T> T throwConnectionException(HttpURLConnection connection, Exception primaryException)
            throws SecurityFacadeException {
        if (connection != null && connection.getErrorStream() != null) {
            try {
                throw new SecurityFacadeException(ERROR_RESPONSE_MESSAGE
                        + readResponseMessage(connection.getErrorStream()), primaryException, 
                        IRRELEVANT);
            } catch (IOException e) {
                throw new SecurityFacadeException(ERROR_READING_RESPONSE, e, IRRELEVANT);
            }
        }
        throw new SecurityFacadeException(ERROR_CONNECTING_TO_SERVICE 
                + primaryException.getMessage(), primaryException, IRRELEVANT);
    }

    /**
     * Checks the return code of the connection, reads the message from the error stream, and throws an appropriate
     * exception.
     * 
     * @param connection the connection to check
     * @param caller the facade that initiated this call
     * @param <T> the type of return value 
     * 
     * @return nothing, method always throws an exception
     * @throws SecurityFacadeException generated exception, based on the return code and error stream of the connection
     * @throws IOException if handling of the stream failed
     */
    static <T> T throwNotOKResponseException(HttpURLConnection connection, ISecurityFacade caller) 
            throws SecurityFacadeException, IOException {
        InputStream stream = connection.getErrorStream();
        if (stream == null) {
            stream = connection.getInputStream();
        }
        String message = FacadeUtilities.INCORRECT_RESPONSE_MESSAGE
                + FacadeUtilities.readResponseMessage(stream);
        if (connection.getResponseCode() == GONE) {
            //logout, because the token no longer exists
            caller.setToken(null);
            throw new AccessDeniedException(message);
        } else if (connection.getResponseCode() == UNAUTHORIZED) {
            throw new AuthenticationFailedException(message);
        } else {
            throw new SecurityFacadeException(message, connection.getResponseCode());
        }
    }

    /**
     * Reads the response message from the provided input stream. If the message is inside an HTML error report, the
     * message is extracted before being returned.
     * 
     * @param inputStream stream from which the message will be read.
     * 
     * @return response message read from the provided input stream.
     * 
     * @throws IOException if there is an error while reading the stream.
     */
    public static String readResponseMessage(InputStream inputStream) throws IOException {
        if (inputStream == null) {
            throw new IOException("InputStream was null.");
        }
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, CHARSET))) {
            StringBuilder messageBuilder = new StringBuilder();
            while (reader.ready()) {
                messageBuilder.append(reader.readLine()).append('\n');
            }
            String message = messageBuilder.toString().trim();
            // Wildfly workaround: The response 404 - Not Found is generated when a non existing
            // url is accessed on the server. 404 returned by the server have a different body.
            if (HTML_404_NOT_FOUND_RESPONSE.equalsIgnoreCase(message)) {
                throw new FileNotFoundException("404 - Not Found");
            }
            if (message.contains(HTML_TAG)) {
                message = parseErrorFromHTML(message);
            }
            return message.trim();
        }
    }

    /**
     * Parses the HTML error report and extracts information about the error. The parsing procedure is adapted
     * specifically for the HTML error reports.
     * 
     * @param htmlString HTML error report.
     * 
     * @return extracted error message; HTTP response code and error description.
     */
    private static String parseErrorFromHTML(String htmlString) {
        StringBuilder errorBuilder = new StringBuilder();
        // Split by possible HTML tags in an error report.
        String[] content = htmlString.split(HTML_SPLIT_REGEX);
        boolean descriptionIsNext = false;
        for (String part : content) {
            // Skip empty space.
            if (!part.trim().isEmpty()) {
                if (descriptionIsNext) {
                    errorBuilder.append(part);
                    break;
                }
                if (part.startsWith(HTTP_STATUS)) {
                    errorBuilder.append(part).append(": ");
                } else if (DESCRIPTION.equals(part)) {
                    descriptionIsNext = true;
                }
            }
        }
        return errorBuilder.toString();
    }

    /**
     * Verifies if any of the given parameter is null or empty. If yes, an exception is thrown.
     * 
     * @param token the token
     * @param resource the resource name
     * @param permissions the permission names, all entries in the array are checked
     * @throws SecurityFacadeException if the token ID is null
     * @throws IllegalArgumentException if any of the parameters is null or empty
     */
    static void verifyParameters(Token token, String resource, String... permissions) 
            throws SecurityFacadeException, IllegalArgumentException {
        if (token == null) {
            throw new SecurityFacadeException("RBAC authentication token is missing.", IRRELEVANT);
        } else if (!FacadeUtilities.isArrayValid(permissions)) {
            throw new IllegalArgumentException("Parameter 'permission' must not be null or empty.");
        } else if (resource == null || resource.isEmpty()) {
            throw new IllegalArgumentException("Parameter 'resource' must not be null or empty.");
        } else if (resource.indexOf(',') > -1) {
            throw new IllegalArgumentException("Resource name '" + resource + "' contains an illegal character ','.");
        } else {
            for (String s : permissions) {
                if (s.indexOf(',') > -1) {
                    throw new IllegalArgumentException("Permission '" + s + "' contains an illegal character ','.");
                }
            }
        }
    }
}
