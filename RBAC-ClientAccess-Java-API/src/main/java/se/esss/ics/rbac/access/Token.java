/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

import se.esss.ics.rbac.jaxb.TokenInfo;

/**
 * Token contains the information, which is issued by the RBAC upon successful login. This information is:
 * <ul>
 * <li>unique ID of the token,</li>
 * <li>username of the user to whom this token has been issued,</li>
 * <li>first name of the user to whom this token has been issued,</li>
 * <li>last name of the user to whom this token has been issued,</li>
 * <li>names of roles assigned to the user,</li>
 * <li>creation time of the token,</li>
 * <li>expiration time of the token,</li>
 * <li>IP of the client for which the token has been issued and</li>
 * <li>encrypted signature of the token data.</li>
 * </ul>
 * 
 * @author <a href="mailto:jakob.battelino@cosylab.com">Jakob Battelino Prelog</a>
 */
public final class Token implements Serializable {

    private static final long serialVersionUID = -2500039682650326527L;

    private final char[] tokenID;
    private final String username;
    private final String firstName;
    private final String lastName;
    private final String[] roles;
    private final Date creationDate;
    private final Date expirationDate;
    private final String ip;
    private final byte[] rbacSignature;

    /**
     * Constructs a new token with fields set to the provided values.
     * 
     * @param tokenID the unique ID of this token.
     * @param username of the user whom this token has been issued to.
     * @param firstName of the user whom this token has been issued to.
     * @param lastName of the user whom this token has been issued to.
     * @param roles for which this token is valid.
     * @param creationDate time of creation of this token.
     * @param expirationDate time of expiration of this token.
     * @param ip of the client for which this token has been issued.
     * @param rbacSignature encrypted signature of the token's data.
     */
    public Token(char[] tokenID, String username, String firstName, String lastName, String[] roles, Date creationDate,
            Date expirationDate, String ip, byte[] rbacSignature) {
        this.tokenID = Arrays.copyOf(tokenID, tokenID.length);
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.roles = roles == null ? null : Arrays.copyOf(roles, roles.length);
        if (this.roles != null) {
            Arrays.sort(this.roles);
        }
        this.expirationDate = expirationDate == null ? null : (Date) expirationDate.clone();
        this.creationDate = creationDate == null ? null : (Date) creationDate.clone();
        this.ip = ip;
        this.rbacSignature = rbacSignature == null ? null : Arrays.copyOf(rbacSignature, rbacSignature.length);
    }

    /**
     * Constructs a new Token from the JAXB info.
     * 
     * @param info the source of data for this token.
     */
    public Token(TokenInfo info) {
        this(info.getTokenID().toCharArray(), info.getUserID(), info.getFirstName(), info.getLastName(), info
                .getRoleNames().toArray(new String[info.getRoleNames().size()]), new Date(info.getCreationTime()),
                new Date(info.getExpirationTime()), info.getIp(), info.getSignature());
    }

    /**
     * Constructs a new token with the specified tokenID. Other fields are set to <code>null</code>.
     * 
     * @param tokenID the unique ID of this token.
     */
    public Token(char[] tokenID) {
        this(tokenID, null, null, null, null, null, null, null, null);
    }

    /**
     * Returns the unique ID of this token.
     * 
     * @return the unique ID of this token.
     */
    public char[] getTokenID() {
        return Arrays.copyOf(tokenID, tokenID.length);
    }

    /**
     * Returns username of the user whom this token has been issued to.
     * 
     * @return username of the user whom this token has been issued to.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Returns first name of the user whom this token has been issued to.
     * 
     * @return first name of the user whom this token has been issued to.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Returns last name of the user whom this token has been issued to.
     * 
     * @return last name of the user whom this token has been issued to.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Returns array of role names for which this token is valid.
     * 
     * @return role names for which this token is valid.
     */
    public String[] getRoles() {
        if (roles == null) {
            return new String[0];
        }
        return Arrays.copyOf(roles, roles.length);
    }

    /**
     * Returns time of creation of this token.
     * 
     * @return time of creation of this token.
     */
    public Date getCreationDate() {
        if (creationDate == null) {
            return null;
        }
        return (Date) creationDate.clone();
    }

    /**
     * Returns time of expiration of this token.
     * 
     * @return time of expiration of this token.
     */
    public Date getExpirationDate() {
        if (expirationDate == null) {
            return null;
        }
        return (Date) expirationDate.clone();
    }

    /**
     * Returns IP of the client for which this token has been issued.
     * 
     * @return IP of the client for which this token has been issued.
     */
    public String getIP() {
        return ip;
    }

    /**
     * Returns encrypted signature of the token's data.
     * 
     * @return encrypted signature of the token's data.
     */
    public byte[] getRBACSignature() {
        if (rbacSignature == null) {
            return new byte[0];
        }
        return Arrays.copyOf(rbacSignature, rbacSignature.length);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Token:" + "\n  Username: " + username + "\n  First Name: " + firstName + "\n  Last Name: " + lastName
                + "\n  Roles: " + Arrays.toString(roles) + "\n  IP: " + ip + "\n  Creation Date: " + creationDate
                + "\n  Expiration Date: " + expirationDate + "\n  Token ID: " + new String(tokenID);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        int result = Objects.hash(creationDate, expirationDate, firstName, ip, lastName, username);
        int prime = 31;
        result = prime * result + Arrays.hashCode(rbacSignature);
        result = prime * result + Arrays.hashCode(roles);
        result = prime * result + Arrays.hashCode(tokenID);
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (getClass() != obj.getClass()) {
            return false;
        }
        Token other = (Token) obj;
        return Objects.equals(creationDate, other.creationDate) && Objects.equals(expirationDate, other.expirationDate)
                && Objects.equals(firstName, other.firstName) && Objects.equals(lastName, other.lastName)
                && Objects.equals(ip, other.ip) && Objects.equals(username, other.username)
                && Arrays.equals(rbacSignature, other.rbacSignature) && Arrays.equals(roles, other.roles)
                && Arrays.equals(tokenID, other.tokenID);
    }
}
