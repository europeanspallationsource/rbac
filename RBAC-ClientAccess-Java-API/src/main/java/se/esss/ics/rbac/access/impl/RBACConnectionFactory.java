/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.esss.ics.rbac.access.ConnectionException;
import se.esss.ics.rbac.access.IConnectionFactory;
import se.esss.ics.rbac.access.RBACProperties;
import se.esss.ics.rbac.access.SecurityCallback;

/**
 * <code>RBACConnectionFactory</code> is a factory for creating HTTPS connections to RBAC authentication services. Upon
 * creation the factory performs a SSL handshake with the RBAC server, retrieves the server's certificate and stores it
 * into key store on the default path (if it is not yet trusted). The returned connections are set to only verify
 * connections to RBAC server.
 * <p>
 * RBAC connection factory is implemented as a singleton; use {@link #getInstance()} to get an instance of the factory.
 * </p>
 *
 * @author <a href="mailto:jakob.battelino@cosylab.com">Jakob Battelino Prelog</a>
 */
public final class RBACConnectionFactory implements IConnectionFactory {

    private static final long serialVersionUID = 5251292517040961407L;

    // There is no need to guard any of the logging statements, because all of them happen
    // once under normal conditions. If they happen more than once, the service is not available
    // and in that case creating unnecessary strings is the least of our concerns
    private static final transient Logger LOGGER = LoggerFactory.getLogger(RBACConnectionFactory.class);

    private static final String LIB_FOLDER = "lib";
    private static final String SECURITY_FOLDER = "security";
    private static final String CA_CERTIFICATES = "cacerts";
    private static final String JSSE_CA_CERTIFICATES = "jssecacerts";
    private static final String JAVA_HOME = "java.home";
    private static final String URL_PROTOCOL_HTTP = "http";
    private static final String URL_PROTOCOL_HTTPS = "https";

    private static final RBACConnectionFactory INSTANCE = new RBACConnectionFactory();

    private File certificateStore;

    private boolean handshakeCompleted = false;
    private Service service = Service.PRIMARY;

    private RBACConnectionFactory() {
    }

    /**
     * Returns the singleton instance of {@link RBACConnectionFactory}.
     *
     * @return singleton instance of RBAC connection factory.
     */
    public static RBACConnectionFactory getInstance() {
        return INSTANCE;
    }

    private void prepare() throws IOException {
        File store = getCertificateStore();
        System.setProperty("javax.net.ssl.keyStore", store.getAbsolutePath());
        System.setProperty("javax.net.ssl.trustStore", store.getAbsolutePath());
    }

    private boolean handshakeWithService(SecurityCallback callback, Service service) throws ConnectionException {
        if (!RBACProperties.getInstance().isPerformHandshake()) {
            return true;
        }
        String host = service == Service.PRIMARY ? RBACProperties.getInstance().getPrimarySSLHostname()
                : RBACProperties.getInstance().getSecondarySSLHostname();
        int port = service == Service.PRIMARY ? RBACProperties.getInstance().getPrimarySSLPort() : RBACProperties
                .getInstance().getSecondarySSLPort();
        try {
            prepare();
            boolean completed = handShakeAndInstallKey(host, port, null, getCertificateStore(), callback);
            if (completed) {
                LOGGER.info("Handshake completed on " + service.name().toLowerCase() + " RBAC service on host " + host
                        + ":" + port + ".");
            } else {
                LOGGER.info(service + " RBAC service on host " + host + ":" + port + " rejected.");
            }
            return completed;
        } catch (Exception e) {
            throw new ConnectionException("Could not perform SSL handshake with " + service.name().toLowerCase()
                    + " host.", e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.IConnectionFactory#getConnection(se.esss.ics.rbac.access. SecurityCallback,
     * java.lang.String, java.lang.String)
     */
    @Override
    public HttpURLConnection getConnection(SecurityCallback callback, String urlString, String requestMethod)
            throws ConnectionException {
        return getConnection(callback, urlString, requestMethod, null);
    }

    /*
     * (non-Javadoc)
     *
     * @see se..esss.ics.rbac.access.IConnectionFactory#getConnection(se..esss.ics.rbac.access. SecurityCallback,
     * java.lang.String, java.lang.String, java.util.Map)
     */
    @Override
    public synchronized HttpURLConnection getConnection(SecurityCallback callback, String urlString,
            String requestMethod, Map<String, String> requestProperties) throws ConnectionException {
        try {
            String urlBase = this.service == Service.PRIMARY ? RBACProperties.getInstance().getPrimaryServicesBaseURL()
                    : RBACProperties.getInstance().getSecondaryServicesBaseURL();
            URL url = new URL(urlBase + urlString);
            HttpURLConnection connection;
            switch (url.getProtocol()) {
                case URL_PROTOCOL_HTTP:
                    connection = (HttpURLConnection) url.openConnection();
                    break;
                case URL_PROTOCOL_HTTPS:
                    if (!handshakeCompleted) {
                        handshakeCompleted = handshakeWithService(callback, this.service);
                        if (!handshakeCompleted) {
                            throw new ConnectionException("Certificate issued by the service was rejected.");
                        }
                    }
                    prepare();
                    connection = (HttpsURLConnection) url.openConnection();
                    ((HttpsURLConnection)connection).setHostnameVerifier(getRBACVerifier());
                    break;
                default:
                    throw new ConnectionException("Unsupported url protocol: " + url.getProtocol() + ".");
            }
            if (requestMethod != null) {
                connection.setRequestMethod(requestMethod);
            }
            if (requestProperties != null) {
                for (Map.Entry<String, String> property : requestProperties.entrySet()) {
                    connection.setRequestProperty(property.getKey(), property.getValue());
                }
            }
            return connection;
        } catch (Exception e) {
            throw new ConnectionException("Cannot create connection to '" + urlString + "' with method '"
                    + requestMethod + "'.", e);
        }
    }

    /**
     * Returns the file into which the server certificate will be stored if it is not trusted. By default java trusts
     * only the certificates in cacerts and jssecacerts in the JRE_HOME. This file should contain all the certificates
     * that are trusted by this application and it should be at a location, which the current user has write access to
     * in order to be able to add new certificates if needed. Default location is .rbac folder within the user.home
     * folder.
     *
     * @return the file in which the certificate will be stored if it is not trusted.
     */
    private File getCertificateStore() throws IOException {
        if (certificateStore == null) {
            String loc = RBACProperties.getInstance().getCertificateStore();
            File file;
            if (loc == null || loc.trim().isEmpty()) {
                file = new File(System.getProperty("user.home"), ".rbac");
            } else {
                file = new File(loc);
            }
            if (!file.exists() && !file.mkdirs()) {
                throw new IOException("Cannot create the rbac key store.");
            }
            certificateStore = new File(file, "cert.keystore");
        }
        return certificateStore;
    }

    private HostnameVerifier getRBACVerifier() {
        final String rbacHostname = service == Service.PRIMARY ? RBACProperties.getInstance().getPrimarySSLHostname()
                : RBACProperties.getInstance().getSecondarySSLHostname();
        return new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return rbacHostname.equals(hostname);
            }
        };
    }

    /**
     * Performs an SSL handshake with the given server. If the handshake is completed without problem the method returns
     * true. If there is an error during the handshake, the server's certificate is retrieved and stored to the key
     * store on the default path. If the acceptance of the certificate was successful, the method returns true. If
     * acceptance was not successful the method returns false.
     *
     * @param host the host to handshake with
     * @param port the port of the host to connect to
     * @param password the pass phrase for the key store (can be null)
     * @param destination the destination file into which the new certificate should be stored
     * @param callback the security callback that is notified when the certificate needs to be confirmed
     *
     * @return true if handshake was successful or false if certificate cannot be accepted
     * @throws IOException if there was an error during certificate accept process
     * @throws FileNotFoundException if there was an error during certificate accept process
     * @throws KeyStoreException if there was an error during certificate accept process
     * @throws CertificateException if there was an error during certificate accept process
     * @throws NoSuchAlgorithmException if there was an error during certificate accept process
     * @throws KeyManagementException if there was an error during certificate accept process
     */
    private static boolean handShakeAndInstallKey(String host, int port, char[] password, File destination,
            SecurityCallback callback) throws FileNotFoundException, IOException, KeyStoreException,
            NoSuchAlgorithmException, CertificateException, KeyManagementException {
        char[] passphrase = password;
        if (passphrase == null) {
            passphrase = "changeit".toCharArray();
        }
        File file = getCertificateFile(destination);

        KeyStore ks = null;
        try (InputStream certInput = new FileInputStream(file)) {
            ks = KeyStore.getInstance(KeyStore.getDefaultType());
            ks.load(certInput, passphrase);
        }

        SSLContext context = SSLContext.getInstance("TLS");
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        tmf.init(ks);
        X509TrustManager defaultTrustManager = (X509TrustManager) tmf.getTrustManagers()[0];
        SavingTrustManager tm = new SavingTrustManager(defaultTrustManager);
        context.init(null, new TrustManager[] { tm }, null);
        SSLSocketFactory factory = context.getSocketFactory();

        try (SSLSocket socket = (SSLSocket) factory.createSocket()) {
            if (socket == null) {
                throw new SSLHandshakeException("Could not open socket to host '" + host + ":" + port + "'.");
            }
            socket.connect(new InetSocketAddress(host, port), RBACProperties.getInstance().getHandhsakeTimeout());
            if (!socket.isConnected()) {
                throw new SSLHandshakeException("Could not open socket to host '" + host + ":" + port + "'.");
            }
            socket.setSoTimeout(10000);
            socket.startHandshake();
            if (destination != null && !destination.exists()) {
                try (InputStream in = new FileInputStream(file); OutputStream out = new FileOutputStream(destination)) {
                    byte[] buf = new byte[1024];
                    int len;
                    while ((len = in.read(buf)) > 0) {
                        out.write(buf, 0, len);
                    }
                }
            }
            return true;
        } catch (SSLException e) {
            LOGGER.info("Certificate for '" + host + "' not found in the certificate store. Attempting to install it.");
            // SSL handshake not successful, certificate is not available
            // install certificate and store it to destination
        }

        X509Certificate[] chain = tm.chain;
        if (chain == null) {
            throw new SSLHandshakeException("Could not obtain server certificate chain for " + host + ":" + port + ".");
        }

        if (!callback.acceptCertificate(host, chain[0])) {
            return false;
        }

        int k = 0;
        X509Certificate cert = chain[k];
        String alias = host + "-" + (k + 1);
        ks.setCertificateEntry(alias, cert);

        File certOut = destination;
        if (certOut == null) {
            certOut = new File(file.getParentFile(), JSSE_CA_CERTIFICATES);
        }
        if (!certOut.exists() && !certOut.getParentFile().exists() && !certOut.getParentFile().mkdirs()) {
            throw new IOException("Cannot create the rbac key store at '" + certOut.getAbsolutePath() + "'.");
        }
        try (OutputStream out = new FileOutputStream(certOut)) {
            ks.store(out, passphrase);
        }

        return true;
    }

    private static File getCertificateFile(File suggestedFile) {
        File file = null;
        if (suggestedFile != null && suggestedFile.exists()) {
            // if destination already exist, it might already contain the necessary certificates
            // just check those
            file = suggestedFile;
        } else {
            // load the certificates from the default storage
            File dir = new File(System.getProperty(JAVA_HOME));
            dir = new File(dir, LIB_FOLDER);
            dir = new File(dir, SECURITY_FOLDER);
            file = new File(dir, JSSE_CA_CERTIFICATES);
            if (!file.exists()) {
                file = new File(dir, CA_CERTIFICATES);
            }
        }
        return file;
    }

    private static class SavingTrustManager implements X509TrustManager {

        private final X509TrustManager tm;
        private X509Certificate[] chain;

        SavingTrustManager(X509TrustManager tm) {
            this.tm = tm;
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
        }

        @Override
        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            throw new UnsupportedOperationException();
        }

        @Override
        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            this.chain = chain;
            tm.checkServerTrusted(chain, authType);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see se..esss.ics.rbac.access.IConnectionFactory#switchService()
     */
    @Override
    public synchronized Service switchService() {
        Service oldService = this.service;
        this.service = this.service == Service.PRIMARY ? Service.SECONDARY : Service.PRIMARY;
        handshakeCompleted = false;
        LOGGER.info("Switched from " + oldService + " to " + this.service);
        return this.service;
    }

    /*
     * (non-Javadoc)
     *
     * @see se..esss.ics.rbac.access.IConnectionFactory#getService()
     */
    @Override
    public Service getService() {
        return this.service;
    }
}
