/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access;

import java.util.EventListener;

/**
 * <code>SecurityFacadeListener</code> is notified about the changes in the logged in user.
 * 
 * A listener that has been registered with a <code>SecurityFacade</code> will receive a notification every time a user
 * is successfully logged in or logged out.
 * 
 * @author <a href="mailto:jakob.battelino@cosylab.com">Jakob Battelino Prelog</a>
 */
public interface SecurityFacadeListener extends EventListener {

    /**
     * Called when a user is logged in, providing the token of the logged in user as the parameter.
     * 
     * @param token authentication token received during login
     */
    void loggedIn(Token token);

    /**
     * Called when a user is logged out, providing token of the logged out user as the parameter.
     * 
     * @param token authentication token of the logged out user
     */
    void loggedOut(Token token);
}
