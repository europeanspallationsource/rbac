/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access;

/**
 * 
 * <code>ConnectionException</code> is an exception thrown by the {@link IConnectionFactory} when an error occurred
 * while creating connection.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class ConnectionException extends Exception {

    private static final long serialVersionUID = 2715288435970962859L;

    /**
     * Constructs a new exception.
     * 
     * @param message the message explaining the reason for exception
     */
    public ConnectionException(String message) {
        super(message);
    }

    /**
     * Constructs a new exception.
     * 
     * @param message the message explaining the reason for exception
     * @param cause the cause of the exception
     */
    public ConnectionException(String message, Throwable cause) {
        super(message, cause);
    }
}
