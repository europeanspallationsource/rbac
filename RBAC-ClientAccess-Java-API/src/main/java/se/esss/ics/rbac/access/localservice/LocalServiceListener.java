/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access.localservice;

import java.util.EventListener;

import se.esss.ics.rbac.access.Token;

/**
 * <code>LocalServiceListener</code> is notified about the changes on the local authentication service.
 * 
 * A listener that has been registered will receive a notification when token is saved or token is deleted from local
 * authentication service.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
public interface LocalServiceListener extends EventListener {

    /**
     * Called when token is saved (user is logged in) or token is deleted (user is logged out) form local authentication
     * service.
     * 
     * @param token
     *            authentication token received from local authentication service
     * @param isLoggedIn
     *            true if token is saved (user is logged in), if token is deleted (user is logged out) false
     */
    public void localServiceEvent(Token token, boolean isLoggedIn);
}
