/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.xml.bind.DataBindingException;
import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.JAXB;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.esss.ics.rbac.access.IConnectionFactory.Service;
import se.esss.ics.rbac.access.impl.RBACConnectionFactory;
import se.esss.ics.rbac.access.impl.RBACThreadFactory;
import se.esss.ics.rbac.access.localservice.LocalAuthServiceDetails;
import se.esss.ics.rbac.access.localservice.LocalServiceListener;
import se.esss.ics.rbac.access.localservice.LocalServiceProxy;
import se.esss.ics.rbac.jaxb.ExclusiveInfo;
import se.esss.ics.rbac.jaxb.PermissionsInfo;
import se.esss.ics.rbac.jaxb.RolesInfo;
import se.esss.ics.rbac.jaxb.TokenInfo;

/**
 * <code>SecurityFacade</code> is the main entry point for all actions. It accepts requests from the client application
 * and communicates with the web service to authenticate the user or to check his permissions.
 * <p>
 * Security facade provides sets of synchronous and asynchronous methods for retrieving information from RBAC
 * authentication and authorisation services. Asynchronous methods use {@link SecurityCallback} to report back the
 * results. Used callback can be set by calling {@link #setDefaultSecurityCallback(SecurityCallback)}.
 * </p>
 * <p>
 * To get the default instance of {@link ISecurityFacade} use {@link #getDefaultInstance()}.
 * </p>
 *
 * @author <a href="mailto:jakob.battelino@cosylab.com">Jakob Battelino Prelog</a>
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
public final class SecurityFacade implements ISecurityFacade {

    private static final long serialVersionUID = -7657682296833934173L;

    private static final Logger LOGGER = LoggerFactory.getLogger(SecurityFacade.class);
    private static final String NOTIFY_LISTENER_ERROR = "Error notifying the registered security facade listeners.";
    private static final String LOCALHOST_IP_UNKNOWN = "Localhost IP is unknown.";
    private static final String CANNOT_INSTANTIATE_FACADE = "Could not instantiate new instance of ISecurityFacade.";
    private static final String FETCHING_TOKEN_FAILED = "Fetching the token from local service failed.";
    private static final String LOGOUT_FAILED = "Logout failed. There is no token.";
    private static final String EXCLUSIVE_ACCESS_RELEASE_FAILED = "Exclusive access could not be released, "
            + "because it does not exist.";

    private static final String AUTH_TOKEN = "/auth/token/";

    private static final class DefaultAutoLogout extends AutoLogout {
        DefaultAutoLogout(ISecurityFacade facade) {
            super(facade);
        }

        @Override
        protected void unRegisterForInputEvents() {
            //nothing to do by default
        }

        @Override
        protected void registerForInputEvents() {
            //nothing to do by default
        }
    }

    private transient LocalServiceListener lsListener = new LocalServiceListener() {
        @Override
        public void localServiceEvent(Token token, boolean isLoggedIn) {
            try {
                if (isLoggedIn) {
                    if (setTokenInternalNotLocal(token)) {
                        fireLoggedIn(token);
                    }
                } else {
                    if (setTokenInternalNotLocal(null)) {
                        fireLoggedOut(token);
                    }
                }
            } catch (SecurityFacadeException e) {
                LOGGER.error(NOTIFY_LISTENER_ERROR, e);
            }
        }
    };

    private static final Map<String, String> NO_PARAMETERS_MAP = new HashMap<>(0);

    private static ISecurityFacade defaultInstance;

    private final IConnectionFactory connectionFactory;
    private transient ExecutorService executor;
    private transient SecurityCallback defaultSecurityCallback;
    private final transient List<SecurityFacadeListener> listeners = new CopyOnWriteArrayList<>();
    private final transient LocalServiceProxy localService = new LocalServiceProxy();
    private transient AutoLogout autoLogout;

    private boolean destroyed = false;
    private String localhostIP;
    private Token token;
    private char[] tokenID;
    private int autoLogoutTimeout = -1;

    private final Object mutex = new Object();

    /**
     * Returns the default instance of {@link ISecurityFacade}. The method will scan the registry if a service that
     * implements {@link ISecurityFacade} exists. If there is at least one implementation that is not this class, it
     * will return it. If there is none, it will return this class. If the default instance is destroyed, any consequent
     * call to this method will create a new default instance.
     *
     * @return the default instance of {@link ISecurityFacade}.
     */
    public static synchronized ISecurityFacade getDefaultInstance() {
        if (defaultInstance == null || defaultInstance.isDestroyed()) {
            ServiceLoader<ISecurityFacade> loader = ServiceLoader.load(ISecurityFacade.class);
            for (ISecurityFacade l : loader) {
                if (!(l instanceof SecurityFacade)) {
                    defaultInstance = l;
                    break;
                }
            }
            if (defaultInstance == null) {
                defaultInstance = new SecurityFacade();
            }
            defaultInstance.setAutoLogoutTimeout(RBACProperties.getInstance().getInactivityDefaultTimeout());
        }
        return defaultInstance;
    }

    /**
     * Constructs and returns a new instance of the security facade. The instance is of the same type as the default
     * instance. Be aware that calling this method will also create the default instance if it has not been created yet.
     *
     * @return new instance of the facade
     */
    public static ISecurityFacade newInstance() {
        Class<? extends ISecurityFacade> clazz = getDefaultInstance().getClass();
        try {
            return clazz.newInstance();
        } catch (Exception e) {
            LOGGER.error(CANNOT_INSTANTIATE_FACADE, e);
        }
        return new SecurityFacade();
    }

    /**
     * Construct new security facade using the {@link RBACConnectionFactory}.
     */
    public SecurityFacade() {
        this(RBACConnectionFactory.getInstance());
    }

    /**
     * Construct new security facade using the provided connection factory.
     *
     * @param factory the factory to use for creating connections
     */
    public SecurityFacade(IConnectionFactory factory) {
        this.connectionFactory = factory;
    }

    private void update() {
        synchronized (mutex) {
            if (autoLogout != null) {
                autoLogout.update();
            }
        }
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#authenticate()
     */
    @Override
    public Token authenticate() throws SecurityFacadeException {
        Credentials credentials = getDefaultSecurityCallback().getCredentials();
        return credentials == null ? null : authenticateInternally(credentials, getDefaultSecurityCallback());
    }

    private Token authenticateInternally(Credentials credentials, final SecurityCallback callback)
            throws SecurityFacadeException {
        update();
        Token aToken = authenticateLocally(callback.getLocalAuthServiceDetails());
        if (aToken != null) {
            return aToken;
        }
        Map<String, String> parameters = new LinkedHashMap<>(3);
        try {
            byte[] usernameData = credentials.getUsername().getBytes(FacadeUtilities.CHARSET);
            ByteBuffer buffer = FacadeUtilities.CHARSET.newEncoder().encode(CharBuffer.wrap(credentials.getPassword()));
            byte[] passwordData = Arrays.copyOfRange(buffer.array(), buffer.position(), buffer.limit());
            byte[] rawAuthData = new byte[usernameData.length + passwordData.length + 1];
            System.arraycopy(usernameData, 0, rawAuthData, 0, usernameData.length);
            rawAuthData[usernameData.length] = (byte) ':';
            System.arraycopy(passwordData, 0, rawAuthData, usernameData.length + 1, passwordData.length);
            parameters.put(FacadeUtilities.HEADER_AUTHORISATION,
                    "BASIC " + DatatypeConverter.printBase64Binary(rawAuthData));
        } catch (CharacterCodingException e) {
            FacadeUtilities.throwConnectionException(null, e);
        }
        parameters.put(FacadeUtilities.HEADER_IP, credentials.getIP() == null ? getLocalhostIP() : credentials.getIP());
        parameters.put(FacadeUtilities.HEADER_ROLES, credentials.getPreferredRole());
        return execute(callback, AUTH_TOKEN, FacadeUtilities.POST, parameters,
                new ConnectionExecutionTask<Token, SecurityFacadeException>() {
                    @Override
                    public Token execute(HttpURLConnection connection) throws IOException, DataBindingException,
                            SecurityFacadeException {
                        if (connection.getResponseCode() == FacadeUtilities.CREATED) {
                            Token bToken = new Token(JAXB.unmarshal(connection.getInputStream(), TokenInfo.class));
                            setTokenInternal(bToken, callback.getLocalAuthServiceDetails());
                            fireLoggedIn(bToken);
                            return bToken;
                        } else {
                            return FacadeUtilities.throwNotOKResponseException(connection, SecurityFacade.this);
                        }
                    }
                });
    }

    private Token authenticateLocally(LocalAuthServiceDetails serviceDetails) {
        if (LocalServiceProxy.useLocalService(serviceDetails)) {
            try {
                Token aToken = localService.getLocalToken(serviceDetails);
                if (aToken != null) {
                    setTokenInternal(aToken, serviceDetails);
                    fireLoggedIn(aToken);
                    return aToken;
                }
            } catch (SecurityFacadeException e) {
                LOGGER.error(FETCHING_TOKEN_FAILED, e);
            }
        }
        return null;
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#authenticate(se.esss.ics.rbac.access.SecurityCallback)
     */
    @Override
    public void authenticate(SecurityCallback callback) {
        final SecurityCallback theCallback = getCallback(callback);
        getExecutor().execute(new Runnable() {
            @Override
            public void run() {
                Credentials credentials = theCallback.getCredentials();
                if (credentials != null) {
                    try {
                        Token aToken = authenticateInternally(credentials, theCallback);
                        theCallback.authenticationCompleted(aToken);
                    } catch (SecurityFacadeException e) {
                        theCallback.authenticationFailed(e.getMessage(), credentials.getUsername(), e);
                    }
                }
                // if credentials are null, do nothing - the user has terminated the authentication process
            }
        });
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#logout()
     */
    @Override
    public boolean logout() throws SecurityFacadeException {
        return logoutInternally(getLocalToken(), getDefaultSecurityCallback());
    }

    private boolean logoutInternally(final Token token, SecurityCallback callback) throws SecurityFacadeException {
        if (token == null) {
            return false;
        }
        final LocalAuthServiceDetails serviceDetails = callback.getLocalAuthServiceDetails();
        if (LocalServiceProxy.useLocalService(serviceDetails)) {
            localService.deleteLocalToken(token, serviceDetails);
        }
        String url = new StringBuilder(50).append(AUTH_TOKEN).append(token.getTokenID()).toString();
        return execute(callback, url, FacadeUtilities.DELETE, NO_PARAMETERS_MAP,
                new ConnectionExecutionTask<Boolean, SecurityFacadeException>() {
                    @Override
                    public Boolean execute(HttpURLConnection connection) throws IOException, DataBindingException,
                            SecurityFacadeException {
                        int respondeCode = connection.getResponseCode();
                        if (respondeCode == FacadeUtilities.DELETED || respondeCode == FacadeUtilities.OK) {
                            setTokenInternal(null, serviceDetails);
                            fireLoggedOut(token);
                            return respondeCode == FacadeUtilities.DELETED;
                        } else {
                            return FacadeUtilities.throwNotOKResponseException(connection, SecurityFacade.this);
                        }
                    }
                });
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#logout(se.esss.ics.rbac.access.SecurityCallback)
     */
    @Override
    public void logout(SecurityCallback callback) {
        final SecurityCallback theCallback = getCallback(callback);
        final Token currentToken = getLocalToken();
        getExecutor().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    if (logoutInternally(currentToken, theCallback)) {
                        theCallback.logoutCompleted(currentToken);
                    } else {
                        theCallback.logoutFailed(LOGOUT_FAILED, currentToken, null);
                    }
                } catch (SecurityFacadeException e) {
                    theCallback.logoutFailed(e.getMessage(), currentToken, e);
                }
            }
        });
    }

    private void fireLoggedIn(Token token) {
        try {
            for (SecurityFacadeListener listener : listeners) {
                listener.loggedIn(token);
            }
        } catch (RuntimeException e) {
            LOGGER.error(NOTIFY_LISTENER_ERROR, e);
        }
    }

    private void fireLoggedOut(Token token) {
        try {
            for (SecurityFacadeListener listener : listeners) {
                listener.loggedOut(token);
            }
        } catch (RuntimeException e) {
            LOGGER.error(NOTIFY_LISTENER_ERROR, e);
        }
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#getToken()
     */
    @Override
    public Token getToken() throws SecurityFacadeException {
        if (token == null && tokenID != null) {
            update();
            String url = new StringBuilder(50).append(AUTH_TOKEN).append(tokenID).toString();
            return execute(getDefaultSecurityCallback(), url, FacadeUtilities.GET, NO_PARAMETERS_MAP,
                    new ConnectionExecutionTask<Token, SecurityFacadeException>() {
                        @Override
                        public Token execute(HttpURLConnection connection) throws IOException, DataBindingException,
                                SecurityFacadeException {
                            if (connection.getResponseCode() == FacadeUtilities.OK) {
                                Token aToken = new Token(JAXB.unmarshal(connection.getInputStream(), TokenInfo.class));
                                setTokenInternal(aToken, getDefaultSecurityCallback().getLocalAuthServiceDetails());
                                return aToken;
                            } else {
                                return FacadeUtilities.throwNotOKResponseException(connection, SecurityFacade.this);
                            }
                        }
                    });
        }
        return token;
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#isTokenValid()
     */
    @Override
    public boolean isTokenValid() throws SecurityFacadeException {
        return isTokenValidInternally(getLocalToken());
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#isTokenValid(se.esss.ics.rbac.access.SecurityCallback)
     */
    @Override
    public void isTokenValid(SecurityCallback callback) {
        final Token currentToken = getLocalToken();
        final SecurityCallback theCallback = getCallback(callback);
        getExecutor().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    theCallback.tokenValidationCompleted(currentToken, isTokenValidInternally(currentToken));
                } catch (SecurityFacadeException e) {
                    theCallback.tokenValidationFailed(e.getMessage(), currentToken, e);
                }
            }
        });
    }

    /**
     * Checks if the provided token is valid and returns true if the service says it is valid or false if it says it is
     * not.
     *
     * @param token the token to check
     * @return true if the token is valid or false otherwise
     * @throws SecurityFacadeException if there was an error in validation
     */
    private boolean isTokenValidInternally(Token token) throws SecurityFacadeException {
        if (token == null) {
            return false;
        }
        update();
        String url = new StringBuilder(60).append(AUTH_TOKEN).append(token.getTokenID()).append("/isvalid").toString();
        return execute(getDefaultSecurityCallback(), url, FacadeUtilities.GET, NO_PARAMETERS_MAP,
                new ConnectionExecutionTask<Boolean, SecurityFacadeException>() {
                    @Override
                    public Boolean execute(HttpURLConnection connection) throws IOException, DataBindingException,
                            SecurityFacadeException {
                        if (connection.getResponseCode() == FacadeUtilities.OK) {
                            return Boolean.TRUE;
                        } else if (connection.getResponseCode() == FacadeUtilities.ERROR
                                || connection.getResponseCode() == FacadeUtilities.BAD_REQUEST) {
                            return FacadeUtilities.throwNotOKResponseException(connection, SecurityFacade.this);
                        } else {
                            return Boolean.FALSE;
                        }
                    }
                });
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#getLocalToken()
     */
    @Override
    public Token getLocalToken() {
        if (token != null) {
            return token;
        } else if (tokenID != null) {
            return new Token(tokenID);
        }
        return null;
    }

    private void setTokenInternal(Token token, LocalAuthServiceDetails serviceDetails) throws SecurityFacadeException {
        setTokenInternalNotLocal(token);
        if (LocalServiceProxy.useLocalService(serviceDetails)) {
            localService.setLocalToken(token, serviceDetails);
        }
    }

    /**
     * Sets the token and returns true if the token was set successfully or false otherwise. If the token
     * already exists and is the same as the one that is being set, the method returns false.
     *
     * @param token the token to set
     * @return true if a new token was set or false otherwise
     * @throws SecurityFacadeException if there was a signature validation error
     */
    private boolean setTokenInternalNotLocal(Token token) throws SecurityFacadeException {
        if (token != null) {
            FacadeUtilities.verifySignatureInternal(token);
        }
        if ((this.token != null && this.token.equals(token))
                || (this.token == null && token == null)) return false;
        this.token = token;
        if (this.token == null) {
            this.tokenID = null;
        } else {
            this.tokenID = this.token.getTokenID();
        }
        return true;
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#setToken(char[])
     */
    @Override
    public void setToken(char[] tokenID) {
        Token oldToken = getLocalToken();
        this.tokenID = tokenID == null ? null : Arrays.copyOf(tokenID, tokenID.length);
        if (token != null && !Arrays.equals(token.getTokenID(), tokenID)) {
            token = null;
        }
        // logout if there was a user logged in and the old token id is different than the new one
        boolean fire = oldToken != null && !Arrays.equals(oldToken.getTokenID(), tokenID);
        if (fire) {
            fireLoggedOut(oldToken);
        }
        // login if there was no user logged in before, if the previous token ID was different than this one
        if ((oldToken == null && tokenID != null) || fire) {
            fireLoggedIn(getLocalToken());
        }
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#hasPermission(java.lang.String, java.lang.String)
     */
    @Override
    public boolean hasPermission(String resource, String permission) throws SecurityFacadeException,
            IllegalArgumentException {
        return hasPermissions(resource, new String[] { permission }).get(permission).booleanValue();
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#hasPermissions(java.lang.String, java.lang.String[])
     */
    @Override
    public Map<String, Boolean> hasPermissions(String resource, String... permissions) throws SecurityFacadeException,
            IllegalArgumentException {
        return hasPermissionsInternally(getLocalToken(), resource, permissions, getDefaultSecurityCallback());
    }

    private Map<String, Boolean> hasPermissionsInternally(Token token, String resource, String[] permissions,
            SecurityCallback callback) throws SecurityFacadeException, IllegalArgumentException {
        FacadeUtilities.verifyParameters(token, resource, permissions);
        update();
        StringBuilder urlBuilder = new StringBuilder(80 + permissions.length * 30);
        try {
            urlBuilder.append(AUTH_TOKEN).append(token.getTokenID()).append('/')
                    .append(URLEncoder.encode(resource.trim(), FacadeUtilities.CHAR_ENCODING)).append('/');
            StringBuilder perms = new StringBuilder(30 * permissions.length);
            perms.append(permissions[0].trim());
            for (int i = 1; i < permissions.length; i++) {
                perms.append(',');
                perms.append(permissions[i].trim());
            }
            urlBuilder.append(URLEncoder.encode(perms.toString(), FacadeUtilities.CHAR_ENCODING));
        } catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException(e);
        }
        return execute(callback, urlBuilder.toString(), FacadeUtilities.GET, NO_PARAMETERS_MAP,
                new ConnectionExecutionTask<Map<String, Boolean>, SecurityFacadeException>() {
                    @Override
                    public Map<String, Boolean> execute(HttpURLConnection connection) throws IOException,
                            DataBindingException, SecurityFacadeException {
                        if (connection.getResponseCode() == FacadeUtilities.OK) {
                            return JAXB.unmarshal(connection.getInputStream(), PermissionsInfo.class).getPermissions();
                        } else {
                            return FacadeUtilities.throwNotOKResponseException(connection, SecurityFacade.this);
                        }
                    }
                });
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#hasPermissions(se.esss.ics.rbac.access.SecurityCallback,
     * java.lang.String, java.lang.String[])
     */
    @Override
    public void hasPermissions(SecurityCallback callback, final String resource, final String... permissions) {
        final SecurityCallback theCallback = getCallback(callback);
        final Token callbackToken = getLocalToken();
        getExecutor().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Map<String, Boolean> permissionMap = hasPermissionsInternally(callbackToken, resource, permissions,
                            theCallback);
                    theCallback.authorisationCompleted(callbackToken, permissionMap);
                } catch (SecurityFacadeException | IllegalArgumentException e) {
                    theCallback.authorisationFailed(e.getMessage(), permissions, callbackToken, e);
                }
            }
        });
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#getRolesForUser(java.lang.String)
     */
    @Override
    public String[] getRolesForUser(String username) throws SecurityFacadeException, IllegalArgumentException {
        return getRolesForUserInternally(username, getDefaultSecurityCallback());
    }

    private String[] getRolesForUserInternally(String username, SecurityCallback callback)
            throws SecurityFacadeException, IllegalArgumentException {
        if (username == null || username.isEmpty()) {
            throw new IllegalArgumentException("Parameter 'username' must not be null or empty.");
        }
        update();
        String url = new StringBuilder(30).append("/auth/").append(username.trim()).append("/role").toString();

        return execute(callback, url, FacadeUtilities.GET, NO_PARAMETERS_MAP,
                new ConnectionExecutionTask<String[], SecurityFacadeException>() {
                    @Override
                    public String[] execute(HttpURLConnection connection) throws IOException, DataBindingException,
                            SecurityFacadeException {
                        if (connection.getResponseCode() == FacadeUtilities.OK) {
                            RolesInfo rolesInfo = JAXB.unmarshal(connection.getInputStream(), RolesInfo.class);
                            return rolesInfo.getRoleNames().toArray(new String[rolesInfo.getRoleNames().size()]);
                        } else {
                            return FacadeUtilities.throwNotOKResponseException(connection, SecurityFacade.this);
                        }
                    }
                });
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#getRolesForUser(se.esss.ics.rbac.access.SecurityCallback,
     * java.lang.String)
     */
    @Override
    public void getRolesForUser(SecurityCallback callback, final String username) {
        final SecurityCallback theCallback = getCallback(callback);
        getExecutor().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    String[] roles = getRolesForUserInternally(username, theCallback);
                    theCallback.rolesLoadingCompleted(username, roles);
                } catch (SecurityFacadeException | IllegalArgumentException e) {
                    theCallback.rolesLoadingFailed(e.getMessage(), username, e);
                }
            }
        });
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#requestExclusiveAccess(java.lang.String, java.lang.String, int)
     */
    @Override
    public ExclusiveAccess requestExclusiveAccess(String resource, String permission, int durationInMinutes)
            throws SecurityFacadeException, IllegalArgumentException {
        return requestExclusiveAccessInternally(getLocalToken(), resource, permission, durationInMinutes,
                getDefaultSecurityCallback());
    }

    private ExclusiveAccess requestExclusiveAccessInternally(Token token, String resource, String permission,
            int durationInMinutes, SecurityCallback callback) throws SecurityFacadeException, IllegalArgumentException {
        FacadeUtilities.verifyParameters(token, resource, permission);
        update();
        StringBuilder url = new StringBuilder(150);
        try {
            url.append(AUTH_TOKEN).append(token.getTokenID()).append('/')
                    .append(URLEncoder.encode(resource.trim(), FacadeUtilities.CHAR_ENCODING)).append('/')
                    .append(URLEncoder.encode(permission.trim(), FacadeUtilities.CHAR_ENCODING)).append("/exclusive");
        } catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException(e);
        }
        Map<String, String> parameters = new HashMap<>(1);
        if (durationInMinutes >= 1) {
            parameters.put(FacadeUtilities.HEADER_EXCLUSIVE_DURATION, String.valueOf(durationInMinutes * 60000));
        }
        return execute(callback, url.toString(), FacadeUtilities.POST, parameters,
                new ConnectionExecutionTask<ExclusiveAccess, SecurityFacadeException>() {
                    @Override
                    public ExclusiveAccess execute(HttpURLConnection connection) throws IOException,
                            DataBindingException, SecurityFacadeException {
                        if (connection.getResponseCode() == FacadeUtilities.CREATED) {
                            ExclusiveInfo info = JAXB.unmarshal(connection.getInputStream(), ExclusiveInfo.class);
                            return new ExclusiveAccess(info);
                        } else {
                            return FacadeUtilities.throwNotOKResponseException(connection, SecurityFacade.this);
                        }
                    }
                });
    }

    /*
     * @see
     * se.esss.ics.rbac.access.ISecurityFacade#requestExclusiveAccess(se.esss.ics.rbac.access.SecurityCallback,
     * java.lang.String, java.lang.String, int)
     */
    @Override
    public void requestExclusiveAccess(SecurityCallback callback, final String resource, final String permission,
            final int durationInMinutes) {
        final SecurityCallback theCallback = getCallback(callback);
        final Token callbackToken = getLocalToken();
        getExecutor().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    ExclusiveAccess access = requestExclusiveAccessInternally(callbackToken, resource, permission,
                            durationInMinutes, theCallback);
                    theCallback.requestExclusiveAccessCompleted(callbackToken, access);
                } catch (SecurityFacadeException | IllegalArgumentException e) {
                    theCallback.requestExclusiveAccessFailed(e.getMessage(), permission, resource, callbackToken, e);
                }
            }
        });
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#releaseExclusiveAccess(java.lang.String, java.lang.String)
     */
    @Override
    public boolean releaseExclusiveAccess(String resource, String permission) throws SecurityFacadeException,
            IllegalArgumentException {
        return releaseExclusiveAccessInternally(getLocalToken(), resource, permission, getDefaultSecurityCallback());
    }

    private boolean releaseExclusiveAccessInternally(Token token, String resource, String permission,
            SecurityCallback callback) throws SecurityFacadeException, IllegalArgumentException {
        FacadeUtilities.verifyParameters(token, resource, permission);
        update();
        StringBuilder url = new StringBuilder(150);
        try {
            url.append(AUTH_TOKEN).append(token.getTokenID()).append('/')
                    .append(URLEncoder.encode(resource.trim(), FacadeUtilities.CHAR_ENCODING)).append('/')
                    .append(URLEncoder.encode(permission.trim(), FacadeUtilities.CHAR_ENCODING)).append("/exclusive");
        } catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException(e);
        }

        return execute(callback, url.toString(), FacadeUtilities.DELETE, NO_PARAMETERS_MAP,
                new ConnectionExecutionTask<Boolean, SecurityFacadeException>() {
                    @Override
                    public Boolean execute(HttpURLConnection connection) throws IOException, DataBindingException,
                            SecurityFacadeException {
                        if (connection.getResponseCode() == FacadeUtilities.DELETED) {
                            return Boolean.TRUE;
                        } else if (connection.getResponseCode() == FacadeUtilities.OK) {
                            return Boolean.FALSE;
                        } else {
                            return FacadeUtilities.throwNotOKResponseException(connection, SecurityFacade.this);
                        }
                    }
                });
    }

    /*
     * @see
     * se.esss.ics.rbac.access.ISecurityFacade#releaseExclusiveAccess(se.esss.ics.rbac.access.SecurityCallback,
     * java.lang.String, java.lang.String)
     */
    @Override
    public void releaseExclusiveAccess(SecurityCallback callback, final String resource, final String permission) {
        final SecurityCallback theCallback = getCallback(callback);
        final Token callbackToken = getLocalToken();
        getExecutor().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    if (releaseExclusiveAccessInternally(callbackToken, resource, permission, theCallback)) {
                        theCallback.releaseExclusiveAccessCompleted(callbackToken, permission, resource);
                    } else {
                        theCallback.releaseExclusiveAccessFailed(EXCLUSIVE_ACCESS_RELEASE_FAILED, permission, resource,
                                callbackToken, null);
                    }
                } catch (SecurityFacadeException | IllegalArgumentException e) {
                    theCallback.releaseExclusiveAccessFailed(e.getMessage(), permission, resource, callbackToken, e);
                }
            }
        });
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#getRBACVersion()
     */
    @Override
    public String getRBACVersion() throws SecurityFacadeException {
        return execute(getDefaultSecurityCallback(), "/auth/version", FacadeUtilities.GET, NO_PARAMETERS_MAP,
                new ConnectionExecutionTask<String, SecurityFacadeException>() {
                    @Override
                    public String execute(HttpURLConnection connection) throws IOException, DataBindingException,
                            SecurityFacadeException {
                        if (connection.getResponseCode() == FacadeUtilities.OK) {
                            try (BufferedReader br = new BufferedReader(new InputStreamReader(connection
                                    .getInputStream(), FacadeUtilities.CHARSET))) {
                                return br.readLine();
                            }
                        } else {
                            return FacadeUtilities.throwNotOKResponseException(connection, SecurityFacade.this);
                        }
                    }
                });
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#renewToken()
     */
    @Override
    public Token renewToken() throws SecurityFacadeException {
        return renewTokenInternally(getLocalToken(), getDefaultSecurityCallback());
    }

    private Token renewTokenInternally(Token token, final SecurityCallback callback) throws SecurityFacadeException {
        if (token == null) {
            throw new SecurityFacadeException("RBAC authentication token is missing.", FacadeUtilities.IRRELEVANT);
        }
        update();
        String url = new StringBuilder(60).append(AUTH_TOKEN).append(token.getTokenID()).append("/renew").toString();
        return execute(callback, url, FacadeUtilities.POST, NO_PARAMETERS_MAP,
                new ConnectionExecutionTask<Token, SecurityFacadeException>() {
                    @Override
                    public Token execute(HttpURLConnection connection) throws IOException, DataBindingException,
                            SecurityFacadeException {
                        if (connection.getResponseCode() == FacadeUtilities.OK) {
                            Token renewedToken = new Token(
                                    JAXB.unmarshal(connection.getInputStream(), TokenInfo.class));
                            setTokenInternal(renewedToken, callback.getLocalAuthServiceDetails());
                            return renewedToken;
                        } else {
                            return FacadeUtilities.throwNotOKResponseException(connection, SecurityFacade.this);
                        }
                    }
                });
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#renewToken(se.esss.ics.rbac.access.SecurityCallback)
     */
    @Override
    public void renewToken(SecurityCallback callback) {
        final SecurityCallback theCallback = getCallback(callback);
        final Token currentToken = getLocalToken();
        getExecutor().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Token renewedToken = renewTokenInternally(currentToken, theCallback);
                    theCallback.tokenRenewalCompleted(renewedToken);
                } catch (SecurityFacadeException e) {
                    theCallback.tokenRenewalFailed(e.getMessage(), currentToken, e);
                }
            }
        });
    }

    /**
     * Creates a connection to the given URL using the given method and parameters and executes the task using the
     * created connection. If there was an error during execution the error will be wrapped into a
     * {@link SecurityFacadeException}.
     *
     * @param callback the callback used for generating the connection
     * @param url the URL to connect to
     * @param method the HTTP request method
     * @param parameters the request parameters
     * @param executor the task
     * @return the task execution result
     * @throws SecurityFacadeException if there was an error during execution
     */
    private <T> T execute(SecurityCallback callback, String url, String method, Map<String, String> parameters,
            ConnectionExecutionTask<T, SecurityFacadeException> task) throws SecurityFacadeException {
        int numServices = Service.values().length;
        for (int i = 1; i <= numServices; i++) {
            HttpURLConnection connection = null;
            try {
                connection = connectionFactory.getConnection(callback, url, method);
                for (Map.Entry<String, String> property : parameters.entrySet()) {
                    if (property.getValue() == null) {
                        continue;
                    }
                    connection.setRequestProperty(property.getKey(), property.getValue());
                }
                return task.execute(connection);
            } catch (IOException | ConnectionException e) {
                // if the host is unavailable we get UnknownHostException
                // if host does not accept connection on the given port we get ConnectException
                // if the service does not exist we get FileNotFoundException
                // if the certificate was rejected we get ConnectionException
                LOGGER.warn("Service connection error (type: "
                        + (e.getCause() == null ? e.getClass().getSimpleName() : e.getCause().getClass()
                                .getSimpleName()) + "): " + e.getMessage());
                if (i == numServices) {
                    return FacadeUtilities.throwConnectionException(connection, e);
                }
            } catch (DataBindingException e) {
                // In case of data binding exception, the service is available, but there is something
                // wrong with the data. This should not happen if the service and client are aligned.
                return FacadeUtilities.throwConnectionException(connection, e);
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }
            connectionFactory.switchService();
        }
        // Should never happen, because one of the other exceptions should occur prior to this line
        throw new SecurityFacadeException("Could not connect to " + url + ".", FacadeUtilities.IRRELEVANT);
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#setAutoLogoutTimeout(int)
     */
    @Override
    public void setAutoLogoutTimeout(int timeoutInMinutes) {
        if (timeoutInMinutes > 0 && timeoutInMinutes < 15) {
            throw new IllegalArgumentException("Auto logout timeout shorter than 15 minutes is not allowed.");
        }
        synchronized (mutex) {
            autoLogoutTimeout = timeoutInMinutes;
            if (autoLogoutTimeout < 1) {
                if (autoLogout != null) {
                    autoLogout.stop();
                }
            } else {
                if (autoLogout == null) {
                    autoLogout = new DefaultAutoLogout(this);
                    autoLogout.setLogoutConfirmationGrace(RBACProperties.getInstance()
                            .getInactivityResponseGracePeriod());
                }
                autoLogout.setLogoutTimeout(autoLogoutTimeout);
                autoLogout.start();
            }
        }
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#setAutoLogout(se.esss.ics.rbac.access.AutoLogout)
     */
    @Override
    public void setAutoLogout(AutoLogout autoLogout) {
        synchronized (mutex) {
            if (this.autoLogout != null) {
                this.autoLogout.stop();
            }
            this.autoLogout = autoLogout;
            if (this.autoLogout != null) {
                this.autoLogout.setLogoutConfirmationGrace(RBACProperties.getInstance()
                        .getInactivityResponseGracePeriod());
                if (autoLogoutTimeout < 1) {
                    this.autoLogout.stop();
                } else {
                    this.autoLogout.setLogoutTimeout(autoLogoutTimeout);
                    this.autoLogout.start();
                }
            }
        }
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#getAutoLogoutTimeout()
     */
    @Override
    public int getAutoLogoutTimeout() {
        synchronized (mutex) {
            return autoLogoutTimeout;
        }
    }

    /**
     * Returns the callback that is to be used in asynchronous call. If the given callback is non-null, it is returned,
     * otherwise the default one is returned.
     *
     * @param callback the suggested callback to use
     * @return the callback to use, always non null
     */
    private SecurityCallback getCallback(SecurityCallback callback) {
        return callback == null ? getDefaultSecurityCallback() : callback;
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#getDefaultSecurityCallback()
     */
    @Override
    public SecurityCallback getDefaultSecurityCallback() {
        if (defaultSecurityCallback == null) {
            defaultSecurityCallback = new SecurityCallbackAdapter() {
            };
        }
        return defaultSecurityCallback;
    }

    /*
     * @see
     * se.esss.ics.rbac.access.ISecurityFacade#setDefaultSecurityCallback(se.esss.ics.rbac.access.SecurityCallback
     * )
     */
    @Override
    public void setDefaultSecurityCallback(SecurityCallback callback) {
        this.defaultSecurityCallback = callback;
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.ISecurityFacade#initLocalServiceUsage()
     */
    @Override
    public void initLocalServiceUsage() {
        LocalAuthServiceDetails serviceDetails = getDefaultSecurityCallback().getLocalAuthServiceDetails();
        if (LocalServiceProxy.useLocalService(serviceDetails)) {
            try {
                localService.removeLocalServiceListener(lsListener);
                token = localService.getLocalToken(serviceDetails);
                localService.addLocalServiceListener(lsListener);
                localService.registerNotifyListener(serviceDetails);
            } catch (SecurityFacadeException e) {
                LOGGER.error(FETCHING_TOKEN_FAILED, e);
            }
        }
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#addSecurityFacadeListener(se.esss.ics.rbac.access.
     * SecurityFacadeListener)
     */
    @Override
    public void addSecurityFacadeListener(SecurityFacadeListener listener) {
        listeners.add(listener);
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#removeSecurityFacadeListener(se.esss.ics.rbac.access.
     * SecurityFacadeListener)
     */
    @Override
    public void removeSecurityFacadeListener(SecurityFacadeListener listener) {
        listeners.remove(listener);
    }

    /**
     * Returns the main executor, which is used for all asynchronous calls.
     *
     * @return the executor service
     */
    private ExecutorService getExecutor() {
        if (executor == null) {
            executor = Executors.newCachedThreadPool(new RBACThreadFactory("RBAC-Async", false, true));
        }
        return executor;
    }

    /**
     * Returns the IP of the local host.
     *
     * @return the local host IP
     */
    private String getLocalhostIP() {
        if (localhostIP == null) {
            try {
                localhostIP = InetAddress.getLocalHost().getHostAddress();
            } catch (UnknownHostException e) {
                LOGGER.error(LOCALHOST_IP_UNKNOWN, e);
            }
        }
        return localhostIP;
    }

    /*
     * @see java.lang.Object#finalize()
     */
    @Override
    protected void finalize() throws Throwable {
        try {
            destroy();
        } finally {
            super.finalize();
        }
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#destroy()
     */
    @Override
    public void destroy() {
        destroyed = true;
        synchronized (mutex) {
            if (autoLogout != null) {
                autoLogout.stop();
            }
        }
        if (localService != null) {
            localService.removeLocalServiceListener(lsListener);
            localService.destroy();
        }
        if (executor != null) {
            executor.shutdownNow();
        }
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#isDestroyed()
     */
    @Override
    public boolean isDestroyed() {
        return destroyed;
    }
}
