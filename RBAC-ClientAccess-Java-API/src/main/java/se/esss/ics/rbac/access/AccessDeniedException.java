/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access;

/**
 * 
 * <code>AccessDeniedException</code> is an exception which is thrown when the user does not have permission to executed
 * the action, because the token has expired, doesn't exist etc.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class AccessDeniedException extends SecurityFacadeException {

    private static final long serialVersionUID = 6318638778198072490L;

    /**
     * Constructs a new exception with the specified detail message. The cause is not initialised, and may subsequently
     * be initialised by a call to {@link #initCause(Throwable)}.
     * 
     * @param message the detailed message of the exception
     */
    public AccessDeniedException(String message) {
        super(message, FacadeUtilities.GONE);
    }
}
