/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.httputil;

import javax.servlet.http.Cookie;

/**
 * 
 * <code>RBACCookie</code> is a convenient extension of the {@link Cookie}, which describes 
 * the RBAC single sign on cookie.
 *
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
public class RBACCookie extends Cookie {

    private static final long serialVersionUID = 2336677636879702529L;

    public static final String PATH = "/";
    public static final String NAME = "RBACCookie";
    public static final String COMMENT = "RBAC Token Cookie";
    public static final String LOGGED_OUT_VALUE = "loggedout";    
    
    /**
     * Creates the cookie describing that user has logged out. This cookie should be issued on logout in 
     * order to let other services know that the user is logged out in case that the service is caching
     * the permissions and login state.
     * 
     */
    public RBACCookie() {
        this(LOGGED_OUT_VALUE);
        setMaxAge(Integer.MAX_VALUE);
    }
    
    /**
     * Constructs a new cookie describing the login with the given token id.
     * 
     * @param tokenID the token id, which will become the value of the cookie
     */
    public RBACCookie(char[] tokenID) {
        this(new String(tokenID));
    }
    
    /**
     * Constructs a new cookie with the given token id.
     * 
     * @param tokenID the token id
     */
    public RBACCookie(String tokenID) {
        super(NAME,tokenID); 
        setHttpOnly(true);
        setMaxAge(Integer.MAX_VALUE);
        setComment(COMMENT);  
        setPath(PATH);
        setDomain(CookieUtilities.getDomainName());
    }
}
