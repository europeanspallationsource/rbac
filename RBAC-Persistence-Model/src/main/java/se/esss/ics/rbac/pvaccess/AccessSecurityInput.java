/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.pvaccess;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import se.esss.ics.rbac.EntityDescriptor;
import se.esss.ics.rbac.Util;

/**
 * 
 * <code>AccessSecurityInput</code> defines a single input field in the access security group. The input field always
 * belong to an {@link AccessSecurityGroup}, it is associated with a single PV and assigned an index within the group.
 * There can be only one input with a specific index per group.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
@Entity(name = "access_security_input")
@Table(name = "access_security_input",
    uniqueConstraints = {
            @UniqueConstraint(name = "UK_ACCESS_SECURITY_INPUT_input_index_group",
                    columnNames = { "input_index", "as_group" }) },
    indexes = {
            @Index(name = "INDEX_ACCESS_SECURITY_INPUT_pv_name", columnList = ("pv_name")),
            @Index(name = "INDEX_ACCESS_SECURITY_INPUT_as_group", columnList = ("as_group")) })
public final class AccessSecurityInput implements Serializable, EntityDescriptor {

    private static final long serialVersionUID = -3833834492492528328L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "pv_name", nullable = false)
    private String pvName;
    @Column(name = "input_index", nullable = false)
    private Integer index;
    @ManyToOne(optional = false)
    @JoinColumn(foreignKey = @ForeignKey(name = "FK_ACCESS_SECURITY_INPUT_as_group"), name = "as_group",
            nullable = false, referencedColumnName = "id")
    private AccessSecurityGroup asGroup;

    /**
     * @return the unique id of this input
     */
    public int getId() {
        return id;
    }

    /**
     * @return the name of the pv that is used as an input
     */
    public String getPvName() {
        return pvName;
    }

    /**
     * @param pvName the name of the PV that is used as an input
     */
    public void setPvName(String pvName) {
        this.pvName = pvName;
    }

    /**
     * @return the index of this input in the group (index 1 represents input A, index 2 represents input B ... index 12
     *         represents input L)
     */
    public Integer getIndex() {
        return index;
    }

    /**
     * @param index the index of this input in the group
     */
    public void setIndex(Integer index) {
        this.index = index;
    }

    /**
     * @return the group that this input is for
     */
    public AccessSecurityGroup getASGroup() {
        return asGroup;
    }

    /**
     * @param group the owner of this input
     */
    public void setASGroup(AccessSecurityGroup group) {
        this.asGroup = group;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, index, pvName, asGroup);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (getClass() != obj.getClass()) {
            return false;
        }
        AccessSecurityInput other = (AccessSecurityInput) obj;
        return id == other.id && Objects.equals(index, other.index) && Objects.equals(pvName, other.pvName)
                && Objects.equals(asGroup, other.asGroup);
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.EntityDescriptor#toString(int)
     */
    @Override
    public String toString(int level) {
        String endTab = '\n' + Util.getIndent(level);
        String tab = endTab + Util.INDENT;
        return new StringBuilder(100)
                .append("ACCESS SECURITY INPUT [").append(tab)
                .append("PV: ").append(pvName).append(tab)
                .append("Index: ").append(index).append(endTab)
                .append(']').toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(0);
    }

}
