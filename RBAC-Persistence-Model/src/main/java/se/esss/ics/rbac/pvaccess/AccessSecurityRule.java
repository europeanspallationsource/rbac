/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.pvaccess;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import se.esss.ics.rbac.NamedEntity;
import se.esss.ics.rbac.Util;
import se.esss.ics.rbac.datamodel.IPGroup;
import se.esss.ics.rbac.datamodel.Role;

/**
 * 
 * <code>AccessSecurityRule</code> specifies the rule that defines when access for a PV group is allowed or denied. The
 * rule is composed of a set of roles and set of IPs. Only the users that have one of this rule's roles are given the
 * permission that this rule describes; the permission is given only if the connection is made from one of the specified
 * IP.
 * <p>
 * In addition the rule also specifies the level and the permission. These are both parameters to the IOC access
 * security file: permission can have one of the three possible values (see {@link Permission}) and level can have two
 * possible values (0 for all fields, except for VAL, CMD and RES; 1 for all fields).
 * </p>
 * <p>
 * The CALC field of the rule specifies the calculation equation that uses the {@link AccessSecurityGroup#getInputs()}
 * values to evaluate an expression. The result of the expression is used by the IOC to grant or deny permission.
 * </p>
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
@Entity(name = "access_security_rule")
@Table(name = "access_security_rule",
        indexes = { @Index(name = "INDEX_ACCESS_SECURITY_RULE_as_group", columnList = ("as_group")) })
public final class AccessSecurityRule implements Serializable, NamedEntity {

    private static final long serialVersionUID = 3663786288104978048L;

    /**
     * <code>Permission</code> defines the type of permission that can be assigned to the rule. WRITE permission defines
     * that the ones that match the criteria of the rule will have write and read permission; READ permission defines
     * that whoever matches the criteria will have read permission, but nor write. NONE defines that whoever matches the
     * criteria does not have any permission.
     */
    public static enum Permission implements Serializable {
        NONE, READ, WRITE;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "level")
    private Boolean level = Boolean.TRUE;
    @Column(name = "permission", nullable = false)
    private Permission permission = Permission.WRITE;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(foreignKey = @ForeignKey(name = "FK_ACCESS_SECURITY_RULE_ROLE_as_rule"),
            inverseForeignKey = @ForeignKey(name = "FK_ACCESS_SECURITY_RULE_ROLE_role"))
    private Set<Role> roles;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(foreignKey = @ForeignKey(name = "FK_ACCESS_SECURITY_RULE_IP_GROUP_rule"),
            inverseForeignKey = @ForeignKey(name = "FK_ACCESS_SECURITY_RULE_IP_GROUP_ip_groups"))
    private Set<IPGroup> ipGroups;
    @Column(name = "calc", length = 40)
    private String calc;
    @ManyToOne(optional = false)
    @JoinColumn(foreignKey = @ForeignKey(name = "FK_ACCESS_SECURITY_RULE_as_group"), name = "as_group",
            nullable = false, referencedColumnName = "id")
    private AccessSecurityGroup asGroup;

    /**
     * @return the unique id of this rule
     */
    public int getId() {
        return id;
    }

    /**
     * @param name the name of this rule (for display purposes only)
     */
    public void setName(String name) {
        this.name = name;
    }

    /*
     * @see se.esss.ics.rbac.NamedEntity#getName()
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Set the description of this rule.
     * 
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the description of this rule
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return the level of permission (true = 1, false = 0)
     */
    public Boolean getLevel() {
        if (level == null) {
            level = Boolean.TRUE;
        }
        return level;
    }

    /**
     * @param level the level of permission to set (true = 1, false = 0)
     */
    public void setLevel(Boolean level) {
        this.level = level;
    }

    /**
     * @return the permission type
     */
    public Permission getPermission() {
        if (permission == null) {
            permission = Permission.WRITE;
        }
        return permission;
    }

    /**
     * @param permission the permission type to set
     */
    public void setPermission(Permission permission) {
        this.permission = permission;
    }

    /**
     * @return the roles that have this permission
     */
    public Set<Role> getRoles() {
        return roles;
    }

    /**
     * @param roles the roles that have this permission
     */
    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    /**
     * @return the ipGroups that contain the IPs that have this permission
     */
    public Set<IPGroup> getIpGroups() {
        return ipGroups;
    }

    /**
     * @param ipGroups the ipGroups to set
     */
    public void setIpGroups(Set<IPGroup> ipGroups) {
        this.ipGroups = ipGroups;
    }

    /**
     * @return the value of CALC field, which defines the equation to evaluate the input field values (e.g. A=1
     *         &#36;&#36; B=2)
     */
    public String getCalc() {
        return calc;
    }

    /**
     * @param calc the CALC field to set
     */
    public void setCalc(String calc) {
        String cl = calc;
        if (cl != null && !cl.trim().isEmpty()) {
            cl = cl.trim();
            if (cl.charAt(0) == '"') {
                cl = cl.substring(1);
            }
            if (cl.charAt(cl.length() - 1) == '"') {
                cl = cl.substring(0, cl.length() - 1);
            }
        }
        this.calc = cl;
    }

    /**
     * @return the group that this rule belongs to
     */
    public AccessSecurityGroup getASGroup() {
        return asGroup;
    }

    /**
     * @param group the group to set
     */
    public void setASGroup(AccessSecurityGroup group) {
        this.asGroup = group;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, calc, description, level, name, permission, asGroup);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (getClass() != obj.getClass()) {
            return false;
        }
        AccessSecurityRule other = (AccessSecurityRule) obj;
        return id == other.id && Objects.equals(name, other.name) && Objects.equals(calc, other.calc)
                && Objects.equals(description, other.description) && Objects.equals(asGroup, other.asGroup)
                && Objects.equals(level, other.level) && permission == other.permission;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.EntityDescriptor#toString(int)
     */
    @Override
    public String toString(int level) {
        String endTab = '\n' + Util.getIndent(level);
        String tab = endTab + Util.INDENT;
        String groupsStr = Util.toString(ipGroups, level + 1);
        String rolesStr = Util.toString(roles, level + 1);
        return new StringBuilder(groupsStr.length() + rolesStr.length() + 200)
                .append("ACCESS SECURITY RULE [").append(tab)
                .append("Name: ").append(name).append(tab)
                .append("Description: ").append(description).append(tab)
                .append("Level: ").append(this.level).append(tab)
                .append("IPGroups: ").append(groupsStr).append(tab)
                .append("Permission: ").append(permission).append(tab)
                .append("CALC: ").append(calc).append(tab)
                .append("Roles: ").append(rolesStr).append(endTab)
                .append(']').toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(0);

    }
}
