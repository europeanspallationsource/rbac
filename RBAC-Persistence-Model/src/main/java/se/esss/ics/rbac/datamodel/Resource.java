/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.datamodel;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import se.esss.ics.rbac.NamedEntity;
import se.esss.ics.rbac.Util;

/**
 * <code>Resource</code> is any type of resource, which is using the authorisation service to protect its actions. The
 * resource can be an application, service or anything else that requires authorisation. Each resource defines several
 * permissions. These are the permissions for which a role can be authorised when using this specific resource. The
 * resource is acting as a filter to prevent asking for permission from applications other than the one for which the
 * permission was specified.
 * <p>
 * The resource is uniquely defined by its name, which should be descriptive enough to know what this resource
 * represents. The description gives more detailed information about it.
 * </p>
 * <p>
 * In addition the resource is associated with several users, which are the only users who can manage the permissions of
 * this resource.
 * </p>
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
@Entity(name = "resource")
@Table(name = "resource", 
    uniqueConstraints = { 
        @UniqueConstraint(name = "UK_RESOURCE_name", columnNames = { "name" }) },
    indexes = { 
        @Index(name = "INDEX_RESOURCE_name", columnList = ("name")) })
@NamedQueries({
        @NamedQuery(name = "Resource.selectAll",
                query = "SELECT r FROM se.esss.ics.rbac.datamodel.Resource r ORDER BY r.name"),
        @NamedQuery(name = "Resource.selectNames",
                query = "SELECT r.name FROM se.esss.ics.rbac.datamodel.Resource r"),
        @NamedQuery(name = "Resource.findByName",
                query = "SELECT r FROM se.esss.ics.rbac.datamodel.Resource r WHERE r.name = :name"),
        @NamedQuery(name = "Resource.findByWildcard",
                query = "SELECT r FROM se.esss.ics.rbac.datamodel.Resource r WHERE r.name LIKE :wildcard") })
public final class Resource implements Serializable, NamedEntity {

    private static final long serialVersionUID = -1202300191619623739L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "description", length = 1000, nullable = true)
    private String description;
    @ElementCollection(fetch = FetchType.LAZY)
    @JoinColumn(foreignKey = @ForeignKey(name = "FK_RESOURCE_MANAGERS_resource"), name = "managers", nullable = false,
            referencedColumnName = "id")
    private Set<String> managers;
    @OneToMany(mappedBy = "resource", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private Set<Permission> permissions;

    /**
     * @return the primary key of this entity
     */
    public int getId() {
        return id;
    }

    /*
     * @see se.esss.ics.rbac.NamedEntity#getName()
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * @param name the name of this resource
     */
    public void setName(String name) {
        if (name != null && name.indexOf(',') >= 0) {
            throw new IllegalArgumentException("Resource name contains an invalid character ','.");
        }
        this.name = name != null ? name.trim() : null;
    }

    /**
     * @return detailed description of this resource
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the detailed description of the resource
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the set of resource managers - people who can manage this resource
     */
    public Set<String> getManagers() {
        return managers;
    }

    /**
     * @param managers the set of resource managers
     */
    public void setManagers(Set<String> managers) {
        this.managers = managers;
    }

    /**
     * @return the permissions associated with this resource
     */
    public Set<Permission> getPermissions() {
        return permissions;
    }

    /**
     * @param permissions the set of permissions associated with this resource
     */
    public void setPermissions(Set<Permission> permissions) {
        this.permissions = permissions;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, name, description);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (getClass() != obj.getClass()) {
            return false;
        }
        Resource other = (Resource) obj;
        return id == other.id && Objects.equals(name, other.name) && Objects.equals(description, other.description);
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.EntityDescriptor#toString(int)
     */
    @Override
    public String toString(int level) {
        String endTab = '\n' + Util.getIndent(level);
        String tab = endTab + Util.INDENT;
        return new StringBuilder(description == null ? 100 : description.length() + 100)
                .append("RESOURCE [").append(tab)
                .append("Name: ").append(name).append(tab)
                .append("Description: ").append(description).append(endTab)
                .append(']').toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(0);
    }
}
