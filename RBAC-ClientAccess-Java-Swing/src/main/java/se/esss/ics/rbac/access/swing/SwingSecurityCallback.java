/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access.swing;

import java.awt.Component;
import java.lang.reflect.InvocationTargetException;
import java.security.cert.X509Certificate;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.esss.ics.rbac.access.Credentials;
import se.esss.ics.rbac.access.SecurityCallback;
import se.esss.ics.rbac.access.SecurityCallbackAdapter;
import se.esss.ics.rbac.access.Token;

/**
 * 
 * <code>SwingSecurityCallback</code> is an implementation of the {@link SecurityCallback}, which uses swing components
 * to request input from user and to show the failed results of the asynchronous calls.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class SwingSecurityCallback extends SecurityCallbackAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(SwingSecurityCallback.class);

    private Component parent;

    /**
     * Sets the parent component, which is used as a parent for all the message dialogs shown by this callback.
     * 
     * @param parent the component to use as a parent
     */
    public void setParentComponent(Component parent) {
        this.parent = parent;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.access.SecurityCallbackAdapter#authenticationFailed(java.lang.String, java.lang.String,
     * java.lang.Throwable)
     */
    @Override
    public void authenticationFailed(final String reason, String username, Throwable e) {
        dispatch(new Runnable() {
            @Override
            public void run() {
                JOptionPane.showMessageDialog(parent, reason, "Authentication Failed", JOptionPane.ERROR_MESSAGE);
            }
        });
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.access.SecurityCallbackAdapter#logoutFailed(java.lang.String,
     * se.esss.ics.rbac.access.Token, java.lang.Throwable)
     */
    @Override
    public void logoutFailed(final String reason, Token token, Throwable e) {
        dispatch(new Runnable() {
            @Override
            public void run() {
                JOptionPane.showMessageDialog(parent, reason, "Sign Out Failed", JOptionPane.ERROR_MESSAGE);
            }
        });
    }

    /*
     * @see se.esss.ics.rbac.access.SecurityCallbackAdapter#authorisationFailed(java.lang.String, java.lang.String[],
     * se.esss.ics.rbac.access.Token, java.lang.Throwable)
     */
    @Override
    public void authorisationFailed(final String reason, String[] permissions, Token token, Throwable e) {
        dispatch(new Runnable() {
            @Override
            public void run() {
                JOptionPane.showMessageDialog(parent, reason, "Authorisation Failed", JOptionPane.ERROR_MESSAGE);
            }
        });
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.access.SecurityCallback#getCredentials()
     */
    @Override
    public Credentials getCredentials() {
        final Credentials[] credentials = new Credentials[1];
        dispatch(new Runnable() {
            @Override
            public void run() {
                AuthenticationPane pane = new AuthenticationPane();
                pane.createDialog(parent).setVisible(true);
                credentials[0] = pane.getCredentials();
            }
        });
        return credentials[0];
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.access.SecurityCallbackAdapter#rolesLoadingFailed(java.lang.String, java.lang.String,
     * java.lang.Throwable)
     */
    @Override
    public void rolesLoadingFailed(final String reason, String username, Throwable e) {
        dispatch(new Runnable() {
            @Override
            public void run() {
                JOptionPane.showMessageDialog(parent, reason, "Roles Loading Failed", JOptionPane.ERROR_MESSAGE);
            }
        });
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.access.SecurityCallbackAdapter#requestExclusiveAccessFailed(java.lang.String,
     * java.lang.String, java.lang.String, se.esss.ics.rbac.access.Token, java.lang.Throwable)
     */
    @Override
    public void requestExclusiveAccessFailed(final String reason, String permission, String resource, Token token,
            Throwable e) {
        dispatch(new Runnable() {
            @Override
            public void run() {
                JOptionPane.showMessageDialog(parent, reason, "Exclusive Access Request Failed",
                        JOptionPane.ERROR_MESSAGE);
            }
        });
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.access.SecurityCallbackAdapter#releaseExclusiveAccessFailed(java.lang.String,
     * java.lang.String, java.lang.String, se.esss.ics.rbac.access.Token, java.lang.Throwable)
     */
    @Override
    public void releaseExclusiveAccessFailed(final String reason, String permission, String resource, Token token,
            Throwable e) {
        dispatch(new Runnable() {
            @Override
            public void run() {
                JOptionPane.showMessageDialog(parent, reason, "Exclusive Access Release Failed",
                        JOptionPane.ERROR_MESSAGE);
            }
        });
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.access.SecurityCallbackAdapter#autoLogoutConfirm(se.esss.ics.rbac. access.Token, int)
     */
    @Override
    public boolean autoLogoutConfirm(final Token token, final int timeoutInSeconds) {
        final boolean[] response = new boolean[1];
        dispatch(new Runnable() {
            @Override
            public void run() {
                AutoLogoutConfirmPane pane = new AutoLogoutConfirmPane(token, timeoutInSeconds);
                pane.createDialog(parent).setVisible(true);
                response[0] = pane.isConfirmed();
            }
        });

        return response[0];
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.access.SecurityCallbackAdapter#acceptCertificate(java.lang.String,
     * java.security.cert.X509Certificate)
     */
    @Override
    public boolean acceptCertificate(final String host, final X509Certificate certificate) {
        final boolean[] response = new boolean[1];
        dispatch(new Runnable() {
            @Override
            public void run() {
                CertificatePane pane = new CertificatePane(host, certificate);
                pane.createDialog(parent).setVisible(true);
                response[0] = pane.isConfirmed();
            }
        });

        return response[0];
    }

    private static void dispatch(Runnable r) {
        try {
            SwingUtilities.invokeAndWait(r);
        } catch (InvocationTargetException | InterruptedException e) {
            LOGGER.error("Could not show a callback message.", e);
        }
    }
}
