/* 
 * Copyright (c) 2008 Stiftung Deutsches Elektronen-Synchrotron, 
 * Member of the Helmholtz Association, (DESY), HAMBURG, GERMANY.
 *
 * THIS SOFTWARE IS PROVIDED UNDER THIS LICENSE ON AN "../AS IS" BASIS. 
 * WITHOUT WARRANTY OF ANY KIND, EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR PARTICULAR PURPOSE AND 
 * NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR 
 * THE USE OR OTHER DEALINGS IN THE SOFTWARE. SHOULD THE SOFTWARE PROVE DEFECTIVE 
 * IN ANY RESPECT, THE USER ASSUMES THE COST OF ANY NECESSARY SERVICING, REPAIR OR 
 * CORRECTION. THIS DISCLAIMER OF WARRANTY CONSTITUTES AN ESSENTIAL PART OF THIS LICENSE. 
 * NO USE OF ANY SOFTWARE IS AUTHORIZED HEREUNDER EXCEPT UNDER THIS DISCLAIMER.
 * DESY HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, 
 * OR MODIFICATIONS.
 * THE FULL LICENSE SPECIFYING FOR THE SOFTWARE THE REDISTRIBUTION, MODIFICATION, 
 * USAGE AND OTHER RIGHTS AND OBLIGATIONS IS INCLUDED WITH THE DISTRIBUTION OF THIS 
 * PROJECT IN THE FILE LICENSE.HTML. IF THE LICENSE IS NOT INCLUDED YOU MAY FIND A COPY 
 * AT HTTP://WWW.DESY.DE/LEGAL/LICENSE.HTM
 */
package org.csstudio.data.values.internal;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.csstudio.data.values.IDoubleValue;
import org.csstudio.data.values.INumericMetaData;
import org.csstudio.data.values.ISeverity;
import org.csstudio.data.values.ITimestamp;

/**
 * Implementation of {@link IDoubleValue}.
 * 
 * @author Kay Kasemir, Xihui Chen
 */
public class DoubleValue extends Value implements IDoubleValue {
    /**
     * Map of NumberFormats by precision.
     * 
     * JProfiler tests showed that about _half_ of the string formatting is spent in creating the suitable NumberFormat,
     * so they are cached for re-use. The key is the 'precision', where a precision >= 0 means decimal format, and a
     * precision < 0 means exponential notation.
     * 
     * Access must sync on the hash, then sync on the format while using it.
     */
    private static final Map<Integer, NumberFormat> FORMAT_CACHE = new HashMap<>();

    /** The values. */
    private final double[] values;

    /**
     * Constructs a new double value from pieces.
     * 
     * @param time the timestamp of the value
     * @param severity the severity
     * @param status the status of the value
     * @param metaData meta information about the PV
     * @param values the values
     */
    public DoubleValue(ITimestamp time, ISeverity severity, String status, INumericMetaData metaData, double[] values) {
        super(time, severity, status, metaData);
        this.values = values;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.IDoubleValue#getValues()
     */
    @Override
    public final double[] getValues() {
        return values;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.IDoubleValue#getValue()
     */
    @Override
    public final double getValue() {
        return values[0];
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.internal.data.Value#format(org.csstudio.platform .data.IValue.Format, int)
     */
    @Override
    public String format(Format how, int prec) {
        // Any value at all?
        if (!getSeverity().hasValue()) {
            return Messages.NO_VALUE;
        }

        // Handle array elements as characters
        if (how == Format.STRING) {
            StringBuilder buf = new StringBuilder(values.length);
            for (int i = 0; i < values.length; i++) {
                char c = getDisplayChar((char) values[i]);
                if (c == 0) {
                    break;
                }
                buf.append(c);
            }
            return buf.toString();
        }

        // Show array elements as numbers
        int precision = prec;
        NumberFormat fmt;
        if (how == Format.EXPONENTIAL) {
            // Assert positive precision
            precision = Math.abs(precision);
            synchronized (FORMAT_CACHE) {
                // Exponential notation itentified as 'negative' precision in
                // cached
                fmt = FORMAT_CACHE.get(Integer.valueOf(-precision));
                if (fmt == null) {
                    StringBuilder pattern = new StringBuilder(10);
                    pattern.append("0."); //$NON-NLS-1$
                    for (int i = 0; i < precision; ++i) {
                        pattern.append('0');
                    }
                    pattern.append("E0"); //$NON-NLS-1$
                    fmt = new DecimalFormat(pattern.toString());
                    FORMAT_CACHE.put(Integer.valueOf(-precision), fmt);
                }
            }
        } else {
            // For the default format, or when requested via <0 prec.,
            // use the precision from meta data
            if (how == Format.DEFAULT || precision < 0) {
                INumericMetaData numMeta = (INumericMetaData) getMetaData();
                // Should have numeric meta data, but in case of errors
                // that might be null.
                if (numMeta != null) {
                    precision = numMeta.getPrecision();
                }
            }
            // If default format precision is 0, assume nobody configured
            // it properly, and fall back to Double.toString
            if (how == Format.DEFAULT && precision == 0) {
                fmt = null;
            } else {
                synchronized (FORMAT_CACHE) {
                    fmt = FORMAT_CACHE.get(Integer.valueOf(precision));
                    if (fmt == null) {
                        fmt = NumberFormat.getNumberInstance();
                        fmt.setMinimumFractionDigits(precision);
                        fmt.setMaximumFractionDigits(precision);
                        FORMAT_CACHE.put(Integer.valueOf(precision), fmt);
                    }
                    if (how == Format.ADAPTIVE_DECIMAL) {
                        fmt.setMinimumFractionDigits(0);
                    }
                }
            }
        }
        StringBuilder buf = new StringBuilder(Math.min(values.length, MAX_FORMAT_VALUE_COUNT) * precision);
        buf.append(formatDouble(fmt, values[0]));
        for (int i = 1; i < values.length; i++) {
            buf.append(Messages.ARRAY_ELEMENTS_SEPARATOR);
            buf.append(formatDouble(fmt, values[i]));
            if (i >= MAX_FORMAT_VALUE_COUNT) {
                buf.append(Messages.ARRAY_ELEMENTS_SEPARATOR);
                buf.append(Messages.DOTS);
                break;
            }
        }
        return buf.toString();
    }

    /**
     * @param fmt NumberFormat or <code>null</code> to use Double.toString
     * @param d Number to format
     * @return String
     */
    private static String formatDouble(final NumberFormat fmt, double d) {
        if (Double.isInfinite(d)) {
            return Messages.INFINITY;
        } else if (Double.isNaN(d)) {
            return Messages.NAN;
        } else if (fmt == null) {
            return NumberFormat.getInstance().format(d);
        } else {
            synchronized (fmt) {
                return fmt.format(d);
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.internal.data.Value#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof DoubleValue)) {
            return false;
        }
        DoubleValue rhs = (DoubleValue) obj;
        if (rhs.values.length != values.length) {
            return false;
        }
        // Compare individual values, using the hint from
        // page 33 of the "Effective Java" book to handle
        // NaN and Infinity

        if (super.equals(obj)) {
            for (int i = 0; i < values.length; ++i) {
                if (Double.doubleToLongBits(values[i]) != Double.doubleToLongBits(rhs.values[i])) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.internal.data.Value#hashCode()
     */
    @Override
    public final int hashCode() {
        return 31 * super.hashCode() + Arrays.hashCode(values);
    }
}
