/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.ejbs;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Stateful;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.esss.ics.rbac.access.AccessDeniedException;
import se.esss.ics.rbac.access.AuthenticationFailedException;
import se.esss.ics.rbac.access.Credentials;
import se.esss.ics.rbac.access.ISecurityFacade;
import se.esss.ics.rbac.access.SecurityCallback;
import se.esss.ics.rbac.access.SecurityCallbackAdapter;
import se.esss.ics.rbac.access.SecurityFacade;
import se.esss.ics.rbac.access.SecurityFacadeException;
import se.esss.ics.rbac.access.Token;
import se.esss.ics.rbac.access.localservice.LocalAuthServiceDetails;
import se.esss.ics.rbac.ejbs.interfaces.RBACAccess;
import se.esss.ics.rbac.jsf.general.Constants;

/**
 * <code>RBACAccessEJB</code> bean is a stateful enterprise java bean, that implements <code>RBACAccess</code>
 * interface.
 * 
 * @author <a href="mailto:jakob.battelino@cosylab.com">Jakob Battelino Prelog</a>
 */
@Stateful
public class RBACAccessEJB implements RBACAccess {

    private static final long serialVersionUID = 7444923554679272314L;
    private static final transient Logger LOGGER = LoggerFactory.getLogger(RBACAccessEJB.class);

    private ISecurityFacade securityFacade;
        
    private String getClientAddress(HttpServletRequest request) {
        String ipAddress = request.getHeader("X-Forwarded-For");
        if (ipAddress == null) {  
            return request.getRemoteAddr();  
        } else {
            String[] ips = ipAddress.split("[,]");
            return ips[0];
        }
    }
    
    private int getClientPort(HttpServletRequest request) {
        String port = request.getHeader("X-Forwarded-Port");
        if (port == null) {  
            return request.getRemotePort();  
        }
        return Integer.parseInt(port);
    }

    @PostConstruct
    private void initialize() {
        securityFacade = new SecurityFacade();
        SecurityCallback callback = new SecurityCallbackAdapter() {
            @Override
            public LocalAuthServiceDetails getLocalAuthServiceDetails() {
                HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance()
                        .getExternalContext().getRequest();
                String address = getClientAddress(request);
                return new LocalAuthServiceDetails(address, address, getClientPort(request));
            }
        };
        securityFacade.setDefaultSecurityCallback(callback);
        securityFacade.initLocalServiceUsage();
    }

    @PreDestroy
    private void destroy() {
        if (securityFacade != null) {
            securityFacade.destroy();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.RBACAccess#authenticate(java.lang.String, char[])
     */
    @Override
    public Token authenticate(final String username, final char[] password) throws SecurityFacadeException {
        SecurityCallback callback = new SecurityCallbackAdapter() {
            @Override
            public Credentials getCredentials() {
                HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance()
                        .getExternalContext().getRequest();                
                char[] nonNullPassword = password == null ? new char[0] : password;
                return new Credentials(username, nonNullPassword, null, getClientAddress(request));
            }

            @Override
            public LocalAuthServiceDetails getLocalAuthServiceDetails() {
                HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance()
                        .getExternalContext().getRequest();
                String address = getClientAddress(request);
                return new LocalAuthServiceDetails(address, address, getClientPort(request));
            }
        };
        securityFacade.setDefaultSecurityCallback(callback);
        return securityFacade.authenticate();
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.RBACAccess#logout()
     */
    @Override
    public boolean logout() throws SecurityFacadeException {
        if (securityFacade.logout()) {
            // on logout updated the components of the main container of the page
            RequestContext.getCurrentInstance().update(Constants.SITE_CONTENT);
            RequestContext.getCurrentInstance().update(Constants.SITE_BANNER);
            RequestContext.getCurrentInstance().update(Constants.SITE_CONTAINER_GROWL);
            return true;
        }
        return false;
    }
    
    /*
     * (non-Javadoc)
     * @see se.esss.ics.rbac.ejbs.interfaces.RBACAccess#setTokenID(char[])
     */
    @Override
    public void setTokenID(char[] tokenID) {
        securityFacade.setToken(tokenID);        
    }

    private void logoutBecauseTokenExpired() {
        try {
            if (logout()) {
                FacesContext.getCurrentInstance().addMessage(
                        null,
                        new FacesMessage(FacesMessage.SEVERITY_WARN, "Sign Out Successful",
                                "You have been signed out, because your token is no longer valid."));
                RequestContext.getCurrentInstance().update(Constants.SITE_CONTAINER_GROWL);
            }
        } catch (SecurityFacadeException e) {
            LOGGER.error("Exception while loging user out.", e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.RBACAccess#getToken()
     */
    @Override
    public Token getToken() {
        SecurityCallback callback = new SecurityCallbackAdapter() {
            @Override
            public LocalAuthServiceDetails getLocalAuthServiceDetails() {
                HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance()
                        .getExternalContext().getRequest();
                String address = getClientAddress(request);
                return new LocalAuthServiceDetails(address, address, getClientPort(request));
            }
        };
        securityFacade.setDefaultSecurityCallback(callback);
        Token token = null;
        try {
            token = securityFacade.getToken();
        } catch (AccessDeniedException | AuthenticationFailedException e) {
            return null;
        } catch (SecurityFacadeException e) {
            //cannot happen
            LOGGER.error("Exception while retrieving token.", e);
        }
        return token;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.RBACAccess#getValidatedToken()
     */
    @Override
    public Token getValidatedToken() {
        if (getToken() != null) {
            try {
                if (securityFacade.isTokenValid()) {
                    return getToken();
                } else {
                    logoutBecauseTokenExpired();
                }
            } catch (SecurityFacadeException e) {
                logoutBecauseTokenExpired();
            }
        }
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.RBACAccess#userHasPermission(java.lang.String, java.lang.String)
     */
    @Override
    public boolean userHasPermission(String permissionName, String resourceName) {
        if (getToken() == null) {
            return false;
        }
        try {
            return securityFacade.hasPermission(resourceName, permissionName);
        } catch (AccessDeniedException e) {
            logoutBecauseTokenExpired();
        } catch (SecurityFacadeException e) {
            LOGGER.error("Exception while checking permissions.", e);
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.RBACAccess#userHasPermissions(java.lang.String[], java.lang.String)
     */
    @Override
    public Map<String, Boolean> userHasPermissions(String[] permissions, String resourceName) {
        Map<String, Boolean> map = new HashMap<>();
        for (String s : permissions) {
            map.put(s, Boolean.FALSE);
        }
        if (getToken() != null) {
            try {
                map.putAll(securityFacade.hasPermissions(resourceName, permissions));
            } catch (AccessDeniedException e) {
                logoutBecauseTokenExpired();
            } catch (SecurityFacadeException e) {
                LOGGER.error("Exception while checking permissions.", e);
            }
        }
        return map;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.RBACAccess#requestExclusiveAccess(java.lang.String, java.lang.String,
     * int)
     */
    @Override
    public boolean requestExclusiveAccess(String resourceName, String permissionName, int durationInMinutes)
            throws SecurityFacadeException {
        if (getToken() == null) {
            return false;
        }
        try {
            return securityFacade.requestExclusiveAccess(resourceName, permissionName, durationInMinutes) != null;
        } catch (AccessDeniedException e) {
            logoutBecauseTokenExpired();
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.RBACAccess#releaseExclusiveAccess(java.lang.String, java.lang.String)
     */
    @Override
    public boolean releaseExclusiveAccess(String resourceName, String permissionName) throws SecurityFacadeException {
        if (getToken() == null) {
            return false;
        }
        try {
            return securityFacade.releaseExclusiveAccess(resourceName, permissionName);
        } catch (AccessDeniedException e) {
            logoutBecauseTokenExpired();
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.RBACAccess#getRBACVersion()
     */
    @Override
    public String getRBACVersion() {
        try {
            return securityFacade.getRBACVersion();
        } catch (SecurityFacadeException e) {
            LOGGER.error("Exception while retrieving RBAC version.", e);
            return "Unknown Version Number";
        }
    }
}
