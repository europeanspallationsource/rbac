/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.converters;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import se.esss.ics.rbac.dsaccess.DirectoryServiceAccessException;
import se.esss.ics.rbac.dsaccess.UserInfo;
import se.esss.ics.rbac.jsf.general.Constants;
import se.esss.ics.rbac.jsf.users.UsersBean;

/**
 * <code>UserConverter</code> is a managed bean
 * (<code>userConverter</code>) which implements {@link Converter} interface. User converter converts between unique
 * user identifier and <code>UserInfo</code> object.
 *
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
@ManagedBean(name = "userConverter")
@RequestScoped
public class UserConverter implements Converter, Serializable {

    private static final long serialVersionUID = -7038390398293597491L;

    @ManagedProperty(value = "#{usersBean}")
    private UsersBean usersBean;

    /*
     * (non-Javadoc)
     *
     * @see javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext,
     * javax.faces.component.UIComponent, java.lang.String)
     */
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String submittedValue) {
        if (submittedValue.trim().isEmpty()) {
            return null;
        } else {
            UserInfo userInfo = null;
            try {
                userInfo = usersBean.getUser(submittedValue);
            } catch (DirectoryServiceAccessException exception) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error",
                        "Not a valid user id."), exception);
            }
            return userInfo;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext,
     * javax.faces.component.UIComponent, java.lang.Object)
     */
    @Override
    public String getAsString(FacesContext facesContext, UIComponent component, Object value) {
        if (value == null || String.valueOf(value).isEmpty()) {
            return Constants.EMPTY_STRING;
        } else {
            return String.valueOf(((UserInfo) value).getUsername());
        }
    }

    /**
     * Sets users bean which is used for retrieving user data from LDAP.
     *
     * @param usersBean users bean
     */
    public void setUsersBean(UsersBean usersBean) {
        this.usersBean = usersBean;
    }
}
