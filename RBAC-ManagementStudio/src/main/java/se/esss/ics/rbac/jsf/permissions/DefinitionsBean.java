/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.permissions;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.esss.ics.rbac.Util;
import se.esss.ics.rbac.datamodel.ExclusiveAccess;
import se.esss.ics.rbac.datamodel.Permission;
import se.esss.ics.rbac.datamodel.Resource;
import se.esss.ics.rbac.datamodel.Role;
import se.esss.ics.rbac.datamodel.Rule;
import se.esss.ics.rbac.ejbs.interfaces.Admin;
import se.esss.ics.rbac.ejbs.interfaces.ManagementStudioLogs;
import se.esss.ics.rbac.ejbs.interfaces.Permissions;
import se.esss.ics.rbac.ejbs.interfaces.Roles;
import se.esss.ics.rbac.jsf.auth.LoginBean;
import se.esss.ics.rbac.jsf.auth.PermissionsBean;
import se.esss.ics.rbac.jsf.general.Constants;
import se.esss.ics.rbac.jsf.general.ManagementStudioExecutionTask;
import se.esss.ics.rbac.jsf.general.ManagementStudioTaskBean;
import se.esss.ics.rbac.jsf.general.Messages;
import se.esss.ics.rbac.jsf.utils.Wildcard;

/**
 * <code>DefinitionsBean</code> is a managed bean (<code>permissionsDefinitionBean</code>), which contains permissions
 * data and methods for executing actions triggered on <code>/Permissions/Definitions</code> page. Definitions bean is
 * view scoped so it lives as long as user interacting with definitions page view.
 *
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@ManagedBean(name = "permissionsDefinitionBean")
@ViewScoped
public class DefinitionsBean implements Serializable {

    private static final long serialVersionUID = -4657224036935545840L;
    private static final transient Logger LOGGER = LoggerFactory.getLogger(DefinitionsBean.class);
    private static final Charset CHARSET = Charset.forName("UTF-8");
    private static final String NO_DESCRIPTION = "<No Description>";

    @ManagedProperty(value = "#{permissionsBean}")
    private PermissionsBean permissionsBean;
    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    @ManagedProperty(value = "#{taskBean}")
    private ManagementStudioTaskBean taskBean;
    @EJB
    private Admin adminEJB;
    @EJB
    private Permissions permissionsEJB;
    @EJB
    private Roles rolesEJB;
    @EJB
    private ManagementStudioLogs managementStudioLogEJB;
    @Inject
    private DefinitionsRequestCacheBean requestCacheBean;

    private String wildcard = Constants.ALL;
    private String newPermissionName;
    private String newPermissionDescription;
    private String updatedPermissionName;
    private String updatedPermissionDescription;
    private Boolean exclusiveAccessAllowed;
    private Resource selectedResource;
    private Permission selectedPermission;
    private Rule selectedRule;
    private Role selectedRole;
    private Date exclusiveAccessDuration;
    private List<Permission> permissions;

    // roles
    private List<Role> rolesSourceList;
    private List<Role> selectedRolesSourceList;
    private List<Role> rolesTargetList;
    private List<Role> selectedRolesTargetList;

    /**
     * Sets default exclusive access duration value. Value is retrieved from database. Method is called post construct.
     */
    @PostConstruct
    private void initialize() {
        rolesSourceList = new ArrayList<Role>();
        rolesTargetList = new ArrayList<Role>();
        resetExclusiveAccessDuration();
    }

    /**
     * Sets permissions bean which contains methods for determining permissions for specific actions for the logged in
     * user.
     *
     * @param permissionsBean the permissions bean through which permission can be checked
     */
    public void setPermissionsBean(PermissionsBean permissionsBean) {
        this.permissionsBean = permissionsBean;
    }

    /**
     * Sets login bean used for checking if a user is logged in.
     *
     * @param loginBean the login bean through which the logged in user info can be retrieved
     */
    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    /**
     * Sets management studio task bean used for executing tasks.
     *
     * @param taskBean the management studio task bean
     */
    public void setTaskBean(ManagementStudioTaskBean taskBean) {
        this.taskBean = taskBean;
    }

    /**
     * Sets wildcard pattern which is used for searching by permissions. Pattern is entered into search field on
     * <code>/Permissions/Definitions</code> page.
     *
     * @param wildcard wildcard pattern
     */
    public void setWildcard(String wildcard) {
        this.wildcard = wildcard;
    }

    /**
     * @return wildcard pattern which is used for searching by permissions. Pattern is entered into search field on
     *         <code>/Permissions/Definitions</code> page.
     */
    public String getWildcard() {
        return wildcard;
    }

    /**
     * @return resource which is selected in drop down list <code>/Permissions/Definitions</code> page.
     */
    public Resource getSelectedResource() {
        return selectedResource;
    }

    /**
     * Sets resource which is selected in drop down list on <code>/Permissions/Definitions</code> page.
     *
     * @param selectedResource selected resource
     */
    public void setSelectedResource(Resource selectedResource) {
        this.selectedResource = selectedResource;
        selectedPermission = null;
        selectedRole = null;
        selectedRule = null;
    }

    /**
     * @return permission which is selected in list box on <code>/Permissions/Definitions</code> page.
     */
    public Permission getSelectedPermission() {
        return selectedPermission;
    }

    /**
     * Sets permission which is selected in list box on <code>/Permissions/Definitions</code> page.
     *
     * @param selectedPermission selected permission
     */
    public void setSelectedPermission(Permission selectedPermission) {
        this.selectedPermission = selectedPermission;
        selectedRole = null;
        selectedRule = null;
    }

    /**
     * @return rule which is selected in drop down list on <code>/Permissions/Definitions</code> page.
     */
    public Rule getSelectedRule() {
        if (isPermissionSelected()) {
            selectedRule = selectedPermission.getRule();
        }
        return selectedRule;
    }

    /**
     * Sets rule which is selected on <code>/Permissions/Definitions</code> page.
     *
     * @param selectedRule selected rule
     */
    public void setSelectedRule(Rule selectedRule) {
        this.selectedRule = selectedRule;
    }

    /**
     * @return role which is selected in roles list on <code>/Permissions/Definitions</code> page.
     */
    public Role getSelectedRole() {
        return selectedRole;
    }

    /**
     * Sets role which is selected in roles list on <code>/Permissions/Definitions</code> page.
     *
     * @param selectedRole selected role
     */
    public void setSelectedRole(Role selectedRole) {
        this.selectedRole = selectedRole;
    }

    /**
     * @return selected permission name which is shown in permission name field on <code>/Permissions/Definitions</code>
     *         page.
     */
    public String getPermissionName() {
        if (isPermissionSelected()) {
            return selectedPermission.getName();
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * Returns new permission name which is entered into permission name field in add new permission dialog on
     * <code>/Permissions/Definitions</code> page.
     *
     * @return new permission name which is entered into permission name field.
     */
    public String getNewPermissionName() {
        return newPermissionName;
    }

    /**
     * Sets new permission name which is entered into permission name field in add new permission dialog on
     * <code>/Permissions/Definitions</code> page.
     *
     * @param newPermissionName new permission name
     */
    public void setNewPermissionName(String newPermissionName) {
        this.newPermissionName = removeSpaces(newPermissionName);
    }

    /**
     * Returns updated permission name which is entered into permission name field in edit permission dialog on
     * <code>/Permissions/Definitions</code> page.
     *
     * @return updated permission name which is entered into permission name field.
     */
    public String getUpdatedPermissionName() {
        return getPermissionName();
    }

    /**
     * Sets updated permission name which is entered into permission name field in edit permission dialog on
     * <code>/Permissions/Definitions</code> page.
     *
     * @param updatedPermissionName updated permission name
     */
    public void setUpdatedPermissionName(String updatedPermissionName) {
        this.updatedPermissionName = removeSpaces(updatedPermissionName);
    }

    /**
     * @return selected permission description which is shown in permission description field on
     *         <code>/Permissions/Definitions</code> page.
     */
    public String getPermissionDescription() {
        if (isPermissionSelected()) {
            return selectedPermission.getDescription();
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * Returns new permission description which is entered into permission description field in add new permission
     * dialog on <code>/Permissions/Definitions</code> page.
     *
     * @return new permission description which is entered into permission description field.
     */
    public String getNewPermissionDescription() {
        return newPermissionDescription;
    }

    /**
     * Sets new permission description which is entered into permission description field in add new permission dialog
     * on <code>/Permissions/Definitions</code> page.
     *
     * @param newPermissionDescription new permission description
     */
    public void setNewPermissionDescription(String newPermissionDescription) {
        this.newPermissionDescription = newPermissionDescription;
    }

    /**
     * Returns updated permission description which is entered into permission description field in edit permission
     * dialog on <code>/Permissions/Definitions</code> page.
     *
     * @return updated permission description which is entered into permission description field.
     */
    public String getUpdatedPermissionDescription() {
        return getPermissionDescription();
    }

    /**
     * Sets updated permission description which is entered into permission description field in edit permission dialog
     * on <code>/Permissions/Definitions</code> page.
     *
     * @param updatedPermissionDescription updated permission description
     */
    public void setUpdatedPermissionDescription(String updatedPermissionDescription) {
        this.updatedPermissionDescription = updatedPermissionDescription;
    }

    /**
     * @return name of the resource which is selected in resource drop down list on
     *         <code>/Permissions/Definitions</code> page.
     */
    public String getResourceName() {
        if (selectedResource != null) {
            return selectedResource.getName();
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * Returns exclusive access duration for the selected permission which is entered in request exclusive access dialog
     * on <code>/Permissions/Definitions</code> page.
     *
     * @return exclusive access duration for the selected permission.
     */
    public Date getExclusiveAccessDuration() {
        return exclusiveAccessDuration;
    }

    /**
     * Sets exclusive access duration for the selected permission which is entered in request exclusive access dialog on
     * <code>/Permissions/Definitions</code> page.
     *
     * @param exclusiveAccessDuration exclusive access duration for the selected permission
     */
    public void setExclusiveAccessDuration(Date exclusiveAccessDuration) {
        this.exclusiveAccessDuration = exclusiveAccessDuration;
    }

    /**
     * @return description of the selected rule which is entered into rule description field on
     *         <code>/Permissions/Definitions</code> page.
     */
    public String getRuleDescription() {
        if (isPermissionSelected() && selectedRule != null) {
            String desc = selectedRule.getDescription();
            return new StringBuilder(500).append(desc == null || desc.isEmpty() ? NO_DESCRIPTION : desc).append(":\n")
                    .append(selectedRule.getDefinition()).toString();
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * @return true if exclusive access is allowed, otherwise false. If returned value is true, check box on
     *         <code>/Permissions/Definitions</code> page is checked otherwise not.
     */
    public Boolean getIsExclusiveAccessAllowed() {
        if (isPermissionSelected()) {
            return selectedPermission.isExclusiveAccessAllowed();
        }
        return Boolean.FALSE;
    }

    /**
     * Sets true if exclusive access for selected permission is allowed or false if not. If value which is set is true,
     * check box on <code>/Permissions/Definitions</code> page is checked otherwise not.
     *
     * @param allowed true if exclusive access is allowed, otherwise false
     */
    public void setIsExclusiveAccessAllowed(Boolean allowed) {
        this.exclusiveAccessAllowed = allowed;
    }

    /**
     * @return unique identifier (username) of the selected permission exclusive access owner. Username is shown in
     *         exclusive access owner field on <code>/Permissions/Definitions</code> page.
     */
    public String getExclusiveAccessOwner() {
        if (isPermissionSelected() && selectedPermission.getExclusiveAccess() != null) {
            return selectedPermission.getExclusiveAccess().getUserId();
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * @return selected permission exclusive access expire date. Date is shown in exclusive access expires field on
     *         <code>/Permissions/Definitions</code> page.
     */
    public String getExclusiveAccessExpireDate() {
        if (isPermissionSelected() && selectedPermission.getExclusiveAccess() != null) {
            ExclusiveAccess exclusiveAccess = selectedPermission.getExclusiveAccess();
            if (exclusiveAccess.getEndTime() != null) {
                return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(exclusiveAccess.getEndTime());
            }
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * Retrieves list of resources from database and returns it. Resources are shown in resources drop down list on
     * <code>/Permissions/Definitions</code> page.
     *
     * @return resources retrieved from database.
     */
    public List<Resource> getResources() {
        return requestCacheBean.getResources();
    }

    /**
     * Retrieves list of permissions which corresponds to the selected resource from database and returns it.
     * Permissions are shown in permissions list on <code>/Permissions/Definitions</code> page.
     *
     * @return permissions which corresponds to the selected resource.
     */
    public List<Permission> getPermissions() {
        if (selectedResource != null) {
            if (Constants.ALL.equals(wildcard)) {
                permissions = requestCacheBean.getPermissionsByResource(selectedResource.getId(), false);
            } else {
                permissions = new ArrayList<>();
                for (Permission permission : requestCacheBean.getPermissionsByResource(
                        selectedResource.getId(), false)) {
                    if (Wildcard.wildcardMatch(permission.getName(), Constants.ALL + wildcard + Constants.ALL)) {
                        permissions.add(permission);
                    }
                }
            }
            if (!Util.contains(permissions, selectedPermission)) {
                selectedPermission = null;
                selectedRole = null;
            }
            return permissions;
        }
        return new ArrayList<>();
    }

    /**
     * Retrieves list of rules from database and returns it. Rules are shown in drop down list on
     * <code>/Permissions/Definitions</code> page.
     *
     * @return rules retrieved from database.
     */
    public List<Rule> getRules() {
        return requestCacheBean.getRules();
    }

    /**
     * @return roles which corresponds to the selected permission. Roles are shown in roles list box on
     *         <code>/Permissions/Definitions</code> page.
     */
    public List<Role> getRoles() {
        if (isPermissionSelected()) {
            return new ArrayList<>(selectedPermission.getRole());
        }
        return new ArrayList<>();
    }

    /**
     * Creates permission if user has permission for managing resources. Method is called when OK button, in add new
     * permission dialog on <code>/Permissions/Definitions</code> page is clicked. Results of the action are logged and
     * shown to the user as growl message.
     */
    public void createPermission() {
        if (!permissionsBean.canManageResource(selectedResource, false)) {
            return;
        }
        final Permission permission = new Permission();
        permission.setDescription(newPermissionDescription);
        permission.setName(newPermissionName);
        permission.setResource(selectedResource);
        String success = Messages.getString(Messages.SUCCESSFUL_PERMISSION_CREATION, loginBean.getUsername(),
                permission.getName(), permission.getResource().getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_PERMISSION_CREATION, loginBean.getUsername(),
                permission.getName(), permission.getResource().getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                permissionsEJB.createPermission(permission);
                selectedPermission = permissionsEJB.getPermissionByName(newPermissionName, selectedResource);
                requestCacheBean.getPermissionsByResource(selectedResource.getId(), true);
            }
        });
        setDefaultValues();
    }

    /**
     * Removes selected permission if user has permission for managing resources. Method is called when remove
     * permission button on <code>/Permissions/Definitions</code> page is clicked and when action is confirmed. Results
     * of the action are logged and shown to the user as growl message.
     */
    public void removePermission() {
        if (!permissionsBean.canManageResource(selectedResource, false) || isRemovePermissionButtonDisabled()) {
            return;
        }
        String success = Messages.getString(Messages.SUCCESSFUL_PERMISSION_REMOVAL, loginBean.getUsername(),
                selectedPermission.getName(), selectedPermission.getResource().getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_PERMISSION_REMOVAL, loginBean.getUsername(),
                selectedPermission.getName(), selectedPermission.getResource().getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                for (Role role : selectedPermission.getRole()) {
                    role = rolesEJB.getRole(role.getId(), false);
                    role.getPermissions().remove(selectedPermission);
                    rolesEJB.updateRole(role);
                }
                permissionsEJB.removePermission(selectedPermission);
                selectedPermission = null;
                requestCacheBean.getPermissionsByResource(selectedResource.getId(), true);
            }
        });
    }

    /**
     * Updates selected permission. Method is called when OK button in edit permission dialog on
     * <code>/Permissions/Definitions</code> page is clicked. Results of the action are logged and shown to the user as
     * growl message.
     */
    public void updatePermission() {
        if (isPermissionSelected()) {
            selectedPermission.setName(updatedPermissionName);
            selectedPermission.setDescription(updatedPermissionDescription);
            String success = Messages.getString(Messages.SUCCESSFUL_PERMISSION_UPDATE, loginBean.getUsername(),
                    selectedPermission.getName(), selectedPermission.getResource().getName());
            String failure = Messages.getString(Messages.UNSUCCESSFUL_PERMISSION_UPDATE, loginBean.getUsername(),
                    selectedPermission.getName(), selectedPermission.getResource().getName());
            taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
                @Override
                public void execute() throws Exception {
                    permissionsEJB.updatePermission(selectedPermission);
                    requestCacheBean.getPermissionsByResource(selectedResource.getId(), true);
                }
            });
        }
    }

    /**
     * Changes permission exclusive access allowed flag if user has permission for managing resources. Method is called
     * when allow exclusive access check box on <code>/Permissions/Definitions</code> page is clicked. Results of the
     * action are logged and shown to the user as growl message.
     */
    public void changeExclusiveAccessAllowed() {
        if (!permissionsBean.canManageResource(selectedResource, false) || isEditPermissionButtonDisabled()) {
            return;
        }
        selectedPermission.setExclusiveAccessAllowed(exclusiveAccessAllowed);
        String success = Constants.EMPTY_STRING;
        if (exclusiveAccessAllowed) {
            success = Messages.getString(Messages.EXCLUSIVE_ACCESS_ALLOWED, loginBean.getUsername(),
                    selectedPermission.getName(), selectedPermission.getResource().getName());
        } else {
            success = Messages.getString(Messages.EXCLUSIVE_ACCESS_NOT_ALLOWED, loginBean.getUsername(),
                    selectedPermission.getName(), selectedPermission.getResource().getName());
        }
        String failure = Messages.getString(Messages.UNSUCCESSFUL_PERMISSION_UPDATE, loginBean.getUsername(),
                selectedPermission.getName(), selectedPermission.getResource().getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                if (!exclusiveAccessAllowed) {
                    removeExclusiveAccess();
                }
                permissionsEJB.updatePermission(selectedPermission);
                requestCacheBean.getPermissionsByResource(selectedResource.getId(), true);
            }
        });
    }

    /**
     * Changes permission rule if user has permission for managing resources. Method is called when new rule from rules
     * drop down list on <code>/Permissions/Definitions</code> page is selected. Results of the action are logged and
     * shown to the user as growl message.
     */
    public void changeRule() {
        if (!permissionsBean.canManageResource(selectedResource, false) || isEditPermissionButtonDisabled()) {
            return;
        }
        selectedPermission.setRule(selectedRule);
        String success = Messages.getString(Messages.SUCCESSFUL_PERMISSION_UPDATE, loginBean.getUsername(),
                selectedPermission.getName(), selectedPermission.getResource().getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_PERMISSION_UPDATE, loginBean.getUsername(),
                selectedPermission.getName(), selectedPermission.getResource().getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                permissionsEJB.updatePermission(selectedPermission);
                requestCacheBean.getPermissionsByResource(selectedResource.getId(), true);
            }
        });
    }

    /**
     * Removes selected role if user has permission for managing resources. Method is called when remove role button on
     * <code>/Permissions/Definitions</code> page is clicked and when action is confirmed. Results of the action are
     * logged and shown to the user as growl message.
     */
    public void removeRole() {
        if (!permissionsBean.canManageResource(selectedResource, false) || isRemoveRoleButtonDisabled()) {
            return;
        }
        selectedRole.getPermissions().remove(selectedPermission);
        String success = Messages.getString(Messages.SUCCESSFUL_ROLE_PERMISSION_REMOVAL, loginBean.getUsername(),
                selectedPermission.getName(), selectedRole.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_ROLE_PERMISSION_REMOVAL, loginBean.getUsername(),
                selectedPermission.getName(), selectedRole.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                rolesEJB.updateRole(selectedRole);
                selectedPermission.getRole().remove(selectedRole);
                selectedRole = null;
            }
        });
    }

    /**
     * Updates selected permission roles, adds roles to the selected permission. Method is called when OK button in add
     * role dialog on <code>/Permissions/Definitions</code> page is clicked. Results of the action are logged and shown
     * to the user as growl message.
     */
    public void updateRoles() {
        if (!permissionsBean.canManageResource(selectedResource, false)) {
            return;
        }
        String success = Messages.getString(Messages.SUCCESSFUL_ROLES_PERMISSION_ASSIGNMENT, loginBean.getUsername(),
                selectedPermission.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_ROLE_PERMISSION_REMOVAL, loginBean.getUsername(),
                selectedPermission.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                for (Role role : rolesTargetList) {
                    if (!role.getPermissions().contains(selectedPermission)) {
                        role.getPermissions().add(selectedPermission);
                        rolesEJB.updateRole(role);
                        selectedPermission.getRole().add(role);
                    }
                }
                selectedRole = null;
                addAllRolesFromTargetToSource();
            }
        });
    }

    /**
     * Creates exclusive access, if user is logged in and if user has permission for requesting exclusive accesses.
     * Method is called when OK button, in request exclusive access dialog on <code>/Permissions/Definitions</code>
     * page, is clicked. Results of the action are logged and shown to the user as growl message.
     */
    public void createExclusiveAccess() {
        if (!permissionsBean.canRequestExclusiveAccessForPermission(selectedPermission, selectedResource, false)) {
            return;
        }
        long rawOffset = TimeZone.getDefault().getRawOffset();
        final long seconds = rawOffset < 0 ? exclusiveAccessDuration.getTime() - rawOffset : exclusiveAccessDuration
                .getTime() + rawOffset;
        String success = Messages.getString(Messages.SUCCESSFUL_EA_REQUEST, loginBean.getUsername(),
                selectedPermission.getName(), selectedPermission.getResource().getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_EA_REQUEST, loginBean.getUsername(),
                selectedPermission.getName(), selectedPermission.getResource().getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                loginBean.getRbacAccessEJB().requestExclusiveAccess(selectedPermission.getResource().getName(),
                        selectedPermission.getName(), (int) TimeUnit.MILLISECONDS.toMinutes(seconds));
                selectedPermission = permissionsEJB.getPermission(selectedPermission.getId());
                requestCacheBean.getPermissionsByResource(selectedResource.getId(), true);
            }
        });
        resetExclusiveAccessDuration();
    }

    /**
     * Removes exclusive access, if user is logged in and if user has permission for releasing exclusive accesses.
     * Method is called when release button on <code>/Permissions/Definitions</code> page is clicked and if action is
     * confirmed. Results of the action are logged and shown to the user as growl message.
     */
    public void removeExclusiveAccess() {
        if (!permissionsBean.canReleaseExclusiveAccessForPermission(selectedPermission, selectedResource, false)
                || isReleaseExclusiveAccessButtonDisabled()) {
            return;
        }
        String success = Messages.getString(Messages.SUCCESSFUL_EA_RELEASE, loginBean.getUsername(),
                selectedPermission.getName(), selectedPermission.getResource().getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_EA_RELEASE, loginBean.getUsername(),
                selectedPermission.getName(), selectedPermission.getResource().getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                loginBean.getRbacAccessEJB().releaseExclusiveAccess(selectedPermission.getResource().getName(),
                        selectedPermission.getName());
                selectedPermission = permissionsEJB.getPermission(selectedPermission.getId());
                requestCacheBean.getPermissionsByResource(selectedResource.getId(), true);
            }
        });
    }

    /**
     * @return true if resource and permission on <code>/Permissions/Definitions</code> page are selected, otherwise
     *         false.
     */
    public boolean isPermissionSelected() {
        return selectedResource != null && selectedPermission != null;
    }

    /**
     * Remove permission button on <code>/Permissions/Definitions</code> page is disabled if permission is not selected.
     *
     * @return true if remove permission button is disabled, otherwise false.
     */
    public boolean isRemovePermissionButtonDisabled() {
        return !isPermissionSelected();
    }

    /**
     * Edit permission button on <code>/Permissions/Definitions</code> page is disabled if permission is not selected.
     *
     * @return true if edit permission button is disabled, otherwise false.
     */
    public boolean isEditPermissionButtonDisabled() {
        return !isPermissionSelected();
    }

    /**
     * Release exclusive access button on <code>/Permissions/Definitions</code> page is disabled if permission is not
     * selected or if exclusive access is not active.
     *
     * @return true if release exclusive access button is disabled, otherwise false.
     */
    public boolean isReleaseExclusiveAccessButtonDisabled() {
        return !isPermissionSelected() || selectedPermission.getExclusiveAccess() == null;
    }

    /**
     * Remove role button on <code>/Permissions/Definitions</code> page is disabled if role or permission is not
     * selected.
     *
     * @return true if remove role button is disabled, otherwise false.
     */
    public boolean isRemoveRoleButtonDisabled() {
        return selectedRole == null || !isPermissionSelected();
    }

    /**
     * Methods that handles file upload. If file ends with <code>.xlsx</code> calls
     * {@link DefinitionsBean#importFromExcelFile(UploadedFile)} method or if file ends with <code>.csv</code>
     * {@link DefinitionsBean#importFromCSVFile(UploadedFile)} method.
     *
     * @param event file upload event
     */
    public void handleFileUpload(FileUploadEvent event) {
        if (!permissionsBean.canManageResource(selectedResource, false)) {
            return;
        }
        UploadedFile file = event.getFile();
        if (file.getFileName().toLowerCase().endsWith(".xlsx")) {
            importFromExcelFile(file);
        } else if (file.getFileName().toLowerCase().endsWith(".csv")) {
            importFromCSVFile(file);
        }
    }

    /**
     * Method that shows dialog for adding permissions. Dialog is shown when add permission button on
     * <code>/Permissions/Definitions</code> page is clicked and if user has permissions for managing resources.
     */
    public void showAddPermissionDialog() {
        if (!permissionsBean.canManageResource(selectedResource, false)) {
            return;
        }
        RequestContext.getCurrentInstance().update("addPermissionForm:addPermissionResourceNameInput");
        RequestContext.getCurrentInstance().execute("PF('addPermissionDialog').show()");
    }

    /**
     * Method that shows dialog for editing permissions. Dialog is shown when edit permission button on
     * <code>/Permissions/Definitions</code> page is clicked and if user has permissions for managing resources.
     */
    public void showEditPermissionDialog() {
        if (!permissionsBean.canManageResource(selectedResource, false) || isEditPermissionButtonDisabled()) {
            return;
        }
        RequestContext.getCurrentInstance().update("editPermissionForm");
        RequestContext.getCurrentInstance().execute("PF('editPermissionDialog').show()");
    }

    /**
     * Method that shows dialog for requesting exclusive access. Dialog is shown when request exclusive access button on
     * <code>/Permissions/Definitions</code> page is clicked and if user has permissions for requesting exclusive
     * access.
     */
    public void showRequestAccessDialog() {
        if (!permissionsBean.canRequestExclusiveAccessForPermission(selectedPermission, selectedResource, false)) {
            return;
        }
        RequestContext.getCurrentInstance().update("requestExclusiveAccessForm");
        RequestContext.getCurrentInstance().execute("PF('requestAccessDialog').show()");
    }

    /**
     * Method that shows dialog for adding roles. Dialog is shown when add roles button on
     * <code>/Permissions/Definitions</code> page is clicked and if user has permissions for managing resources.
     */
    public void showAddRolesDialog() {
        if (!permissionsBean.canManageResource(selectedResource, false)) {
            return;
        }
        RequestContext.getCurrentInstance().execute("PF('addRolesDialog').show()");
    }

    /**
     * Method that shows dialog for importing permissions. Dialog is shown when import permission button on
     * <code>/Permissions/Definitions</code> page is clicked and if user has permissions for managing resources.
     */
    public void showImportPermissionsDialog() {
        if (!permissionsBean.canManageResource(selectedResource, false)) {
            return;
        }
        RequestContext.getCurrentInstance().execute("PF('importPermissionsDialog').show()");
    }

    /**
     * Resets exclusive access duration to the default exclusive access duration. This method is called when new
     * exclusive access from request exclusive access dialog on <code>/Users/Definitions</code> page is created or if
     * cancel button on the same dialog is clicked.
     */
    public void resetExclusiveAccessDuration() {
        try {
            long duration = adminEJB.getExclusiveAccessExpirationPeriod();
            long rawOffset = TimeZone.getDefault().getRawOffset();
            duration = rawOffset < 0 ? duration + rawOffset : duration - rawOffset;
            exclusiveAccessDuration = new Date(duration);
            setDefaultValues();
        } catch (Exception e) {
            LOGGER.error(Messages.getString(Messages.EA_RESET_EXCEPTION), e);
        }
    }

    /**
     * Resets <code>newPermissionName</code> and <code>newPermissionDescription</code> variables to the default values.
     */
    public void setDefaultValues() {
        newPermissionName = Constants.EMPTY_STRING;
        newPermissionDescription = Constants.EMPTY_STRING;
    }

    /**
     * Returns list which contains roles from source list and is shown on add roles dialog on
     * <code>/Permissions/Definitions</code> page.
     *
     * @return list which contains roles from source list.
     */
    public List<Role> getRolesSourceList() {
        rolesSourceList = requestCacheBean.getRoles();
        if (rolesSourceList == null) {
            rolesSourceList = new ArrayList<Role>();
        } else {
            rolesSourceList.removeAll(rolesTargetList);
        }
        return rolesSourceList;
    }

    /**
     * Sets list which contains roles from source list and is shown on add roles dialog on
     * <code>/Permissions/Definitions</code> page.
     *
     * @param rolesSourceList list which contains roles from source list
     */
    public void setRolesSourceList(List<Role> rolesSourceList) {
        this.rolesSourceList = rolesSourceList;
    }

    /**
     * Returns list which contains roles from target list and is shown on add roles dialog on
     * <code>/Permissions/Definitions</code> page.
     *
     * @return list which contains roles from target list.
     */
    public List<Role> getRolesTargetList() {
        return rolesTargetList;
    }

    /**
     * Sets list which contains roles from target list and is shown on add roles dialog on
     * <code>/Permissions/Definitions</code> page.
     *
     * @param rolesTargetList list which contains roles from target list
     */
    public void setRolesTargetList(List<Role> rolesTargetList) {
        this.rolesTargetList = rolesTargetList;
    }

    /**
     * Returns list which contains selected roles from source list and is shown on add roles dialog on
     * <code>/Permissions/Definitions</code> page.
     *
     * @return list which contains selected roles from source list.
     */
    public List<Role> getSelectedRolesSourceList() {
        return selectedRolesSourceList;
    }

    /**
     * Sets list which contains selected roles from source list and is shown on add roles dialog on
     * <code>/Permissions/Definitions</code> page.
     *
     * @param selectedRolesSourceList list which contains selected roles from source list
     */
    public void setSelectedRolesSourceList(List<Role> selectedRolesSourceList) {
        this.selectedRolesSourceList = selectedRolesSourceList;
    }

    /**
     * Returns list which contains selected roles from target list and is shown on add roles dialog on
     * <code>/Permissions/Definitions</code> page.
     *
     * @return list which contains selected roles from target list.
     */
    public List<Role> getSelectedRolesTargetList() {
        return selectedRolesTargetList;
    }

    /**
     * Sets list which contains selected roles from target list, and is shown on add roles dialog on
     * <code>/Permissions/Definitions</code> page.
     *
     * @param selectedRolesTargetList list which contains selected roles from target list
     */
    public void setSelectedRolesTargetList(List<Role> selectedRolesTargetList) {
        this.selectedRolesTargetList = selectedRolesTargetList;
    }

    /**
     * Moves selected roles from source list to the target list. Lists are shown on the select roles dialog on
     * <code>/Permissions/Definitions</code> page. page.
     */
    public void addRolesFromSourceToTarget() {
        if (!selectedRolesSourceList.isEmpty()) {
            rolesTargetList.addAll(selectedRolesSourceList);
        }
        rolesSourceList.removeAll(selectedRolesSourceList);
        selectedRolesSourceList.clear();
    }

    /**
     * Moves selected roles from target list to the source list. Lists are shown on the select roles dialog on
     * <code>/Permissions/Definitions</code> page. page.
     */
    public void addRolesFromTargetToSource() {
        if (!selectedRolesTargetList.isEmpty()) {
            rolesSourceList.addAll(selectedRolesTargetList);
        }
        rolesTargetList.removeAll(selectedRolesTargetList);
        selectedRolesTargetList.clear();
    }

    /**
     * Moves all roles from source list to the target list. Lists are shown on the select roles dialog on
     * <code>/Permissions/Definitions</code> page. page.
     */
    public void addAllRolesFromSourceToTarget() {
        if (!rolesSourceList.isEmpty()) {
            rolesTargetList.addAll(rolesSourceList);
        }
        rolesSourceList.clear();
        selectedRolesSourceList.clear();
    }

    /**
     * Moves all roles from target list to the source list. Lists are shown on the select roles dialog on
     * <code>/Permissions/Definitions</code> page. page.
     */
    public void addAllRolesFromTargetToSource() {
        if (!rolesTargetList.isEmpty()) {
            rolesSourceList.addAll(rolesTargetList);
        }
        rolesTargetList.clear();
        selectedRolesTargetList.clear();
    }

    /**
     * Generates excel file which format is defined as: |Name|Description|Resource|Exclusive Access Allowed|.
     *
     * @return generated excel file.
     */
    public StreamedContent getExcelFile() {
        if (permissions == null) {
            return null;
        }
        try (XSSFWorkbook workbook = new XSSFWorkbook()) {
            XSSFSheet sheet = workbook.createSheet("Permissions");
            int rowNumber = 0;
            Row row = sheet.createRow(rowNumber++);
            row.createCell(0).setCellValue("Name");
            row.createCell(1).setCellValue("Description");
            row.createCell(2).setCellValue("Resource");
            row.createCell(3).setCellValue("Exclussive Access Allowed");
            for (Permission permission : permissions) {
                row = sheet.createRow(rowNumber++);
                row.createCell(0).setCellValue(permission.getName());
                row.createCell(1).setCellValue(
                        permission.getDescription() == null ? Constants.EMPTY_STRING : permission.getDescription());
                row.createCell(2).setCellValue(permission.getResource().getName());
                row.createCell(3).setCellValue(permission.isExclusiveAccessAllowed());
            }
            // auto size columns
            for (int i = 0; i < 4; i++) {
                sheet.autoSizeColumn(i);
            }
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);
            InputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
            StreamedContent sc = new DefaultStreamedContent(inputStream,
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "permissions.xlsx");
            taskBean.showFacesMessage(FacesMessage.SEVERITY_INFO,
                    Messages.getString(Messages.SUCCESSFUL_PERMISSION_EXPORT, loginBean.getUsername()),
                    Constants.EMPTY_STRING, true);
            return sc;
        } catch (IOException e) {
            LOGGER.error(Messages.getString(Messages.EXPORT_IO_EXCEPTION), e);
        }

        return null;
    }

    /**
     * Generates CSV file which format is defined as: Name,Description,Resource,Exclusive Access Allowed.
     *
     * @return generated CSV file.
     */
    public StreamedContent getCSVFile() {
        if (permissions == null) {
            return null;
        }
        StringBuilder content = new StringBuilder(60 + permissions.size() * 150)
                .append("Name,Description,Resource,Exclussive Access Allowed\n");
        for (Permission permission : permissions) {
            content.append(permission.getName())
                    .append(',')
                    .append((permission.getDescription() == null) ? Constants.EMPTY_STRING : permission
                            .getDescription()).append(',').append(permission.getResource().getName()).append(',')
                    .append(permission.isExclusiveAccessAllowed()).append('\n');
        }
        InputStream inputStream = new ByteArrayInputStream(content.toString().getBytes(CHARSET));
        StreamedContent sc = new DefaultStreamedContent(inputStream, "text/csv", "permissions.csv");
        managementStudioLogEJB.info(loginBean.getUsername(), loginBean.getUsername()
                + " successfully exported permissions as csv file.");
        return sc;
    }

    /**
     * Imports permissions from excel file. Imports only permissions which resource name is equals to the selected
     * resource name. Excel file format is defined as:
     *
     * |Name|Description|Resource|Exclusive Access Allowed|
     *
     * @param uploadedFile excel file from which permissions will be imported
     */
    private void importFromExcelFile(UploadedFile uploadedFile) {
        try (XSSFWorkbook workbook = new XSSFWorkbook(uploadedFile.getInputstream())) {
            XSSFSheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rowIterator = sheet.iterator();
            // jump over first row - headings
            if (rowIterator.hasNext()) {
                rowIterator.next();
            }
            int createdPermissions = 0;
            int allPermissions = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                if (row.getCell(0) != null) {
                    Permission permission = new Permission();
                    permission.setName(row.getCell(0).getStringCellValue());
                    permission.setDescription((row.getCell(1) == null) ? Constants.EMPTY_STRING : row.getCell(1)
                            .getStringCellValue());
                    if (row.getCell(2) == null || selectedResource.getName().equals(row.getCell(2)
                            .getStringCellValue())) {
                        permission.setResource(selectedResource);
                    }
                    permission.setExclusiveAccessAllowed((row.getCell(3) == null) ? false : row.getCell(3)
                            .getBooleanCellValue());
                    try {
                        permissionsEJB.createPermission(permission);
                        createdPermissions++;
                        managementStudioLogEJB.info(loginBean.getUsername(),
                                loginBean.getUsername() + " successfully created permission " + permission.getName()
                                + "(" + permission.getResource() + ").");
                    } catch (Exception e) {
                        managementStudioLogEJB.error(loginBean.getUsername(),
                                loginBean.getUsername() + " unsuccessfully created permission " + permission.getName()
                                        + "(" + permission.getResource() + ").");
                        // log exception
                    }
                }
                allPermissions++;
            }
            if (allPermissions == createdPermissions) {
                FacesContext.getCurrentInstance().addMessage(
                        null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "All permissions were successfully created.",
                                Constants.EMPTY_STRING));
            } else {
                FacesContext.getCurrentInstance().addMessage(
                        null,
                        new FacesMessage(FacesMessage.SEVERITY_WARN, "Created Permissions:" + createdPermissions + "/"
                                + allPermissions, Constants.EMPTY_STRING));
            }
        } catch (IOException e) {
            LOGGER.error(Messages.getString(Messages.IMPORT_IO_EXCEPTION), e);
        }
    }

    /**
     * Imports permissions from CSV file. Imports only permissions which resource name is equals to the selected
     * resource name. CSV file format is defined as:
     *
     * Name,Description,Resource,Exclusive Access Allowed
     *
     * @param uploadedFile CSV file from which permissions will be imported
     */
    private void importFromCSVFile(UploadedFile file) {
        int createdPermissions = 0;
        int allPermissions = 0;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(file.getInputstream(), CHARSET))) {
            String line = br.readLine();
            while ((line = br.readLine()) != null) {
                String[] permissionData = line.split(Constants.SPLIT_COMMA);
                int permissionDataLength = permissionData.length;
                if (permissionDataLength > 0 && !permissionData[0].isEmpty()) {
                    Permission permission = new Permission();
                    permission.setName(permissionData[0]);
                    permission.setDescription(permissionDataLength > 1 ? permissionData[1] : Constants.EMPTY_STRING);
                    if ((permissionDataLength > 2 && selectedResource.getName().equals(permissionData[2]))
                            || permissionData[2].isEmpty()) {
                        permission.setResource(selectedResource);
                    } else if (permissionDataLength < 2) {
                        permission.setResource(selectedResource);
                    }
                    permission.setExclusiveAccessAllowed(permissionDataLength > 3 ? Boolean.valueOf(permissionData[3])
                            : false);
                    try {
                        permissionsEJB.createPermission(permission);
                        createdPermissions++;
                        managementStudioLogEJB.info(loginBean.getUsername(),
                                loginBean.getUsername() + " successfully created permission " + permission.getName()
                                        + "(" + permission.getResource() + ").");
                    } catch (Exception e) {
                        managementStudioLogEJB.error(loginBean.getUsername(),
                                loginBean.getUsername() + " unsuccessfully created permission " + permission.getName()
                                        + "(" + permission.getResource() + ").");
                    }
                }
                allPermissions++;
            }
            if (allPermissions == createdPermissions) {
                FacesContext.getCurrentInstance().addMessage(
                        null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "All permissions were successfully created.",
                                Constants.EMPTY_STRING));
            } else {
                FacesContext.getCurrentInstance().addMessage(
                        null,
                        new FacesMessage(FacesMessage.SEVERITY_WARN, "Created Permissions:" + createdPermissions + "/"
                                + allPermissions, Constants.EMPTY_STRING));
            }
        } catch (IOException e) {
            LOGGER.error(Messages.getString(Messages.IMPORT_IO_EXCEPTION), e);
        }
    }

    /**
     * Removes spaces from permission name.
     *
     * @param permissionName permission name
     *
     * @return permission name without spaces.
     */
    private static String removeSpaces(String permissionName) {
        return permissionName.replaceAll("\\p{Z}", Constants.EMPTY_STRING);
    }
}
