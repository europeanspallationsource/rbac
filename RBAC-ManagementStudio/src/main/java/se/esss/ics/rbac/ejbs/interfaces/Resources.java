/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.ejbs.interfaces;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;

import se.esss.ics.rbac.datamodel.Resource;

/**
 * <code>Resources</code> interface defines methods for dealing with resources.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@Local
public interface Resources extends Serializable {

    /**
     * Retrieves resource identified by <code>resourceId</code> from database and returns it.
     * 
     * @param resourceId unique resource identifier
     * 
     * @return specific resource identified by id.
     */
    Resource getResource(int resourceId);

    /**
     * Retrieves resource whose name matches the given <code>resourceName</code> and returns it.
     * 
     * @param resourceName resource name
     * 
     * @return resource whose name matches the given resource name.
     */
    Resource getResourceByName(String resourceName);

    /**
     * @return list of the resources retrieved from database.
     */
    List<Resource> getResources();

    /**
     * @return list of the resource names retrieved from database.
     */
    List<String> getResourceNames();

    /**
     * Retrieves all resources whose name matches the <code>wildcard</code> pattern from database and returns it.
     * 
     * @param wildcard wildcard pattern
     * 
     * @return list of the resources whose name matches the wildcard pattern.
     */
    List<Resource> getResourcesByWildcard(String wildcard);

    /**
     * Inserts created resource into database.
     * 
     * @param resource created resource
     */
    void createResource(Resource resource);

    /**
     * Removes resource from database.
     * 
     * @param resource resource which will be removed from database
     */
    void removeResource(Resource resource);

    /**
     * Updates resource.
     * 
     * @param resource updated resource
     */
    void updateResource(Resource resource);
}
