/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.ejbs.interfaces;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import se.esss.ics.rbac.datamodel.Role;

/**
 * <code>Roles</code> interface defines methods for dealing with roles.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
@Local
public interface Roles extends Serializable {

    /**
     * Retrieves role identified by <code>roleId</code> from database and returns it.
     * 
     * @param roleId unique role identifier
     * @param loadASRules true if the lazy set of access security rules should be fetched as well
     * 
     * @return specific role identified by id.
     */
    Role getRole(int roleId, boolean loadASRules);

    /**
     * Retrieves role whose name matches the given <code>name</code> and returns it.
     * 
     * @param name role name
     * 
     * @return role whose name matches the given role name.
     */
    Role getRoleByName(String name);

    /**
     * @return list of roles retrieved from database.
     */
    List<Role> getRoles();
    
    /**
     * Retrieves all roles whose name matches the <code>wildcard</code> pattern from database and returns it.
     * 
     * @param wildcard wildcard pattern
     * 
     * @return list of the roles whose name matches the wildcard pattern.
     */
    List<Role> getRolesByWildcard(String wildcard);

    /**
     * Inserts created role into database.
     * 
     * @param role created role
     */
    void createRole(Role role);

    /**
     * Removes role from database.
     * 
     * @param roleId id of the role which will be removed from database
     */
    void removeRole(int roleId);

    /**
     * Updates role.
     * 
     * @param role updated role
     */
    void updateRole(Role role);

    /**
     * Adds permissions from <code>permissions</code> map which boolean value is <code>true</code> to the role
     * identified by <code>roleId</code>.
     * 
     * @param roleId unique role identifier
     * @param permissions map of permissions with permission id as a key
     */
    void addPermissions(int roleId, Map<Integer, Boolean> permissions);

    /**
     * @param roleId role id
     * @param permissionId permission id
     * 
     * @return true if role contains permission
     */
    boolean containsPermission(int roleId, int permissionId);
}
