/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.users;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.AjaxBehaviorEvent;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.esss.ics.rbac.datamodel.ExclusiveAccess;
import se.esss.ics.rbac.datamodel.Permission;
import se.esss.ics.rbac.datamodel.Resource;
import se.esss.ics.rbac.datamodel.Role;
import se.esss.ics.rbac.datamodel.UserRole;
import se.esss.ics.rbac.datamodel.UserRole.AssignmentType;
import se.esss.ics.rbac.dsaccess.DirectoryServiceAccessException;
import se.esss.ics.rbac.dsaccess.UserInfo;
import se.esss.ics.rbac.ejbs.interfaces.Admin;
import se.esss.ics.rbac.ejbs.interfaces.Permissions;
import se.esss.ics.rbac.ejbs.interfaces.UserRoles;
import se.esss.ics.rbac.jsf.auth.LoginBean;
import se.esss.ics.rbac.jsf.auth.PermissionsBean;
import se.esss.ics.rbac.jsf.general.Constants;
import se.esss.ics.rbac.jsf.general.ManagementStudioExecutionTask;
import se.esss.ics.rbac.jsf.general.ManagementStudioProperties;
import se.esss.ics.rbac.jsf.general.ManagementStudioTaskBean;
import se.esss.ics.rbac.jsf.general.Messages;
import se.esss.ics.rbac.jsf.utils.MailService;

/**
 * <code>UserInfoBean</code> is a managed bean (<code>userInfoBean</code>), which contains data about users and methods
 * for executing actions triggered on users page (<code>/Users</code>). User info bean is view scoped so it lives as
 * long as user interacting with users page view.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
@ManagedBean(name = "userInfoBean")
@ViewScoped
public class UserInfoBean implements Serializable {

    private static final long serialVersionUID = 3644178768625899229L;
    private static final transient Logger LOGGER = LoggerFactory.getLogger(UserInfoBean.class);

    @ManagedProperty(value = "#{usersBean}")
    private UsersBean usersBean;
    @ManagedProperty(value = "#{permissionsBean}")
    private PermissionsBean permissionsBean;
    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    @ManagedProperty(value = "#{taskBean}")
    private ManagementStudioTaskBean taskBean;
    @EJB
    private Admin adminEJB;
    @EJB
    private Permissions permissionsEJB;
    @EJB
    private UserRoles userRolesEJB;
    @EJB
    private MailService mailServiceEJB;

    private int newRoleAssignmentType = 1; // AssignmentType.NORMAL
    private int editRoleAssignmentType = 1;
    private boolean notifyByEmail;
    private String wildcard = "*";
    private String delegateRoleTo;
    private Date exclusiveAccessDuration;
    private Date newRoleAssignmentStartTime;
    private Date newRoleAssignmentEndTime;
    private Date editRoleAssignmentStartTime;
    private Date editRoleAssignmentEndTime;
    private Date delegateRoleStartTime;
    private Date delegateRoleEndTime;
    private UserInfo selectedUser;
    private UserRole selectedAssignedRole;
    private ExclusiveAccess selectedActiveExclusiveAccess;
    private Resource selectedResource;
    private Role selectedRole;

    // permissions
    private List<Permission> permissionsSourceList;
    private List<Permission> selectedPermissionsSourceList;
    private List<Permission> permissionsTargetList;
    private List<Permission> selectedPermissionsTargetList;

    /**
     * Initialises empty permissions dual list component which is used in request exclusive access dialog and shows all
     * permissions which belongs to the selected resource. Method is called post construct.
     */
    @PostConstruct
    private void initialize() {
        permissionsSourceList = new ArrayList<Permission>();
        permissionsTargetList = new ArrayList<Permission>();
        resetExclusiveAccessDuration();
    }

    /**
     * Sets users bean which contains methods for retrieving users from LDAP.
     * 
     * @param usersBean users bean
     */
    public void setUsersBean(UsersBean usersBean) {
        this.usersBean = usersBean;
    }

    /**
     * Sets permissions bean which contains methods for determining permissions for specific actions for the logged in
     * user.
     * 
     * @param permissionsBean the permissions bean through which permission can be checked
     */
    public void setPermissionsBean(PermissionsBean permissionsBean) {
        this.permissionsBean = permissionsBean;
    }

    /**
     * Sets login bean used for checking if a user is logged in.
     * 
     * @param loginBean the login bean through which the logged in user info can be retrieved
     */
    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    /**
     * Sets management studio task bean used for executing tasks.
     * 
     * @param taskBean the management studio task bean
     */
    public void setTaskBean(ManagementStudioTaskBean taskBean) {
        this.taskBean = taskBean;
    }

    /**
     * Sets wildcard pattern which is used for searching by users. Pattern is entered into search field on
     * <code>/Users/Definitions</code> page.
     * 
     * @param wildcard wildcard pattern
     */
    public void setWildcard(String wildcard) {
        this.wildcard = wildcard;
    }

    /**
     * @return wildcard pattern which is used for searching by users. Pattern is entered into search field on
     *         <code>/Users/Definitions</code> page.
     */
    public String getWildcard() {
        return wildcard;
    }

    /**
     * @return new assigned role start time. After this time role became active. Assignment start time is entered in
     *         assign new role dialog on <code>/Users/Definitions</code> page.
     */
    public Date getNewRoleAssignmentStartTime() {
        return newRoleAssignmentStartTime;
    }

    /**
     * Sets new assigned role start time. After this time role became active. Assignment start time is entered in assign
     * new role dialog on <code>/Users/Definitions</code> page.
     * 
     * @param newRoleAssignmentStartTime new assigned role start time.
     */
    public void setNewRoleAssignmentStartTime(Date newRoleAssignmentStartTime) {
        this.newRoleAssignmentStartTime = newRoleAssignmentStartTime;
    }

    /**
     * @return new assigned role end time. After this time role will not be active anymore. Assignment end time is
     *         entered in assign new role dialog on <code>/Users/Definitions</code> page.
     */
    public Date getNewRoleAssignmentEndTime() {
        return newRoleAssignmentEndTime;
    }

    /**
     * Sets new assigned role end time. After this time role will not be active anymore. Assignment end time is entered
     * in assign new role dialog on <code>/Users/Definitions</code> page.
     * 
     * @param newRoleAssignmentEndTime assigned role end time
     */
    public void setNewRoleAssignmentEndTime(Date newRoleAssignmentEndTime) {
        this.newRoleAssignmentEndTime = newRoleAssignmentEndTime;
    }

    /**
     * @return edited assigned role start time. After this time role became active. Assignment start time is entered in
     *         edit assigned role dialog on <code>/Users/Definitions</code> page.
     */
    public Date getEditRoleAssignmentStartTime() {
        if (selectedAssignedRole != null && selectedAssignedRole.getAssignment() != AssignmentType.NORMAL) {
            return selectedAssignedRole.getStartTime();
        }
        return null;
    }

    /**
     * Sets edited assigned role start time. After this time role became active. Assignment start time is entered in
     * edit assigned role dialog on <code>/Users/Definitions</code> page.
     * 
     * @param editRoleAssignmentStartTime edited assigned role start time.
     */
    public void setEditRoleAssignmentStartTime(Date editRoleAssignmentStartTime) {
        this.editRoleAssignmentStartTime = editRoleAssignmentStartTime;
    }

    /**
     * @return edited assigned role end time. After this time role will not be active anymore. Assignment end time is
     *         entered in edit assigned role dialog on <code>/Users/Definitions</code> page.
     */
    public Date getEditRoleAssignmentEndTime() {
        if (selectedAssignedRole != null && selectedAssignedRole.getAssignment() != AssignmentType.NORMAL) {
            return selectedAssignedRole.getEndTime();
        }
        return null;
    }

    /**
     * Sets edited assigned role end time. After this time role will not be active anymore. Assignment end time is
     * entered in edit assigned role dialog on <code>/Users/Definitions</code> page.
     * 
     * @param editRoleAssignmentEndTime assigned role end time
     */
    public void setEditRoleAssignmentEndTime(Date editRoleAssignmentEndTime) {
        this.editRoleAssignmentEndTime = editRoleAssignmentEndTime;
    }

    /**
     * @return delegated assigned role start time. After this time role became active. Assignment start time is entered
     *         in delegate assigned role dialog on <code>/Users/Definitions</code> page.
     */
    public Date getDelegateRoleStartTime() {
        return delegateRoleStartTime;
    }

    /**
     * Sets delegated assigned role start time. After this time role became active. Assignment start time is entered in
     * delegate assigned role dialog on <code>/Users/Definitions</code> page.
     * 
     * @param delegateRoleStartTime delegated assigned role start time.
     */
    public void setDelegateRoleStartTime(Date delegateRoleStartTime) {
        this.delegateRoleStartTime = delegateRoleStartTime;
    }

    /**
     * @return delegated assigned role end time. After this time role will not be active anymore. Assignment end time is
     *         entered in delegate assigned role dialog on <code>/Users/Definitions</code> page.
     */
    public Date getDelegateRoleEndTime() {
        return delegateRoleEndTime;
    }

    /**
     * Sets delegated assigned role end time. After this time role will not be active anymore. Assignment end time is
     * entered in delegate assigned role dialog on <code>/Users/Definitions</code> page.
     * 
     * @param delegateRoleEndTime assigned role end time
     */
    public void setDelegateRoleEndTime(Date delegateRoleEndTime) {
        this.delegateRoleEndTime = delegateRoleEndTime;
    }

    /**
     * Sets exclusive access duration which is entered in request exclusive access duration dialog on
     * <code>/Users/Definitions</code> page.
     * 
     * @param exclusiveAccessDuration exclusive access duration
     */
    public void setExclusiveAccessDuration(Date exclusiveAccessDuration) {
        this.exclusiveAccessDuration = exclusiveAccessDuration;
    }

    /**
     * @return exclusive access duration which is entered in request exclusive access duration dialog on
     *         <code>/Users/Definitions</code> page.
     */
    public Date getExclusiveAccessDuration() {
        return exclusiveAccessDuration;
    }

    /**
     * Sets the username (unique user identifier) which is selected in delegate role dialog on
     * <code>/Users/Definitions</code> page. Role will be delegated to user with selected username.
     * 
     * @param delegateRoleTo unique user identifier
     */
    public void setDelegateRoleTo(String delegateRoleTo) {
        this.delegateRoleTo = delegateRoleTo;
    }

    /**
     * @return username (unique user identifier) which is selected in delegate role dialog on
     *         <code>/Users/Definitions</code> page. Role will be delegated to user with selected username.
     */
    public String getDelegateRoleTo() {
        if (selectedAssignedRole != null && selectedAssignedRole.getAssignment() == AssignmentType.DELEGATED) {
            delegateRoleTo = selectedAssignedRole.getDelegator();
        }
        return delegateRoleTo;
    }

    /**
     * Sets role which is selected in assign new role dialog on <code>/Users/Definitions</code> page.
     * 
     * @param selectedRole selected role
     */
    public void setSelectedRole(Role selectedRole) {
        this.selectedRole = selectedRole;
    }

    /**
     * @return role which is selected in assign new role dialog on <code>/Users/Definitions</code> page.
     */
    public Role getSelectedRole() {
        if (selectedAssignedRole != null) {
            selectedRole = selectedAssignedRole.getRole();
        }
        return selectedRole;
    }

    /**
     * Returns selected role assignment type as <code>Integer</code>.
     * 
     * <ul>
     * <li>1 - <code>AssignmentType.NORMAL</code></li>
     * <li>2 - <code>AssignmentType.LIMITED</code></li>
     * <li>else - <code>AssignmentType.DELEGATED</code></li>
     * </ul>
     * 
     * @return selected role assignment type represented as integer.
     */
    public int getEditRoleAssignmentType() {
        int type = 0;
        if (selectedAssignedRole != null) {
            if (selectedAssignedRole.getAssignment() == AssignmentType.NORMAL) {
                type = 1;
            } else if (selectedAssignedRole.getAssignment() == AssignmentType.LIMITED) {
                type = 2;
            } else {
                type = 3;
            }
        }
        setEditRoleAssignmentType(type);
        return type;
    }

    /**
     * Sets edit role assignment type represented as integer.
     * 
     * @param editRoleAssignmentType role assignment type
     */
    public void setEditRoleAssignmentType(int editRoleAssignmentType) {
        this.editRoleAssignmentType = editRoleAssignmentType;
    }

    /**
     * @return true if date fields in edit assigned role dialog on <code>/Users/Definitions</code> page, are disabled,
     *         otherwise false. Date fields are disabled (not editable) if role assignment type is
     *         <code>AssignmentType.NORMAL</code> or <code>AssignmentType.DELEGATED</code>.
     */
    public boolean getDateFieldsDisabled() {
        return editRoleAssignmentType != 2;
    }

    /**
     * @return assignment type which is used in assign new role dialog. If assignment type is
     *         <code>AssignmentType.NORMAL</code> date fields are disabled.
     */
    public int getNewRoleAssignmentType() {
        return newRoleAssignmentType;
    }

    /**
     * Sets new role assignment type represented as integer.
     * 
     * @param newRoleAssignmentType role assignment type
     */
    public void setNewRoleAssignmentType(int newRoleAssignmentType) {
        this.newRoleAssignmentType = newRoleAssignmentType;
    }

    /**
     * Sets flag which tells if user should be notified by email about his new role. Value is retrieved from notify by
     * email checkbox which is placed in dialogs on <code>/Users/Definitions</code> page.
     * 
     * @param notifyByEmail if true user will be notified by email otherwise not
     */
    public void setNotifyByEmail(boolean notifyByEmail) {
        this.notifyByEmail = notifyByEmail;
    }

    /**
     * @return value of the flag which tells if user should be notified by email about his new role.
     */
    public boolean getNotifyByEmail() {
        return notifyByEmail;
    }

    /**
     * @return user which is selected on <code>/Users/Definitions</code> page.
     */
    public UserInfo getSelectedUser() {
        return selectedUser;
    }

    /**
     * Sets user which is selected on <code>/Users/Definitions</code> page.
     * 
     * @param selectedUser selected user
     */
    public void setSelectedUser(UserInfo selectedUser) {
        this.selectedUser = selectedUser;
    }

    /**
     * @return resource which is selected in request exclusive access dialog on <code>/Users/Definitions</code> page..
     */
    public Resource getSelectedResource() {
        return selectedResource;
    }

    /**
     * Sets resource which is selected in request exclusive access dialog on <code>/Users/Definitions</code> page.
     * 
     * @param selectedResource selected resource
     */
    public void setSelectedResource(Resource selectedResource) {
        this.selectedResource = selectedResource;
    }

    /**
     * @return selected assigned role from assigned role data table or <code>null</code> if no user or no assigned role
     *         is selected.
     */
    public UserRole getSelectedAssignedRole() {
        if (selectedUser != null && selectedAssignedRole != null) {
            return selectedAssignedRole;
        }
        return null;
    }

    /**
     * @return list of users from directory service, which corresponds to the wildcard pattern, or an empty list if
     *         <code>DirectoryServiceAccessException</code> occurs. Users are shown in list box on
     *         <code>/Users/Definitions</code> page.
     */
    public List<UserInfo> getUsersList() {
        try {
            List<UserInfo> users = usersBean.getUsersList(wildcard);
            if (!users.contains(selectedUser)) {
                selectedUser = null;
            }
            return users;
        } catch (DirectoryServiceAccessException e) {
            LOGGER.error(Messages.getString(Messages.LDAP_EXCEPTION), e);
        }
        return new ArrayList<>();
    }

    /**
     * @return list of all users from directory service or an empty list if <code>DirectoryServiceAccessException</code>
     *         occurs. Users are shown in drop down component in delegate role dialog on <code>/Users/Definitions</code>
     *         page.
     */
    public List<UserInfo> getAllUsers() {
        try {
            return usersBean.getUsersList(Constants.ALL);
        } catch (DirectoryServiceAccessException e) {
            LOGGER.error(Messages.getString(Messages.LDAP_EXCEPTION), e);
        }
        return new ArrayList<>();
    }

    /**
     * Method is called when row in the assigned roles data table is selected. <code>SelectEvent</code> is triggered.
     * 
     * @param event the event that triggered this action
     */
    public void onAssignedRoleRowSelect(SelectEvent event) {
        if (event.getObject() != null && event.getObject() instanceof UserRole) {
            selectedAssignedRole = (UserRole) event.getObject();
        }
    }

    /**
     * Method is called when row in the assigned roles data table is unselected. <code>UnselectEvent</code> is
     * triggered.
     * 
     * @param event the event that triggered this action
     */
    public void onAssignedRoleRowUnselect(UnselectEvent event) {
        selectedAssignedRole = null;
    }

    /**
     * Method is called when row in the active exclusive access data table is selected. <code>SelectEvent</code> is
     * triggered.
     * 
     * @param event the event that triggered this action
     */
    public void onExclusiveAccessRowSelect(SelectEvent event) {
        if (event.getObject() != null && event.getObject() instanceof ExclusiveAccess) {
            selectedActiveExclusiveAccess = (ExclusiveAccess) event.getObject();
        }
    }

    /**
     * Method is called when row in the active exclusive access data table is unselected. <code>UnselectEvent</code> is
     * triggered.
     * 
     * @param event the event that triggered this action
     */
    public void onExclusiveAccessRowUnselect(UnselectEvent event) {
        selectedActiveExclusiveAccess = null;
    }

    /**
     * Assigns selected new role to the selected user, if user has permission for managing roles. Method is called when
     * OK button in add role dialog on <code>/Users/Definitions</code> page is clicked. Results of the action are logged
     * and shown to the user as growl message.
     */
    public void assignNewRole() {
        if (!permissionsBean.canManageRole(selectedRole, false) || isAddRoleButtonDisabled()) {
            return;
        }
        AssignmentType assignment = (newRoleAssignmentType == 1) ? AssignmentType.NORMAL : AssignmentType.LIMITED;
        final UserRole userRole = new UserRole();
        userRole.setUserId(selectedUser.getUsername());
        userRole.setRole(selectedRole);
        userRole.setAssignment(assignment);
        if (assignment == AssignmentType.LIMITED) {
            if (areDatesInvalid(newRoleAssignmentStartTime, newRoleAssignmentEndTime)) {
                taskBean.showFacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString(Messages.INVALID_DATES),
                        Constants.EMPTY_STRING, false);
                return;
            }
            userRole.setStartTime(new Timestamp(newRoleAssignmentStartTime.getTime()));
            userRole.setEndTime(new Timestamp(newRoleAssignmentEndTime.getTime()));
        } else {
            userRole.setStartTime(null);
            userRole.setEndTime(null);
        }
        String success = Messages.getString(Messages.SUCCESSFUL_ROLE_ASSIGNMENT, loginBean.getUsername(),
                userRole.getUserId(), userRole.getRole().getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_ROLE_ASSIGNMENT, loginBean.getUsername(),
                userRole.getUserId(), userRole.getRole().getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                userRolesEJB.createUserRole(userRole);
                sendEmail(usersBean.getUser(userRole.getUserId()).getEMail(), userRole, false, notifyByEmail);
            }
        });
        setDefaultValues();
    }

    /**
     * Delegates selected assigned role to other user if user has permission for delegating roles. Method is called when
     * OK button, in delegate role dialog on <code>/Users/Definitions</code> page, is clicked. Results of the action are
     * logged and shown to the user as growl message.
     */
    public void delegateRole() {
        if (!loginBean.isLoggedInValidated() || isDelegateRoleButtonDisabled()
                || !permissionsBean.canDelegateUserRole(selectedAssignedRole)) {
            return;
        }
        if (areDatesInvalid(delegateRoleStartTime, delegateRoleEndTime)) {
            taskBean.showFacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString(Messages.INVALID_DATES),
                    Constants.EMPTY_STRING, false);
            return;
        }
        final UserRole userRole = new UserRole();
        userRole.setDelegator(selectedAssignedRole.getUserId());
        userRole.setUserId(delegateRoleTo);
        userRole.setAssignment(AssignmentType.DELEGATED);
        userRole.setStartTime(new Timestamp(delegateRoleStartTime.getTime()));
        userRole.setEndTime(new Timestamp(delegateRoleEndTime.getTime()));
        userRole.setRole(selectedAssignedRole.getRole());
        String success = Messages.getString(Messages.SUCCESSFUL_ROLE_DELEGATION, userRole.getDelegator(),
                userRole.getUserId(), userRole.getRole().getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_ROLE_DELEGATION, userRole.getDelegator(),
                userRole.getUserId(), userRole.getRole().getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                userRolesEJB.createUserRole(userRole);
                sendEmail(usersBean.getUser(userRole.getUserId()).getEMail(), userRole, false, notifyByEmail);
            }
        });
        setDefaultValues();
    }

    /**
     * Edits selected assigned role, if user has permission for managing roles. Method is called when OK button, in edit
     * role dialog on <code>/Users/Definitions</code> page, is clicked. Results of the action are logged and shown to
     * the user as growl message.
     */
    public void editRole() {
        if (!permissionsBean.canManageRole(selectedAssignedRole.getRole(), false) || isEditRoleButtonDisabled()) {
            return;
        }
        if (selectedAssignedRole.getAssignment() == AssignmentType.DELEGATED) {
            if (areDatesInvalid(editRoleAssignmentStartTime, editRoleAssignmentEndTime)) {
                taskBean.showFacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString(Messages.INVALID_DATES),
                        Constants.EMPTY_STRING, false);
                return;
            }
            selectedAssignedRole.setStartTime(new Timestamp(editRoleAssignmentStartTime.getTime()));
            selectedAssignedRole.setEndTime(new Timestamp(editRoleAssignmentEndTime.getTime()));
        } else {
            AssignmentType assignment = (editRoleAssignmentType == 1) ? AssignmentType.NORMAL : AssignmentType.LIMITED;
            selectedAssignedRole.setAssignment(assignment);
            if (assignment == AssignmentType.LIMITED) {
                if (areDatesInvalid(editRoleAssignmentStartTime, editRoleAssignmentEndTime)) {
                    taskBean.showFacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString(Messages.INVALID_DATES),
                            Constants.EMPTY_STRING, false);
                    return;
                }
                selectedAssignedRole.setStartTime(new Timestamp(editRoleAssignmentStartTime.getTime()));
                selectedAssignedRole.setEndTime(new Timestamp(editRoleAssignmentEndTime.getTime()));
            } else {
                selectedAssignedRole.setStartTime(null);
                selectedAssignedRole.setEndTime(null);
            }
        }
        String success = Messages.getString(Messages.SUCCESSFUL_ROLE_EDIT, loginBean.getUsername(),
                selectedAssignedRole.getRole().getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_ROLE_EDIT, loginBean.getUsername(),
                selectedAssignedRole.getRole().getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                userRolesEJB.updateUserRole(selectedAssignedRole);
                sendEmail(usersBean.getUser(selectedAssignedRole.getUserId()).getEMail(), selectedAssignedRole, true,
                        notifyByEmail);
            }
        });
        setDefaultValues();
    }

    /**
     * Removes assigned role, if user has permission for managing roles. Method is called when remove button on
     * <code>/Users/Definitions</code> page is clicked and if action is confirmed. Results of the action are logged and
     * shown to the user as growl message.
     */
    public void removeAssignedRole() {
        if (selectedAssignedRole == null || !permissionsBean.canManageRole(selectedAssignedRole.getRole(), false)
                || isRemoveRoleButtonDisabled()) {
            return;
        }
        String success = Messages.getString(Messages.SUCCESSFUL_ROLE_ASSIGNMENT_REMOVAL, loginBean.getUsername(),
                selectedAssignedRole.getUserId(), selectedAssignedRole.getRole().getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_ROLE_ASSIGNMENT_REMOVAL, loginBean.getUsername(),
                selectedAssignedRole.getUserId(), selectedAssignedRole.getRole().getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                userRolesEJB.removeUserRole(selectedAssignedRole);
                selectedAssignedRole = null;
            }
        });
    }

    /**
     * Creates exclusive access, if user is logged in and if user has permission for requesting exclusive accesses.
     * Method is called when OK button, in request exclusive access dialog on <code>/Users/Definitions</code> page, is
     * clicked. Results of the action are logged and shown to the user as growl message.
     */
    public void requestExclusiveAccess() {
        if (!loginBean.isLoggedInValidated() || isRequestExclusiveAccessButtonDisabled()
                || !permissionsBean.canRequestExclusiveAccessForUser(selectedUser)) {
            return;
        }
        long rawOffset = TimeZone.getDefault().getRawOffset();
        final long seconds = rawOffset < 0 ? exclusiveAccessDuration.getTime() - rawOffset : exclusiveAccessDuration
                .getTime() + rawOffset;

        for (final Permission permission : permissionsTargetList) {
            String success = Messages.getString(Messages.SUCCESSFUL_EA_REQUEST, loginBean.getUsername(),
                    permission.getName(), permission.getResource().getName());
            String failure = Messages.getString(Messages.UNSUCCESSFUL_EA_REQUEST, loginBean.getUsername(),
                    permission.getName(), permission.getResource().getName());
            taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
                @Override
                public void execute() throws Exception {
                    loginBean.getRbacAccessEJB().requestExclusiveAccess(permission.getResource().getName(),
                            permission.getName(), (int) TimeUnit.MILLISECONDS.toMinutes(seconds));
                }
            });
        }
        resetExclusiveAccessDuration();
        setDefaultValues();
    }

    /**
     * Removes exclusive access, if user is logged in and if user has permission for releasing exclusive accesses.
     * Method is called when release button on <code>/Users/Definitions</code> page is clicked and if action is
     * confirmed. Results of the action are logged and shown to the user as growl message.
     */
    public void releaseActiveExclusiveAccess() {
        if (!permissionsBean.canReleaseExclusiveAccessForUser(selectedUser, false)
                || isReleaseExclusiveAccessButtonDisabled() || !loginBean.isLoggedInValidated()) {
            return;
        }
        String success = Messages.getString(Messages.SUCCESSFUL_EA_RELEASE, loginBean.getUsername(),
                selectedActiveExclusiveAccess.getPermission().getName(), selectedActiveExclusiveAccess.getPermission()
                        .getResource().getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_EA_RELEASE, loginBean.getUsername(),
                selectedActiveExclusiveAccess.getPermission().getName(), selectedActiveExclusiveAccess.getPermission()
                        .getResource().getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                loginBean.getRbacAccessEJB().releaseExclusiveAccess(
                        selectedActiveExclusiveAccess.getPermission().getResource().getName(),
                        selectedActiveExclusiveAccess.getPermission().getName());
                selectedActiveExclusiveAccess = null;
            }
        });
    }

    /**
     * Add role button on <code>/Users/Definitions</code> page is disabled if user is not selected.
     * 
     * @return true if add role button is disabled, otherwise false.
     */
    public boolean isAddRoleButtonDisabled() {
        return selectedUser == null || !loginBean.isLoggedIn();
    }

    /**
     * Remove role button on <code>/Users/Definitions</code> page is disabled if user is not selected or if assigned
     * role in assigned roles table is not selected.
     * 
     * @return true if remove role button is disabled, otherwise false.
     */
    public boolean isRemoveRoleButtonDisabled() {
        return selectedUser == null || selectedAssignedRole == null;
    }

    /**
     * Edit role button on <code>/Users/Definitions</code> page is disabled if user is not selected or if assigned role
     * in assigned roles table is not selected.
     * 
     * @return true if edit role button is disabled, otherwise false.
     */
    public boolean isEditRoleButtonDisabled() {
        return selectedUser == null || selectedAssignedRole == null;
    }

    /**
     * Delegate role button on <code>/Users/Definitions</code> page is disabled if user is not selected or if assigned
     * role in assigned roles table is not selected.
     * 
     * @return true if delegate role button is disabled, otherwise false.
     */
    public boolean isDelegateRoleButtonDisabled() {
        return selectedUser == null || selectedAssignedRole == null;
    }

    /**
     * Request exclusive access button on <code>/Users/Definitions</code> page is disabled if user is not selected.
     * 
     * @return true if request exclusive access button is disabled, otherwise false.
     */
    public boolean isRequestExclusiveAccessButtonDisabled() {
        return selectedUser == null;
    }

    /**
     * Release exclusive access button on <code>/Users/Definitions</code> page is disable if user is not selected or if
     * active exclusive access in active exclusive access table is not selected.
     * 
     * @return true if release exclusive access button is disabled, otherwise false.
     */
    public boolean isReleaseExclusiveAccessButtonDisabled() {
        return selectedUser == null || selectedActiveExclusiveAccess == null;
    }

    /**
     * Notify buttons in dialogs on <code>/Users/Definitions</code> page are disabled if <code>KEY_MAIL_NOTIFY</code>
     * property is false. Property is read from <code>mail.properties</code> file.
     * 
     * @return true if notify buttons (checkboxes) are disabled, otherwise false.
     */
    public boolean isNotifyButtonDisabled() {
        return !ManagementStudioProperties.isMailNotificationEnabled();
    }

    /**
     * Method that shows dialog for new role assignment. Dialog is shown when add button on
     * <code>/Users/Definitions</code> page is clicked.
     */
    public void showAssignNewRoleDialog() {
        if (isAddRoleButtonDisabled() || !loginBean.isLoggedInValidated()) {
            return;
        }
        RequestContext.getCurrentInstance().update("assignNewRoleForm:assignToInput");
        RequestContext.getCurrentInstance().execute("PF('assignNewRoleDialog').show()");
    }

    /**
     * Method that shows dialog for role editing. Dialog is shown when edit button on <code>/Users/Definitions</code>
     * page is clicked and if user has permissions for managing roles.
     */
    public void showEditRoleDialog() {
        if (!permissionsBean.canManageRole(selectedAssignedRole.getRole(), false) || isEditRoleButtonDisabled()) {
            return;
        }
        RequestContext.getCurrentInstance().update("editRoleForm:editAssignToInput");
        RequestContext.getCurrentInstance().execute("PF('editRoleDialog').show()");
    }

    /**
     * Method that shows dialog for role delegating. Dialog is shown when delegate button on
     * <code>/Users/Definitions</code> page is clicked and if user has permissions for delegating user roles.
     */
    public void showDelegateRoleDialog() {
        if (!loginBean.isLoggedInValidated() || isDelegateRoleButtonDisabled()
                || !permissionsBean.canDelegateUserRole(selectedAssignedRole)) {
            return;
        }
        RequestContext.getCurrentInstance().update("delegateRoleForm:delegatorInput");
        RequestContext.getCurrentInstance().execute("PF('delegateRoleDialog').show()");
    }

    /**
     * Method that shows dialog for exclusive access request. Dialog is shown when request button on
     * <code>/Users/Definitions</code> page is clicked and if user has permissions for requesting exclusive access.
     */
    public void showRequestAccessDialog() {
        if (!loginBean.isLoggedInValidated() || isRequestExclusiveAccessButtonDisabled()
                || !permissionsBean.canRequestExclusiveAccessForUser(selectedUser)) {
            return;
        }
        RequestContext.getCurrentInstance().update("requestExclusiveAccessForm");
        RequestContext.getCurrentInstance().execute("PF('requestAccessDialog').show()");
    }

    /**
     * Method that shows warning message if user checks notify check box in add new role dialog or in edit role dialog
     * or in delegate role dialog on <code>/Users/Definition</code> page.
     * 
     * @param e event which is fired if user checks notify check box
     */
    public void showWarning(AjaxBehaviorEvent e) {
        if (notifyByEmail) {
            taskBean.showFacesMessage(FacesMessage.SEVERITY_WARN, Messages.getString(Messages.EMAIL_WARNING),
                    Constants.EMPTY_STRING, false);
        }
    }

    /**
     * Deselects assigned role and active exclusive access if they are selected. This method is called when user changed
     * user selection in users list box on <code>/Users/Definitions</code> page
     */
    public void releaseSelection() {
        selectedAssignedRole = null;
        selectedActiveExclusiveAccess = null;
    }

    /**
     * Resets all values used in dialogs on <code>/Users/Definitions</code> page to the default ones, cleans dialogs
     * fields.
     */
    public void setDefaultValues() {
        newRoleAssignmentType = 1; // AssignmentType.NORMAL
        editRoleAssignmentType = 1;
        notifyByEmail = false;
        delegateRoleTo = null;
        newRoleAssignmentStartTime = null;
        newRoleAssignmentEndTime = null;
        delegateRoleStartTime = null;
        delegateRoleEndTime = null;
        editRoleAssignmentStartTime = null;
        editRoleAssignmentEndTime = null;
        selectedResource = null;
        selectedRole = null;
        permissionsSourceList = new ArrayList<Permission>();
        permissionsTargetList = new ArrayList<Permission>();
        selectedPermissionsSourceList = new ArrayList<Permission>();
        selectedPermissionsTargetList = new ArrayList<Permission>();
    }

    /**
     * Resets exclusive access duration to the default exclusive access duration. This method is called when new
     * exclusive access from request exclusive access dialog on <code>/Users/Definitions</code> page is created or if
     * cancel button on the same dialog is clicked.
     */
    public void resetExclusiveAccessDuration() {
        try {
            long duration = adminEJB.getExclusiveAccessExpirationPeriod();
            long rawOffset = TimeZone.getDefault().getRawOffset();
            duration = rawOffset < 0 ? duration + rawOffset : duration - rawOffset;
            exclusiveAccessDuration = new Date(duration);
            setDefaultValues();
        } catch (Exception e) {
            LOGGER.error(Messages.getString(Messages.EA_RESET_EXCEPTION), e);
        }
    }

    /**
     * Returns list which contains permissions from source list and is shown on add permissions dialog on
     * <code>/Users/Definition</code> page.
     * 
     * @return list which contains permissions from source list.
     */
    public List<Permission> getPermissionsSourceList() {
        if (selectedResource != null && selectedUser != null) {
            permissionsSourceList = permissionsEJB.getPermissionsByResourceAndUser(selectedResource,
                    selectedUser.getUsername());
            if (permissionsSourceList == null) {
                permissionsSourceList = new ArrayList<>();
            } else {
                permissionsSourceList.removeAll(permissionsTargetList);
            }
        }
        return permissionsSourceList;
    }

    /**
     * Sets list which contains permissions from source list and is shown on add assignees dialog on
     * <code>/Users/Definition</code> page.
     * 
     * @param permissionsSourceList list which contains permissions from source list
     */
    public void setPermissionsSourceList(List<Permission> permissionsSourceList) {
        this.permissionsSourceList = permissionsSourceList;
    }

    /**
     * Returns list which contains permissions from target list and is shown on add assignees dialog on
     * <code>/Users/Definition</code> page.
     * 
     * @return list which contains permissions from target list.
     */
    public List<Permission> getPermissionsTargetList() {
        return permissionsTargetList;
    }

    /**
     * Sets list which contains permissions from target list and is shown on add assignees dialog on
     * <code>/Users/Definition</code> page.
     * 
     * @param permissionsTargetList list which contains permissions from target list
     */
    public void setPermissionsTargetList(List<Permission> permissionsTargetList) {
        this.permissionsTargetList = permissionsTargetList;
    }

    /**
     * Returns list which contains selected permissions from source list and is shown on add assignees dialog on
     * <code>/Users/Definition</code> page.
     * 
     * @return list which contains selected permissions from source list.
     */
    public List<Permission> getSelectedPermissionsSourceList() {
        return selectedPermissionsSourceList;
    }

    /**
     * Sets list which contains selected permissions from source list and is shown on add assignees dialog on
     * <code>/Users/Definition</code> page.
     * 
     * @param selectedPermissionsSourceList list which contains selected permissions from source list
     */
    public void setSelectedPermissionsSourceList(List<Permission> selectedPermissionsSourceList) {
        this.selectedPermissionsSourceList = selectedPermissionsSourceList;
    }

    /**
     * Returns list which contains selected permissions from target list and is shown on add assignees dialog on
     * <code>/Users/Definition</code> page.
     * 
     * @return list which contains selected permissions from target list.
     */
    public List<Permission> getSelectedPermissionsTargetList() {
        return selectedPermissionsTargetList;
    }

    /**
     * Sets list which contains selected permissions from target list, and is shown on add assignees dialog on
     * <code>/Users/Definition</code> page.
     * 
     * @param selectedPermissionsTargetList list which contains selected permissions from target list
     */
    public void setSelectedPermissionsTargetList(List<Permission> selectedPermissionsTargetList) {
        this.selectedPermissionsTargetList = selectedPermissionsTargetList;
    }

    /**
     * Moves selected permissions from source list to the target list. Lists are shown on add permissions dialog on
     * <code>/Users/Definition</code> page.
     */
    public void addPermissionsFromSourceToTarget() {
        if (!selectedPermissionsSourceList.isEmpty()) {
            permissionsTargetList.addAll(selectedPermissionsSourceList);
        }
        permissionsSourceList.removeAll(selectedPermissionsSourceList);
        selectedPermissionsSourceList.clear();
    }

    /**
     * Moves selected permissions from target list to the source list. Lists are shown on add permissions dialog on
     * <code>/Users/Definition</code> page.
     */
    public void addPermissionsFromTargetToSource() {
        if (!selectedPermissionsTargetList.isEmpty()) {
            permissionsSourceList.addAll(selectedPermissionsTargetList);
        }
        permissionsTargetList.removeAll(selectedPermissionsTargetList);
        selectedPermissionsTargetList.clear();
    }

    /**
     * Moves all permissions from source list to the target list. Lists are shown on add permissions dialog on
     * <code>/Users/Definition</code> page.
     */
    public void addAllPermissionsFromSourceToTarget() {
        if (!permissionsSourceList.isEmpty()) {
            permissionsTargetList.addAll(permissionsSourceList);
        }
        permissionsSourceList.clear();
        selectedPermissionsSourceList.clear();
    }

    /**
     * Moves all permissions from target list to the source list. Lists are shown on add permissions dialog on
     * <code>/Users/Definition</code> page.
     */
    public void addAllPermissionsFromTargetToSource() {
        if (!permissionsTargetList.isEmpty()) {
            permissionsSourceList.addAll(permissionsTargetList);
        }
        permissionsTargetList.clear();
        selectedPermissionsTargetList.clear();
    }

    /**
     * Generates email, about role assignment or about role changes, and send it through <code>SendEmailService</code>.
     * Mail is send successfully if <code>send</code> flag is true and if <code>to</code> field is valid.
     * 
     * @param to to whom email is sent
     * @param userRole informations about role assignment
     * @param edit parameter is true if role was edited, otherwise false
     * @param send parameter is true if email should be send, otherwise false
     */
    private void sendEmail(String to, UserRole userRole, boolean edit, boolean send) {
        if (send) {
            String[] emailData = mailServiceEJB.generateDefaultContent(userRole, edit);
            mailServiceEJB.send("miha.novak@cosylab.com", emailData[0], emailData[1]);
        }
    }

    /**
     * Checks if the end time is greater than start time and if end time is in the future. If both conditions are met,
     * dates are valid, otherwise they are invalid.
     * 
     * @return true if dates are invalid, otherwise false.
     */
    private static boolean areDatesInvalid(Date startDate, Date endDate) {
        if (startDate == null || endDate == null) {
            return true;
        }
        return endDate.getTime() < System.currentTimeMillis() || endDate.getTime() < startDate.getTime();
    }

    /**
     * Class which holds user permission data (permission name, resource name and role names). Data are shown in
     * permissions table on <code>/Users/Permission</code> page.
     */
    public static class UserPermission implements Comparable<UserPermission> {

        private final String permissionName;
        private final String resourceName;
        private String roleName;

        /**
         * Constructs new <code>UserPermission</code> object.
         * 
         * @param permissionName permission name
         * @param resourceName resource name
         * @param roleName role name
         */
        public UserPermission(String permissionName, String resourceName, String roleName) {
            this.permissionName = permissionName;
            this.resourceName = resourceName;
            this.roleName = roleName;
        }

        /**
         * @return permission name which is shown in user permissions table on <code>/Users/Permission</code> page.
         */
        public String getPermissionName() {
            return permissionName;
        }

        /**
         * @return resource name which is shown in user permissions table on <code>/Users/Permission</code> page..
         */
        public String getResourceName() {
            return resourceName;
        }

        /**
         * @return role name which is shown in user permissions table on <code>/Users/Permission</code> page.
         */
        public String getRoleName() {
            return roleName;
        }

        /**
         * Merge this permission with the given one. Permissions can be merged if they represent the same permission,
         * but are assigned to different roles.
         * 
         * @param p the permission to merge with this permission
         */
        public void merge(UserPermission p) {
            roleName += ", " + p.getRoleName();
        }

        /*
         * (non-Javadoc)
         * 
         * @see java.lang.Comparable#compareTo(java.lang.Object)
         */
        @Override
        public int compareTo(UserPermission o) {
            int r = resourceName.compareTo(o.resourceName);
            return r == 0 ? permissionName.compareTo(o.permissionName) : r;
        }

        /*
         * (non-Javadoc)
         * 
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            return Objects.hash(permissionName, resourceName);
        }

        /*
         * (non-Javadoc)
         * 
         * @see java.lang.Object#equals(java.lang.Object)
         */
        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            } else if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            UserPermission other = (UserPermission) obj;
            return Objects.equals(permissionName, other.permissionName)
                    && Objects.equals(resourceName, other.resourceName);
        }
    }
}
