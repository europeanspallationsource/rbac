/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.utils;

import java.io.Serializable;

import javax.ejb.Local;

import se.esss.ics.rbac.datamodel.UserRole;

/**
 * 
 * <code>MailService</code> is a local service that is responsible for generating mail content and sending mails.
 *
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
@Local
public interface MailService extends Serializable {

    /**
     * Sends email to the given recipient with the given subject and content. All other needed properties such as who is
     * sending the email, SMTP host, SMTP port are retrieved from properties file
     * 
     * @param to recipient's email
     * @param subject subject of the email
     * @param content content of the email
     * 
     * @return true if email was successfully send, otherwise false.
     */
    boolean send(String to, String subject, String content);

    /**
     * Generates default email content about role assignment or about role changes.
     * 
     * @param userRole informations about role assignment
     * @param edit parameter is true if role was edited, otherwise false
     * @return an array of length two, where the first element represents the subject and the second represents the
     *         content
     */
    String[] generateDefaultContent(UserRole userRole, boolean edit);
}
