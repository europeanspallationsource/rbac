/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.utils;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

/**
 * 
 * <code>Wildcard</code> is a utility class, which provides a wildcard string matcher. It provides a matcher, which
 * matches a given string to the wildcard string, which contains '*' or '?' as a character or string replacement.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 * <p>Origin of code: Apache Commons IO</p>
 * 
 * 
 */
public final class Wildcard {

    private static final String ALL_STRING = "*";
    private static final String ANY_STRING = "?";
    private static final char ANY = '?';
    private static final char ALL = '*';

    private Wildcard() {
    }

    /**
     * Splits a string into a number of tokens. The text is split by '?' and '*'. Where multiple '*' occur consecutively
     * they are collapsed into a single '*'.
     * 
     * @param text the text to split
     * @return the array of tokens, never null
     */
    private static String[] splitOnTokens(String text) {
        // used by wildcardMatch
        // package level so a unit test may run on this

        if (text.indexOf(ANY) == -1 && text.indexOf(ALL) == -1) {
            return new String[] { text };
        }

        char[] array = text.toCharArray();
        List<String> list = new ArrayList<>();
        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            if (array[i] == ANY || array[i] == ALL) {
                if (buffer.length() != 0) {
                    list.add(buffer.toString());
                    buffer.setLength(0);
                }
                if (array[i] == ANY) {
                    list.add(ANY_STRING);
                } else if (list.isEmpty() || (i > 0 && !ALL_STRING.equals(list.get(list.size() - 1)))) {
                    list.add(ALL_STRING);
                }
            } else {
                buffer.append(array[i]);
            }
        }
        if (buffer.length() != 0) {
            list.add(buffer.toString());
        }

        return list.toArray(new String[list.size()]);
    }

    /**
     * Checks a filename to see if it matches the specified wildcard matcher allowing control over case-sensitivity.
     * <p>
     * The wildcard matcher uses the characters '?' and '*' to represent a single or multiple (zero or more) wildcard
     * characters. N.B. the sequence "*?" does not work properly at present in match strings.
     * 
     * @param filename the filename to match on
     * @param wildcardMatcher the wildcard string to match against
     * @return true if the filename matches the wildcard string
     * @since Commons IO 1.3
     */
    public static boolean wildcardMatch(String filename, String wildcardMatcher) {
        if (filename == null && wildcardMatcher == null) {
            return true;
        }
        if (filename == null || wildcardMatcher == null) {
            return false;
        }

        String[] wcs = splitOnTokens(wildcardMatcher);
        boolean anyChars = false;
        int textIdx = 0;
        int wcsIdx = 0;
        Deque<int[]> backtrack = new ArrayDeque<>();

        // loop around a backtrack stack, to handle complex * matching
        do {
            if (!backtrack.isEmpty()) {
                int[] array = backtrack.pop();
                wcsIdx = array[0];
                textIdx = array[1];
                anyChars = true;
            }

            // loop whilst tokens and text left to process
            while (wcsIdx < wcs.length) {

                if (ANY_STRING.equals(wcs[wcsIdx])) {
                    // ? so move to next text char
                    textIdx++;
                    if (textIdx > filename.length()) {
                        break;
                    }
                    anyChars = false;

                } else if (ALL_STRING.equals(wcs[wcsIdx])) {
                    // set any chars status
                    anyChars = true;
                    if (wcsIdx == wcs.length - 1) {
                        textIdx = filename.length();
                    }

                } else {
                    // matching text token
                    if (anyChars) {
                        // any chars then try to locate text token
                        textIdx = checkIndexOf(filename, textIdx, wcs[wcsIdx]);
                        if (textIdx == -1) {
                            // token not found
                            break;
                        }
                        int repeat = checkIndexOf(filename, textIdx + 1, wcs[wcsIdx]);
                        if (repeat >= 0) {
                            backtrack.push(new int[] { wcsIdx, repeat });
                        }
                    } else {
                        // matching from current position
                        if (!checkRegionMatches(filename, textIdx, wcs[wcsIdx])) {
                            // couldnt match token
                            break;
                        }
                    }

                    // matched text token, move text index to end of matched token
                    textIdx += wcs[wcsIdx].length();
                    anyChars = false;
                }

                wcsIdx++;
            }

            // full match
            if (wcsIdx == wcs.length && textIdx == filename.length()) {
                return true;
            }

        } while (!backtrack.isEmpty());

        return false;
    }
    
    /**
     * Checks if one string contains another starting at a specific index using the case-sensitivity rule.
     * <p>
     * This method mimics parts of {@link String#indexOf(String, int)} but takes case-sensitivity into account.
     * 
     * @param str the string to check, not null
     * @param strStartIndex the index to start at in str
     * @param search the start to search for, not null
     * @return the first index of the search String, -1 if no match or <code>null</code> string input
     * @throws NullPointerException if either string is null
     * @since Commons IO 2.0
     */
    public static int checkIndexOf(String str, int strStartIndex, String search) throws NullPointerException {
        int endIndex = str.length() - search.length();
        if (endIndex >= strStartIndex) {
            for (int i = strStartIndex; i <= endIndex; i++) {
                if (checkRegionMatches(str, i, search)) {
                    return i;
                }
            }
        }
        return -1;
    }

    /**
     * Checks if one string contains another at a specific index using the case-sensitivity rule.
     * <p>
     * This method mimics parts of {@link String#regionMatches(boolean, int, String, int, int)} but takes
     * case-sensitivity into account.
     * 
     * @param str the string to check, not null
     * @param strStartIndex the index to start at in str
     * @param search the start to search for, not null
     * @return true if equal using the case rules
     * @throws NullPointerException if either string is null
     */
    public static boolean checkRegionMatches(String str, int strStartIndex, String search) throws NullPointerException {
        return str.regionMatches(true, strStartIndex, search, 0, search.length());
    }
}
