/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.ejbs;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.primefaces.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.esss.ics.rbac.ConfigurationParameter;
import se.esss.ics.rbac.ejbs.interfaces.Admin;
import se.esss.ics.rbac.jsf.utils.PasswordEncryptionService;
import se.esss.ics.rbac.pvaccess.AccessSecurityRule.Permission;

/**
 * <code>AdminEJB</code> is a stateless bean containing utility methods for dealing with administrator authentication
 * and changing system settings. All methods are defined in <code>Admin</code> interface.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@Stateless
public class AdminEJB implements Admin {

    private static final long serialVersionUID = -1165432903839324256L;

    // configuration parameters names
    public static final String PARAM_ADMIN_PASSWORD = "ADMIN_PASSWORD";
    public static final String PARAM_ADMIN_SALT = "ADMIN_SALT";
    public static final String PARAM_EXLUSIVE_ACCESS_DURATION = "EXCLUSIVE_ACCESS_DURATION";
    public static final String PARAM_EXPIRATION_TIME = "EXPIRATION_TIME";
    public static final String PARAM_PUBLIC_KEY_PATH = "RSA_PUB_PATH";
    public static final String PARAM_PRIVATE_KEY_PATH = "RSA_PVT_PATH";
    public static final String PARAM_DEFAULT_ASG_RULE = "DEFAULT_ASG_RULE";

    // default system settings
    private static final String DEFAULT_ADMIN_PASSWORD = "HU++mUtKSNtKPcaqGD/XpU/Y2CU=";
    private static final String DEFAULT_ADMIN_SALT = "VV2Jt79VBrA=";
    private static final long DEFAULT_EXCLUSIVE_ACCESS_DURATION = 3600000;
    private static final long DEFAULT_EXPIRATION_TIME = 3600000 * 8;
    private static final String DEFAULT_ASG_RULE = Permission.READ.name();
    private static final transient Logger LOGGER = LoggerFactory.getLogger(AdminEJB.class);

    @PersistenceContext(unitName = "RBAC")
    private transient EntityManager em;

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Admin#authenticate(char[])
     */
    @Override
    public boolean authenticate(char[] password) {
        try {
            return PasswordEncryptionService.authenticate(password, getPassword(), getSalt());
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            LOGGER.error("Error while authenticating administrator.", e);
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Admin#changePassword(char[])
     */
    @Override
    public boolean changePassword(char[] password) {
        try {
            byte[] salt = PasswordEncryptionService.generateSalt();
            setPassword(PasswordEncryptionService.getEncryptedPassword(password, salt));
            setSalt(salt);
            return true;
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            LOGGER.error("Error while authenticating administrator.", e);
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Admin#getTokenExpirationPeriod()
     */
    @Override
    public long getTokenExpirationPeriod() {
        ConfigurationParameter expirationParameter = em.find(ConfigurationParameter.class, PARAM_EXPIRATION_TIME);
        if (expirationParameter == null) {
            expirationParameter = new ConfigurationParameter();
            expirationParameter.setName(PARAM_EXPIRATION_TIME);
            expirationParameter.setValue(String.valueOf(DEFAULT_EXPIRATION_TIME));
            em.persist(expirationParameter);
        }
        return expirationParameter.getValueAsLong();
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Admin#setTokenExpirationPeriod(long)
     */
    @Override
    public void setTokenExpirationPeriod(long tokenExpirationPeriod) {
        long expirationPeriod = getTokenExpirationPeriod();
        if (tokenExpirationPeriod != expirationPeriod) {
            ConfigurationParameter expirationParameter = em.find(ConfigurationParameter.class, PARAM_EXPIRATION_TIME);
            expirationParameter.setValue(String.valueOf(tokenExpirationPeriod));
            em.persist(expirationParameter);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Admin#getExclusiveAccessExpirationPeriod()
     */
    @Override
    public long getExclusiveAccessExpirationPeriod() {
        ConfigurationParameter expirationParameter = em.find(ConfigurationParameter.class,
                PARAM_EXLUSIVE_ACCESS_DURATION);
        if (expirationParameter == null) {
            expirationParameter = new ConfigurationParameter();
            expirationParameter.setName(PARAM_EXLUSIVE_ACCESS_DURATION);
            expirationParameter.setValue(String.valueOf(DEFAULT_EXCLUSIVE_ACCESS_DURATION));
            em.persist(expirationParameter);
        }
        return expirationParameter.getValueAsLong();
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Admin#setExclusiveAccessExpirationPeriod(long)
     */
    @Override
    public void setExclusiveAccessExpirationPeriod(long exclusiveAccessExpirationPeriod) {
        long expirationPeriod = getExclusiveAccessExpirationPeriod();
        if (exclusiveAccessExpirationPeriod != expirationPeriod) {
            ConfigurationParameter expirationParameter = em.find(ConfigurationParameter.class,
                    PARAM_EXLUSIVE_ACCESS_DURATION);
            expirationParameter.setValue(String.valueOf(exclusiveAccessExpirationPeriod));
            em.persist(expirationParameter);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Admin#regenerateKeyPair()
     */
    @Override
    public void regenerateKeyPair() {
        ConfigurationParameter keyParameter = em.find(ConfigurationParameter.class, PARAM_PRIVATE_KEY_PATH);
        if (keyParameter != null) {
            em.remove(keyParameter);
        }
        keyParameter = em.find(ConfigurationParameter.class, PARAM_PUBLIC_KEY_PATH);
        if (keyParameter != null) {
            em.remove(keyParameter);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Admin#getAccessSecurityRulePermission()
     */
    @Override
    public String getAccessSecurityRulePermission() {
        ConfigurationParameter ruleParameter = em.find(ConfigurationParameter.class, PARAM_DEFAULT_ASG_RULE);
        if (ruleParameter == null) {
            ruleParameter = new ConfigurationParameter();
            ruleParameter.setName(PARAM_DEFAULT_ASG_RULE);
            ruleParameter.setValue(DEFAULT_ASG_RULE);
            em.persist(ruleParameter);
        }
        return ruleParameter.getValue();
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Admin#setAccessSecurityRulePermission(java.lang.String)
     */
    @Override
    public void setAccessSecurityRulePermission(String permission) {
        String rulePermission = getAccessSecurityRulePermission();
        if (!rulePermission.equals(permission)) {
            ConfigurationParameter ruleParameter = em.find(ConfigurationParameter.class, PARAM_DEFAULT_ASG_RULE);
            ruleParameter.setValue(permission);
            em.persist(ruleParameter);
        }
    }

    /**
     * @return encrypted administrator password as byte array. Password is retrieved from database, if password does not
     *         exist default password is inserted in database and returned.
     */
    private byte[] getPassword() {
        ConfigurationParameter passwordParameter = em.find(ConfigurationParameter.class, PARAM_ADMIN_PASSWORD);
        if (passwordParameter == null) {
            passwordParameter = new ConfigurationParameter();
            passwordParameter.setName(PARAM_ADMIN_PASSWORD);
            passwordParameter.setValue(DEFAULT_ADMIN_PASSWORD);
            em.persist(passwordParameter);
        }
        return Base64.decode(passwordParameter.getValue());
    }

    /**
     * @return password salt as byte array. Salt is retrieved from database, if salt does not exist default salt is
     *         inserted in database and returned.
     */
    private byte[] getSalt() {
        ConfigurationParameter saltParameter = em.find(ConfigurationParameter.class, PARAM_ADMIN_SALT);
        if (saltParameter == null) {
            saltParameter = new ConfigurationParameter();
            saltParameter.setName(PARAM_ADMIN_SALT);
            saltParameter.setValue(DEFAULT_ADMIN_SALT);
            em.persist(saltParameter);
        }
        return Base64.decode(saltParameter.getValue());
    }

    /**
     * Saves new encrypted administrator password into database.
     * 
     * @param password new administrator password given as byte array
     */
    private void setPassword(byte[] password) {
        ConfigurationParameter passwordParameter = em.find(ConfigurationParameter.class, PARAM_ADMIN_PASSWORD);
        if (passwordParameter != null) {
            passwordParameter.setValue(Base64.encodeToString(password, false));
            em.merge(passwordParameter);
        }
    }

    /**
     * Saves password salt into database.
     * 
     * @param salt password salt given as byte array
     */
    private void setSalt(byte[] salt) {
        ConfigurationParameter saltParameter = em.find(ConfigurationParameter.class, PARAM_ADMIN_SALT);
        if (saltParameter != null) {
            saltParameter.setValue(Base64.encodeToString(salt, false));
            em.merge(saltParameter);
        }
    }
}
