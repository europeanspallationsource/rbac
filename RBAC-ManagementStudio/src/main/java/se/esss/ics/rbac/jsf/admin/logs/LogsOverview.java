/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.admin.logs;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.esss.ics.rbac.ManagementStudioLog;
import se.esss.ics.rbac.ManagementStudioLog.Severity;
import se.esss.ics.rbac.access.Token;
import se.esss.ics.rbac.dsaccess.DirectoryServiceAccessException;
import se.esss.ics.rbac.dsaccess.UserInfo;
import se.esss.ics.rbac.jsf.auth.LoginBean;
import se.esss.ics.rbac.jsf.general.Constants;
import se.esss.ics.rbac.jsf.general.ManagementStudioExecutionTask;
import se.esss.ics.rbac.jsf.general.ManagementStudioTaskBean;
import se.esss.ics.rbac.jsf.general.Messages;
import se.esss.ics.rbac.jsf.users.UsersBean;

/**
 * <code>LogsOverview</code> is a managed bean (<code>studioLogBean</code>), which contains data about management studio
 * log entries and methods for executing actions triggered on administrator logs page (<code>/Admin/Logs</code>). Logs
 * overview bean is view scoped so it lives as long as user interacting with administrator logs view.
 *
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@ManagedBean(name = "studioLogBean")
@ViewScoped
public class LogsOverview implements Serializable {

    private static final long serialVersionUID = 7121094385978551148L;
    private static final String[] SEVERITIES = { "INFO", "WARNING", "ERROR" };
    private static final transient Logger LOGGER = LoggerFactory.getLogger(LogsOverview.class);

    @ManagedProperty(value = "#{usersBean}")
    private UsersBean usersBean;
    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    @ManagedProperty(value = "#{taskBean}")
    private ManagementStudioTaskBean taskBean;
    @Inject
    private RequestCacheBean requestCacheBean;

    private Date startTime;
    private Date endTime;
    private String selectedUser;
    private List<String> severitiesList;
    private List<String> usernames;
    private List<ManagementStudioLog> logs;
    private String[] selectedSeverities;
    private LazyDataModel<ManagementStudioLog> lazyModel;

    // users
    private List<UserInfo> usersSourceList;
    private List<UserInfo> selectedUsersSourceList;
    private List<UserInfo> usersTargetList;
    private List<UserInfo> selectedUsersTargetList;

    /**
     * Initialises parameters needed for retrieving management studio log entries and users dual list. Method is called
     * post construct.
     */
    @PostConstruct
    private void initialize() {
        startTime = new Date(System.currentTimeMillis() - 86400000);
        endTime = new Date(System.currentTimeMillis() + 86400000);
        severitiesList = Arrays.asList(SEVERITIES);
        selectedSeverities = SEVERITIES;
        usernames = new ArrayList<>();
        selectLogedInUser();
        lazyModel = new LazyLogsDataModel(new ArrayList<ManagementStudioLog>());
        initializeUsersLists();
    }

    /**
     * Sets users bean, which holds informations about users from LDAP.
     *
     * @param usersBean managed bean with informations about users
     */
    public void setUsersBean(UsersBean usersBean) {
        this.usersBean = usersBean;
    }

    /**
     * Sets login bean used for checking if a user is logged in.
     *
     * @param loginBean the login bean through which the logged in user info can be retrieved
     */
    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    /**
     * Sets management studio task bean used for executing tasks.
     *
     * @param taskBean the management studio task bean
     */
    public void setTaskBean(ManagementStudioTaskBean taskBean) {
        this.taskBean = taskBean;
    }

    /**
     * Returns time from which logs will be retrieved. Value is entered into start time field on
     * <code>/Admin/Logs</code> page.
     *
     * @return start time, from which logs will be retrieved.
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * Sets time from which logs will be retrieved. Value is entered into start time field on <code>/Admin/Logs</code>
     * page.
     *
     * @param startTime time, from which logs will be retrieved
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * Returns time to which logs will be retrieved. Value is entered into end time field on <code>/Admin/Logs</code>
     * page.
     *
     * @return end time, to which logs will be retrieved.
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * Sets time to which logs will be retrieved. Value is entered into end time field on <code>/Admin/Logs</code> page.
     *
     * @param endTime end time, to which logs will be retrieved
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * Returns user which is selected in users list box on <code>/Admin/Logs</code> page.
     *
     * @return user selected in users list box.
     */
    public String getSelectedUser() {
        return selectedUser;
    }

    /**
     * Sets user which is selected in users list box on <code>/Admin/Logs</code> page.
     *
     * @param selectedUser user selected in users list box.
     */
    public void setSelectedUser(String selectedUser) {
        this.selectedUser = selectedUser;
    }

    /**
     * Returns severities array selected on <code>/Admin/Logs</code> page.
     *
     * @return selected severities array.
     */
    public String[] getSelectedSeverities() {
        return selectedSeverities;
    }

    /**
     * Sets severities array selected on <code>/Admin/Logs</code> page.
     *
     * @param selectedSeverities severities array.
     */
    public void setSelectedSeverities(String[] selectedSeverities) {
        this.selectedSeverities = selectedSeverities;
    }

    /**
     * @return lazy data model for management studio log entries. Data model is used for loading entries in log entries
     *         table on <code>/Admin/Logs</code> page.
     */
    public LazyDataModel<ManagementStudioLog> getLazyModel() {
        return lazyModel;
    }

    /**
     * @return list of all possible severities (info, warning, error).
     */
    public List<String> getSeverities() {
        return severitiesList;
    }

    /**
     * @return list of management studio log entries filtered by chosen parameters.
     */
    public List<ManagementStudioLog> getManagementStudioLogs() {
        if (logs != null) {
            return logs;
        }
        return new ArrayList<>();
    }

    /**
     * @return list of selected users whose log entries will be shown.
     */
    public List<String> getUsers() {
        if (usernames != null) {
            return usernames;
        }
        return new ArrayList<>();
    }

    /**
     * Adds selected users, from select users dialog (<code>/Admin/Logs</code> page), usernames to the usernames list.
     */
    public void selectUsers() {
        if (usersTargetList != null) {
            usernames.clear();
            for (UserInfo userInfo : usersTargetList) {
                usernames.add(userInfo.getUsername());
            }
        }
    }

    /**
     * Checks all filters and retrieves log entries from database. Retrieved log entries are added to the
     * <code>LazyLogsDataModel</code> model. Method is called when apply button on <code>/Admin/Logs</code> page is
     * clicked.
     */
    public void applyFilter() {
        startTime = startTime == null ? new Date(System.currentTimeMillis() - 86400000) : startTime;
        endTime = endTime == null ? new Date(System.currentTimeMillis()) : endTime;
        selectedSeverities = selectedSeverities == null || selectedSeverities.length == 0 ? new String[] {}
                : selectedSeverities;
        usernames = usernames == null ? new ArrayList<String>() : usernames;
        final Severity[] severityEnum = convertToSeverity(selectedSeverities);
        if (severityEnum.length == 0 || usernames.isEmpty()) {
            taskBean.showFacesMessage(FacesMessage.SEVERITY_WARN, Messages.getString(Messages.FILTER_WARN),
                    Constants.EMPTY_STRING, false);
        } else {
            String success = Messages.getString(Messages.SUCCESSFUL_LOGS_RETRIEVAL, loginBean.getUsername());
            String failure = Messages.getString(Messages.UNSUCCESSFUL_LOGS_RETRIEVAL, loginBean.getUsername());
            taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
                @Override
                public void execute() throws Exception {
                    logs = requestCacheBean.getLogs(startTime, endTime, severityEnum, usernames);
                    lazyModel = new LazyLogsDataModel(logs);
                }
            });
        }
    }

    /**
     * Removes selected user from users list on <code>/Admin/Logs</code> page. Method is called when remove user (-)
     * button on <code>/Admin/Logs</code> page is clicked.
     */
    public void removeUser() {
        if (!isRemoveUserButtonDisabled()) {
            usernames.remove(selectedUser);
            selectedUser = Constants.EMPTY_STRING;
        }
    }

    /**
     * Shows select users dialog. Method is called when add users (+) button on <code>/Admin/Logs</code> page is
     * clicked.
     */
    public void showSelectUsersDialog() {
        RequestContext.getCurrentInstance().execute("PF('selectUsersDialog').show()");
    }

    /**
     * Remove user (-) button on <code>/Admin/Logs</code> page is disabled if user is not selected.
     *
     * @return true if remove user (-) button is disabled, otherwise false.
     */
    public boolean isRemoveUserButtonDisabled() {
        return selectedUser == null || selectedUser.isEmpty();
    }

    /**
     * Returns list which contains users from source list and is shown on <code>/Admin/Logs</code> page.
     *
     * @return list which contains users from source list.
     */
    public List<UserInfo> getUsersSourceList() {
        return usersSourceList;
    }

    /**
     * Sets list which contains users from source list and is shown on <code>/Admin/Logs</code> page.
     *
     * @param usersSourceList list which contains users from source list
     */
    public void setUsersSourceList(List<UserInfo> usersSourceList) {
        this.usersSourceList = usersSourceList;
    }

    /**
     * Returns list which contains users from target list and is shown on <code>/Admin/Logs</code> page.
     *
     * @return list which contains users from target list.
     */
    public List<UserInfo> getUsersTargetList() {
        return usersTargetList;
    }

    /**
     * Sets list which contains users from target list and is shown on <code>/Admin/Logs</code> page.
     *
     * @param usersTargetList list which contains users from target list
     */
    public void setUsersTargetList(List<UserInfo> usersTargetList) {
        this.usersTargetList = usersTargetList;
    }

    /**
     * Returns list which contains selected users from source list and is shown on <code>/Admin/Logs</code> page.
     *
     * @return list which contains selected users from source list.
     */
    public List<UserInfo> getSelectedUsersSourceList() {
        return selectedUsersSourceList;
    }

    /**
     * Sets list which contains selected users from source list and is shown on <code>/Admin/Logs</code> page.
     *
     * @param selectedUsersSourceList list which contains selected users from source list
     */
    public void setSelectedUsersSourceList(List<UserInfo> selectedUsersSourceList) {
        this.selectedUsersSourceList = selectedUsersSourceList;
    }

    /**
     * Returns list which contains selected users from target list and is shown on <code>/Admin/Logs</code> page.
     *
     * @return list which contains selected users from target list.
     */
    public List<UserInfo> getSelectedUsersTargetList() {
        return selectedUsersTargetList;
    }

    /**
     * Sets list which contains selected users from target list, and is shown on <code>/Admin/Logs</code> page.
     *
     * @param selectedUsersTargetList list which contains selected users from target list
     */
    public void setSelectedUsersTargetList(List<UserInfo> selectedUsersTargetList) {
        this.selectedUsersTargetList = selectedUsersTargetList;
    }

    /**
     * Moves selected users from source list to the target list. Lists are shown on the select users dialog on
     * <code>/Admin/Logs</code> page.
     */
    public void addUsersFromSourceToTarget() {
        if (!selectedUsersSourceList.isEmpty()) {
            usersTargetList.addAll(selectedUsersSourceList);
        }
        usersSourceList.removeAll(selectedUsersSourceList);
        selectedUsersSourceList.clear();
    }

    /**
     * Moves selected users from target list to the source list. Lists are shown on the select users dialog on
     * <code>/Admin/Logs</code> page.
     */
    public void addUsersFromTargetToSource() {
        if (!selectedUsersTargetList.isEmpty()) {
            usersSourceList.addAll(selectedUsersTargetList);
        }
        usersTargetList.removeAll(selectedUsersTargetList);
        selectedUsersTargetList.clear();
    }

    /**
     * Moves all users from source list to the target list. Lists are shown on the select users dialog on
     * <code>/Admin/Logs</code> page.
     */
    public void addAllUsersFromSourceToTarget() {
        if (!usersSourceList.isEmpty()) {
            usersTargetList.addAll(usersSourceList);
        }
        usersSourceList.clear();
        selectedUsersSourceList.clear();
    }

    /**
     * Moves all users from target list to the source list. Lists are shown on the select users dialog on
     * <code>/Admin/Logs</code> page.
     */
    public void addAllUsersFromTargetToSource() {
        if (!usersTargetList.isEmpty()) {
            usersSourceList.addAll(usersTargetList);
        }
        usersTargetList.clear();
        selectedUsersTargetList.clear();
    }

    /**
     * Initialises users source and target list which data are used in select users dialog on <code>/Admin/Logs</code>
     * page.
     */
    private void initializeUsersLists() {
        List<UserInfo> sourceList = null;
        try {
            sourceList = usersBean.getUsersList(Constants.ALL);
        } catch (DirectoryServiceAccessException e) {
            LOGGER.error(Messages.getString(Messages.LDAP_EXCEPTION), e);
        }
        sourceList = (sourceList == null) ? new ArrayList<UserInfo>() : sourceList;
        usersSourceList = new ArrayList<UserInfo>(sourceList);
        selectedUsersSourceList = new ArrayList<UserInfo>();
        usersTargetList = new ArrayList<UserInfo>();
        selectedUsersTargetList = new ArrayList<UserInfo>();
    }

    /**
     * Converts array with selected severities names to the array with severities values.
     *
     * @param array severities names
     *
     * @return array with values of selected severities.
     */
    private static Severity[] convertToSeverity(String[] array) {
        Severity[] severityEnum = new Severity[array.length];
        for (int i = 0; i < severityEnum.length; i++) {
            severityEnum[i] = Severity.valueOf(array[i].toUpperCase());
        }
        return severityEnum;
    }

    /**
     * Adds logged in user to the list which is shown in users list box on <code>/Admin/Logs</code> page.
     */
    private void selectLogedInUser() {
        Token t = loginBean.getRbacAccessEJB().getToken();
        if (t != null) {
            usernames.add(t.getUsername());
        }
    }
}
