/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.ejbs.interfaces;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import se.esss.ics.rbac.ManagementStudioLog;
import se.esss.ics.rbac.ManagementStudioLog.Severity;

/**
 * <code>ManagementStudioLogs</code> interface defines methods for dealing with management studio log entries.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@Local
public interface ManagementStudioLogs extends Serializable {

    /**
     * Retrieves management studio log entries which corresponds to the given parameters from database and returns it.
     * 
     * @param startTime start time
     * @param endTime end time
     * @param severities severities of the log entries
     * @param users unique users identifiers that initiated logged actions
     * 
     * @return retrieved log entries which corresponds to the given parameters.
     */
    List<ManagementStudioLog> getLogs(Date startTime, Date endTime, Severity[] severities, List<String> users);

    /**
     * Constructs an {@link ManagementStudioLog} entity from the parameters. The timestamp of the log entry is set to
     * the current time in milliseconds. If <code>userID</code> parameter is <code>null</code>, unknown is used instead.
     * Once the log entry fields are set, the entry is persisted by the entity manager.
     * 
     * @param userID unique user identifier that initiated this logged action.
     * @param severity of the log entry.
     * @param content message containing additional information about the action.
     */
    void queueLogEntry(String userID, Severity severity, String content);

    /**
     * Queues a log entry with severity level {@link se.esss.ics.rbac.ManagementStudioLog.Severity#INFO}. This is
     * equivalent to calling <code>queueLogEntry(String, Severity.INFO, String)</code>.
     * 
     * @param userID unique user identifier that initiated this logged action
     * @param content message containing additional information about the action
     */
    void info(String userID, String content);

    /**
     * Queues a log entry with severity level {@link se.esss.ics.rbac.ManagementStudioLog.Severity#WARNING}. This is
     * equivalent to calling <code>queueLogEntry(String, Severity.WARNING, String)</code>.
     * 
     * @param userID unique user identifier that initiated this logged action
     * @param content message containing additional information about the action
     */
    void warn(String userID, String content);

    /**
     * Queues a log entry with severity level {@link se.esss.ics.rbac.ManagementStudioLog.Severity#ERROR}. This is
     * equivalent to calling <code>queueLogEntry(String, Severity.ERROR, String)</code>.
     * 
     * @param userID unique user identifier that initiated this logged action
     * @param content message containing additional information about the action
     */
    void error(String userID, String content);
}
