/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.permissions;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import se.esss.ics.rbac.datamodel.Expression;
import se.esss.ics.rbac.datamodel.Rule;
import se.esss.ics.rbac.ejbs.interfaces.Expressions;
import se.esss.ics.rbac.ejbs.interfaces.Rules;
import se.esss.ics.rbac.jsf.general.Constants;

/**
 * <code>ExpressionsRequestCacheBean</code> is a request bean used on the expressions page. It provides access to DB
 * content by caching the results for the duration of the request. Using this bean eliminates multiple calls to the
 * database, which can be a result of primefaces requesting specific data multiple times.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@ManagedBean(name = "expressionsRequestCacheBean")
@RequestScoped
public class ExpressionsRequestCacheBean implements Serializable {

    private static final long serialVersionUID = 8643499674331770581L;

    @EJB
    private Expressions expressionsEJB;
    @EJB
    private Rules rulesEJB;

    private String currentWildcard = Constants.ALL_DB;
    private Expression currentExpression;

    private List<Expression> expressions;
    private List<Expression> expressionsWildcard;
    private List<Rule> rules;

    /**
     * Retrieves list of expressions from database and returns it. Expressions are shown in expressions list on
     * <code>/Permissions/Expressions</code> page.
     * 
     * @param refresh if true retrieves expressions from database otherwise return cached expressions
     * 
     * @return list of expressions retrieved from database.
     */
    public List<Expression> getExpressions(boolean refresh) {
        if (refresh || expressions == null) {
            expressions = expressionsEJB.getExpressions();
        }
        return expressions;
    }

    /**
     * Retrieves list of expressions (which matches wildcard) from database and returns it. Expressions are shown in
     * expressions list on <code>/Permissions/Expressions</code> page.
     * 
     * @param wildcard search expressions by wildcard
     * @param refresh if true retrieves expressions from database otherwise return cached expressions
     * 
     * @return list of expressions retrieved from database.
     */
    public List<Expression> getExpressionsByWildcard(String wildcard, boolean refresh) {
        if (expressionsWildcard == null || !currentWildcard.equals(wildcard)) {
            currentWildcard = wildcard;
            expressionsWildcard = expressionsEJB.getExpressionsByWildcard(wildcard);
        }
        return expressions;
    }

    /**
     * @param expression the expression for which the rules should be loaded
     * @return list of rules retrieved from database which corresponds to the selected expression
     */
    public List<Rule> getRulesByExpression(Expression expression) {
        if (rules == null || currentExpression.getId() != expression.getId()) {
            currentExpression = expression;
            rules = rulesEJB.getRulesByExpression(expression);
        }
        return rules;
    }
}
