package se.esss.ics.rbac.jsf.roles;

import java.io.Serializable;

import se.esss.ics.rbac.datamodel.UserRole.AssignmentType;
import se.esss.ics.rbac.dsaccess.UserInfo;

/**
 *
 * <code>UserRoleInfo</code> is a wrapper for the {@link UserInfo} and assignment type to a specific role. This wrapper
 * is not to be used for anything else but presentation of the data in the UI.
 *
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
public class UserRoleInfo implements Comparable<UserRoleInfo>, Serializable {

    private static final long serialVersionUID = 8615977500595813973L;
    private final UserInfo user;
    private final AssignmentType assignment;

    /**
     * Construct a new UserRoleInfo.
     *
     * @param user the user providing the username, first name, last name etc.
     * @param assignment the assignment type that needs to be displayed next to the user info
     */
    public UserRoleInfo(UserInfo user, AssignmentType assignment) {
        this.assignment = assignment;
        this.user = user;
    }

    /**
     * @return the user information
     */
    public UserInfo getUser() {
        return user;
    }

    /**
     * @return the assignment type of the user to a particular role
     */
    public AssignmentType getAssignment() {
        return assignment;
    }

    @Override
    public String toString() {
        return user.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((assignment == null) ? 0 : assignment.hashCode());
        result = prime * result + ((user == null) ? 0 : user.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        UserRoleInfo other = (UserRoleInfo) obj;
        if (assignment != other.assignment)
            return false;
        if (user == null) {
            if (other.user != null)
                return false;
        } else if (!user.equals(other.user))
            return false;
        return true;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(UserRoleInfo o) {
        int a = user.compareTo(o.user);
        return a == 0 ? assignment.compareTo(o.assignment) : a;
    }

}
