/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.admin;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import se.esss.ics.rbac.datamodel.Permission;
import se.esss.ics.rbac.datamodel.Resource;
import se.esss.ics.rbac.datamodel.Role;
import se.esss.ics.rbac.datamodel.UserRole;
import se.esss.ics.rbac.datamodel.UserRole.AssignmentType;
import se.esss.ics.rbac.ejbs.interfaces.ManagementStudioLogs;
import se.esss.ics.rbac.ejbs.interfaces.Permissions;
import se.esss.ics.rbac.ejbs.interfaces.Resources;
import se.esss.ics.rbac.ejbs.interfaces.Roles;
import se.esss.ics.rbac.ejbs.interfaces.UserRoles;
import se.esss.ics.rbac.jsf.auth.LoginBean;

/**
 * <code>InstallPermissions</code> is a managed bean (<code>installPermissions</code>), that contains methods for
 * installing default permissions. Install permissions bean is session scoped, so it lives for the duration of the whole
 * session.
 *
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@ManagedBean(name = "installPermissions")
@ViewScoped
public class InstallPermissions implements Serializable {

    private static final long serialVersionUID = 2694354595018058039L;

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    @EJB
    private Resources resourcesEJB;
    @EJB
    private Permissions permissionsEJB;
    @EJB
    private Roles rolesEJB;
    @EJB
    private UserRoles userRoleEJB;
    @EJB
    private ManagementStudioLogs managementStudioLogEJB;

    /**
     * Sets login bean used for checking if a user is logged in.
     *
     * @param loginBean the login bean through which logged in user info is retrieved
     */
    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    /**
     * Installs default permission. Method is called if install permissions button is clicked and if entered
     * administrator password in enter administrator password dialog on <code>/Admin/Settings</code> page is correct.
     */
    public void installPermissions() {
        Resource rbacManagementStudio = createResource("RBAC Management Studio", "RBACManagementStudio");
        Permission manageRole = createPermission("Allow to manage roles even if not a role manager", Boolean.FALSE,
                "ManageRole", rbacManagementStudio);
        Permission manageResource = createPermission("Allow to manage resources even if not a resource manager",
                Boolean.FALSE, "ManageResource", rbacManagementStudio);
        Permission manageRule = createPermission("Allow to manage rules", Boolean.FALSE, "ManageRule",
                rbacManagementStudio);
        Permission releaseEA = createPermission("Release exclusive access for any user", Boolean.FALSE,
                "ReleaseExclusiveAccess", rbacManagementStudio);
        Permission invalidateTokens = createPermission("Invalidate tokens owned by any user", Boolean.TRUE,
                "InvalidateTokens", rbacManagementStudio);
        Permission manageASG = createPermission("Manage access security groups", Boolean.TRUE, "ManageASG",
                rbacManagementStudio);
        Set<Permission> permissions = new HashSet<>(Arrays.asList(new Permission[] { manageRole, manageResource,
                manageRule, releaseEA, invalidateTokens, manageASG }));
        Role rbacAdministrator = createRole("RBAC Administrator", "RBACAdministrator", permissions);
        createUserRole(AssignmentType.NORMAL, loginBean.getUsername(), rbacAdministrator);
    }

    /**
     * Creates resource with <code>name</code> and <code>description</code>. First checks if resource with the same
     * <code>name</code> already exist, if not creates it.
     *
     * @param description resource description
     * @param name resource name
     *
     * @return resource with the given name.
     */
    private Resource createResource(String description, String name) {
        try {
            Resource resource = resourcesEJB.getResourceByName(name);
            if (resource == null) {
                resource = new Resource();
                resource.setName(name);
                resource.setDescription(description);
                resourcesEJB.createResource(resource);
                managementStudioLogEJB.info(loginBean.getUsername(), "Resource " + name + " was successfully created.");
            } else {
                managementStudioLogEJB.warn(loginBean.getUsername(), "Resource " + name + " already exist.");
            }
            if (resource.getManagers() == null) {
                resource.setManagers(new HashSet<>(Arrays.asList(new String[] { loginBean.getUsername() })));
            } else {
                resource.getManagers().add(loginBean.getUsername());
            }
            resourcesEJB.updateResource(resource);
            return resourcesEJB.getResourceByName(name);
        } catch (Exception e) {
            managementStudioLogEJB.error(loginBean.getUsername(), "Resource " + name + "was unsuccessfully created.");
        }
        return null;
    }

    /**
     * Creates permission with the given properties. First checks if permission with the same <code>name</code> already
     * exist, if not creates it.
     *
     * @param description permission description
     * @param exclusiveAccessAllowed is exclusive access on this permission allowed
     * @param name permission name
     * @param resource permission resource
     *
     * @return permission with the given name.
     */
    private Permission createPermission(String description, Boolean exclusiveAccessAllowed, String name,
            Resource resource) {
        try {
            Permission permission = permissionsEJB.getPermissionByName(name, resource);
            if (permission == null) {
                permission = new Permission();
                permission.setDescription(description);
                permission.setExclusiveAccessAllowed(exclusiveAccessAllowed);
                permission.setName(name);
                permission.setResource(resource);
                permission.setRule(null);
                permissionsEJB.createPermission(permission);
                permission = permissionsEJB.getPermissionByName(name, resource);
                managementStudioLogEJB.info(loginBean.getUsername(), "Permission " + name
                        + " was successfully created.");
            } else {
                managementStudioLogEJB.warn(loginBean.getUsername(), "Permission " + name + " already exist.");
            }
            return permission;
        } catch (Exception e) {
            managementStudioLogEJB.error(loginBean.getUsername(), "Permission " + name + "was unsuccessfully created.");
        }
        return null;
    }

    /**
     * Creates role with the given properties. First check if role with the same <code>name</code> already exist, if not
     * creates it.
     *
     * @param description role description
     * @param name role name
     * @param permissions role permissions
     *
     * @return role with the given name.
     */
    private Role createRole(String description, String name, Set<Permission> permissions) {
        try {
            Role role = rolesEJB.getRoleByName(name);
            if (role == null) {
                role = new Role();
                role.setDescription(description);
                role.setName(name);
                rolesEJB.createRole(role);
                managementStudioLogEJB.info(loginBean.getUsername(), "Role " + name + " was successfully created.");
            } else {
                managementStudioLogEJB.warn(loginBean.getUsername(), "Role " + name + " already exist.");
            }
            if (role.getPermissions() == null) {
                role.setPermissions(new HashSet<>(permissions));
            } else {
                role.getPermissions().addAll(permissions);
            }
            if (role.getManagers() == null) {
                role.setManagers(new HashSet<>(Arrays.asList(new String[] { loginBean.getUsername() })));
            } else {
                role.getManagers().add(loginBean.getUsername());
            }
            rolesEJB.updateRole(role);
            return rolesEJB.getRoleByName(name);
        } catch (Exception e) {
            managementStudioLogEJB.error(loginBean.getUsername(), "Role " + name + "was unsuccessfully created.");
        }
        return null;
    }

    /**
     * Creates user role with the given properties. First check if user with <code>username</code> already has
     * <code>role</code>, if not creates it.
     *
     * @param assignment user role assignment
     * @param username username of the user
     * @param role role
     */
    private void createUserRole(AssignmentType assignment, String username, Role role) {
        try {
            List<String> users = userRoleEJB.getUsernames(role.getId());
            if (users.contains(username)) {
                managementStudioLogEJB.warn(loginBean.getUsername(), "Role " + role.getName()
                        + " is already assigned to the " + username + ".");
            } else {
                UserRole userRole = new UserRole();
                userRole.setAssignment(assignment);
                userRole.setUserId(username);
                userRole.setRole(role);
                userRoleEJB.createUserRole(userRole);
                managementStudioLogEJB.info(loginBean.getUsername(), "Role " + role.getName()
                        + " was successfully assigned to the " + username + ".");
            }
        } catch (Exception e) {
            managementStudioLogEJB.error(loginBean.getUsername(), "Role " + role.getName()
                    + " was unsuccessfully assigned to the " + username + ".");
        }
    }
}
