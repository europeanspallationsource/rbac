/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.ejbs;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import se.esss.ics.rbac.datamodel.Expression;
import se.esss.ics.rbac.ejbs.interfaces.Expressions;
import se.esss.ics.rbac.jsf.general.Constants;

/**
 * <code>ExpressionEJB</code> is a stateless bean containing utility methods for dealing with expressions. Contains
 * methods for retrieving, inserting, deleting and updating expression informations. All methods are defined in
 * <code>Expressions</code> interface.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@Stateless
public class ExpressionEJB implements Expressions {

    private static final long serialVersionUID = 5195346671204130953L;

    @PersistenceContext(unitName = Constants.PERSISTENCE_CONTEXT_NAME)
    private transient EntityManager em;

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Expressions#getExpression(int)
     */
    @Override
    public Expression getExpression(int expressionId) {
        return em.find(Expression.class, expressionId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Expressions#getExpressions()
     */
    @Override
    public List<Expression> getExpressions() {
        return em.createNamedQuery("Expression.selectAll", Expression.class).getResultList();
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Expressions#getExpressionsByWildcard(java.lang.String)
     */
    @Override
    public List<Expression> getExpressionsByWildcard(String wildcard) {
        return em.createNamedQuery("Expression.findByWildcard", Expression.class)
                .setParameter("wildcard", wildcard).getResultList();
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Expressions#createExpression(se.esss.ics.rbac.datamodel.Expression)
     */
    @Override
    public void createExpression(Expression expression) {
        em.persist(expression);
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Expressions#removeExpression(se.esss.ics.rbac.datamodel.Expression)
     */
    @Override
    public void removeExpression(Expression expression) {
        em.remove(em.find(Expression.class, expression.getId()));
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Expressions#updateExpression(se.esss.ics.rbac.datamodel.Expression)
     */
    @Override
    public void updateExpression(Expression expression) {
        em.merge(expression);
    }
}
