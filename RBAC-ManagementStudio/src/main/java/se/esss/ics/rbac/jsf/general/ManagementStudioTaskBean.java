/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.general;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.esss.ics.rbac.ejbs.interfaces.ManagementStudioLogs;
import se.esss.ics.rbac.jsf.auth.LoginBean;
import se.esss.ics.rbac.jsf.users.UserInfoBean;

/**
 * <code>ManagementStudioTaskBean</code> is a managed bean (<code>taskBean</code>), which executes tasks and logs
 * results. Management Studio bean is session scoped, so it lives for the duration of the whole session.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@ManagedBean(name = "taskBean")
@SessionScoped
public class ManagementStudioTaskBean implements Serializable {

    private static final long serialVersionUID = 3670881484503958713L;
    private static final transient Logger LOGGER = LoggerFactory.getLogger(UserInfoBean.class);

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    @EJB
    private ManagementStudioLogs managementStudioLogEJB;

    /**
     * Sets login bean used for checking if a user is logged in.
     * 
     * @param loginBean the login bean through which the logged in user info can be retrieved
     */
    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    /**
     * Executes given task. If task is successfully executed log <code>success</code> message in management studio log
     * and shows it as growl message. Otherwise if task is not successfully executed log failure message.
     * 
     * @param success success message
     * @param failure failure message
     * @param task task which will be executed
     */
    public void execute(String success, String failure, ManagementStudioExecutionTask<? extends Exception> task) {
        try {
            task.execute();
            showFacesMessage(FacesMessage.SEVERITY_INFO, success, Constants.EMPTY_STRING, true);
        } catch (Exception e) {
            LOGGER.error(failure, e);
            Throwable cause = e.getCause();
            Throwable up = e;
            while(cause != null) {
                up = cause;
                cause = cause.getCause();
            }
            String exceptionMessage = up.getMessage();
            if (exceptionMessage == null) {
                exceptionMessage = e.getMessage();
            } 
            showFacesMessage(FacesMessage.SEVERITY_ERROR, failure, exceptionMessage, false);
        }
    }

    /**
     * Shows message with the given severity to the user as growl message.
     * 
     * @param severity message severity
     * @param msg message content
     * @param detail detailed message
     * @param managementStudioLog true if the action is logged into the management studio log
     */
    public void showFacesMessage(Severity severity, String msg, String detail, boolean managementStudioLog) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(severity, msg, detail));
        if (managementStudioLog) {
            managementStudioLogEJB.info(loginBean.getUsername(), msg);
        }
    }
}
