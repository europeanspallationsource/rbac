/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.auth;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.primefaces.context.RequestContext;

import se.esss.ics.rbac.access.SecurityFacadeException;
import se.esss.ics.rbac.access.Token;
import se.esss.ics.rbac.ejbs.interfaces.RBACAccess;
import se.esss.ics.rbac.httputil.CookieUtilities;
import se.esss.ics.rbac.httputil.RBACCookie;
import se.esss.ics.rbac.jsf.general.Constants;

/**
 * Login bean is a managed bean (<code>loginBean</code>), that holds user's credentials during the login procedure.
 * Login bean is session scoped, so it lives for the duration of the whole session.
 * 
 * @author <a href="mailto:jakob.battelino@cosylab.com">Jakob Battelino Prelog</a>
 */
@ManagedBean(name = "loginBean")
@SessionScoped
public class LoginBean implements Serializable {

    private static final long serialVersionUID = -6967709541680657116L;
    
    @EJB
    private RBACAccess rbacAccessEJB;
    
    private String username;
    private char[] password;    
        
    @PostConstruct
    private void checkCookie() {
        if (CookieUtilities.isUseSingleSignOn()) {
            Map<String,Object> cookies = FacesContext.getCurrentInstance().getExternalContext().getRequestCookieMap();
            Cookie rbacCookie = (Cookie)cookies.get(RBACCookie.NAME);
            if (rbacCookie != null) {
                String value = rbacCookie.getValue();
                if (!RBACCookie.LOGGED_OUT_VALUE.equals(value)) {
                    char[] token = CookieUtilities.decode(value);
                    Token oldToken = rbacAccessEJB.getToken();
                    if (oldToken == null || !Arrays.equals(token, oldToken.getTokenID())) {
                        rbacAccessEJB.setTokenID(token);
                        Token tt = rbacAccessEJB.getValidatedToken();
                        if (tt == null) {
                            HttpServletResponse response = (HttpServletResponse)FacesContext.getCurrentInstance()
                                    .getExternalContext().getResponse();
                            response.addCookie(new RBACCookie());
                        } else {
                            RequestContext.getCurrentInstance().update(Constants.SITE_CONTENT);
                            RequestContext.getCurrentInstance().update(Constants.SITE_BANNER);
                            RequestContext.getCurrentInstance().update(Constants.SITE_CONTAINER_GROWL);
                            
                        }
                    }
                }
            }
        }
    }
    
    /**
     * Returns <code>true</code> if a user is currently logged in.
     * 
     * @return <code>true</code> if a user is currently logged in.
     */
    public boolean isLoggedInValidated() {
        checkCookie();
        return rbacAccessEJB.getValidatedToken() != null;
    }

    /**
     * Returns <code>true</code> if a user is currently logged in.
     * 
     * @return <code>true</code> if a user is currently logged in.
     */
    public boolean isLoggedIn() {        
        checkCookie();
        return rbacAccessEJB.getToken() != null;
    }

    /**
     * Returns this bean's injected instance of {@link RBACAccess} EJB.
     * 
     * @return this bean's instance of {@link RBACAccess}.
     */
    public RBACAccess getRbacAccessEJB() {
        return rbacAccessEJB;
    }

    /**
     * Returns the RBAC version number.
     * 
     * @return RBAC version number
     */
    public String getVersion() {
        return rbacAccessEJB.getRBACVersion();
    }

    /**
     * Returns the welcome message. Welcome message depends on whether a user is logged in.
     * 
     * @return the welcome message.
     */
    public String getWelcomeMessage() {
        checkCookie();
        Token token = rbacAccessEJB.getValidatedToken();
        if (token == null) {
            return "Welcome, unknown visitor.";
        } else {
            return "Welcome, " + (token.getFirstName() == null ? token.getUsername() : token.getFirstName()) + ".";
        }
    }

    /**
     * Called when user enters and confirms his login credentials.
     * 
     * Method calls {@link RBACAccess#authenticate(String, char[])} on its injected {@link RBACAccess} EJB and then
     * nulls the password property.
     */
    public void onLogin() {
        try {
            Token token = rbacAccessEJB.authenticate(getUsername(), getPassword());
            setPassword(null);
            if (token != null && CookieUtilities.isUseSingleSignOn()) {
                HttpServletResponse response = (HttpServletResponse)FacesContext.getCurrentInstance()
                        .getExternalContext().getResponse();
                response.addCookie(new RBACCookie(CookieUtilities.encode(token.getTokenID())));
            }
            // refresh the view on login to recreate the permissions cache
            refreshView();
            if (token == null) {
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Sign In Failed", "Sign in failed"));
            } else {
                FacesContext.getCurrentInstance().addMessage(
                        null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "Sign In Successful", "Signed in as "
                                + token.getUsername()));
            }
        } catch (SecurityFacadeException | IllegalArgumentException e) {
            setPassword(null);
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Sign In Failed", e.getMessage()));
        }
    }

    /**
     * Called when the currently logged in user presses a 'Log out' button.
     * 
     * Method calls the {@link RBACAccess#logout()} method on its injected {@link RBACAccess} EJB and nulls username and
     * password properties.
     */
    public void onLogout() {
        try {
            Token token = rbacAccessEJB.getToken();
            if (rbacAccessEJB.logout()) {
                if (CookieUtilities.isUseSingleSignOn()) {
                    HttpServletResponse response = (HttpServletResponse)FacesContext.getCurrentInstance()
                            .getExternalContext().getResponse();
                    response.addCookie(new RBACCookie());
                }
                
                setUsername(null);
                setPassword(null);
                // refresh the view on logout to recreate the permissions cache
                refreshView();
                if (token == null) {
                    FacesContext.getCurrentInstance().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_INFO, "Sign Out Successful", "Sign out successful"));
                } else {
                    FacesContext.getCurrentInstance().addMessage(
                            null,
                            new FacesMessage(FacesMessage.SEVERITY_INFO, "Sign Out Successful", token.getUsername()
                                    + " signed out."));
                }
            } else {
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Sign Out Failed", "Sign out failed"));
            }
        } catch (SecurityFacadeException | IllegalArgumentException e) {
            setUsername(null);
            setPassword(null);
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Sign Out Failed", e.getMessage()));
        }
    }

    private static void refreshView() {
        FacesContext context = FacesContext.getCurrentInstance();
        String viewId = context.getViewRoot().getViewId();
        ViewHandler handler = context.getApplication().getViewHandler();
        UIViewRoot root = handler.createView(context, viewId);
        root.setViewId(viewId);
        context.setViewRoot(root);
    }

    /**
     * Returns the bean's username property value.
     * 
     * @return the bean's username property value.
     */
    public String getUsername() {
        if (username == null) {
            Token token = rbacAccessEJB.getToken();
            if (token != null) {
                username = token.getUsername();
            }
        }
        return username;
    }

    /**
     * Sets the bean's username property value.
     * 
     * @param username the bean's username property value.
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Returns the bean's password property value.
     * 
     * @return the bean's password property value.
     */
    public char[] getPassword() {
        return password;
    }

    /**
     * Sets the bean's password property value.
     * 
     * @param password the bean's password property value.
     */
    public void setPassword(char[] password) {
        this.password = password;
    }

    /**
     * Check permissions and show the add group dialog.
     */
    public void showLoginDialog() {
        checkCookie();
        if (rbacAccessEJB.getValidatedToken() == null) {
            RequestContext.getCurrentInstance().execute("PF('loginDialog').show()");
        } else {
            RequestContext.getCurrentInstance().update(Constants.SITE_CONTAINER);
        }
    }
}
