/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.resources;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import se.esss.ics.rbac.datamodel.Resource;
import se.esss.ics.rbac.ejbs.interfaces.Resources;
import se.esss.ics.rbac.jsf.general.Constants;

/**
 * <code>RequestCacheBean</code> is a request bean used on the resources page. It provides access to DB content by
 * caching the results for the duration of the request. Using this bean eliminates multiple calls to the database, which
 * can be a result of primefaces requesting specific data multiple times.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@ManagedBean(name = "resourcesRequestCacheBean")
@RequestScoped
public class RequestCacheBean implements Serializable {

    private static final long serialVersionUID = -5238562612191090983L;

    @EJB
    private Resources resourcesEJB;

    private List<Resource> resources;
    private List<Resource> resourcesByWildcard;

    private String currentWildcard = Constants.ALL;

    /**
     * Retrieves list of resources from database and returns it. Resources are shown in resources list on
     * <code>/Resources/Definitions</code> page.
     * 
     * @param refresh if true retrieves resources from database otherwise return cached resources
     * 
     * @return list of resources retrieved from database.
     */
    public List<Resource> getResources(boolean refresh) {
        if (refresh || resources == null) {
            resources = resourcesEJB.getResources();
        }
        return resources;
    }

    /**
     * Retrieves list of resources (which matches wildcard) from database and returns it. Resources are shown in
     * resources list on <code>/Resources/Definitions</code> page.
     * 
     * @param wildcard search resources by wildcard
     * @param refresh if true retrieves resources from database otherwise return cached resources
     * 
     * @return list of resources retrieved from database.
     */
    public List<Resource> getResourcesByWildcard(String wildcard, boolean refresh) {
        if (refresh || resourcesByWildcard == null || !currentWildcard.equals(wildcard)) {
            currentWildcard = wildcard;
            resourcesByWildcard = resourcesEJB.getResourcesByWildcard(wildcard);
        }
        return resourcesByWildcard;
    }
}
