/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.users;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import se.esss.ics.rbac.dsaccess.DirectoryServiceAccess;
import se.esss.ics.rbac.dsaccess.DirectoryServiceAccessException;
import se.esss.ics.rbac.dsaccess.UserInfo;
import se.esss.ics.rbac.jsf.general.Constants;
import se.esss.ics.rbac.jsf.general.ManagementStudioTaskBean;
import se.esss.ics.rbac.jsf.general.Messages;
import se.esss.ics.rbac.jsf.utils.Wildcard;

/**
 * <code>UsersBean</code> is a managed bean (<code>usersBean</code>), that retrieves user's informations. User's
 * informations are automatically retrieved from LDAP on bean creation. Users bean is session scoped, so it lives for
 * the duration of the whole session.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
@ManagedBean(name = "usersBean")
@SessionScoped
public class UsersBean implements Serializable {

    private static final long serialVersionUID = 7391125337566009754L;

    @ManagedProperty(value = "#{taskBean}")
    private ManagementStudioTaskBean taskBean;
    @EJB
    private transient DirectoryServiceAccess dsAccess;

    private List<UserInfo> usersList;
    private boolean areUsersLoaded = false;

    /**
     * Sets management studio task bean used for executing tasks.
     * 
     * @param taskBean the management studio task bean
     */
    public void setTaskBean(ManagementStudioTaskBean taskBean) {
        this.taskBean = taskBean;
    }

    /**
     * Sets the flag <code>areUsersLoaded</code> which tells if users are already retrieved from LDAP.
     * 
     * @param areUsersLoaded flag which tells if users are already retrieved from LDAP
     */
    public void setAreUsersLoaded(boolean areUsersLoaded) {
        this.areUsersLoaded = areUsersLoaded;
    }

    /**
     * @return the flag <code>areUsersLoaded</code> value. Return true if users are already retrieved from LDAP,
     *         otherwise false.
     */
    public boolean getAreUsersLoaded() {
        return areUsersLoaded;
    }

    /**
     * Retrieves all users from LDAP. If users have not been successfully loaded exception is thrown.
     * 
     * @throws DirectoryServiceAccessException if there was an error accessing the directory service
     */
    public void loadUsers() throws DirectoryServiceAccessException {
        try {

            UserInfo[] usersInfoArray = dsAccess.getUsers(Constants.ALL, true, true, true);
            usersList = usersInfoArray == null ? null : Arrays.asList(usersInfoArray);
            setAreUsersLoaded(true);

        } catch (DirectoryServiceAccessException e) {
            areUsersLoaded = false;
            taskBean.showFacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString(Messages.LDAP_EXCEPTION),
                    e.getMessage(), false);
            throw e;
        }
    }

    /**
     * Return all informations about specific user which is identified by username. If users have not yet been retrieved
     * from LDAP, first retrieves all users.
     * 
     * @param username unique user identifier
     * 
     * @return all informations about specific user identified by username.
     * 
     * @throws DirectoryServiceAccessException if there was an error access directory service
     */
    public UserInfo getUser(String username) throws DirectoryServiceAccessException {
        if (!areUsersLoaded) {
            loadUsers();
        }
        if (usersList != null) {
            for (UserInfo userInfo : usersList) {
                if (userInfo.getUsername().equals(username)) {
                    return userInfo;
                }
            }
        }
        return null;
    }

    /**
     * Returns list of all users filtered by wildcard pattern. List contains only users whose name or middle name or
     * last name corresponds to given pattern. If users have not yet been retrieved from LDAP, first retrieves all
     * users.
     * 
     * @param wildcard wildcard pattern used for filtering users
     * 
     * @return list of all users whose name or middle name or last name corresponds to given pattern.
     * 
     * @throws DirectoryServiceAccessException if there was an error access directory service
     */
    public List<UserInfo> getUsersList(String wildcard) throws DirectoryServiceAccessException {
        if (!areUsersLoaded) {
            loadUsers();
        }
        if (usersList == null) {
            return new ArrayList<>();
        } else if (Constants.ALL.equals(wildcard)) {
            return usersList;
        } else {
            List<UserInfo> searchResultList = new ArrayList<>();
            for (UserInfo user : usersList) {
                if (Wildcard.wildcardMatch(user.getFirstName(), wildcard + Constants.ALL)
                        || Wildcard.wildcardMatch(user.getLastName(), wildcard + Constants.ALL)
                        || Wildcard.wildcardMatch(user.getMiddleName(), wildcard + Constants.ALL)) {
                    searchResultList.add(user);
                }
            }
            return searchResultList;
        }
    }
}
