/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.pvaccess;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.esss.ics.rbac.datamodel.IPGroup;
import se.esss.ics.rbac.datamodel.Role;
import se.esss.ics.rbac.datamodel.UserRole;
import se.esss.ics.rbac.ejbs.interfaces.AccessSecurityGroups;
import se.esss.ics.rbac.jsf.auth.LoginBean;
import se.esss.ics.rbac.jsf.auth.PermissionsBean;
import se.esss.ics.rbac.jsf.general.Constants;
import se.esss.ics.rbac.jsf.general.ManagementStudioExecutionTask;
import se.esss.ics.rbac.jsf.general.ManagementStudioTaskBean;
import se.esss.ics.rbac.jsf.general.Messages;
import se.esss.ics.rbac.jsf.permissions.DefinitionsBean;
import se.esss.ics.rbac.jsf.utils.NumericStringComparator;
import se.esss.ics.rbac.pvaccess.AccessSecurityGroup;
import se.esss.ics.rbac.pvaccess.AccessSecurityInput;
import se.esss.ics.rbac.pvaccess.AccessSecurityRule;
import se.esss.ics.rbac.access.RBACConnector;
import se.esss.ics.rbac.access.RBACConnectorException;

/**
 * <code>AccessSecurityGroupBean</code> is a managed bean (<code>asgBean</code> ), which contains access security groups
 * data and methods for executing actions triggered on <code>/PVAccessManagement</code> page. Access security group bean
 * is view scoped so it lives as long as user interacting with PV access management page view.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@ManagedBean(name = "asgBean")
@ViewScoped
public class AccessSecurityGroupBean implements Serializable {

    private static final long serialVersionUID = -4389798566301234259L;
    private static final String FILE_NAME = "generatedFile.acf";
    private static final String[] INPUT_PREFIXES = { "INPA", "INPB", "INPC", "INPD", "INPE", "INPF", "INPG", "INPH",
            "INPI", "INPJ", "INPK", "INPL" };
    private static final int MAX_INPUTS_SIZE = 12;
    private static final transient Logger LOGGER = LoggerFactory.getLogger(DefinitionsBean.class);
    private static final Charset CHARSET = Charset.forName("UTF-8");

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    @ManagedProperty(value = "#{permissionsBean}")
    private PermissionsBean permissionsBean;
    @ManagedProperty(value = "#{taskBean}")
    private ManagementStudioTaskBean taskBean;
    @EJB
    private AccessSecurityGroups groupEJB;
    @Inject
    private RequestCacheBean requestCacheBean;

    private String wildcard = Constants.ALL;
    private String newGroupName;
    private String newGroupDescription;
    private String updatedGroupName;
    private String updatedGroupDescription;
    private String newRuleName;
    private String newRuleDescription;
    private String newRuleCalc;
    private Boolean newRuleLevel;
    private String updatedRuleName;
    private String updatedRuleDescription;
    private String updatedRuleCalc;
    private Boolean updatedRuleLevel;
    private String inputName;
    private AccessSecurityInput selectedInput;
    private AccessSecurityGroup selectedGroup;
    private AccessSecurityRule selectedRule;
    private Role selectedRole;
    private IPGroup selectedIPGroup;
    private List<AccessSecurityInput> inputsList;
    private String generatedContent;
    private List<AccessSecurityGroup> selectedGroups;
    private AccessSecurityGroup defaultGroup;

    // roles
    private List<Role> rolesSourceList;
    private List<Role> selectedRolesSourceList;
    private List<Role> rolesTargetList;
    private List<Role> selectedRolesTargetList;

    // ip groups
    private List<IPGroup> ipGroupsSourceList;
    private List<IPGroup> selectedIPGroupsSourceList;
    private List<IPGroup> ipGroupsTargetList;
    private List<IPGroup> selectedIPGroupsTargetList;

    /**
     * Initialises roles and IP groups dual list models. Method is called post construct.
     */
    @PostConstruct
    private void initialize() {
        FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        initializeRolesDualList();
        initializeIPGroupsDualList();
    }

    /**
     * Sets permissions bean which contains methods for determining permissions for specific actions for the logged in
     * user.
     * 
     * @param permissionsBean the permissions bean through which permission can be checked
     */
    public void setPermissionsBean(PermissionsBean permissionsBean) {
        this.permissionsBean = permissionsBean;
    }

    /**
     * Sets login bean used for checking if a user is logged in.
     * 
     * @param loginBean the login bean through which logged in user info is retrieved
     */
    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    /**
     * Sets management studio task bean used for executing tasks.
     * 
     * @param taskBean the management studio task bean
     */
    public void setTaskBean(ManagementStudioTaskBean taskBean) {
        this.taskBean = taskBean;
    }

    /**
     * Returns wildcard pattern which is used for searching by access security groups. Pattern is entered into search
     * field on <code>/PVAccessManagement</code> page.
     * 
     * @return wildcard pattern used for searching by access security groups.
     */
    public String getWildcard() {
        return wildcard;
    }

    /**
     * Sets wildcard pattern which is used for searching by access security groups. Pattern is entered into search field
     * on <code>/PVAccessManagement</code> page.
     * 
     * @param wildcard pattern used for searching by access security groups
     */
    public void setWildcard(String wildcard) {
        this.wildcard = wildcard;
    }

    /**
     * Returns access security group which is selected in access security groups list on
     * <code>/PVAccessManagement</code> page.
     * 
     * @return access security group selected in access security groups list.
     */
    public AccessSecurityGroup getSelectedGroup() {
        return selectedGroup;
    }

    /**
     * Sets access security group which is selected in access security groups list on <code>/PVAccessManagement</code>
     * page.
     * 
     * @param selectedGroup access security group selected in access security groups list
     */
    public void setSelectedGroup(AccessSecurityGroup selectedGroup) {
        this.selectedGroup = selectedGroup;
        this.selectedInput = null;
        this.selectedRule = null;
    }

    /**
     * Returns default access security group which is selected in access security groups drop down list on
     * <code>/ACFFileGenerator</code> page.
     * 
     * @return access security group selected in access security groups drop down list.
     */
    public AccessSecurityGroup getDefaultAccessSecurityGroup() {
        return defaultGroup;
    }

    /**
     * Sets default access security group which is selected in access security groups drop down list on
     * <code>/ACFFileGenerator</code> page.
     * 
     * @param defaultGroup access security group selected in access security groups drop down list.
     */
    public void setDefaultAccessSecurityGroup(AccessSecurityGroup defaultGroup) {
        this.defaultGroup = defaultGroup;
    }

    /**
     * Returns access security groups which are selected in access security groups list on
     * <code>/ACFFileGenerator</code> page.
     * 
     * @return access security groups selected in access security groups list.
     */
    public List<AccessSecurityGroup> getSelectedGroups() {
        return selectedGroups;
    }

    /**
     * Sets access security groups which are selected in access security groups list on <code>/ACFFileGenerator</code>
     * page.
     * 
     * @param selectedGroups access security groups selected in access security groups list
     */
    public void setSelectedGroups(List<AccessSecurityGroup> selectedGroups) {
        this.selectedGroups = selectedGroups;
    }

    /**
     * Returns access security group rule which is selected in access security rules list on
     * <code>/PVAccessManagement</code> page.
     * 
     * @return access security group rule selected in access security rules list.
     */
    public AccessSecurityRule getSelectedRule() {
        return selectedRule;
    }

    /**
     * Sets access security group rule which is selected in access security rules list on
     * <code>/PVAccessManagement</code> page.
     * 
     * @param selectedRule access security group rule selected in access security rules list
     */
    public void setSelectedRule(AccessSecurityRule selectedRule) {
        this.selectedRule = selectedRule;
        this.selectedRole = null;
        this.selectedIPGroup = null;
    }

    /**
     * Returns access security group input which is selected in access security inputs list on
     * <code>/PVAccessManagement</code> page.
     * 
     * @return access security group input selected in access security inputs list.
     */
    public AccessSecurityInput getSelectedInput() {
        return selectedInput;
    }

    /**
     * Sets access security group input which is selected in access security inputs list on
     * <code>/PVAccessManagement</code> page.
     * 
     * @param selectedInput access security group input selected in access security inputs list
     */
    public void setSelectedInput(AccessSecurityInput selectedInput) {
        this.selectedInput = selectedInput;
    }

    /**
     * Returns access security rule role which is selected in roles list on <code>/PVAccessManagement</code> page.
     * 
     * @return access security rule role selected in roles list.
     */
    public Role getSelectedRole() {
        return selectedRole;
    }

    /**
     * Sets access security rule role which is selected in roles list on <code>/PVAccessManagement</code> page.
     * 
     * @param selectedRole access security rule role selected in roles list
     */
    public void setSelectedRole(Role selectedRole) {
        this.selectedRole = selectedRole;
    }

    /**
     * Returns access security rule IP group which is selected in IP groups list on <code>/PVAccessManagement</code>
     * page.
     * 
     * @return access security IP group selected in IP groups list.
     */
    public IPGroup getSelectedIPGroup() {
        return selectedIPGroup;
    }

    /**
     * Sets access security rule IP group which is selected in IP groups list on <code>/PVAccessManagement</code> page.
     * 
     * @param selectedIPGroup access security IP group selected in IP groups list
     */
    public void setSelectedIPGroup(IPGroup selectedIPGroup) {
        this.selectedIPGroup = selectedIPGroup;
    }

    /**
     * @return selected access security group name which is shown in access security group name field on
     *         <code>/PVAccessManagement</code> page.
     */
    public String getGroupName() {
        if (getIsGroupSelected()) {
            return selectedGroup.getName();
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * Returns new access security group name which is entered into access security group name field in add access
     * security group dialog on <code>/PVAccessManagement</code> page.
     * 
     * @return new access security group name entered into access security group name field.
     */
    public String getNewGroupName() {
        return newGroupName;
    }

    /**
     * Sets new access security group name which is entered into access security group name field in add access security
     * group dialog on <code>/PVAccessManagement</code> page.
     * 
     * @param newGroupName new access security group name entered into access security group name field
     */
    public void setNewGroupName(String newGroupName) {
        this.newGroupName = removeSpaces(newGroupName);
    }

    /**
     * Returns updated access security group name which is entered into access security group name field in edit access
     * security group dialog on <code>/PVAccessManagement</code> page.
     * 
     * @return updated access security group name entered into access security group name field.
     */
    public String getUpdatedGroupName() {
        return getGroupName();
    }

    /**
     * Sets updated access security group name which is entered into access security group name field in edit access
     * security group dialog on <code>/PVAccessManagement</code> page.
     * 
     * @param updatedGroupName updated access security group name entered into access security group name field.
     */
    public void setUpdatedGroupName(String updatedGroupName) {
        this.updatedGroupName = removeSpaces(updatedGroupName);
    }

    /**
     * @return selected access security group description which is shown in access security group description field on
     *         <code>/PVAccessManagement</code> page.
     */
    public String getGroupDescription() {
        if (getIsGroupSelected()) {
            return selectedGroup.getDescription();
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * Returns new access security group description which is entered into access security group description field in
     * add access security group dialog on <code>/PVAccessManagement</code> page.
     * 
     * @return new access security group description entered into access security group description field.
     */
    public String getNewGroupDescription() {
        return newGroupDescription;
    }

    /**
     * Sets new access security group description which is entered into access security group description field in add
     * access security group dialog on <code>/PVAccessManagement</code> page.
     * 
     * @param newGroupDescription new access security group description entered into access security group description
     *        field.
     */
    public void setNewGroupDescription(String newGroupDescription) {
        this.newGroupDescription = newGroupDescription;
    }

    /**
     * Returns updated access security group description which is entered into access security group description field
     * in edit access security group dialog on <code>/PVAccessManagement</code> page.
     * 
     * @return updated access security group description entered into access security group description field.
     */
    public String getUpdatedGroupDescription() {
        return getGroupDescription();
    }

    /**
     * Sets updated access security group description which is entered into access security group description field in
     * edit access security group dialog on <code>/PVAccessManagement</code> page.
     * 
     * @param updatedGroupDescription updated access security group description entered into access security group
     *        description field
     */
    public void setUpdatedGroupDescription(String updatedGroupDescription) {
        this.updatedGroupDescription = updatedGroupDescription;
    }

    /**
     * Returns access security group input name which is entered into access security input name field in add access
     * security input dialog on <code>/PVAccessManagement</code> page.
     * 
     * @return access security group input name entered into access security input name field.
     */
    public String getInputName() {
        return inputName;
    }

    /**
     * Sets access security group input name which is entered into access security input name field in add access
     * security input dialog on <code>/PVAccessManagement</code> page.
     * 
     * @param inputName access security group input name entered into access security input name field
     */
    public void setInputName(String inputName) {
        this.inputName = inputName;
    }

    /**
     * Returns access security inputs list which corresponds to the selected access security group. List is ordered
     * ascending by input index.
     * 
     * @return access security inputs list ordered ascending by input index.
     */
    public List<AccessSecurityInput> getAccessSecurityGroupInputs() {
        if (getIsGroupSelected() && selectedGroup.getInputs() != null) {
            inputsList = new ArrayList<>(selectedGroup.getInputs());
            if (inputsList != null) {
                Collections.sort(inputsList, new Comparator<AccessSecurityInput>() {
                    @Override
                    public int compare(AccessSecurityInput input1, AccessSecurityInput input2) {
                        if (input1.getIndex() == null) {
                            return -1;
                        } else if (input2.getIndex() == null) {
                            return 1;
                        } else {
                            return input1.getIndex().compareTo(input2.getIndex());
                        }
                    }
                });
                return inputsList;
            }
        }
        return new ArrayList<>();
    }

    /**
     * @return selected access security group rule name which is shown in access security rule name field on
     *         <code>/PVAccessManagement</code> page.
     */
    public String getRuleName() {
        if (getIsRuleSelected()) {
            return selectedRule.getName();
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * Returns new access security group rule name which is entered into access security rule name field in add access
     * security rule dialog on <code>/PVAccessManagement</code> page.
     * 
     * @return new access security group rule name entered into access security rule name field.
     */
    public String getNewRuleName() {
        return newRuleName;
    }

    /**
     * Sets new access security group rule name which is entered into access security rule name field in add access
     * security rule dialog on <code>/PVAccessManagement</code> page.
     * 
     * @param newRuleName new access security group rule name entered into access security rule name field
     */
    public void setNewRuleName(String newRuleName) {
        this.newRuleName = newRuleName;
    }

    /**
     * Returns updated access security group rule name which is entered into access security rule name field in edit
     * access security rule dialog on <code>/PVAccessManagement</code> page.
     * 
     * @return updated access security group rule name entered into access security rule name field.
     */
    public String getUpdatedRuleName() {
        return getRuleName();
    }

    /**
     * Sets updated access security group rule name which is entered into access security rule name field in edit access
     * security rule dialog on <code>/PVAccessManagement</code> page.
     * 
     * @param updatedRuleName updated access security group rule name entered into access security rule name field
     */
    public void setUpdatedRuleName(String updatedRuleName) {
        this.updatedRuleName = updatedRuleName;
    }

    /**
     * @return selected access security group rule description which is shown in access security rule description field
     *         on <code>/PVAccessManagement</code> page.
     */
    public String getRuleDescription() {
        if (getIsRuleSelected()) {
            return selectedRule.getDescription();
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * Returns new access security group rule description which is entered into access security rule description field
     * in add access security rule dialog on <code>/PVAccessManagement</code> page.
     * 
     * @return new access security group rule description entered into access security rule name field.
     */
    public String getNewRuleDescription() {
        return newRuleDescription;
    }

    /**
     * Sets new access security group rule description which is entered into access security rule description field in
     * add access security rule dialog on <code>/PVAccessManagement</code> page.
     * 
     * @param newRuleDescription new access security group rule description entered into access security rule name field
     */
    public void setNewRuleDescription(String newRuleDescription) {
        this.newRuleDescription = newRuleDescription;
    }

    /**
     * Returns updated access security group rule description which is entered into access security rule description
     * field in edit access security rule dialog on <code>/PVAccessManagement</code> page.
     * 
     * @return updated access security group rule description entered into access security rule name field.
     */
    public String getUpdatedRuleDescription() {
        return getRuleDescription();
    }

    /**
     * Sets updated access security group rule description which is entered into access security rule description field
     * in edit access security rule dialog on <code>/PVAccessManagement</code> page.
     * 
     * @param updatedRuleDescription updated access security group rule description entered into access security rule
     *        name field
     */
    public void setUpdatedRuleDescription(String updatedRuleDescription) {
        this.updatedRuleDescription = updatedRuleDescription;
    }

    /**
     * @return selected access security group rule level which is selected in access security rule level field on
     *         <code>/PVAccessManagement</code> page.
     */
    public Boolean getRuleLevel() {
        if (getIsRuleSelected()) {
            return selectedRule.getLevel();
        }
        return Boolean.FALSE;
    }

    /**
     * Returns new access security group rule level which is selected in access security rule level field in add access
     * security rule dialog on <code>/PVAccessManagement</code> page.
     * 
     * @return new access security group rule level selected in access security rule name field.
     */
    public Boolean getNewRuleLevel() {
        return newRuleLevel;
    }

    /**
     * Sets new access security group rule level which is selected in access security rule level field in add access
     * security rule dialog on <code>/PVAccessManagement</code> page.
     * 
     * @param newRuleLevel new access security group rule level selected in access security rule name field
     */
    public void setNewRuleLevel(Boolean newRuleLevel) {
        this.newRuleLevel = newRuleLevel;
    }

    /**
     * Returns updated access security group rule level which is selected in access security rule level field in edit
     * access security rule dialog on <code>/PVAccessManagement</code> page.
     * 
     * @return updated access security group rule level selected in access security rule name field.
     */
    public Boolean getUpdatedRuleLevel() {
        return getRuleLevel();
    }

    /**
     * Sets updated access security group rule level which is selected in access security rule level field in edit
     * access security rule dialog on <code>/PVAccessManagement</code> page.
     * 
     * @param updatedRuleLevel updated access security group rule level selected in access security rule name field
     */
    public void setUpdatedRuleLevel(Boolean updatedRuleLevel) {
        this.updatedRuleLevel = updatedRuleLevel;
    }

    /**
     * @return selected access security group rule CALC which is shown in access security rule CALC field on
     *         <code>/PVAccessManagement</code> page.
     */
    public String getRuleCalc() {
        if (getIsRuleSelected()) {
            return selectedRule.getCalc();
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * Returns new access security group rule CALC which is entered into access security rule CALC field in add access
     * security rule dialog on <code>/PVAccessManagement</code> page.
     * 
     * @return new access security group rule CALC entered into access security rule CALC field.
     */
    public String getNewRuleCalc() {
        return newRuleCalc;
    }

    /**
     * Sets new access security group rule CALC which is entered into access security rule CALC field in add access
     * security rule dialog on <code>/PVAccessManagement</code> page.
     * 
     * @param newRuleCalc new access security group rule CALC entered into access security rule CALC field
     */
    public void setNewRuleCalc(String newRuleCalc) {
        this.newRuleCalc = newRuleCalc;
    }

    /**
     * Returns updated access security group rule CALC which is entered into access security rule CALC field in edit
     * access security rule dialog on <code>/PVAccessManagement</code> page.
     * 
     * @return updated access security group rule CALC entered into access security rule CALC field.
     */
    public String getUpdatedRuleCalc() {
        return getRuleCalc();
    }

    /**
     * Sets updated access security group rule CALC which is entered into access security rule CALC field in edit access
     * security rule dialog on <code>/PVAccessManagement</code> page.
     * 
     * @param updatedRuleCalc updated access security group rule CALC entered into access security rule CALC field
     */
    public void setUpdatedRuleCalc(String updatedRuleCalc) {
        this.updatedRuleCalc = updatedRuleCalc;
    }

    /**
     * @return generated <code>.acf</code> file content. Content is shown in text area on <code>/ACFFileGenerator</code>
     *         page.
     */
    public String getGeneratedContent() {
        return generatedContent;
    }

    /**
     * Sets generated <code>.acf</code> file content. Content is shown in text area on <code>/ACFFileGenerator</code>
     * page.
     * 
     * @param generatedContent generated .acf file content
     */
    public void setGeneratedContent(String generatedContent) {
        this.generatedContent = generatedContent;
    }

    /**
     * @return generated .acf file. Method is called when download file button on <code>/ACFFileGenerator</code> page is
     *         clicked.
     */
    public StreamedContent getGeneratedFile() {
        if (generatedContent == null) {
            return null;
        }
        InputStream is = new ByteArrayInputStream(generatedContent.getBytes(CHARSET));
        return new DefaultStreamedContent(is, "text/plain", FILE_NAME);
    }

    /**
     * Retrieves list of access security groups from database and returns it. Access security groups are shown in access
     * security groups list on <code>/PVAccessManagement</code> page.
     * 
     * @return list of access security groups retrieved from database.
     */
    public List<AccessSecurityGroup> getGroups() {
        List<AccessSecurityGroup> groups = fetchGroups(false);
        if (groups != null) {
            if (selectedGroup != null && !groups.contains(groupEJB.getAccessSecurityGroup(selectedGroup.getId()))) {
                selectedGroup = null;
                selectedInput = null;
                selectedRule = null;
                selectedRole = null;
                selectedIPGroup = null;
            }
            Collections.sort(groups, new Comparator<AccessSecurityGroup>() {
                private NumericStringComparator comparator = new NumericStringComparator();

                @Override
                public int compare(AccessSecurityGroup o1, AccessSecurityGroup o2) {
                    return comparator.compare(o1.getName(), o2.getName());
                }
            });
        }
        return groups;
    }

    /**
     * Retrieves list of all access security groups from database and returns it. Access security groups are shown in
     * access security groups drop down list on <code>/ACFFileGenerator</code> page.
     * 
     * @return list of access security groups retrieved from database.
     */
    public List<AccessSecurityGroup> getDefaultGroups() {
        List<AccessSecurityGroup> groups = requestCacheBean.getAccessSecurityGroups(false);
        Collections.sort(groups, new Comparator<AccessSecurityGroup>() {
            private NumericStringComparator comparator = new NumericStringComparator();

            @Override
            public int compare(AccessSecurityGroup o1, AccessSecurityGroup o2) {
                return comparator.compare(o1.getName(), o2.getName());
            }
        });
        return groups;
    }

    /**
     * Retrieves list of access security rules corresponds to the selected access security group. Access security rules
     * are shown in access security rules list on <code>/PVAccessManagement</code> page.
     * 
     * @return access security group rules.
     */
    public List<AccessSecurityRule> getRules() {
        if (getIsGroupSelected() && selectedGroup.getRules() != null) {
            List<AccessSecurityRule> rules = new ArrayList<>(selectedGroup.getRules());
            Collections.sort(rules, new Comparator<AccessSecurityRule>() {
                private NumericStringComparator comparator = new NumericStringComparator();

                @Override
                public int compare(AccessSecurityRule o1, AccessSecurityRule o2) {
                    return comparator.compare(o1.getName(), o2.getName());
                }
            });
            if (selectedRule == null) {
                selectedRule = rules.isEmpty() ? null : rules.get(0);
            }
            return rules;
        }
        return new ArrayList<>();
    }

    /**
     * Retrieves list of roles corresponds to the selected access security group rule. Roles are shown in roles list on
     * <code>/PVAccessManagement</code> page.
     * 
     * @return access security rule roles.
     */
    public List<Role> getRoles() {
        if (getIsRuleSelected() && selectedRule.getRoles() != null) {
            return new ArrayList<>(selectedRule.getRoles());
        }
        return new ArrayList<>();
    }

    /**
     * Retrieves list of IP groups corresponds to the selected access security group rule. IP groups are shown in roles
     * list on <code>/PVAccessManagement</code> page.
     * 
     * @return access security rule IP groups.
     */
    public List<IPGroup> getIPGroups() {
        if (getIsRuleSelected() && selectedRule.getIpGroups() != null) {
            return new ArrayList<>(selectedRule.getIpGroups());
        }
        return new ArrayList<>();
    }

    /**
     * Retrieves list of users which are assigned to the selected role. Users are shown in list in role assignees dialog
     * which is shown when role in roles list on <code>/PVAccessManagement</code> page is double clicked.
     * 
     * @return list of users assigned to the selected role.
     */
    public List<String> getSelectedRoleAssignees() {
        if (selectedRole != null && selectedRole.getUsers() != null) {
            Set<String> users = new HashSet<>();
            for (UserRole userRole : selectedRole.getUsers()) {
                users.add(userRole.getUserId());
            }
            return new ArrayList<>(users);
        }
        return new ArrayList<>();
    }

    /**
     * Retrieves list of selected IP group members. IP group members are shown in list in IP group members dialog which
     * is shown when IP group in IP group list on <code>/PVAccessManagement</code> page is double clicked.
     * 
     * @return list of selected IP group members.
     */
    public List<String> getSelectedIPGroupMembers() {
        if (selectedIPGroup != null && selectedIPGroup.getIp() != null) {
            return new ArrayList<>(selectedIPGroup.getIp());
        }
        return new ArrayList<>();
    }

    /**
     * @return input prefixes.
     */
    public List<String> getInputPrefixes() {
        return new ArrayList<>(Arrays.asList(INPUT_PREFIXES));
    }

    /**
     * Returns true if access security group in access security groups list on <code>/PVAccessManagement</code> page is
     * selected. If not returns false.
     * 
     * @return true if access security group is selected, otherwise false.
     */
    public boolean getIsGroupSelected() {
        return selectedGroup != null;
    }

    /**
     * Returns true if access security group in access security groups list is selected and if access security input in
     * access security inputs list on <code>/PVAccessManagement</code> page is selected. If not returns false.
     * 
     * @return true if access security input is selected, otherwise false.
     */
    public boolean getIsInputSelected() {
        return getIsGroupSelected() && selectedInput != null;
    }

    /**
     * Returns true if access security group in access security groups list is selected and if access security rule in
     * access security rules list on <code>/PVAccessManagement</code> page is selected. If not returns false.
     * 
     * @return true if access security rule is selected, otherwise false.
     */
    public boolean getIsRuleSelected() {
        return getIsGroupSelected() && selectedRule != null;
    }

    /**
     * Returns true if access security rule in access security rules list is selected and if role in roles list on
     * <code>/PVAccessManagement</code> page is selected. If not returns false.
     * 
     * @return true if role is selected, otherwise false.
     */
    public boolean getIsRoleSelected() {
        return getIsRuleSelected() && selectedRole != null;
    }

    /**
     * Returns true if access security rule in access security rules list is selected and if IP group in IP groups list
     * on <code>/PVAccessManagement</code> page is selected. If not returns false.
     * 
     * @return true if IP group is selected, otherwise false.
     */
    public boolean getIsIPGroupSelected() {
        return getIsRuleSelected() && selectedIPGroup != null;
    }

    /**
     * Returns true if top access security input in access security inputs list on <code>/PVAccessManagement</code> page
     * is selected. If not returns false.
     * 
     * @return true if top access security input is selected, otherwise false.
     */
    public boolean getIsTopInputSelected() {
        return getIsInputSelected() && selectedInput.getIndex() != 1;
    }

    /**
     * Returns true if bottom access security input in access security inputs list on <code>/PVAccessManagement</code>
     * page is selected. If not returns false.
     * 
     * @return true if bottom access security input is selected, otherwise false.
     */
    public boolean getIsBottomInputSelected() {
        return getIsInputSelected() && inputsList != null && selectedInput.getIndex() != inputsList.size();
    }

    /**
     * Creates new access security group if user has permission for managing access security groups. Method is called
     * when OK button, in add access security group dialog on <code>/PVAccessManagement</code> page is clicked. Results
     * of the action are logged and shown to the user as growl message.
     */
    public void createAccessSecurityGroup() {
        if (!permissionsBean.canManageAccessSecurityGroups(false)) {
            return;
        }
        final AccessSecurityGroup group = new AccessSecurityGroup();
        group.setName(newGroupName);
        group.setDescription(newGroupDescription);
        String success = Messages.getString(Messages.SUCCESSFUL_ASG_CREATION, loginBean.getUsername(), group.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_ASG_CREATION, loginBean.getUsername(),
                group.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                groupEJB.createAccessSecurityGroup(group);
                selectedGroup = group;
                selectedRule = null;
                fetchGroups(true);
            }
        });
        setDefaultValues();
    }

    /**
     * Removes selected access security group if user has permission for managing access security groups. Method is
     * called when remove access security group button on <code>/PVAccessManagement</code> page is clicked. Results of
     * the action are logged and shown to the user as growl message.
     */
    public void removeAccessSecurityGroup() {
        if (!permissionsBean.canManageAccessSecurityGroups(false) || !getIsGroupSelected()) {
            return;
        }
        String success = Messages.getString(Messages.SUCCESSFUL_ASG_REMOVAL, loginBean.getUsername(),
                selectedGroup.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_ASG_REMOVAL, loginBean.getUsername(),
                selectedGroup.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                groupEJB.removeAccessSecurityGroup(selectedGroup);
                selectedGroup = null;
                fetchGroups(true);
            }
        });
    }

    /**
     * Updates access security group if user has permission for managing access security groups. Method is called when
     * OK button, in edit access security group dialog on <code>/PVAccessManagement</code> page is clicked. Results of
     * the action are logged and shown to the user as growl message.
     */
    public void updateAccessSecurityGroup() {
        if (!permissionsBean.canManageAccessSecurityGroups(false) || !getIsGroupSelected()) {
            return;
        }
        selectedGroup.setName(updatedGroupName);
        selectedGroup.setDescription(updatedGroupDescription);
        String success = Messages.getString(Messages.SUCCESSFUL_ASG_UPDATE, loginBean.getUsername(),
                selectedGroup.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_ASG_UPDATE, loginBean.getUsername(),
                selectedGroup.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                groupEJB.updateAccessSecurityGroup(selectedGroup);
                fetchGroups(true);
            }
        });
    }

    /**
     * Creates new access security rule if user has permission for managing access security groups. Method is called
     * when OK button, in add access security rule dialog on <code>/PVAccessManagement</code> page is clicked. Results
     * of the action are logged and shown to the user as growl message.
     */
    public void createAccessSecurityRule() {
        if (!permissionsBean.canManageAccessSecurityGroups(false) || !getIsGroupSelected()) {
            return;
        }
        final AccessSecurityRule rule = new AccessSecurityRule();
        rule.setName(newRuleName);
        rule.setASGroup(selectedGroup);
        rule.setCalc(newRuleCalc);
        rule.setDescription(newRuleDescription);
        rule.setLevel(newRuleLevel);
        String success = Messages.getString(Messages.SUCCESSFUL_ASG_RULE_CREATION, loginBean.getUsername(),
                selectedGroup.getName(), rule.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_ASG_RULE_CREATION, loginBean.getUsername(),
                selectedGroup.getName(), rule.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                groupEJB.createAccessSecurityRule(rule);
                selectedGroup.getRules().add(rule);
                selectedRule = rule;
            }
        });
        setDefaultValues();
    }

    /**
     * Removes selected access security rule if user has permission for managing access security groups. Method is
     * called when remove access security rule button on <code>/PVAccessManagement</code> page is clicked. Results of
     * the action are logged and shown to the user as growl message.
     */
    public void removeAccessSecurityRule() {
        if (!permissionsBean.canManageAccessSecurityGroups(false) || !getIsRuleSelected()) {
            return;
        }
        String success = Messages.getString(Messages.SUCCESSFUL_ASG_RULE_REMOVAL, loginBean.getUsername(),
                selectedGroup.getName(), selectedRule.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_ASG_RULE_REMOVAL, loginBean.getUsername(),
                selectedGroup.getName(), selectedRule.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                selectedGroup.getRules().remove(selectedRule);
                groupEJB.removeAccessSecurityRule(selectedRule);
                selectedRule = null;
            }
        });
    }

    /**
     * Updates access security rule if user has permission for managing access security groups. Method is called when OK
     * button, in edit access security rule dialog on <code>/PVAccessManagement</code> page is clicked. Results of the
     * action are logged and shown to the user as growl message.
     */
    public void updateAccessSecurityRule() {
        if (!permissionsBean.canManageAccessSecurityGroups(false) || !getIsRuleSelected()) {
            return;
        }
        selectedGroup.getRules().remove(selectedRule);
        selectedRule.setName(updatedRuleName);
        selectedRule.setDescription(updatedRuleDescription);
        selectedRule.setLevel(updatedRuleLevel);
        selectedRule.setCalc(updatedRuleCalc);
        String success = Messages.getString(Messages.SUCCESSFUL_ASG_RULE_UPDATE, loginBean.getUsername(),
                selectedGroup.getName(), selectedRule.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_ASG_RULE_UPDATE, loginBean.getUsername(),
                selectedGroup.getName(), selectedRule.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                groupEJB.updateAccessSecurityRule(selectedRule);
                selectedGroup.getRules().add(selectedRule);
            }
        });
    }

    /**
     * Adds roles to the selected access security rule if user has permission for managing access security groups.
     * Method is called when OK button, in add roles for selected rule dialog on <code>/PVAccessManagement</code> page
     * is clicked. Results of the action are logged and shown to the user as growl message.
     */
    public void addAccessSecurityRoles() {
        if (!permissionsBean.canManageAccessSecurityGroups(false) || !getIsRuleSelected() 
                || rolesTargetList.isEmpty()) {
            return;
        }
        selectedGroup.getRules().remove(selectedRule);
        Set<Role> rolesSet = new HashSet<>(selectedRule.getRoles());
        rolesSet.addAll(rolesTargetList);
        selectedRule.setRoles(rolesSet);
        String success = Messages.getString(Messages.SUCCESSFUL_ASG_RULE_ROLE_ASSIGNMENT, loginBean.getUsername(),
                selectedGroup.getName(), selectedRule.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_ASG_RULE_ROLE_ASSIGNMENT, loginBean.getUsername(),
                selectedGroup.getName(), selectedRule.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                groupEJB.updateAccessSecurityRule(selectedRule);
                selectedGroup.getRules().add(selectedRule);
            }
        });
        addAllRolesFromTargetToSource();
    }

    /**
     * Removes selected role from selected access security rule if user has permission for managing access security
     * groups. Method is called when remove role button on <code>/PVAccessManagement</code> page is clicked. Results of
     * the action are logged and shown to the user as growl message.
     */
    public void removeRole() {
        if (!permissionsBean.canManageAccessSecurityGroups(false) || !getIsRoleSelected()) {
            return;
        }
        selectedGroup.getRules().remove(selectedRule);
        selectedRule.getRoles().remove(selectedRole);
        String success = Messages.getString(Messages.SUCCESSFUL_ASG_RULE_ROLE_REMOVAL, loginBean.getUsername(),
                selectedRole.getName(), selectedGroup.getName(), selectedRule.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_ASG_RULE_ROLE_REMOVAL, loginBean.getUsername(),
                selectedRole.getName(), selectedGroup.getName(), selectedRule.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                groupEJB.updateAccessSecurityRule(selectedRule);
                selectedGroup.getRules().add(selectedRule);
                selectedRole = null;
            }
        });
    }

    /**
     * Adds IP groups to the selected access security rule if user has permission for managing access security groups.
     * Method is called when OK button, in add IP groups for selected rule dialog on <code>/PVAccessManagement</code>
     * page is clicked. Results of the action are logged and shown to the user as growl message.
     */
    public void addAccessSecurityIPGroup() {
        if (!permissionsBean.canManageAccessSecurityGroups(false) || !getIsRuleSelected()
                || ipGroupsTargetList.isEmpty()) {
            return;
        }
        selectedGroup.getRules().remove(selectedRule);
        Set<IPGroup> ipGroupsSet = new HashSet<>(selectedRule.getIpGroups());
        ipGroupsSet.addAll(ipGroupsTargetList);
        selectedRule.setIpGroups(ipGroupsSet);
        String success = Messages.getString(Messages.SUCCESSFUL_ASG_RULE_IPGROUP_ASSIGNMENT, loginBean.getUsername(),
                selectedGroup.getName(), selectedRule.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_ASG_RULE_IPGROUP_ASSIGNMENT, loginBean.getUsername(),
                selectedGroup.getName(), selectedRule.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                groupEJB.updateAccessSecurityRule(selectedRule);
                selectedGroup.getRules().add(selectedRule);
            }
        });
        addAllIPGroupsFromTargetToSource();
    }

    /**
     * Removes selected IP group from selected access security rule if user has permission for managing access security
     * groups. Method is called when remove IP group button on <code>/PVAccessManagement</code> page is clicked. Results
     * of the action are logged and shown to the user as growl message.
     */
    public void removeIPGroup() {
        if (!permissionsBean.canManageAccessSecurityGroups(false) || !getIsIPGroupSelected()) {
            return;
        }
        selectedGroup.getRules().remove(selectedRule);
        selectedRule.getIpGroups().remove(selectedIPGroup);
        String success = Messages.getString(Messages.SUCCESSFUL_ASG_RULE_IPGROUP_REMOVAL, loginBean.getUsername(),
                selectedIPGroup.getName(), selectedGroup.getName(), selectedRule.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_ASG_RULE_IPGROUP_REMOVAL, loginBean.getUsername(),
                selectedIPGroup.getName(), selectedGroup.getName(), selectedRule.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                groupEJB.updateAccessSecurityRule(selectedRule);
                selectedGroup.getRules().add(selectedRule);
                selectedIPGroup = null;
            }
        });
    }

    /**
     * Creates new access security input if user has permission for managing access security groups. Method is called
     * when OK button, in add access security input dialog on <code>/PVAccessManagement</code> page is clicked. Results
     * of the action are logged and shown to the user as growl message.
     */
    public void createAccessSecurityInput() {
        if (!permissionsBean.canManageAccessSecurityGroups(false) || !getIsGroupSelected()) {
            return;
        }
        if (selectedGroup.getInputs().size() == MAX_INPUTS_SIZE) {
            taskBean.showFacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString(
                    Messages.UNSUCCESSFUL_INPUT_CREATION_MAX_INDEX, loginBean.getUsername(), selectedGroup.getName()),
                    Constants.EMPTY_STRING, false);
        } else {
            final AccessSecurityInput input = new AccessSecurityInput();
            input.setPvName(inputName);
            input.setASGroup(selectedGroup);
            input.setIndex(selectedGroup.getInputs().size() + 1);
            String success = Messages.getString(Messages.SUCCESSFUL_ASG_INPUT_CREATION, loginBean.getUsername(),
                    selectedGroup.getName(), input.getPvName());
            String failure = Messages.getString(Messages.UNSUCCESSFUL_ASG_INPUT_CREATION, loginBean.getUsername(),
                    selectedGroup.getName(), input.getPvName());
            taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
                @Override
                public void execute() throws Exception {
                    groupEJB.createAccessSecurityInput(input);
                    selectedGroup.getInputs().add(input);
                }
            });
        }
        setDefaultValues();
    }

    /**
     * Removes selected access security input if user has permission for managing access security groups. Method is
     * called when remove access security input button on <code>/PVAccessManagement</code> page is clicked. Results of
     * the action are logged and shown to the user as growl message.
     */
    public void removeAccessSecurityGroupInput() {
        if (!permissionsBean.canManageAccessSecurityGroups(false) || !getIsInputSelected() || inputsList.isEmpty()) {
            return;
        }
        final AccessSecurityInput input = inputsList.get(selectedInput.getIndex() - 1);
        String success = Messages.getString(Messages.SUCCESSFUL_ASG_INPUT_CREATION, loginBean.getUsername(),
                selectedGroup.getName(), input.getPvName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_ASG_INPUT_CREATION, loginBean.getUsername(),
                selectedGroup.getName(), input.getPvName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                groupEJB.removeAccessSecurityInput(input.getId());
                inputsList.remove(input);
                sortAccessSecurityInputs();
                groupEJB.updateAccessSecurityInputs(inputsList);
                selectedGroup.setInputs(new HashSet<>(inputsList));
            }
        });
    }

    /**
     * Moves selected access security input up if user has permission for managing access security groups. Method is
     * called when move access security input up button on <code>/PVAccessManagement</code> page is clicked. Results of
     * the action are logged and shown to the user as growl message.
     */
    public void moveAccessSecurityGroupInputUp() {
        if (!permissionsBean.canManageAccessSecurityGroups(false) || !getIsInputSelected()
                || selectedInput.getIndex() == 1) {
            return;
        }
        AccessSecurityInput tmp = inputsList.get(selectedInput.getIndex() - 2);
        inputsList.set(selectedInput.getIndex() - 2, inputsList.get(selectedInput.getIndex() - 1));
        inputsList.set(selectedInput.getIndex() - 1, tmp);
        sortAccessSecurityInputs();
        String success = Messages.getString(Messages.SUCCESSFUL_ASG_INPUTS_UPDATE, loginBean.getUsername(),
                selectedGroup.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_ASG_INPUTS_UPDATE, loginBean.getUsername(),
                selectedGroup.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                groupEJB.updateAccessSecurityInputs(inputsList);
                selectedGroup.setInputs(new HashSet<>(inputsList));
                selectedInput.setIndex(selectedInput.getIndex() - 1);

            }
        });
    }

    /**
     * Moves selected access security input down if user has permission for managing access security groups. Method is
     * called when move access security input down button on <code>/PVAccessManagement</code> page is clicked. Results
     * of the action are logged and shown to the user as growl message.
     */
    public void moveAccessSecurityGroupInputDown() {
        if (!permissionsBean.canManageAccessSecurityGroups(false) || !getIsInputSelected()
                || (selectedInput.getIndex() - 1) == inputsList.size() - 1) {
            return;
        }
        AccessSecurityInput tmp = inputsList.get(selectedInput.getIndex());
        inputsList.set(selectedInput.getIndex(), inputsList.get(selectedInput.getIndex() - 1));
        inputsList.set(selectedInput.getIndex() - 1, tmp);
        sortAccessSecurityInputs();
        String success = Messages.getString(Messages.SUCCESSFUL_ASG_INPUTS_UPDATE, loginBean.getUsername(),
                selectedGroup.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_ASG_INPUTS_UPDATE, loginBean.getUsername(),
                selectedGroup.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                groupEJB.updateAccessSecurityInputs(inputsList);
                selectedGroup.setInputs(new HashSet<>(inputsList));
                selectedInput.setIndex(selectedInput.getIndex() + 1);
            }
        });
    }

    /**
     * Generates content for .acf files. Method is called when generate file button on <code>\ACFFileGenerator</code>
     * page is clicked.
     */
    public void generateContent() {
        String[] groups = new String[selectedGroups.size()];
        for (int i = 0; i < groups.length; i++) {
            groups[i] = selectedGroups.get(i).getName();
        }
        try {
            if (defaultGroup == null) {
                generatedContent = RBACConnector.getInstance().getACFFileForGroups(null, groups);
            } else {
                generatedContent = RBACConnector.getInstance().getACFFileForGroups(defaultGroup.getName(), groups);
            }
        } catch (IllegalArgumentException | RBACConnectorException e) {
            generatedContent = e.getCause() == null ? e.getMessage() : e.getCause().getMessage();
            LOGGER.error(Messages.getString(Messages.ACF_FILE_GENERATION_PROBLEM), e);
        }
    }

    /**
     * Sorts access security group inputs by index.
     */
    private void sortAccessSecurityInputs() {
        for (int i = 1; i <= inputsList.size(); i++) {
            inputsList.get(i - 1).setIndex(i);
        }
    }

    /**
     * Method that shows dialog for adding access security inputs. Dialog is shown when add access security input button
     * on <code>/PVAccessManagement</code> page is clicked and if user has permissions for managing access security
     * groups.
     */
    public void showAddInputDialog() {
        if (!permissionsBean.canManageAccessSecurityGroups(false) || !getIsGroupSelected()) {
            return;
        }
        RequestContext.getCurrentInstance().execute("PF('addInputDialog').show()");
    }

    /**
     * Method that shows dialog for adding access security rules. Dialog is shown when add access security rule button
     * on <code>/PVAccessManagement</code> page is clicked and if user has permissions for managing access security
     * groups.
     */
    public void showAddRuleDialog() {
        if (!permissionsBean.canManageAccessSecurityGroups(false) || !getIsGroupSelected()) {
            return;
        }
        RequestContext.getCurrentInstance().execute("PF('addRuleDialog').show()");
    }

    /**
     * Method that shows dialog for editing access security groups. Dialog is shown when edit access security group
     * button on <code>/PVAccessManagement</code> page is clicked and if user has permissions for managing access
     * security groups.
     */
    public void showEditGroupInfoDialog() {
        if (!permissionsBean.canManageAccessSecurityGroups(false) || !getIsGroupSelected()) {
            return;
        }
        RequestContext.getCurrentInstance().execute("PF('editGroupInfoDialog').show()");
    }

    /**
     * Method that shows dialog for adding roles to the access security rule. Dialog is shown when add role button on
     * <code>/PVAccessManagement</code> page is clicked and if user has permissions for managing access security groups.
     */
    public void showAddRolesDialog() {
        if (!permissionsBean.canManageAccessSecurityGroups(false) || !getIsRuleSelected()) {
            return;
        }
        RequestContext.getCurrentInstance().execute("PF('addRolesDialog').show()");
    }

    /**
     * Method that shows dialog for adding IP groups to the access security rule. Dialog is shown when add IP group
     * button on <code>/PVAccessManagement</code> page is clicked and if user has permissions for managing access
     * security groups.
     */
    public void showAddIPGroupsDialog() {
        if (!permissionsBean.canManageAccessSecurityGroups(false) || !getIsRuleSelected()) {
            return;
        }
        RequestContext.getCurrentInstance().execute("PF('addIPGroupsDialog').show()");
    }

    /**
     * Method that shows dialog for editing access security rules. Dialog is shown when edit access security rule button
     * on <code>/PVAccessManagement</code> page is clicked and if user has permissions for managing access security
     * groups.
     */
    public void showEditRuleInfoDialog() {
        if (!permissionsBean.canManageAccessSecurityGroups(false) || !getIsRuleSelected()) {
            return;
        }
        RequestContext.getCurrentInstance().execute("PF('editRuleInfoDialog').show()");
    }

    /**
     * Method that shows dialog for adding access security groups. Dialog is shown when add access security group button
     * on <code>/PVAccessManagement</code> page is clicked and if user has permissions for managing access security
     * groups.
     */
    public void showAddGroupDialog() {
        if (!permissionsBean.canManageAccessSecurityGroups(false)) {
            return;
        }
        RequestContext.getCurrentInstance().execute("PF('addGroupDialog').show()");
    }

    /**
     * Method that shows dialog for IP group members review. Dialog is shown when IP group in IP groups list on
     * <code>/PVAccessManagement</code> page is double clicked.
     */
    public void showSelectedIPGroupMembersDialog() {
        RequestContext.getCurrentInstance().execute("PF('selectedIPGroupMembers').show()");
    }

    /**
     * Method that shows dialog for role users review. Dialog is shown when role in roles list on
     * <code>/PVAccessManagement</code> page is double clicked.
     */
    public void showSelectedRoleAssigneesDialog() {
        RequestContext.getCurrentInstance().execute("PF('selectedRoleAssignees').show()");
    }

    /**
     * Resets variables used in add rule dialog, add group dialog and add input dialog on
     * <code>/PVAccessManagement</code> page to the default values.
     */
    public void setDefaultValues() {
        newGroupName = Constants.EMPTY_STRING;
        newGroupDescription = Constants.EMPTY_STRING;
        newRuleName = Constants.EMPTY_STRING;
        newRuleDescription = Constants.EMPTY_STRING;
        newRuleCalc = Constants.EMPTY_STRING;
        newRuleLevel = null;
        inputName = Constants.EMPTY_STRING;
    }

    /**
     * Returns list which contains ip groups from source list and is shown on add ip groups dialog on
     * <code>/PVAccessManagement</code> page.
     * 
     * @return list which contains ip groups from source list.
     */
    public List<IPGroup> getIPGroupsSourceList() {
        return ipGroupsSourceList;
    }

    /**
     * Sets list which contains ip groups from source list and is shown on add ip groups dialog on
     * <code>/PVAccessManagement</code> page.
     * 
     * @param ipGroupsSourceList list which contains ip groups from source list
     */
    public void setIPGroupsSourceList(List<IPGroup> ipGroupsSourceList) {
        this.ipGroupsSourceList = ipGroupsSourceList;
    }

    /**
     * Returns list which contains ip groups from target list and is shown on add ip groups dialog on
     * <code>/PVAccessManagement</code> page.
     * 
     * @return list which contains ip groups from target list.
     */
    public List<IPGroup> getIPGroupsTargetList() {
        return ipGroupsTargetList;
    }

    /**
     * Sets list which contains ip groups from target list and is shown on add ip groups dialog on
     * <code>/PVAccessManagement</code> page.
     * 
     * @param ipGroupsTargetList list which contains ip groups from target list
     */
    public void setIPGroupsTargetList(List<IPGroup> ipGroupsTargetList) {
        this.ipGroupsTargetList = ipGroupsTargetList;
    }

    /**
     * Returns list which contains selected ip groups from source list and is shown on add ip groups dialog on
     * <code>/PVAccessManagement</code> page.
     * 
     * @return list which contains selected ip groups from source list.
     */
    public List<IPGroup> getSelectedIPGroupsSourceList() {
        return selectedIPGroupsSourceList;
    }

    /**
     * Sets list which contains selected ip groups from source list and is shown on add ip groups dialog on
     * <code>/PVAccessManagement</code> page.
     * 
     * @param selectedIPGroupsSourceList list which contains selected ip groups from source list
     */
    public void setSelectedIPGroupsSourceList(List<IPGroup> selectedIPGroupsSourceList) {
        this.selectedIPGroupsSourceList = selectedIPGroupsSourceList;
    }

    /**
     * Returns list which contains selected ip groups from target list and is shown on add ip groups dialog on
     * <code>/PVAccessManagement</code> page.
     * 
     * @return list which contains selected ip groups from target list.
     */
    public List<IPGroup> getSelectedIPGroupsTargetList() {
        return selectedIPGroupsTargetList;
    }

    /**
     * Sets list which contains selected ip groups from target list, and is shown on add ip groups dialog on
     * <code>/PVAccessManagement</code> page.
     * 
     * @param selectedIPGroupsTargetList list which contains selected ip groups from target list
     */
    public void setSelectedIPGroupsTargetList(List<IPGroup> selectedIPGroupsTargetList) {
        this.selectedIPGroupsTargetList = selectedIPGroupsTargetList;
    }

    /**
     * Moves selected ip groups from source list to the target list. Lists are shown on add ip groups dialog on
     * <code>/PVAccessManagement</code> page.
     */
    public void addIPGroupsFromSourceToTarget() {
        if (!selectedIPGroupsSourceList.isEmpty()) {
            ipGroupsTargetList.addAll(selectedIPGroupsSourceList);
        }
        ipGroupsSourceList.removeAll(selectedIPGroupsSourceList);
        selectedIPGroupsSourceList.clear();
    }

    /**
     * Moves selected ip groups from target list to the source list. Lists are shown on add ip groups dialog on
     * <code>/PVAccessManagement</code> page.
     */
    public void addIPGroupsFromTargetToSource() {
        if (!selectedIPGroupsTargetList.isEmpty()) {
            ipGroupsSourceList.addAll(selectedIPGroupsTargetList);
        }
        ipGroupsTargetList.removeAll(selectedIPGroupsTargetList);
        selectedIPGroupsTargetList.clear();
    }

    /**
     * Moves all ip groups from source list to the target list. Lists are shown on add ip groups dialog on
     * <code>/PVAccessManagement</code> page.
     */
    public void addAllIPGroupsFromSourceToTarget() {
        if (!ipGroupsSourceList.isEmpty()) {
            ipGroupsTargetList.addAll(ipGroupsSourceList);
        }
        ipGroupsSourceList.clear();
        selectedIPGroupsSourceList.clear();
    }

    /**
     * Moves all ip groups from target list to the source list. Lists are shown on add ip groups dialog on
     * <code>/PVAccessManagement</code> page.
     */
    public void addAllIPGroupsFromTargetToSource() {
        if (!ipGroupsTargetList.isEmpty()) {
            ipGroupsSourceList.addAll(ipGroupsTargetList);
        }
        ipGroupsTargetList.clear();
        selectedIPGroupsTargetList.clear();
    }

    /**
     * Returns list which contains roles from source list and is shown on add roles dialog on
     * <code>/PVAccessManagement</code> page.
     * 
     * @return list which contains roles from source list.
     */
    public List<Role> getRolesSourceList() {
        return rolesSourceList;
    }

    /**
     * Sets list which contains roles from source list and is shown on add roles dialog on
     * <code>/PVAccessManagement</code> page.
     * 
     * @param rolesSourceList list which contains roles from source list
     */
    public void setRolesSourceList(List<Role> rolesSourceList) {
        this.rolesSourceList = rolesSourceList;
    }

    /**
     * Returns list which contains roles from target list and is shown on add roles dialog on
     * <code>/PVAccessManagement</code> page.
     * 
     * @return list which contains roles from target list.
     */
    public List<Role> getRolesTargetList() {
        return rolesTargetList;
    }

    /**
     * Sets list which contains roles from target list and is shown on add roles dialog on
     * <code>/PVAccessManagement</code> page.
     * 
     * @param rolesTargetList list which contains roles from target list
     */
    public void setRolesTargetList(List<Role> rolesTargetList) {
        this.rolesTargetList = rolesTargetList;
    }

    /**
     * Returns list which contains selected roles from source list and is shown on add roles dialog on
     * <code>/PVAccessManagement</code> page.
     * 
     * @return list which contains selected roles from source list.
     */
    public List<Role> getSelectedRolesSourceList() {
        return selectedRolesSourceList;
    }

    /**
     * Sets list which contains selected roles from source list and is shown on add roles dialog on
     * <code>/PVAccessManagement</code> page.
     * 
     * @param selectedRolesSourceList list which contains selected roles from source list
     */
    public void setSelectedRolesSourceList(List<Role> selectedRolesSourceList) {
        this.selectedRolesSourceList = selectedRolesSourceList;
    }

    /**
     * Returns list which contains selected roles from target list and is shown on add roles dialog on
     * <code>/PVAccessManagement</code> page.
     * 
     * @return list which contains selected roles from target list.
     */
    public List<Role> getSelectedRolesTargetList() {
        return selectedRolesTargetList;
    }

    /**
     * Sets list which contains selected roles from target list, and is shown on add roles dialog on
     * <code>/PVAccessManagement</code> page.
     * 
     * @param selectedRolesTargetList list which contains selected roles from target list
     */
    public void setSelectedRolesTargetList(List<Role> selectedRolesTargetList) {
        this.selectedRolesTargetList = selectedRolesTargetList;
    }

    /**
     * Moves selected roles from source list to the target list. Lists are shown on add roles dialog on
     * <code>/PVAccessManagement</code> page.
     */
    public void addRolesFromSourceToTarget() {
        if (!selectedRolesSourceList.isEmpty()) {
            rolesTargetList.addAll(selectedRolesSourceList);
        }
        rolesSourceList.removeAll(selectedRolesSourceList);
        selectedRolesSourceList.clear();
    }

    /**
     * Moves selected roles from target list to the source list. Lists are shown on add roles dialog on
     * <code>/PVAccessManagement</code> page.
     */
    public void addRolesFromTargetToSource() {
        if (!selectedRolesTargetList.isEmpty()) {
            rolesSourceList.addAll(selectedRolesTargetList);
        }
        rolesTargetList.removeAll(selectedRolesTargetList);
        selectedRolesTargetList.clear();
    }

    /**
     * Moves all roles from source list to the target list. Lists are shown on add roles dialog on
     * <code>/PVAccessManagement</code> page.
     */
    public void addAllRolesFromSourceToTarget() {
        if (!rolesSourceList.isEmpty()) {
            rolesTargetList.addAll(rolesSourceList);
        }
        rolesSourceList.clear();
        selectedRolesSourceList.clear();
    }

    /**
     * Moves all roles from target list to the source list. Lists are shown on add roles dialog on
     * <code>/PVAccessManagement</code> page.
     */
    public void addAllRolesFromTargetToSource() {
        if (!rolesTargetList.isEmpty()) {
            rolesSourceList.addAll(rolesTargetList);
        }
        rolesTargetList.clear();
        selectedRolesTargetList.clear();
    }

    /**
     * Initialises roles dual list model which data are used in add roles for selected rule dialog on
     * <code>/PVAccessManagement</code> page.
     */
    private void initializeRolesDualList() {
        rolesSourceList = requestCacheBean.getRoles();
        if (rolesSourceList == null) {
            rolesSourceList = new ArrayList<>();
        }
        rolesTargetList = new ArrayList<Role>();
    }

    /**
     * Initialises IP groups dual list model which data are used in add IP groups for selected rule dialog on
     * <code>/PVAccessManagement</code> page.
     */
    private void initializeIPGroupsDualList() {
        ipGroupsSourceList = requestCacheBean.getIPGroups();
        if (ipGroupsSourceList == null) {
            ipGroupsSourceList = new ArrayList<>();
        }
        ipGroupsTargetList = new ArrayList<IPGroup>();
    }

    /**
     * Removes spaces from access security group name.
     * 
     * @param accessSecurityGroupName access security group name
     * 
     * @return access security group name without spaces.
     */
    private static String removeSpaces(String accessSecurityGroupName) {
        return accessSecurityGroupName.replaceAll("\\p{Z}", Constants.EMPTY_STRING);
    }

    private List<AccessSecurityGroup> fetchGroups(boolean fromDB) {
        List<AccessSecurityGroup> groups = null;
        if (Constants.ALL.equals(wildcard)) {
            groups = requestCacheBean.getAccessSecurityGroups(fromDB);
        } else {
            groups = requestCacheBean.getAccessSecurityGroupsByWildcard(wildcard + Constants.ALL_DB, fromDB);
        }
        return groups;
    }
}
