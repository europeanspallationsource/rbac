/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.utils;

import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.esss.ics.rbac.datamodel.UserRole;
import se.esss.ics.rbac.datamodel.UserRole.AssignmentType;
import se.esss.ics.rbac.jsf.general.ManagementStudioProperties;

/**
 * <code>SendEmailService</code> is a utility class which contains method for email sending. All properties needed for
 * successful email sending are given in properties file.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@Singleton
public class MailServiceEJB implements MailService {

    private static final long serialVersionUID = 1135796553778679944L;
    private static final transient Logger LOGGER = LoggerFactory.getLogger(MailServiceEJB.class);

    @Resource(name = "java:jboss/mail/Remote")
    private transient Session mailSession;

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.jsf.utils.MailService#send(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public boolean send(String to, String subject, String content) {
        if (!ManagementStudioProperties.isMailNotificationEnabled()) {
            return false;
        }
        if (mailSession == null) {
            throw new IllegalStateException("Cannot send email. Mail session has not been initialised.");
        }
        try {
            Message message = new MimeMessage(mailSession);
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setSubject(subject);
            message.setSentDate(new Date());
            message.setText(content);
            Transport.send(message);
            return true;
        } catch (MessagingException e) {
            LOGGER.error("Exception while sending email.", e);
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.jsf.utils.MailService#generateDefaultContent(se.esss.ics.rbac.datamodel.UserRole, boolean)
     */
    @Override
    public String[] generateDefaultContent(UserRole userRole, boolean edit) {
        StringBuilder subject = new StringBuilder(80);
        StringBuilder content = new StringBuilder(500);
        if (edit) {
            subject.append("RBAC Role Changed: ");
            content.append("The following role, you are assigned to, has changed:\n\n");
        } else {
            subject.append("RBAC Role Assignment: ");
            content.append("You have been assigned the following role:\n\n");
        }
        subject.append(userRole.getRole().getName());
        content.append("Name: ").append(userRole.getRole().getName()).append("\nDescription: ")
                .append(userRole.getRole().getDescription()).append("\nAssignment type: ");
        if (userRole.getAssignment() == AssignmentType.DELEGATED) {
            content.append("DELEGATED by ").append(userRole.getDelegator());
        } else {
            content.append(userRole.getAssignment());
        }
        if (userRole.getAssignment() != AssignmentType.NORMAL) {
            content.append("\nFrom: ");
            content.append(userRole.getStartTime());
            content.append("\nTo: ");
            content.append(userRole.getEndTime());
        }
        return new String[] { subject.toString(), content.toString() };
    }
}
