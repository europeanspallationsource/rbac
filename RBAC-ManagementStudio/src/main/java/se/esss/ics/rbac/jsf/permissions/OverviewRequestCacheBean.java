/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.permissions;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import se.esss.ics.rbac.datamodel.Expression;
import se.esss.ics.rbac.datamodel.IPGroup;
import se.esss.ics.rbac.datamodel.Permission;
import se.esss.ics.rbac.datamodel.Resource;
import se.esss.ics.rbac.datamodel.Role;
import se.esss.ics.rbac.ejbs.interfaces.Expressions;
import se.esss.ics.rbac.ejbs.interfaces.IPGroups;
import se.esss.ics.rbac.ejbs.interfaces.Permissions;
import se.esss.ics.rbac.ejbs.interfaces.Resources;
import se.esss.ics.rbac.ejbs.interfaces.Roles;

/**
 * <code>OverviewRequestCacheBean</code> is a request bean used on the overview permissions page. It provides access to
 * DB content by caching the results for the duration of the request. Using this bean eliminates multiple calls to the
 * database, which can be a result of primefaces requesting specific data multiple times.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@ManagedBean(name = "overviewRequestCacheBean")
@RequestScoped
public class OverviewRequestCacheBean implements Serializable {

    private static final long serialVersionUID = 3199239355007365979L;

    @EJB
    private Roles rolesEJB;
    @EJB
    private Resources resourcesEJB;
    @EJB
    private IPGroups ipGroupsEJB;
    @EJB
    private Permissions permissionsEJB;
    @EJB
    private Expressions expressionsEJB;

    private List<Role> currentRoles;
    private List<Resource> currentResources;
    private List<String> currentIPs;
    private List<IPGroup> currentIPGroups;
    private List<Expression> currentExpressions;

    private List<Role> roles;
    private List<Resource> resources;
    private List<IPGroup> ipGroups;
    private List<Expression> expressions;
    private List<Permission> permissions;

    /**
     * Retrieves list of roles from database and returns it. Roles are shown in roles dual list on
     * <code>/Permissions/Overview</code> page.
     * 
     * @return list of roles
     */
    public List<Role> getRoles() {
        if (roles == null) {
            roles = rolesEJB.getRoles();
        }
        return roles;
    }

    /**
     * Retrieves list of resources from database and returns it. Resources are shown in resources dual list on
     * <code>/Permissions/Overview</code> page.
     * 
     * @return list of resources
     */
    public List<Resource> getResources() {
        if (resources == null) {
            resources = resourcesEJB.getResources();

        }
        return resources;
    }

    /**
     * Retrieves list of ip groups from database and returns it. IP groups are shown in ip groups dual list on
     * <code>/Permissions/Overview</code> page.
     * 
     * @return list of ip groups
     */
    public List<IPGroup> getIPGroups() {
        if (ipGroups == null) {
            ipGroups = ipGroupsEJB.getIPGroups();

        }
        return ipGroups;
    }

    /**
     * Retrieves list of expressions from database and returns it. Expressions are shown in expressions dual list on
     * <code>/Permissions/Overview</code> page.
     * 
     * @return list of expressions
     */
    public List<Expression> getExpressions() {
        if (expressions == null) {
            expressions = expressionsEJB.getExpressions();

        }
        return expressions;
    }

    /**
     * Retrieves list of permissions from database and returns it. Only the permissions that belong to the given roles,
     * resources, IPs, ip groups and expressions are shown. If one of these lists is empty or null that filter is
     * ignored. Permissions are shown in permissions list on <code>/Permissions/Overview</code> page.
     * 
     * @param roles the roles to use in filtering
     * @param resources the resources to use in filtering
     * @param ips the IP addresses to use in filtering
     * @param ipGroups the IP groups to use in filtering
     * @param expressions the expressions to use in filtering
     * @return list of permissions
     */
    public List<Permission> getFilteredPermissions(List<Role> roles, List<Resource> resources, List<String> ips,
            List<IPGroup> ipGroups, List<Expression> expressions) {
        if (permissions == null || newRequest(roles, resources, ips, ipGroups, expressions)) {
            initialise(roles, resources, ips, ipGroups, expressions);
            permissions = permissionsEJB.filterPermissions(roles, resources, ips, ipGroups, expressions);

        }
        return permissions;
    }

    /**
     * Initialises cached search parameters.
     */
    private void initialise(List<Role> roles, List<Resource> resources, List<String> ips, List<IPGroup> ipGroups,
            List<Expression> expressions) {
        currentRoles = roles;
        currentResources = resources;
        currentIPs = ips;
        currentIPGroups = ipGroups;
        currentExpressions = expressions;
    }

    /**
     * Checks if new request search parameters differs from cached search parameters.
     * 
     * @param roles roles
     * @param resources resources
     * @param ips IPs
     * @param ipGroups IP groups
     * @param expressions expressions
     * 
     * @return true if differs otherwise false.
     */
    private boolean newRequest(List<Role> roles, List<Resource> resources, List<String> ips, List<IPGroup> ipGroups,
            List<Expression> expressions) {
        if (currentRoles.size() != roles.size() || currentResources.size() != resources.size()
                || currentIPs.size() != ips.size() || currentIPGroups.size() != ipGroups.size()
                || currentExpressions.size() != expressions.size()) {
            return true;
        }
        if (!currentRoles.containsAll(roles) || !currentResources.containsAll(resources)
                || !currentIPs.containsAll(ips) || !currentIPGroups.containsAll(ipGroups)
                || !currentExpressions.containsAll(expressions)) {
            return true;
        }
        return false;
    }
}
